var gulp = require('gulp'),
$ = require('gulp-load-plugins')();
var bower = require('gulp-bower');

var config = {
  inputDir      : 'src',
//  outputDir     : 'public',
  outputDir     : '../application/ryderonlineweb/ux2-public',
  bowerDir      : '/vendor',
  bowerSources  : ['bower_components/**'],
  lessSources   : ['src/**/*.less'],
  fontSources   : ['assets/fonts/**'],
  htmlSources   : ['src/index.html'],
  imgSources    : ['src/img/**/*'],
  jsSources     : ['src/**/*.js'],
  htmlTemplates : 'src/**/*.tpl',
  production    : !! $.util.env.production,
  build         : !! $.util.env.build
}

if (config.build) {
 config.production=true; 
}

gulp.task('bowerInstall', function() {
	return bower();
});
	
gulp.task('bower', ['bowerInstall', 'html', 'fonts'], function() {
  var bowerSrc=gulp.src('bower.json')
  .pipe($.mainBowerFiles({
    overrides: {
      'font-awesome': {
        main: [
        './fonts/*.*',
        './css/*.css'
        ]
      }
    }
  }), {read:false}).pipe($.flatten()),
  jsFiles = bowerSrc.pipe($.filter('**/*.js')).pipe(config.production ? $.uglify() : $.util.noop()),
  cssFiles = bowerSrc.pipe($.filter(['**/*.css','!**/*.min.css'])).pipe(config.production ? $.cleanCss() : $.util.noop()),
  fontFiles = bowerSrc.pipe($.filter(['**/*.eot', '**/*.woff','**/*.woff2', '**/*.svg', '**/*.ttf','**/*.otf']));

  jsFiles.pipe(gulp.dest(config.outputDir + config.bowerDir + '/js/').pipe(config.production ? $.rename({suffix: ".min"}) : $.util.noop()));
  cssFiles.pipe(gulp.dest(config.outputDir + config.bowerDir + '/css/').pipe(config.production ? $.rename({suffix: ".min"}) : $.util.noop()));
  fontFiles.pipe(gulp.dest(config.outputDir + config.bowerDir +'/fonts/'));

  gulp.src(config.inputDir+'/index.html')
  .pipe($.inject(jsFiles.pipe(gulp.dest(config.outputDir + config.bowerDir + '/js/')), {ignorePath: config.outputDir, addRootSlash:false, name:'vendor', removeTags:true}))
  .pipe($.inject(cssFiles.pipe(gulp.dest(config.outputDir + config.bowerDir + '/css/')), {ignorePath: config.outputDir, addRootSlash:false, name:'vendor', removeTags:true}))
  .pipe(gulp.dest(config.outputDir))

  .pipe($.connect.reload())

});

gulp.task('fonts', [], function() {
  var fontSrc=gulp.src(config.fontSources);
  
  fontSrc.pipe($.filter(['**/*.css','!**/*.min.css']))
  .pipe(config.production ? $.cleanCss() : $.util.noop())
  .pipe(config.production ? $.rename("fonts.css",{suffix: ".min"}) : $.rename("fonts.css"))
  .pipe(gulp.dest(config.outputDir + config.bowerDir + '/fontastic/'));
  
  fontSrc.pipe($.filter(['**/*.eot', '**/*.woff','**/*.woff2', '**/*.svg', '**/*.ttf','**/*.otf']))
  .pipe(gulp.dest(config.outputDir + config.bowerDir + '/fontastic/'));
});

gulp.task('html',function() {
  gulp.src(config.inputDir+'/index.html')
  .pipe(gulp.dest(config.outputDir))
});

gulp.task('img',function() {
  gulp.src(config.imgSources)
  .pipe(gulp.dest(config.outputDir+'/img'))
  .pipe($.connect.reload())
});

gulp.task('less', function () {
  gulp.src(config.lessSources)
  .pipe($.lessImport('style.less'))
  .pipe($.less())
  .pipe($.autoprefixer([
    'Android 2.3',
    'Android >= 4',
    'Chrome >= 20',
    'Firefox >= 24',
    'Explorer >= 9',
    'iOS >= 7',
    'Opera >= 12',
    'Safari >= 6']))
  .pipe(config.production ? $.cleanCss() : $.util.noop())
  .pipe(gulp.dest(config.outputDir+'/css'))
  .pipe($.connect.reload())
});

gulp.task('js', function() {
  gulp.src(config.jsSources)
  .pipe($.order([
    '!**/run.js'
    ]))
  .pipe(config.production ? $.uglify() : $.util.noop())
  .pipe($.concat('app.js'))
  .pipe(gulp.dest(config.outputDir+'/js'))
  .pipe($.connect.reload())
});

gulp.task('templates', function () {
  gulp.src(config.htmlTemplates)
  .pipe($.htmlmin({collapseWhitespace: true}))
  .pipe($.flatten())
  .pipe($.angularTemplatecache('templates.js',{standalone:true}))
  .pipe(config.production ? $.uglify() : $.util.noop())
  .pipe(gulp.dest(config.outputDir+'/js/'))
  .pipe($.connect.reload());
});

gulp.task('watch', function() {
  if (!config.build){
    gulp.watch(config.bowerSources, ['bower']);
    gulp.watch(config.jsSources, ['js']);
    gulp.watch(config.imgSources, ['img']);
    gulp.watch(config.lessSources, ['less']);
    gulp.watch(config.htmlSources, ['bower']);
    gulp.watch(config.htmlTemplates, ['templates']);
  }
});

gulp.task('$.connect', function() {
  if (!config.build){
    $.connect.server({
      root: config.outputDir,
      port:8082,
      livereload: true,
      fallback: config.outputDir+'/index.html',
    })
  }
});

gulp.task('default', [
  'bower', 
  'html', 
  'img',
  'js', 
  'templates', 
  'less', 
  '$.connect', 
  'watch'
  ])

gulp.task('compile', [
	  'bower', 
	  'html', 
	  'img',
	  'js', 
	  'templates', 
	  'less'
	  ])

