var $colors = function(index,max,alpha){
    if (max < 1) max = 1;
    var h=index * (360 / max + 45) % 360;
    var s=0.65;
    var l=0.6;
    return alpha?"hsla("+h+","+s*100+"%,"+l*100+"%,"+alpha+")":"hsl("+h+","+s*100+"%,"+l*100+"%)";
}
