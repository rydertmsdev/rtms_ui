App.service('permissionsSvc', ['$window',function ($window) {
  var svc=this;
  //DEFAULT DENY/ALLOW ALL:
  svc.default=false;

  svc.roles={
    "superuser":{
      name:"Superuser",
      permissions:{

      }
    },
    "supplier":{
      name:"Supplier",
      permissions:{
        "shipment":false,
        "shipment.create.details.urgent":true,
        "fulfillment.po.details.edit":false
      }
    },
    "desktopShipper":{
      name:"Desktop Shipper",
      permissions:{
        "shipment.create.details.urgent":true,
        "shipment.create.details":false,
        "shipment.create.additionalOptions":false,
        "shipment.create.items":false,
        "fulfillment":false,
      }
    },
    "viewOnly":{
      name:"View Only",
      permissions:{
        "shipment":false,
        "shipment.create.details.urgent":true,
        "fulfillment":false,
      }
    },
  }

  svc.activeRole=$window.localStorage.getItem("userRole")||"superuser";

  svc.setRole=function(roleName){
    svc.activeRole=roleName;
    $window.localStorage.setItem('userRole',roleName);
  }

  return svc;
}]);

App.directive('permission', ['permissionsSvc',function (svc) {
  var checkPermissions=function(permission,element){
    if (!svc.activeRole) return;
    element.prop("disabled",false);
    element.removeAttr("no-role");
    element.removeClass("no-hover");
    if (svc.roles[svc.activeRole].permissions[permission]===false){
      element.attr("no-role","");
    }
    else if(svc.roles[svc.activeRole].permissions[permission]===true){
      element.attr("disabled","disabled");
      element.addClass("no-hover");
    }
  }
  return{
    scope:{
      permission:"@",
    },
    link:function(scope, element, attrs){
      var permissionWatch = scope.$watch(function(){
        return svc.activeRole;
      },function(newval,oldval){
        if (newval!=oldval){
          checkPermissions(scope.permission,element)
        }
      },true);
      scope.$on("$destroy",function(){
        permissionWatch();
      })
      checkPermissions(scope.permission,element);
    }
  }
}]);
