var App = angular.module('App', [
  'ngAnimate',
  'angular.filter',
  'ui.router',
  'ui.router.stateHelper',
  'ui.select',
  'ngSanitize',
  'angular-timeline',
  'templates',
  'ryder.ui',
  'thatisuday.dropzone',
  'vs-repeat',
  'barcode',
  'nvd3',
  'gridster'
  ]);

App.config(['$locationProvider', '$stateProvider', 'stateHelperProvider', '$urlRouterProvider', '$httpProvider', '$compileProvider', function($locationProvider, $stateProvider, stateHelperProvider, $urlRouterProvider, $httpProvider, $compileProvider) {
//$locationProvider.html5Mode(true).hashPrefix('!');

// $urlRouterProvider.otherwise('/');

stateHelperProvider.state(
///////////////////////////////////////////////////
// Home/Dashboard
//
{
  name:'home',
  url: '/',
  templateUrl: 'home.tpl',
  // redirect:'home.dashboard',
  // controller:'homeCtrl',
  children:[
  {
    name:'dashboard',
    url:'dashboard',
    templateUrl:'dashboard.tpl',
    controller:'dashboardCtrl',
    redirect:'home.dashboard.default',
    children:[
    {
      name:'default',
      templateUrl:'dashboard-default.tpl',
    }
    ]
  },
///////////////////////////////////////////////////
// Dock Management
//
{
  name:'dock-management',
  url:'dock-management',
  templateUrl:'dock-management.tpl',
  controller:'dockManagementCtrl',
},
{
  name:'tracking',
  url:'tracking',
  templateUrl:'shipment-tracking.tpl',
  redirect:'home.tracking.search',
  children:[
  {
    name:'search',
    url:'/search',
    templateUrl:'shipment-tracking-results.tpl',
    controller:'shipmentTrackingResultsCtrl'
  },
  {
    name:'export',
    url:'/export',
    templateUrl:'shipment-tracking-export.tpl',
    controller:'shipmentTrackingExportCtrl'
  }
  ]
},
///////////////////////////////////////////////////
// Settings
//
{
  name:'settings',
  url:'settings',
  templateUrl:'settings.tpl',
  redirect:'home.settings.administration',
  children:[
  {
    name:'administration',
    url:'/administration',
    templateUrl:'settings-administration.tpl',
    controller:'settingsAdministrationGeneralCtrl',
    redirect:'home.settings.administration.general',
    children:[
    {
      name:'general',
      url:'/general',
      redirect:'home.settings.administration.general.evr',
      templateUrl:'settings-administration-general.tpl',
      children:[
      {
        name:'evr',
        url:'/evr',
        templateUrl:'settings-administration-general-evr.tpl',
      }
      ]
    }
    ]
  },
  {
    name:'event-matching',
    url:'/event-matching',
    templateUrl:'settings-event-matching.tpl',
    redirect:'home.settings.event-matching.rules',
    children:[
    {
      name:'rules',
      url:'/event-matching/rules',
      templateUrl:'settings-event-matching-rules.tpl',
      controller:'settingsEventMatchingRules'
    },
    {
      name:'errors',
      url:'/event-matching/errors',
      templateUrl:'settings-event-matching-errors.tpl',
      controller:'settingsEventMatchingErrorsCtrl'
    }
    ]
  },
  ]
},
///////////////////////////////////////////////////
// Orders
//
{
  name:'orders',
  url:'order-fulfillment',
  templateUrl:'orders.tpl',
  redirect:'home.orders.fulfillment.search',
  children:[
  {
    name:'fulfillment',
    url:'',
    templateUrl:'orders-fulfillment.tpl',
    controller:'ordersFulfillmentCtrl',
    redirect:'home.orders.fulfillment.search',
    children:[
    {
      name:'search',
      url:'/',
      module: 'order',
      getCodeLists : true,
      codeListParams: ["OrderItemReferenceList","ReferenceList", "SearchFieldList", "SearchResultsFieldList", "CountryList", "StateList", "ShipmentItemReferenceList", "HandlingUnitReferenceList", "ShipmentReferenceList", "ExtAttributeList", "CustomerConfigList", "UserDefaultLocationsList", "OrderDetailsItemsRolViewList", "OrderDetailsShipmentRolViewList", "OrderDetailsHandlingUnitsRolViewList", "FieldDependencyList", "FieldValidationList", "PODetailsRolViewList"],
      resolve: {},
      templateUrl:'of-search-results.tpl',
      controller:'ofSearchResultsCtrl'
    },
    {
      name:'import',
      url:'/import',
      templateUrl:'of-import-results.tpl',
      controller:'ofImportResultsCtrl'
    },
    {
      name:'shipments',
      url:'/shipments',
      templateUrl:'of-shipments.tpl',
      controller:'ofShipmentsCtrl',
      children:[
      {
        name:'details',
        url:'/details/:uuid',        
        templateUrl:'of-shipments-details.tpl',
      },
      {
        name:'submit',
        url:'/submit/:uuid',
        templateUrl:'of-shipments-submit.tpl',
      }
      ]
    }
    ]
  }
  ]
},
{
  name:'location-search',
  url:'location-master',
  module: 'address/v1',
  getCodeLists : true,
  codeListParams: ["SearchFieldList", "SearchResultsFieldList", "CountryList", "StateList", "CreateUpdateFieldList", "TimezoneList", "LanguageList", "UseTypeList"],
  resolve: {},
  templateUrl:'orders-location-search.tpl',
  controller:'ordersLocationSearchCtrl',
},
{
  name:'item-search',
  url:'item-master',
  module: 'itemmaster/v1',
  getCodeLists : true,
  codeListParams: ["SearchFieldList", "SearchResultsFieldList", "CountryList", "StateList", "ItemEditFieldsList", "ItemReferenceList"],
  resolve: {},
  templateUrl:'orders-item-search.tpl',
  controller:'ordersItemSearchCtrl',
},
///////////////////////////////////////////////////
// Create Shipment
//
{
  name:'shipment',
  url:'shipment',
  templateUrl:'shipments.tpl',
  redirect:'home.shipment.create.default',
  children:[
  {
    name:'create',
    url:'/create',
    templateUrl:'shipments-create.tpl',
    redirect:'home.shipment.create.default',
    children:[
    {
      name:'default',
      module: 'shipment',
      getCodeLists : true,
      userProfileURL: "scs/userProfile",
      codeListURL: "scs/rolViewList",
      codeListParams: ["OrderItemReferenceList","ReferenceList", "SearchFieldList", "SearchResultsFieldList", "CountryList", "StateList", "ShipmentItemReferenceList", "HandlingUnitReferenceList", "ShipmentReferenceList", "ExtAttributeList", "CustomerConfigList", "UserDefaultLocationsList", "OrderDetailsItemsRolViewList", "OrderDetailsShipmentRolViewList", "OrderDetailsHandlingUnitsRolViewList", "FieldDependencyList", "FieldValidationList", "PODetailsRolViewList"],
      resolve: {},
      url:'/standard',
      templateUrl:'of-standard-search-results.tpl',
      controller:'ofStandardSearchResultsCtrl',
      children:[
        {
          name:'details',
          url:'/details',
          templateUrl:'shipments-edit-default.tpl',
          controller:'shipmentsEditDefaultCtrl',
        }]
      // templateUrl:'shipments-create-default.tpl',
      // controller:'shipmentsCreateDefaultCtrl'
    },
    {
      name:'desktop',
      url:'/desktop',
      templateUrl:'shipments-create-default.tpl',
      controller:'shipmentsCreateDefaultCtrl',
      param: 'desktop'
    },
    {
      name:'manual',
      url:'/manual',
      templateUrl:'shipments-create-default.tpl',
      controller:'shipmentsCreateDefaultCtrl',
      param: 'manual'
    },
    {
      name:'misc',
      url:'/misc',
      templateUrl:'shipments-create-default.tpl',
      controller:'shipmentsCreateDefaultCtrl',
      param: 'misc'
    },
    {
      name:'standarddetails',
      url:'/standarddetails',
      templateUrl:'shipments-edit-default.tpl',
      controller:'shipmentsEditDefaultCtrl',
    }
    ]
  },
  {
    name:'create-submit',
    url:'/submit',
    templateUrl:'shipments-create-submit.tpl',
    controller:'shipmentsCreateSubmit'
  }
  ]
}
]
})
}]);


App.run(['$rootScope','$state', '$window', '$document', 'uiSidebarSvc', 'uiAlertSvc', 'uiResponsiveSvc', 'uiLoaderSvc', 'uiModalSvc', 'UserProfileService', 'codeListService', 'generateSearchQuery', '$q', '$timeout' ,function ($rootScope, $state, $window, $document, uiSidebarSvc, uiAlertSvc, uiResponsiveSvc, uiLoaderSvc, uiModalSvc, UserProfileService, codeListService, generateSearchQuery, $q, $timeout, permissionsSvc) {
///////////////////////////////////////////////////
// GLOBAL SERVICES
//

$rootScope.uiSidebarSvc=uiSidebarSvc;
$rootScope.uiAlertSvc=uiAlertSvc;
$rootScope.uiModalSvc=uiModalSvc;
$rootScope.uiResponsiveSvc=uiResponsiveSvc;
$rootScope.uiLoaderSvc=uiLoaderSvc;

///////////////////////////////////////////////////
// ROUTING
//

var stateName = "";
$rootScope.$on('$stateChangeStart', function (event, toState, toParams) {
  if (toState.resolve){
    toState.resolve.pauseStateChange = [ '$q', function($q) { 
      var defer = $q.defer();
      uiLoaderSvc.show();
        UserProfileService.fetchUserProfile(toState.module, toState.userProfileURL).then(function(resp){
          uiLoaderSvc.hide();
         
          if(resp.status == 200){

            if(!!$rootScope.userProfile && (toState.name == stateName)){
              defer.resolve();
            }
            else{

              stateName = toState.name;
              $rootScope.userProfile = resp;

              if (toState.getCodeLists) 
              {   
                  uiLoaderSvc.show();
                  var params = {"codeLists": toState.codeListParams, "customerNums": ["NGX"]}
                  codeListService.codeListsPostCall(toState.module, params, toState.codeListURL).then(function(codesListResp){
                    codeListService.codeLists = codesListResp.data.content;
                    uiLoaderSvc.hide();
                    $rootScope.generatedResponse = generateSearchQuery.searchQuery(codesListResp);

                    if (toState.redirect){
                      event.preventDefault();
                      $state.go(toState.redirect)
                    }
                    defer.resolve();
                  });
              }
              else{
                if (toState.redirect){
                  event.preventDefault();
                  $state.go(toState.redirect)
                }
                defer.resolve();
              }       
          }           
        }
        else{
          var url = "../../rydertrac/splash";
          $window.open(url, '_self');
          return defer.reject();
        }                 
        }, function(err){
          uiLoaderSvc.hide();

          var errMessage = "<ul>"
          for(i=0; i<err.data.messages.length; i++){
            errMessage += "<li>"+ err.data.messages[i].message+"</li>";
          }
          errMessage += "</ul>";

          uiModalSvc.show('ok-cancel-modal.tpl',{
            title:"Warning",
            html: errMessage,
            callback:function(){
              var url = "../../rydertrac/splash";
              $window.open(url, '_self');
            }
          });

        });
        return defer.promise;
    }]
  }
  else{
    if (toState.redirect){
      event.preventDefault();
      $state.go(toState.redirect)
    }
  }
});
$rootScope.$on("$stateChangeSuccess", function (event, currentRoute, previousRoute) {
  if (uiResponsiveSvc.isDesktop) $window.scrollTo(0, 0);
  if (!uiResponsiveSvc.isDesktop) $document[0].body.scrollTop = $document[0].documentElement.scrollTop = 0;
});

}]);


App.directive('includeReplace', function () {
  return {
    require: 'ngInclude',
    restrict: 'A', /* optional */
    link: function (scope, el, attrs) {
      el.replaceWith(el.children());
    }
  };
});


App.controller("mainCtrl", ['$state','$scope', '$timeout','uiLoaderSvc','uiModalSvc', function ($state, $scope, $timeout, uiLoaderSvc, uiModalSvc) { 
  $scope.uiLoaderSvc=uiLoaderSvc;
  $scope.uiModalSvc=uiModalSvc;
}])

