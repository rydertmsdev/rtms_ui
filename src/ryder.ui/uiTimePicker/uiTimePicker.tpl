<div class="selectize">
	<ui-select ng-model="value" theme="selectize">
		<ui-select-match placeholder="" ng-bind-html="$select.selected">
		</ui-select-match>
		<ui-select-choices repeat="i in options | filter:$select.search track by $index">
			{{i}}
		</ui-select-choices>
	</ui-select>
</div>