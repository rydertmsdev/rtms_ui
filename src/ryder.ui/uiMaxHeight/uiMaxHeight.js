angular.module('ryder.ui').directive('uiMaxHeight', ['$timeout',function ($timeout) {
	return {        
		restrict:'A',
		link:function(scope, element, attrs) {
			element.parent().attr('ui-max-height-cloak',"");
			$timeout(function(){
				var maxHeight=element[0].clientHeight + "px";
				element.attr("ui-max-height",maxHeight);
				element[0].style.maxHeight=maxHeight;
				element.parent().removeAttr('ui-max-height-cloak');
			})
		}
	}
}]);