angular.element(document.body).append(angular.element("<div ui-info-container></div>"));
angular.module('ryder.ui').directive('uiInfoContainer', ['$rootScope','$timeout', '$window' ,function ($rootScope, $timeout, $window) {
	return {        
		restrict:'A',
		template:'\
		<div ui-info-content ng-show="showInfo">\
			<span ng-if="uiInfoText" ng-bind-html="uiInfoText"></span>\
		</div>\
		',
		link:function(scope, element, attrs, ngModel) {
			var content=angular.element(element[0].querySelector('[ui-info-content]'));
			var setPosition=function($event,attrs,config){
				scope.showInfo=true;
				$timeout(function(){
					var left=($event.clientX+(attrs.uiInfoOffsetX||config.offsetX))-content[0].clientWidth/2;
					var top=$event.clientY+(attrs.uiInfoOffsetY||config.offsetY);
					if (left<0) left=2;
					if (left+content[0].clientWidth>$window.innerWidth) left=$window.innerWidth-content[0].clientWidth-2
					if (top+content[0].clientHeight>$window.innerHeight) top=$window.innerHeight-content[0].clientHeight-2
					content[0].style.left=left+'px';
					content[0].style.top=top+'px';
				})
				angular.element($window).one('click',off);
			}

			var on=function(event, args){
				if (scope.showInfo) return;
				scope.$apply(function(){
					currentTarget=args.currentTarget;
					if (args.attrs.uiInfo){
						scope.uiInfoText=args.attrs.uiInfo;
					}
					setPosition(args.event,args.attrs,args.config);
				})
			}

			var off=function(){
				scope.$evalAsync(function(){
					scope.showInfo=false;
					angular.element($window).off('click',off);
				})
			}

			$rootScope.$on('uiInfo.show',on)
			$rootScope.$on('uiInfo.hide',off)
		}
	}
}]);

angular.module('ryder.ui').directive('uiInfo', ['$rootScope','$timeout', function ($rootScope,$timeout) {
	var config={
		delay:250,
		offsetX:0,
		offsetY:25
	};
	return {        
		restrict:'A',
		link:function(scope, element, attrs, ngModel) {
			var timeout;
			var enter=function($event){
				if (!attrs.uiInfo) return;
				var currentTarget=angular.element($event.currentTarget);
				$timeout.cancel(timeout);
				timeout=$timeout(function(){
					$rootScope.$broadcast('uiInfo.show', {event:$event,attrs:attrs,config:config,currentTarget:currentTarget})
				},attrs.uiInfoDelay||config.delay)
			}
			var leave=function(){
				$timeout.cancel(timeout);
				$rootScope.$broadcast('uiInfo.hide')
			}

			element.on('mousemove', enter)
			element.on('mouseleave', leave)
			scope.$on('$destroy',function(){
				leave();
				element.off();
			})
		}
	}
}]);

