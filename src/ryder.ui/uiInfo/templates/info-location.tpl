<div class="form rows">
	<div class="cols">
		<ui-field type="text" label="Name" ng-disabled="true" ng-model="address.name"></ui-field>	
		<ui-field type="text" label="Address" ng-disabled="true" ng-model="address.address"></ui-field>	
	</div>
	<div class="cols">
		<ui-field type="text" label="City" ng-disabled="true" ng-model="address.city"></ui-field>	
		<ui-field type="text" label="State or Province" ng-disabled="true" ng-model="address.sau"></ui-field>	
	</div>
	<div class="cols">
		<ui-field type="text" label="Postal Code" ng-disabled="true" ng-model="address.postalCode"></ui-field>	
		<ui-field type="text" label="Country" ng-disabled="true" ng-model="address.country"></ui-field>	
	</div>
</div>