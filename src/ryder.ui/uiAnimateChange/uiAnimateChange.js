angular.module('ryder.ui').directive('uiAnimateChange', ['$timeout',function ($timeout) {
  return {        
    restrict:'A',
    scope:{
      'uiAnimateChange':'=',
      'uiAnimateChangeClass':'@'
    },
    link:function(scope, element, attrs) {
      var unwatch=scope.$watch(function(){
        return scope.uiAnimateChange;
      }, function(newval,oldval) {
        if (newval!=oldval) {
          element.addClass(scope.uiAnimateChangeClass);
          $timeout(function() {
            element.removeClass(scope.uiAnimateChangeClass);
          }, 1000)
        }
      });
      scope.$on('$destroy',function(){
        unwatch();
      })
    }
  }
}]);