angular.module('ryder.ui').directive('uiDragDrop', [function () {
    return {
        scope: {
            "uiDragDrop": "&"
        },
        link: function (scope, el, attrs, controller) {
            var enterTarget;
            el.bind("dragenter", function ($event) {
                el.addClass('dragOver');
                enterTarget = $event.target;
            });

            el.bind("dragleave", function ($event) {
                if ($event.target == enterTarget) {
                    el.removeClass('dragOver')
                }
            });

            el.bind("dragover", function ($event) {
                $event.preventDefault && $event.preventDefault();
                return false;
            });

            el.bind("drop", function ($event) {
                el.removeClass('dragOver')
                scope.$apply(function () {
                    scope.uiDragDrop && scope.uiDragDrop({ $event: $event });
                });
            });

            scope.$on('$destroy', function () {
                el.unbind();
            })
        }
    }
}]).directive('uiDrag', [function () {
    return {
        scope: {
            uiDragStart: '&',
            uiDragStop: '&'
        },
        link: function (scope, el, attrs, controller) {
            angular.element(el).attr("draggable", "true");

            if (scope.uiDragStart) {
                el.bind("dragstart", function ($event) {
                    $event.dataTransfer.effectAllowed = "move"
                    scope.$apply(function () {
                        scope.uiDragStart && scope.uiDragStart();
                    })
                });
            }
            if (scope.uiDragStop) {
                el.bind("dragend", function ($event) {
                    scope.$apply(function () {
                        scope.uiDragStop && scope.uiDragStop();
                    })
                });
            }

            scope.$on('$destroy', function () {
                el.unbind();
            })
        }
    }
}]);