angular.module('ryder.ui').directive('subform', ['$window','$timeout',function($window,$timeout){
	return {
		restrict: 'AEC',
		template:'<div class="ui-subform"><div class="ui-subform-top-divider"></div><ng-transclude></ng-transclude><div class="ui-subform-bottom-divider"></div></div>',
		transclude:true,
		replace:true,
		link: function(scope, element, attrs) {
		}
	};
}])