angular.module('ryder.ui').directive('uiBgImg', [function () {
	return {        
		restrict:'A',
		scope:{
			'uiBgImg':'@'
		},
		link:function(scope, element, attrs) {
			element[0].style.backgroundImage="url('"+scope.uiBgImg+"')";
		}
	}
}]);