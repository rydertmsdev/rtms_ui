angular.module('ryder.ui').directive('uiMenuCollapse', ['$timeout',function ($timeout) {
	return {        
		restrict:'A',
		link:function(scope, element, attrs) {
			var group=angular.element(element.closest('ui-menu-group'));
			if(group.attr('collapsed')==undefined){
				group.attr('no-overflow','true');	
			}
			element.on('click',function(){
				if (group.attr('collapsed')!=undefined){
					scope.$apply(function(){
						group.parent().children().attr('collapsed','true');
						group.parent().children().removeAttr('no-overflow');
						group.removeAttr('collapsed').one('transitionEnd webkitTransitionEnd',function(){
							if(group.attr('collapsed')==undefined){
								group.attr('no-overflow','true');	
							}
						});
					});
					scope.timeout=$timeout(function(){
						if(group.attr('collapsed')==undefined){
							group.attr('no-overflow','true');	
						}
					},500)
				}
				else{	
					$timeout.cancel(scope.timeout);
					scope.$apply(function(){
						group.parent().children().removeAttr('no-overflow');
						group.attr('collapsed','true');
					});
				}
			})
			scope.$on('$destroy',function(){
				group.off();
				element.off();
			})
		}
	}
}]);

angular.module('ryder.ui').directive('uiMenuItem', ['$state',function ($state) {
	return {        
		restrict:'AE',
		link:function(scope, element, attrs) {
			if (attrs.uiSref&&attrs.uiSref==$state.current.name){
				angular.element(element.closest('ui-menu-group'));
				angular.element(element.closest('ui-menu-group')).removeAttr('collapsed');
			}
		}
	}
}]);


























