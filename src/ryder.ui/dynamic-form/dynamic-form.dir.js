// A Directive to generate Dynamic Template
App.directive('dynamicTemplate', function ($compile, generateSearchQuery, codeListService) {
  return {
    restrict: 'E',
    scope: { form_generator_schema: '=data' },
    link: function (scope, elm, attrs) {
      // console.log("Directive Called");

      scope.generateSearchQuery = generateSearchQuery;

      scope.codeLists = codeListService.codeLists;

      scope.CountryList = [];
      scope.StateList = [];
      scope.TimezoneList = [];

      if(scope.codeLists.CountryList){
        scope.CountryList = scope.codeLists.CountryList;
      }

      if(scope.codeLists.StateList){
        scope.StateList = scope.codeLists.StateList;
      }

      if(scope.codeLists.TimezoneList){
        scope.TimezoneList = scope.codeLists.TimezoneList;
      }


      var field_inputs = "";
      // var searchFieldsList = scope.form_generator_schema.SearchFieldList;

      var searchFieldsList = [];
      var tempArray = [];
      // tempArray = scope.form_generator_schema.ReferenceList;
      // tempArray.forEach(function(item){
      //   searchFieldsList.push(item);
      // });

      // tempArray = scope.form_generator_schema.OrderItemReferenceList;
      // tempArray.forEach(function(item){
      //   searchFieldsList.push(item);
      // });

      tempArray = scope.form_generator_schema;
      tempArray.forEach(function(item){
        searchFieldsList.push(item);
      });

      getOptionVal=function(arr){
        if(arr){
          var tempFlag = false;
          angular.forEach(arr,function(opt, oIndex){
            if(opt.defaultValue){
              if(opt.code == "Y" || opt.code == "YES"){
                tempFlag = true;
              }
            }
          });
          return tempFlag;
        }
        else{
          return true;
        }
      }

      // var searchFieldsList = codeListService.codeLists.SearchFieldList;
      
      // scope.onchangeevent = function(){
      //   console.log(generateSearchQuery.values);
      // }

      for (var field in searchFieldsList){
        var new_field = "";
        var field_type = searchFieldsList[field].displayType == "boolean" ? "freeform text" : searchFieldsList[field].displayType;

        var hoverText = searchFieldsList[field].hoverText || '';
       
        switch (field_type){
          case 'freeform text':
            new_field = '<ui-field type="text" code="'+searchFieldsList[field].fieldName+'" ng-model="generateSearchQuery.values['+searchFieldsList[field].uuid+']" label="'+searchFieldsList[field].displayName+'" ui-info="'+hoverText+'" placeholder="---" ng-required="false" ng-disabled="false"></ui-field>';
            break;
          case 'dropdown':
            scope[searchFieldsList[field].code] = searchFieldsList[field].options;

            if(!scope[searchFieldsList[field].code]){
              scope[searchFieldsList[field].code] = [];
            }
            new_field = '<ui-field type="select" code="'+searchFieldsList[field].fieldName+'" prop="name" ui-info="'+hoverText+'" options="'+searchFieldsList[field].code+'" ng-model="generateSearchQuery.values['+searchFieldsList[field].uuid+']" label="'+searchFieldsList[field].displayName+'" placeholder="---" ng-required="false" ng-disabled="false"></ui-field>';
            break;
          case 'country_dropdown':
            scope[searchFieldsList[field].code] = scope.CountryList;

            if(!scope[searchFieldsList[field].code]){
              scope[searchFieldsList[field].code] = [];
            }

            new_field = '<ui-field type="select" code="'+searchFieldsList[field].fieldName+'" prop="name" ui-info="'+hoverText+'" options="'+searchFieldsList[field].code+'" ng-model="generateSearchQuery.values['+searchFieldsList[field].uuid+']" label="'+searchFieldsList[field].displayName+'" placeholder="---" ng-required="false" ng-disabled="false"></ui-field>';
            break;
          case 'state_dropdown':
            scope[searchFieldsList[field].code] = scope.StateList;

            if(!scope[searchFieldsList[field].code]){
              scope[searchFieldsList[field].code] = [];
            }
            new_field = '<ui-field type="select" code="'+searchFieldsList[field].fieldName+'" prop="name" ui-info="'+hoverText+'" options="'+searchFieldsList[field].code+'" ng-model="generateSearchQuery.values['+searchFieldsList[field].uuid+']" label="'+searchFieldsList[field].displayName+'" placeholder="---" ng-required="false" ng-disabled="false"></ui-field>';
            break;
          case 'timezone_dropdown':
            scope[searchFieldsList[field].code] = scope.TimezoneList;

            if(!scope[searchFieldsList[field].code]){
              scope[searchFieldsList[field].code] = [];
            }
            new_field = '<ui-field type="select" code="'+searchFieldsList[field].fieldName+'" prop="timezoneDesc" ui-info="'+hoverText+'" options="'+searchFieldsList[field].code+'" ng-model="generateSearchQuery.values['+searchFieldsList[field].uuid+']" label="'+searchFieldsList[field].displayName+'" placeholder="---" ng-required="false" ng-disabled="false"></ui-field>';
            break; 
          case 'multi sel dropdown':
            searchFieldsList[field].qualifierCode = searchFieldsList[field].fieldName;
            scope[searchFieldsList[field].qualifierCode] = searchFieldsList[field].options;

            if(!scope[searchFieldsList[field].qualifierCode]){
              scope[searchFieldsList[field].qualifierCode] = [];
            }

            new_field = '<ui-field type="multi" code="'+searchFieldsList[field].fieldName+'" options="'+searchFieldsList[field].qualifierCode+'" ng-model="generateSearchQuery.values['+searchFieldsList[field].uuid+']" label="'+searchFieldsList[field].displayName+'" placeholder="---" ng-required="false" ng-disabled="false"></ui-field>';
            break;    
          case 'date':
            new_field = '<ui-field type="date" ui-info="'+hoverText+'" code="'+searchFieldsList[field].fieldName+'" label="'+ searchFieldsList[field].displayName + '" ng-model="generateSearchQuery.values['+searchFieldsList[field].uuid+']" placeholder="---" ng-required="false" ng-disabled="false"></ui-field>';
            break;
          case 'single checkbox':
            if(searchFieldsList[field].options){
              generateSearchQuery.values[searchFieldsList[field].uuid] = getOptionVal(searchFieldsList[field].options);
            }

            new_field = '<ui-field type="toggle" class="no-label" ng-model="generateSearchQuery.values['+searchFieldsList[field].uuid+']" ui-info="'+hoverText+'" title="'+searchFieldsList[field].displayName+'"  ng-required="false" ng-disabled="false"></ui-field>';
            break;
          case 'none':
            // new_field = '<ui-field type="date" code="'+searchFieldsList[field].fieldName+'" label="'+ searchFieldsList[field].displayName + '" ng-model="generateSearchQuery.values['+searchFieldsList[field].uuid+']" placeholder="---" ng-required="false" ng-disabled="false"></ui-field>';
            break;
          default:
            new_field = '<ui-field type="text" ui-info="'+hoverText+'" code="'+searchFieldsList[field].fieldName+'" ng-model="generateSearchQuery.values['+searchFieldsList[field].uuid+']" label="'+searchFieldsList[field].displayName+'" placeholder="---" ng-required="false" ng-disabled="false"></ui-field>';
        }

        field_inputs = field_inputs + new_field;
      }
      elm.append($compile(field_inputs)(scope)); 
    }
  };
});
