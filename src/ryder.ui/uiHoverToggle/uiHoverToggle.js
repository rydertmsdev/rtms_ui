angular.module('ryder.ui').directive('uiHoverToggle', [function () {
	return {        
		restrict:'A',
		scope:{
			uiHoverToggle:"=uiHoverToggle"
		},
		link:function(scope, element, attrs, ngModel) {
			element.on('touchstart mouseenter',function(){
				scope.$apply(function(){
					scope.uiHoverToggle=true;
				})
			})
			element.on('touchend mouseleave',function(){
				scope.$apply(function(){
					scope.uiHoverToggle=false;
				})
			})
			scope.$on('$destroy',function(){
				element.off();
			})
		}
	}
}]);



