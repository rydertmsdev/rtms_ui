angular.module('ryder.ui').directive('uiHeaderActionsBtn', ['$window','$timeout',function($window,$timeout){
	return {
		restrict: 'AE',
		link: function(scope, element, attrs) {
			var open=false;
			var parent=angular.element(element.closest('ui-header-controls'));
			if (parent.length<1) {
				throw('uiHeaderActionsBtn - no closest parent')
				return;
			}
			var showMenu=function(){
					parent.addClass('show-actions');
					$timeout(function(){
						angular.element(window).one('click',hideMenu);
					})
			}
			var hideMenu=function(){
				parent.removeClass('show-actions');
			}
			element.on('click',showMenu);
			scope.$on('$destroy',function(){
				angular.element(window).off('click',hideMenu);
				element.off();
			})
		}
	};
}]);