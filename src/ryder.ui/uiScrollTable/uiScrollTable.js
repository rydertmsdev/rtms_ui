angular.module('ryder.ui').directive('uiScrollTable', [function(){
    return {
        replace:true,
        transclude:{
            'corner':'?corner',
            'left-header':'?leftHeader',
            'top-header':'?topHeader',
            'content':'?content'
        },
        templateUrl:'uiScrollTable.tpl',
        link: function(scope, element, attrs, ctrl) {
            var content=angular.element(element[0].querySelector('.ui-scroll-table-content'))[0];
            var topHeader=angular.element(element[0].querySelector('top-header'))[0];
            var leftHeader=angular.element(element[0].querySelector('left-header'))[0];
            var handleScroll=function(e){
                if (topHeader){
                    topHeader.style.transform="translateX("+(-content.scrollLeft)+"px)";
                }
                if (leftHeader){
                    leftHeader.style.transform="translateY("+(-content.scrollTop)+"px)";
                }
            }
            if (content){
                angular.element(content).on('scroll',handleScroll);
            }
            scope.$on('$destroy',function(){
                angular.element(content).off();
            })
        }
    };
}]);