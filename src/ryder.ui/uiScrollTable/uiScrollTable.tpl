<div class="ui-scroll-table">
	<div class="ui-scroll-table-header">
		<div class="ui-scroll-table-corner" ng-transclude="corner"></div>
		<div class="ui-scroll-table-top-header" ng-transclude="top-header">
		</div>
	</div>
	<div class="ui-scroll-table-body">
		<div class="ui-scroll-table-left-header" ng-transclude="left-header">
		</div>
		<div class="ui-scroll-table-content" ng-transclude="content">
		</div>
	</div>
</div>