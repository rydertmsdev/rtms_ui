angular.module('ryder.ui').directive('focusOn', ['$timeout',function($timeout) {
  return {
    scope: { 
      focusOn: '&' 
    },
    link: function(scope, element) {
      scope.$watch(scope.focusOn, function(value) {
        if(value == true) { 
          $timeout(function() {
            element[0].focus(); 
          });
        }
      });
    }
  };
}]);