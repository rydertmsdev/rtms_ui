App.service('uiLoaderSvc', ['$rootScope','$q','$timeout',function ($rootScope, $q, $timeout) {
	var svc=this;
	svc.showLoader=false;
	svc.show=function(){
		svc.showLoader=true;
	}
	svc.hide=function(){
		svc.showLoader=false;
	}
	svc.preload=function(func){
		$rootScope.$evalAsync(function(){
			svc.show();
		});
		svc.watcher=$rootScope.$watch(function(){
			return svc.preloadObserver;
		},function(val){
			if (val){
				svc.hide();
				delete svc.preloadObserver;
				svc.watcher();
			}
		})
		$timeout(function(){
			svc.preloadObserver=func();
		},350);
	}
}]);