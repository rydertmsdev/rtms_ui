angular.module('ryder.ui').directive('uiTabs', [function () {
	return {        
		restrict:'AE',
		templateUrl:'uiTabs.tpl',
		link:function(scope, element, attrs){
			scope.tabs.filter(function(tab,index){
				if (tab.active==true&&scope.activeTab==undefined){
					scope.activeTab=index;
				}
			})
			scope.setActiveTab=function(index){
				scope.tabs[scope.activeTab].active=false;
				if (index>scope.activeTab){
					element.addClass('aniLeft');
				}
				else{
					element.removeClass('aniLeft');
				}
				scope.activeTab=index;
				scope.tabs[scope.activeTab].active=true;
			}
		}
	}
}])