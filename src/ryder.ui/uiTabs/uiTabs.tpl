<ui-tabs-controls>
	<div ui-button ng-class="{'active':tab.active}" ng-click="tab.url?openUrl(tab.url):setActiveTab($index)" ng-repeat="tab in tabs track by $index">
		<span>{{tab.name}}</span>
		<i class="fa fa-external-link" ng-if="tab.url"></i>
	</div>
</ui-tabs-controls>
<ui-tabs-body>
	<div ng-if="tab.active" ng-include="tab.template" ng-repeat="tab in tabs track by $index"></div>
</ui-tabs-body>
