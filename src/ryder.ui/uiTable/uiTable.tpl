<div class="ui-table-desktop">
	<table ng-if="uiResponsiveSvc.isDesktop">
		<thead>
			<th ng-if="config.selectable" class="text-center">Select</th>
			<th ng-repeat="col in columns" class="{{col.className}}" ng-if="!col.hideDesktop">
				<span ng-bind-html="col.title"></span>
			</th>
			<th ng-if="config.selectable" class="text-center" ng-repeat="action in actions">{{action.title}}</th>
		</thead>
		<tbody>
			<tr ng-repeat="row in data" ng-class="{'selected':row.$$selected,'selectable':config.selectable}" ng-click="config.rowClick(row)">
				<td ng-if="config.selectable" class="text-center ui-table-select">
					<input type="checkbox" ng-model="row.$$selected" ng-click="$event.stopPropagation()" class="ui-table-checkbox" />
				</td>
				<td ng-repeat="col in columns" class="{{col.className}}"  ng-if="!col.hideDesktop">
					<span ng-if="col.type=='text'" ng-bind-html="row[col.key]">
						{{subrow.prepend}}
						{{row[col.key]}}
						{{subrow.append}}
					</span>
					<ui-field ng-if="col.type=='field'" class="{{col.field.className}}" type="{{col.field.type}}" ng-value="row[col.key]" ng-required="col.field.required" ng-disabled="col.field.disabled">
					</ui-field>
					<div ng-if="col.type=='rows'" class="rows">
						<span ng-repeat="subrow in col.rows" class="{{subrow.className}}">
							{{subrow.prepend}}
							{{row[subrow.key]}}
							{{subrow.append}}
						</span>
					</div>
				</td>
				<td ng-repeat="action in actions" class="text-center ui-table-action">
					<div ui-button ng-click="action.action(row);$event.stopPropagation()" class="ui-table-action-btn {{action.className}}">
						<i class="{{action.iconClass}}"></i>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
</div>

<div ng-if="uiResponsiveSvc.isMobile||uiResponsiveSvc.isTablet" class="ui-table-mobile">
	<div class="ui-table-mobile-row" ng-repeat="row in data" ng-class="{'selected':row.$$selected,'selectable':config.selectable}" ng-click="config.rowClick(row)">
		<div ng-if="config.selectable" class="text-center ui-table-select">
			<input type="checkbox" ng-model="row.$$selected" ng-click="$event.stopPropagation()" class="ui-table-checkbox" />
		</div>
		<div class="ui-table-mobile-cols">
			<div ng-repeat="col in columns" class="{{col.className}}" ng-class="{'multi':col.type=='rows'}" ng-if="!col.hideMobile">
				<label ng-if="col.type!='rows'">{{col.title}}</label>
				<span ng-if="col.type=='text'" ng-bind-html="row[col.key]">
					{{subrow.prepend}}
					{{row[col.key]}}
					{{subrow.append}}
				</span>
				<ui-field ng-if="col.type=='field'" class="{{col.field.className}}" type="{{col.field.type}}" ng-value="row[col.key]" ng-required="col.field.required" ng-disabled="col.field.disabled">
				</ui-field>
				<div ng-if="col.type=='rows'" class="ui-table-mobile-cols-multi" ng-repeat="subrow in col.rows" >
					<label>{{subrow.title}}</label>
					<span>
						{{row[subrow.key]}}
					</span>
				</div>
			</div>
		</div>
		<div class="ui-table-actions">
			<div ng-repeat="action in actions" class="text-center ui-table-action">
				<div ui-button ng-click="action.action(row);$event.stopPropagation()" class="ui-table-action-btn {{action.className}}">
					<i class="{{action.iconClass}}"></i>
				</div>
			</div>
		</div>
	</div>
</div>