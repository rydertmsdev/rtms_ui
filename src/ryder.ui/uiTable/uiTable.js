angular.module('ryder.ui').directive('uiTable', ['uiResponsiveSvc', function(uiResponsiveSvc){
    return {
        scope:{
            config:"=uiTable",
            columns:"=uiTableColumns",
            data:"=uiTableData",
            actions:"=uiTableActions"
        },
        templateUrl:'uiTable.tpl',
        link: function(scope, element, attrs, ctrl) {
            scope.uiResponsiveSvc=uiResponsiveSvc;
        }
    };
}]);