<ui-sidebar-tabs>
<!-- 	<div ui-button ng-click="togglePin()" ui-sidebar-pin-btn ng-class="{'active':uiSidebarSvc.pinned}" ui-info="Pin Sidebar" ng-hide="showPin===false" ng-disabled="uiSidebarSvc.noSidebar">
		<i class="fa fa-thumb-tack"></i>
	</div> -->
	<div ui-button ng-class="{'active':tab.active,'media-tablet-hide':tab.hideMobile}" ng-click="tab.action?tab.action():setActiveTab($index)" ng-repeat="tab in tabs track by $index" ui-info="{{tab.uiInfo}}">
		<i class="{{tab.iconClass}}"></i>
		<b ng-show="tab.badge" ui-animate-change="tab.badge" ui-animate-change-class="highlight" ng-bind="tab.badge"></b>
	</div>
	<ui-sidebar-spacer></ui-sidebar-spacer>
</ui-sidebar-tabs>
<ui-sidebar-panel-container>
	<ui-sidebar-panel ng-show="tabs[activeTab].sidebarTemplate">
		<ui-sidebar-panel-bg>
		</ui-sidebar-panel-bg>
		<div class="content" ng-if="!sidebarTemplate" ng-include="::tab.sidebarTemplate" ng-show="tab.active" ng-repeat="tab in tabs track by $index"></div>
		<div class="content" ng-if="sidebarTemplate" ng-include="::sidebarTemplate"></div>
	</ui-sidebar-panel>
</ui-sidebar-panel-container>
<ui-sidebar-body-container>
	<ui-sidebar-body>
		<div ui-view></div>
	</ui-sidebar-body>
	<ui-sidebar-body-overlay></ui-sidebar-body-overlay>
</ui-sidebar-body-container>

