angular.module('ryder.ui')
.service('uiSidebarSvc', ['$rootScope', function($rootScope){
	var svc=this;
	svc.showSidebar=true;
}])
.directive('uiSidebarNav', [function () {
	return {        
		restrict:'E',
		template:'<div class="ui-sidebar-nav" ng-transclude ng-class="{\'show-sidebar\':uiSidebarSvc.showSidebar,\'tabs-only\':!uiSidebarSvc.hasSidebar}"></div>',
		transclude:true,
		replace:true,
	}
}])
.directive('uiSidebarNavContent', ['uiSidebarSvc',function (uiSidebarSvc) {
	return {        
		restrict:'E',
		template:'<div class="ui-sidebar-nav-content" ng-transclude></div>',
		transclude:true,
		replace:true,
		link:function(scope,element,attrs){
			var sidebarWatch=scope.$watch(function(){
				return element[0].querySelector('.ui-sidebar-panel');
			},function(newval,oldval){
				if (newval==null){
					uiSidebarSvc.hasSidebar=false;
				}
				else{
					uiSidebarSvc.hasSidebar=true;
				}
			});
			scope.$on('$destroy',function(){
				sidebarWatch();
			})
		}
	}
}])
.directive('uiSidebar', [function () {
	return {        
		restrict:'E',
		template:'<div class="ui-sidebar" ng-transclude ng-class="{\'show-sidebar\':uiSidebarSvc.showSidebar}"></div>',
		transclude:true,
		replace:true,
	}
}])
.directive('uiSidebarPanel', [function () {
	return {        
		restrict:'E',
		template:'<div class="ui-sidebar-panel-container"><div class="ui-sidebar-panel" ng-transclude></div></div>',
		transclude:true,
		replace:true
	}
}])
.directive('uiSidebarContent', [function () {
	return {        
		restrict:'E',
		template:'<div class="ui-sidebar-content" ng-transclude></div>',
		transclude:true,
		replace:true
	}
}])
.directive('uiSidebarBtn', [function(){
	return {
		restrict: 'E',
		replace:true,
		template: '<a class="ui-sidebar-btn" ng-disabled="!uiSidebarSvc.hasSidebar&&uiResponsiveSvc.isDesktop" ng-class="{\'show-sidebar\':uiSidebarSvc.showSidebar}" ui-info="{{uiSidebarSvc.showSidebar?\'Hide Sidebar\':\'Show Sidebar\'}}"><span></span><span></span></a>',
		link: function(scope, element, attrs) {
			element.on('click',function(){
				scope.$apply(function(){
					scope.uiSidebarSvc.showSidebar=!scope.uiSidebarSvc.showSidebar;
				})
			})
			scope.$on('$destroy',function(){
				element.off();
			})
		}
	};
}])