angular.module("ryder.ui").service('uiDashboardSvc', ['$rootScope', '$timeout', '$interval', function($rootScope, $timeout, $interval){
	var svc=this;
	svc.gridConfig = {
columns: 4, // the width of the grid, in columns
pushing: true, // whether to push other items out of the way on move or resize
floating: true, // whether to automatically float items up so they stack (you can temporarily disable if you are adding unsorted items with ng-repeat)
swapping: true, // whether or not to have items of the same size switch places instead of pushing down if they are the same size
width: 'auto', // can be an integer or 'auto'. 'auto' scales gridster to be the full width of its containing element
colWidth: 'auto', // can be an integer or 'auto'.  'auto' uses the pixel width of the element divided by 'columns'
rowHeight: 'match', // can be an integer or 'match'.  Match uses the colWidth, giving you square widgets.
margins: [10, 10], // the pixel distance between each widget
outerMargin: true, // whether margins apply to outer edges of the grid
sparse: false, // "true" can increase performance of dragging and resizing for big grid (e.g. 20x50)
isMobile: false, // stacks the grid items if true
mobileBreakPoint: 667, // if the screen is not wider that this, remove the grid layout and stack the items
mobileModeEnabled: true, // whether or not to toggle mobile mode when screen width is less than mobileBreakPoint
minColumns: 1, // the minimum columns the grid must have
minRows: 2, // the minimum height of the grid, in rows
maxRows: 100,
defaultSizeX: 1, // the default width of a gridster item, if not specifed
defaultSizeY: 1, // the default height of a gridster item, if not specified
minSizeX: 1, // minimum column width of an item
maxSizeX: null, // maximum column width of an item
minSizeY: 1, // minumum row height of an item
maxSizeY: null, // maximum row height of an item
resizable: {
	enabled: true,
	handles: ['se'],
	start: function(event, $element, widget) {
	},
	resize: function(event, $element, widget) {
		$rootScope.$broadcast('renderCharts');
	},
	stop: function(event, $element, widget) {
		var resizeStopInterval=0;
		var interval=$interval(function(){
			$rootScope.$broadcast('renderCharts');
			resizeStopInterval++;
			if (resizeStopInterval>30){
				$interval.cancel(interval)
			}
		},1)
	} 
},
draggable: {
enabled: true, // whether dragging items is supported
handle: 'ui-header-body', // optional selector for drag handle
//start: function(event, $element, widget) {}, // optional callback fired when drag is started,
//drag: function(event, $element, widget) {}, // optional callback fired when item is moved,
//stop: function(event, $element, widget) {} // optional callback fired when item is finished dragging
}
};

svc.lineOptions = {
	chart: {
		type: 'lineChart',
		margin : {
			top: 20,
			right: 20,
			bottom: 25,
			left: 30
		},
		interpolate:"basis",
		clipEdge:true,
		showLegend:true,
		interactive:false,
		x: function(d){return d[0];},
		y: function(d){return d[1];},
		showValues: true,
		valueFormat: function(d){
			return d3.format(',.1f')(d);
		},
		duration: 0,
		xAxis: {
			tickFormat: function(d) {
				return d3.time.format('%b %d')(new Date(d))
			},
			rotateLabels: 0,
			showMaxMin: false
		},
		yAxis: {
			showMaxMin:false,
			range:[100,1000]
		},
		tooltip: {
			tickFormat: function(d) {
				return d3.time.format('%b %d')(new Date(d))
			}
		},
		yDomain: [0,250],
	}
};

svc.donutOptions = {
	"chart": {
		type: "pieChart",
		margin : {
			top: 0,
			right: 0,
			bottom: 0,
			left: 0
		},
		donutRatio:0.75,
		donut: true,
		showLabels: false,
		pie: {},
		duration: 0,
		showLegend:false,
		labelType:"key",
		tooltip:{
			enabled:false
		},
		growOnHover:false
	}
}
}]).directive('uiDashboardPanelContent', ['$timeout','$rootScope','uiDashboardSvc',function($timeout,$rootScope,uiDashboardSvc){
	return {
		scope: {
			"panel":"="
		},
		templateUrl: 'dashboard-panel.tpl',
		link: function(scope, element, attrs) {
			scope.uiDashboardSvc=uiDashboardSvc;
			$timeout(function(){
				scope.render=true;
				$rootScope.$broadcast('renderCharts')
			},350)
			scope.events={    
				renderCharts: function(e, scope){
					scope.api.refresh();
				}
			}
		}
	};
}]);
















