<ui-dashboard-chart ng-if="panel.type=='lineChart'" ui-visible="render">
	<nvd3 options="uiDashboardSvc.lineOptions" data="panel.data" events="events"></nvd3>	
</ui-dashboard-chart>
<ui-dashboard-chart ng-if="panel.type=='ratio'" ui-visible="render">
	<nvd3 options='uiDashboardSvc.donutOptions' data='panel.data' events="events"></nvd3>
	<div class="nvd3-donut-data">
		<div class="title">
			<span ng-style="{'color':panel.data[0].color}" ng-bind-html="panel.dataValue" ui-text-fill></span>
			<span class="text-lgray" ng-if="panel.dataTotal" ui-text-fill>|</span>
			<span class="text-mgray" ng-bind-html="panel.dataTotal" ui-text-fill></span>
		</div>
	</div>			
</ui-dashboard-chart>
<ui-dashboard-chart class="tiles" ng-if="panel.type=='tiles'" ui-visible="render">
	<div class="tile" ng-repeat="tile in panel.data track by $index" ng-style="{'background-color':tile.color}">
		<div class="title" ng-bind-html="tile.y"></div>
		<div class="subtitle" ng-bind-html="tile.title"></div>
	</div>
</ui-dashboard-chart>