angular.module('ryder.ui').directive('uiField', ['$templateCache','$compile', function ($templateCache,$compile) {
	return {        
		restrict:'AE',
		require: '?^ngModel',
		scope:{
			'ngModel':'=',
			'ngValue':'=',
			'prop':'@',
			'modelProp':'@',			
			'type':'@',			
			'label':'@',
			'title':'@',
			'hover':'@',
			'tabindex' : '@',
			'className':'@class',
			'ngDisabled':'=ngDisabled',
			'ngRequired':'=ngRequired',
			'placeholder':'@',
			'options':'=',
			'tagging':'@',
			'ngChange':'=ngChange',
			'ngBlur':'=ngBlur',
			'refresh':'&',
			'refreshArgs': "=",
			'selectedArgs': "=",
			'ngPattern' : "@",
			'onSelect':'=',
			'minimumInputLength':'@',
			'refreshDelay':'@'
		},
		transclude:true,
		link:function(scope, element, attrs, ngModel) {
			scope.hasModel=!!attrs.ngModel;
			if (scope.type=="time"){
				scope.altOptions=["00:00","00:15","00:30","00:45","01:00","01:15","01:30","01:45","02:00","02:15","02:30","02:45","03:00","03:15","03:30","03:45","04:00","04:15","04:30","04:45","05:00","05:15","05:30","05:45","06:00","06:15","06:30","06:45","07:00","07:15","07:30","07:45","08:00","08:15","08:30","08:45","09:00","09:15","09:30","09:45","10:00","10:15","10:30","10:45","11:00","11:15","11:30","11:45","12:00","12:15","12:30","12:45","13:00","13:15","13:30","13:45","14:00","14:15","14:30","14:45","15:00","15:15","15:30","15:45","16:00","16:15","16:30","16:45","17:00","17:15","17:30","17:45","18:00","18:15","18:30","18:45","19:00","19:15","19:30","19:45","20:00","20:15","20:30","20:45","21:00","21:15","21:30","21:45","22:00","22:15","22:30","22:45","23:00","23:15","23:30","23:45"]
			}

			if(!scope.ngModel && scope.options){
				if(scope.options.length == 0 || Object.keys(scope.options[0]).length <= 3){//Condition below is to avoid this to be added for HU object 
					var flag = false;
					for(i=0;i<scope.options.length;i++){
						if(scope.options[i].name == "- Select -"){
							flag = true;
						}
					}
					if(!flag){
						scope.options.splice(0,0,{"code":"","name":"- Select -"});
					}
				}
				angular.forEach(scope.options, function(option, index) {
					if(option.defaultValue){						
						if(scope.type == "select"){
							scope.ngModel = option;
						} 
						else{
							scope.ngModel = option.name;
						}
					}
				});
			}

			scope.selectValFunction=function(tempCode){
				if(!scope.options){
					return;
				}

				// var tempObj = scope.options.find(function(option){
				// 	return option.code == tempCode
				// });

				var tempObj;

				for(i=0;i<scope.options.length;i++){
					if(scope.options[i].code == tempCode){
						tempObj = scope.options[i];
					}
					else if(scope.options[i].timezoneCode == tempCode){
						tempObj = scope.options[i];
					}
				}

				if(tempObj){
					return (tempObj.name || tempObj.timezoneDesc);
				}
				else{
					return "";
				}
			}

			var templateUrl='empty.tpl';
			if (scope&&scope.type){
				switch (scope.type){
					case 'text': templateUrl='uiFieldText.tpl'; break;
					case 'number': templateUrl='uiFieldText.tpl'; break;
					case 'select': templateUrl='uiFieldSelect.tpl'; break;
					case 'multi': templateUrl='uiFieldMulti.tpl'; break;
					case 'time': templateUrl='uiFieldSelect.tpl'; break;
					case 'date': templateUrl='uiFieldDate.tpl'; break;
					case 'toggle': templateUrl='uiFieldToggle.tpl'; break;
					case 'color': templateUrl='uiFieldColor.tpl'; break;
				}
				$compile($templateCache.get(templateUrl).trim())(scope, function (template, scope) {
					element.append(template)
				});
			}
		},      
	}
}]);

angular.module('ryder.ui').directive('uiFieldDate', ['$window', '$timeout', function ($window,$timeout) {
	var buildMonth=function(year,month){
		var monthArr=[];
		var monthLength=new Date(year, month+1, 0).getDate();
		var weeksInMonth=Math.ceil( ((new Date(year, month, 1).getDay() + 7) % 7 + new Date(year, month+1, 0).getDate()) / 7);
		var firstDay=new Date(year, month, 1).getDay();
		for (var w=0;w<weeksInMonth;w++){
			var week=[];
			for (var d=0;d<7;d++){
				var day=w*7+d-firstDay;
				if (day<0||day>monthLength-1){
					week.push(null)
				}
				else{
					var date=new Date(year,month,day+1)
					week.push({
						year:year,
						month:month+1,
						day:day+1,
						timestamp:Math.floor(date.getTime()/1000),
						fullDate:date
					});
				}
			}
			monthArr.push(week);
		}
		return monthArr;
	}

	return {        
		restrict:'AE',
		require: '?^ngModel',
		link:function(scope, element, attrs, ngModel) {
			var input=angular.element(element[0].querySelector('input')),
			picker=angular.element(element[0].querySelector('[ui-date-picker-selector]'));

			scope.monthNames=["January","February","March","April","May","June","July","August","September","October","November","December"]
			scope.weekDays=["Sun","Mon","Tue","Wed","Thu","Fri","Sat"];
			var today = new Date();

			scope.monthInd=today.getMonth();
			scope.yearInd=today.getFullYear();
			
			today.setHours(0);
			today.setMinutes(0);
			today.setSeconds(0);
			scope.todayTimestamp=Math.floor(today.getTime()/1000);

			scope.prevMonth=function(e){
				e.stopPropagation();
				e.preventDefault();
				if (scope.monthInd>0){
					scope.monthInd--;
				}
				else{
					scope.yearInd--;
					scope.monthInd=11	
				}
				scope.update();
			}

			scope.nextMonth=function(e){
				e.stopPropagation();
				e.preventDefault();
				if (scope.monthInd>=11){
					scope.monthInd=0;
					scope.yearInd++;
				}
				else{
					scope.monthInd++;	
				}
				scope.update();
			}

			scope.setValue=function(timestamp){
				ngModel.$setViewValue(timestamp);
				$timeout(function(){
					picker.addClass('ng-hide');
				},200);
			}

			scope.update=function(){
				scope.$evalAsync(function(){
					scope.month=buildMonth(scope.yearInd,scope.monthInd);	
				})
			}

			scope.update();

			input.on('focus',function(){
				picker.removeClass('ng-hide')
			});

			input.on('keydown',function(e){
				if(e.keyCode==9){
					picker.addClass('ng-hide')
				}
				else if(e.keyCode==13){
					picker.removeClass('ng-hide')
					if (!ngModel.$viewValue){
						ngModel.$setViewValue(scope.todayTimestamp);
					}
					var d=new Date(ngModel.$viewValue*1000);
					if (d==="invalid Date"||!ngModel.$viewValue){
						return;
					}
					else{
						scope.yearInd=d.getFullYear();
						scope.monthInd=d.getMonth();
						scope.update();
					}
				}
				else if(e.keyCode==27){
					picker.addClass('ng-hide')
				}
			});

			angular.element(window).on('click touchstart touchmove',function(e){
				if (!element[0].contains(e.target)){
					picker.addClass('ng-hide')
				}
			})

			scope.$on('$destroy',function(){
				angular.element(window).off("click touchstart touchmove");
				input.off();
			})
		}
	}
}]);


angular.module('ryder.ui').directive('uiFieldDateInput', ['$filter','codeListService','uiModalSvc', function ($filter,codeListService, uiModalSvc) {
	return {        
		restrict:'AE',
		require: 'ngModel',
		scope:{
			'tempVar':'='
		},
		link:function(scope, element, attrs, ngModel) {
			
			picker=angular.element(element[0].querySelector('[ui-date-picker-selector]'));

			ngModel.$parsers.unshift(function(value){
				var d=Date.parse(value);
				return Math.floor(d/1000);
			});
			ngModel.$formatters.unshift(function(value){
				var d=new Date(value*1000);
				if (d==="invalid Date"||!value){
					return null
				}
				else{
					 if(scope.$parent.label == "Ready to Pickup" && d){

					 	scope.configList;
				 		
				 		if(codeListService.codeLists && codeListService.codeLists.CustomerConfigList[0]){
							scope.configList = codeListService.codeLists.CustomerConfigList[0];
						}

					 	var val = d;
					 	var readyDate = new Date(val);
				    	var todayDate = new Date();

				    	var timeDiff = readyDate.getTime() - todayDate.getTime();
						var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
						if(diffDays < scope.configList.OF_UI_SHIP_FROM_DATE_CUTOFF_DAYS_MIN || diffDays > scope.configList.OF_UI_SHIP_FROM_DATE_CUTOFF_DAYS_MAX){
							var alertText;
							if(diffDays < scope.configList.OF_UI_SHIP_FROM_DATE_CUTOFF_DAYS_MIN){
								alertText = "Ready to Pickup date cannot be before Today's date."
							}
							else{
								alertText = "Ready to pick up date cannot be more than "+scope.configList.OF_UI_SHIP_FROM_DATE_CUTOFF_DAYS_MAX+ " days from Today, Please modify to continue."
							}

							uiModalSvc.show('ok-cancel-modal.tpl',{
							title:"Alert",
							icon: "fa fa-exclamation-triangle",
							text: alertText,
							callback:function(){
									angular.element(document.querySelector('ui-field[label = "Ready to Pickup"] input'))[0].focus();
								}
							});	
						}
					 }
					return $filter('date')(d,"MM/dd/yy"); 
				}
			});

			element.on('blur', function(){
				if (!!ngModel.$modelValue){
				 element.val($filter('date')(ngModel.$modelValue*1000, "MM/dd/yy"));
				 if(scope.$parent.label == "Ready to Pickup" ){

				 	scope.configList;
			 		
			 		if(codeListService.codeLists && codeListService.codeLists.CustomerConfigList[0]){
						scope.configList = codeListService.codeLists.CustomerConfigList[0];
					}

				 	var val = new Date(ngModel.$modelValue*1000);
				 	var readyDate = new Date(val);
			    	var todayDate = new Date();

			    	var timeDiff = readyDate.getTime() - todayDate.getTime();
					var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
					if(diffDays < scope.configList.OF_UI_SHIP_FROM_DATE_CUTOFF_DAYS_MIN || diffDays > scope.configList.OF_UI_SHIP_FROM_DATE_CUTOFF_DAYS_MAX){
						var alertText;
						if(diffDays < scope.configList.OF_UI_SHIP_FROM_DATE_CUTOFF_DAYS_MIN){
							alertText = "Ready to Pickup date cannot be before Today's date."
						}
						else{
							alertText = "Ready to pick up date cannot be more than "+scope.configList.OF_UI_SHIP_FROM_DATE_CUTOFF_DAYS_MAX+ " days from Today, Please modify to continue."
						}

						uiModalSvc.show('ok-cancel-modal.tpl',{
						title:"Alert",
						icon: "fa fa-exclamation-triangle",
						text: alertText,
						callback:function(){
								angular.element(document.querySelector('ui-field[label = "Ready to Pickup"] input'))[0].focus();
							}
						});	
					}
				 }
				}
			});

			scope.$on('$destroy',function(){
				element.off();
			})
		}
	}
}]);























