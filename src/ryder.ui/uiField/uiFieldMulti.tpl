<ui-select 	class="uiFieldSelect" 
		   	multiple
			ng-model="$parent.ngModel" 
		   	theme="selectize" 
		   	ng-attr-tagging="{{tagging && 'true' || undefined }}" 
		   	tagging-label="false"
		   	tagging-tokens="SPACE|," 
		   	ng-disabled="ngDisabled">
	<ui-select-match placeholder="{{placeholder}}">
		{{$item||$select.selected.name}}
	</ui-select-match>
	<ui-select-choices refresh="refresh()($select.search)" 
					   refresh-delay="{{refreshDelay}}" 
					   minimum-input-length="{{minimumInputLength}}"
					   repeat="o in options||altOptions | filter:$select.search track by $index">
		{{o.name}}
	</ui-select-choices>
	<ui-select-no-choice ng-class="{'show':(options.length>0||altOptions.length>0)&&$select.search!=''}">
		{{tagging?'"'+$select.search+'"':'No matching options'}}
	</ui-select-no-choice>
</ui-select>

<label ng-bind-html="label"></label>
<span></span>