<ui-field-date ng-model="ngModel">
	<div ui-date-picker-selector class="ng-hide">
		<div class="monthHeader">
			<div ui-button ng-click="prevMonth($event)"><i class="fa fa-chevron-left"></i></div>
			<span class="text">
			<div class="title">{{monthNames[monthInd]}}</div>
			<div class="subtitle">{{yearInd}}</div>
			</span>
			<div ui-button ng-click="nextMonth($event)"><i class="fa fa-chevron-right"></i></div>
		</div>
		<div class="weekHeader">
		<div ng-repeat="d in weekDays">{{d}}</div>
		</div>
		<div ng-repeat="w in month track by $index" class="week">
			<div ng-repeat="d in w track by $index" class="day" 
				ng-click="d.timestamp&&setValue(d.timestamp)" 
				ng-class="{'today':d.timestamp==todayTimestamp,'enabled':d.timestamp,'active':d.timestamp==ngModel&&ngModel!=null}">
				{{d.day}}
			</div>
		</div>
	</div>
	<input ng-model="ngModel" ui-field-date-input
	type="text" 
	ng-required="ngRequired" 
	ng-disabled="ngDisabled" 
	placeholder="{{placeholder}}" />
	<label ng-bind-html="label"></label>
	<span></span>
</ui-field-date>