<input ng-if="hasModel" ng-model="$parent.ngModel" type="{{$parent.type}}" ng-required="ngRequired" ng-disabled="ngDisabled" ng-pattern="ngPattern" placeholder="{{$parent.placeholder}}" class="uiFieldText {{className}}" next-focus/>
<input ng-if="!hasModel" ng-value="ngValue" ng-pattern="ngPattern" type="{{$parent.type}}" ng-required="ngRequired" ng-disabled="ngDisabled" placeholder="{{$parent.placeholder}}" class="uiFieldText {{className}}" next-focus/>
<label ng-bind-html="label"></label>
<span></span>