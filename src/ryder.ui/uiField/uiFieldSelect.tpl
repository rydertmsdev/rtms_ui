<ui-select 	class="uiFieldSelect" 
			ng-model="$parent.ngModel" 
		   	theme="selectize" 
		   	ng-attr-tagging="{{tagging && 'true' || undefined }}" 
		   	tagging-label="false"
		   	tagging-tokens="SPACE|," 
		   	ng-disabled="ngDisabled"
		   	on-select="onSelect($item, selectedArgs)">
	<ui-select-match placeholder="{{placeholder}}">
		{{$item||prop?($select.selected[prop] || selectValFunction($select.selected)):$select.selected || selectValFunction($select.selected)}}
	</ui-select-match>
	<ui-select-choices refresh="refresh()($select.search, refreshArgs)"
					   refresh-delay="{{refreshDelay}}" 
					   repeat="o in options||altOptions | filter:$select.search track by $index">
		{{prop?o[prop]:o}}
	</ui-select-choices>
	<ui-select-no-choice ng-class="{'show':(options.length>0||altOptions.length>0)&&$select.search!=''}">
		{{tagging?'"'+$select.search+'"':'No matching options'}}
	</ui-select-no-choice>
</ui-select>
<label ng-bind-html="label"></label>
<span></span>