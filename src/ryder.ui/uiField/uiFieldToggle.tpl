<div class="uiFieldCheckbox" ng-class="className">
	<input ng-model="ngModel" type="checkbox" ng-required="ngRequired" ng-disabled="ngDisabled"/>
	<label ng-bind-html="label"></label>
	<ui-field-checkbox-controls>
		<ui-field-checkbox-title ng-bind-html="title"></ui-field-checkbox-title>
		<ui-field-checkbox-toggle></ui-field-checkbox-toggle>
	</ui-field-checkbox-controls>
</div>