angular.module('ryder.ui').directive('uiWidth', [function(){
	return {
		restrict: 'AE',
		link: function(scope, element, attrs) {
			if (attrs.uiWidth){
				element[0].style.minWidth=attrs.uiWidth;
				element[0].style.maxWidth=attrs.uiWidth;
			}
		}
	};
}]).directive('uiHeight', [function(){
	return {
		restrict: 'AE',
		link: function(scope, element, attrs) {
			if (attrs.uiHeight){
				element[0].style.minHeight=attrs.uiHeight;
				element[0].style.maxHeight=attrs.uiHeight;
			}
		}
	};
}]);