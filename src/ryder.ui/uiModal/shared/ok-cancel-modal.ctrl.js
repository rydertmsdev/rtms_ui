App.controller('okCancelModal',['$scope', '$timeout', 'uiModalSvc', 'orderSvc', function($scope, $timeout, uiModalSvc, orderSvc){	
	$scope.title=$scope.args.title;
	$scope.text=$scope.args.text;
	$scope.html=$scope.args.html;
	$scope.icon=$scope.args.icon||"fa fa-question";
	$scope.closeIcon=$scope.args.close|| 'false';

	$scope.close=function(){
		uiModalSvc.hide();
	}
	$scope.confirm=function(){
		$scope.args.callback()&&$scope.args.callback();
		$scope.close();
	}
}]);