<div class="modal-small" ng-controller="okCancelModal">
	<ui-header theme="ui-modal-header">
		<ui-header-body>
			<div>
				<i class="{{icon}}"></i>
				<span>
					{{title}}
				</span>
			</div>
		</ui-header-body>
		<ui-header-controls>
			<button ui-button ng-click="close()">
				<i class="fa fa-times"></i>
			</button>
		</ui-header-controls>
	</ui-header>
	<div class="modal-body form">
		<div class="text">
			{{text}}
		</div>
		<div ng-bind-html="html">
		</div>
	</div>
	<ui-header theme="ui-modal-footer">
		<ui-header-controls>
			<button ui-button ng-click="close()" class="bg-gray" ng-if="closeIcon == 'true'">
				<span>Cancel</span>
			</button>
			<button ui-button type="submit" ng-click="confirm()" class="bg-accent">
				<span>Ok</span>
			</button>
		</ui-header-controls>
	</ui-header>
</div>