angular.module('ryder.ui').directive('uiModalContent', ['$rootScope', '$templateCache', '$compile', 'uiModalSvc', function ($rootScope, $templateCache, $compile, uiModalSvc) {
	return {        
		restrict:'AE',
		transclude:true,
		link:function(scope, element, attrs) {
			$compile($templateCache.get(uiModalSvc.templateUrl).trim())(scope, function (template, scope) {
				scope.args=uiModalSvc.args;
				element.append(template)
			});
		},      
	}
}]).service('uiModalSvc', ['$rootScope', '$q','$timeout',function ($rootScope, $q, $timeout) {
	var svc=this;
	svc.show=function(templateUrl,args){
		if (svc.showModal==true) svc.showModal=false;
		$timeout(function(){
			svc.showModalBg=true;
			svc.showModal=true;
			svc.templateUrl=templateUrl;
			svc.args=args;
		})
	}
	svc.hide=function(){
		svc.showModalBg=false;
		svc.showModal=false;
	}
}])