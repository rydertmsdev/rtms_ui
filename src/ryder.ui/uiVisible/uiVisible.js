angular.module("ryder.ui").directive('uiVisible', [function(){
	return {
		scope:{
			uiVisible:"="
		},
		link: function(scope, element, attrs) {
			var watcher=scope.$watch('uiVisible',function(newval){
				element[0].style.visibility=newval?'visible':'hidden';
			})
			scope.$on("$destroy",function(){
				watcher();
			})
		}
	};
}]);