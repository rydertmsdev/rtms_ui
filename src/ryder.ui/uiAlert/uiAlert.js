angular.module('ryder.ui').directive('uiAlertContainer', ['uiAlertSvc',function (uiAlertSvc) {
	return {        
		restrict:'AE',
		scope:{
			'uiAlert':'=',
			'uiAlertStyle':'='
		},
		templateUrl:'uiAlert.tpl',
		link:function(scope, element, attrs) {
			scope.alerts=uiAlertSvc.alerts;
			scope.dismiss=function($index){
				uiAlertSvc.dismiss($index);
			}
		}
	}
}]);