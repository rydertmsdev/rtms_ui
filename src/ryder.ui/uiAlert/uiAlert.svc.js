App.service('uiAlertSvc', ['$rootScope','$q','$timeout',function ($rootScope, $q, $timeout) {
	var svc=this;
	svc.alerts=[];
	svc.show=function(alert){
		svc.alerts.push(alert);
	}
	svc.dismiss=function(index){
		svc.alerts.splice(index,1);
	}
	svc.clear=function(){
		svc.alerts.length=0;
	}
}]);