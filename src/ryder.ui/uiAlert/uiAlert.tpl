<div class="ui-alert" ng-repeat="alert in alerts">
	<div class="ui-alert-content {{alert.style?alert.style:'bg-accent'}}">
		<div class="ui-alert-body">
			<i class="{{alert.icon}}" ng-if="alert.icon"></i>
			<div class="text">
				<label ng-if="alert.label" ng-bind-html="alert.label"></label>
				<div ng-if="alert.text" ng-bind-html="alert.text">Alert description</div>
			</div>
		</div>
		<div class="ui-alert-controls">
			<div ui-button ng-click="dismiss($index)" ng-if="!alert.static" ui-info="Dismiss">
				<i class="fa fa-times"></i>
			</div>
		</div>
	</div>
</div>