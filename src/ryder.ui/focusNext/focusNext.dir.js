/** Usage:
  <input next-focus tabindex="0">
  <input next-focus tabindex="1">
  <input tabindex="2">
  Upon pressing ENTER key the directive will switch focus to
  the next tabindex.
  The last field should not have next-focus directive to avoid
  focusing on non-existing element.
  Works for Web, iOS (Go button) & Android (Next button) browsers, 
**/ 
 App.directive('nextFocus', [function() {
  return {
    restrict: 'A',
    link: function(scope, elem, attrs) {
      elem.bind('keydown', function(e) {
        var code = e.keyCode || e.which;
        if (code === 13) {
          e.preventDefault();
          // angular.element(elem)[0].focus();
          // elem.parent().parent().next().find('input').focus();
          var a = document.querySelectorAll("ui-field, ui-field-date");
          var b = elem.parent()[0];

          var tempIndex;

          angular.forEach(a, function(val, key) {
            if(angular.element(b)[0] == val){
              tempIndex = key
            }
          });

          if(tempIndex){
            var tempFlag = false;
            for(i = tempIndex; i<= a.length; i++){
              if(angular.element(a[tempIndex+1]).find('input')[0].hasAttribute('disabled')){
                  tempIndex = tempIndex + 1;
                  //scope.getTempIndex(a, tempIndex);
              }
              else{
                tempFlag = true;
               
              }

              if(tempFlag){
                break;
              }
            }
            //var returnVal = scope.getTempIndex(a, tempIndex);
            // if(angular.element(a[tempIndex+1]).find('input')[0]).hasAttribute('disabled'){
            //   tempIndex = tempIndex + 1;
            // }
            newIndex = tempIndex+1;

            // if(a[newIndex].attributes['type'].value == 'select' || a[newIndex].attributes['type'].value == 'time'){
            //   angular.element(a[newIndex]).find(".selectize-input").click()
            // }
            // else{
            //   angular.element(a[newIndex]).find('input')[0].focus();
            // }
            angular.element(a[newIndex]).find('input')[0].focus();
          }
        }
      });

      scope.getTempIndex=function(a, tempIndex){
        var returnVal = tempIndex;
        var tempFlag = false;
        if(angular.element(a[tempIndex+1]).find('input')[0].hasAttribute('disabled')){
            tempIndex = tempIndex + 1;
            scope.getTempIndex(a, tempIndex);
        }
        else{
          tempFlag = true;
         
        }

        if(tempFlag){
          returnVal = tempIndex + 1;
          return returnVal;
        }      
      }
    }
  };
}]);