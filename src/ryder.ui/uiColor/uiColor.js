angular.element(document.body).append(angular.element("<div ui-color-container></div>"));

angular.module('ryder.ui').directive('uiColorContainer', ['$rootScope','$timeout', '$window' ,function ($rootScope, $timeout, $window) {
	return {        
		restrict:'A',
		template:'\
		<div ui-color-picker ng-show="showPicker">\
		<div class="ui-color-picker-options">\
		<div ng-repeat="color in colors" class="ui-color-picker-option">\
		<span ng-style="{\'background-color\':color}" ng-show="showPicker" ng-click="setValue(color)"></span>\
		</div>\
		</div>\
		<div class="field-color" ng-style="{\'background-color\':ngModel.$modelValue}"></div>\
		</div>\
		',
		link:function(scope, element, attrs) {
			var ngModel,sourceElement;
			var picker=angular.element(element[0].querySelector('[ui-color-picker]'));
			var options=angular.element(element[0].querySelector('.ui-color-picker-options'));
			scope.colors=['#ff6259','#3394ff','#6fe083','#ffaa33','#33ccff','#ffcc00','#ff5777','#7977de','#999'];
			var configure=function(){
				var angle=360/scope.colors.length;
				angular.forEach(options.children(),function(color,index){
					color.style.transform="rotateZ("+angle*index+"deg)"
				});
			}

			scope.setValue=function(color){
				scope.ngModel&&scope.ngModel.$setViewValue(color);
				sourceElement.style.backgroundColor=color;
			}

			var show=function($event,args){
				if (scope.showPicker) return;
				scope.$apply(function(){
					configure();
					sourceElement=args.element;
					scope.ngModel=args.ngModel;
					scope.showPicker=true;
					$timeout(function(){
						var left=args.left-picker[0].clientWidth/2;
						var top=args.top-picker[0].clientHeight/2;
						if (left<0) left=2;
						if (left+picker[0].clientWidth>$window.innerWidth) {
							left=$window.innerWidth-picker[0].clientWidth-2;
						}
						if (top<0) top=2;
						if (top+picker[0].clientWidth>$window.innerHeight) {
							top=$window.innerHeight-picker[0].clientHeight-2;
						}
						picker[0].style.left=left+'px';
						picker[0].style.top=top+'px';
						angular.element($window).one('click',hide);
					},1)
				})
			};
			var hide=function($event){
				scope.$apply(function(){
					scope.showPicker=false;	
				})
				angular.element($window).off('click',hide);
			}
			$rootScope.$on('uiColor.show',show)
		}
	}
}]);

angular.module('ryder.ui').directive('uiColor', ['$rootScope','$timeout', function ($rootScope,$timeout) {
	return {        
		restrict:'A',
		require: '?^ngModel',
		link:function(scope, element, attrs, ngModel) {
			$timeout(function(){
				if (ngModel){
					element[0].style.backgroundColor=ngModel.$modelValue;
				}
			})
			var onClick=function($event){
				var top=element[0].getBoundingClientRect().top+element[0].getBoundingClientRect().height/2;
				var left=element[0].getBoundingClientRect().left+element[0].getBoundingClientRect().width/2;
				$rootScope.$broadcast('uiColor.show',{top:top,left:left,ngModel:ngModel,element:element[0]});
			}
			element.on('click',onClick)
			scope.$on("$destroy",function(){
				element.off();
			})
		}
	}
}]);