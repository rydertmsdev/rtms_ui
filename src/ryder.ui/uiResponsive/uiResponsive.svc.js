angular.module('ryder.ui').service('uiResponsiveSvc', ['$rootScope', '$window','$timeout',function ($rootScope, $window, $timeout) {
	var svc=this;
	var win=angular.element($window);

/*	
@mobile: ~"screen and (max-width:667px)";
@tablet: ~"screen and (max-width: 1023px)";
@desktop: ~"screen and (min-width: 1024px)";
@extended: ~"screen and (min-width: 1300px)";
@max: ~"screen and (min-width: 1920px)";
@print: ~"print";
*/

	var onResize=function(){
		$rootScope.$evalAsync(function(){
			var width=win[0].innerWidth;
			svc.isMobile=svc.isTablet=svc.isDesktop=svc.isExtended=svc.isMax=false;
			if (width<=667){
				svc.isMobile=true;
			}
			if (width>667 && width<1024){
				svc.isTablet=true;
			}
			if (width>=1024){
				svc.isDesktop=true;
			}
			if (width>=1300){
				svc.isExtended=true;
			}
			if (width>=1920){
				svc.isMax=true;
			}
		})
	}

	onResize();
	win.on('resize',onResize);
}])