angular.module('ryder.ui').directive('uiListActionsBtn', ['$window','$timeout',function($window,$timeout){
	return {
		restrict: 'AE',
		link: function(scope, element, attrs) {
			var open=false;
			var ul=angular.element(element.closest('ui-list'));
			var li=angular.element(element.closest('li'));
			if (li.length<1) {
				throw('uiHeaderActionsBtn - no closest li')
				return;
			}
			var content=li.find('ui-list-content');
			var showMenu=function(){
				if (!open){
					content.off('transitionEnd webkitTransitionEnd')
					open=true;
					$timeout(function(){
						li.addClass('show-actions show-actions-prepare');
						ul.addClass('show-actions show-actions-prepare');
						content[0].style.transform = "translateX("+(-li.find('ui-list-actions')[0].clientWidth)+"px)";
						angular.element(window).one('click',hideMenu);
					})
				}else{
					hideMenu();
				}
			}
			var hideMenu=function(){
				open=false;
				li.removeClass('show-actions-prepare');
				ul.removeClass('show-actions-prepare');
				content.one('transitionEnd webkitTransitionEnd',function(){
					li.removeClass('show-actions');
					ul.removeClass('show-actions');
				})
				content[0].style.transform="";
			}
			element.on('click',showMenu);
			scope.$on('$destroy',function(){
				angular.element(window).off('click',hideMenu);
				content.off();
				element.off();
			})
		}
	};
}]).filter('listPlaceholder',function() {
  return function(val, param) {
  	if (param===false) return val;
  	if (val&&Object.keys(angular.copy(val[val.length-1])).length>0){
  		val.push({})
  	}
    return val;
  }
});