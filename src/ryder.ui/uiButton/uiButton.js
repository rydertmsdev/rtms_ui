angular.module('ryder.ui').directive('uiToggle', [function () {
	var toggleState=function(element,value){
		if (!!value){
			element.addClass('active');
		}
		else{
			element.removeClass('active');	
		}
	}
	return {        
		restrict:'A',
		scope:{
			'uiToggle':'='
		},
		link:function(scope, element, attrs) {
			var watch=scope.$watch(function(){
				return scope.uiToggle;
			},function(value){
				toggleState(element,value)
			})
			element.on('click',function(event){
				toggleState(element,scope.uiToggle);
				scope.$apply(function(){
					scope.uiToggle=!scope.uiToggle;
				})
			})
			scope.$on('$destroy',function(){
				watch();
				element.off();
			});
		}
	}
}]);