App.controller('settingsAdministrationGeneralCtrl',['$scope', function($scope){
	$scope.menuCategories=[
	{
		title:"Views",
		items:[
		{
			title:"Rol Views Editor",
		},
		{
			title:"EVR",
			state:"home.settings.administration.general.evr"
		},
		{
			title:"View Layout Editor",
		},
		{
			title:"Workflow Editor",
		}
		]
	},
	{
		title:"Customer",
		items:[

		{
			title:"Add New Customer",
		},
		{
			title:"Customer Definition",
		},
		{
			title:"Customer Lookup Codes",
		},

		{
			title:"Customer Service Codes",
		},
		]
	},

	{
		title:"Rydertrac",
		items:[
		{
			title:"Rydertrac Config",
		},
		{
			title:"Rydertrac Ref Numbers",
		},

		{
			title:"Rydertrac Ext Attributes",
		},
		]
	},
	{
		title:"Events",
		items:[
		{
			title:"Event Codes",
		},
		{
			title:"Reason Codes",
		},
		{
			title:"Event/Reason Code Security",
		},
		{
			title:"Event/Reason Code Mapping",
		},

		]
	},
	{
		title:"Carrier",
		items:[
		{
			title:"Carriers",
		},
		{
			title:"Carrier Profile",
		},
		]
	}
	]

}]);

