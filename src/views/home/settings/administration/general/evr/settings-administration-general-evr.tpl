<div class="overflow-y" ui-section-container>
	
	<ui-section>
		<div class="form cols">
			<ui-field type="toggle" class="no-label" title="Client" ng-model="evr.client.on" ng-required="false" ng-disabled="false"></ui-field>
		</div>

		<div class="form rows subform" ng-class="{'min':!evr.client.on}" ui-max-height>
			<ui-field type="toggle" class="no-label checkbox left" ng-model="evr.client.ryder" title="Ryder" ng-required="false" ng-disabled="false"></ui-field>
		</div>

		<div class="form rows">
			<ui-field type="toggle" class="no-label" ng-model="evr.division.on" title="Division" ng-required="false" ng-disabled="false"></ui-field>
		</div>

		<div class="form rows subform" ng-class="{'min':!evr.division.on}" ui-max-height>
			<ui-field type="toggle" class="no-label checkbox left" ng-model="evr.division.ryd1" title="RYD1" ng-required="false" ng-disabled="false"></ui-field>
			<ui-field type="toggle" class="no-label checkbox left" ng-model="evr.division.ryd2" title="RYD2" ng-required="false" ng-disabled="false"></ui-field>
			<ui-field type="toggle" class="no-label checkbox left" ng-model="evr.division.ryd3" title="RYD3" ng-required="false" ng-disabled="false"></ui-field>
		</div>

		<div class="form rows" >
			<ui-field type="toggle" class="no-label" ng-model="evr.logGroup.on" ng-required="false" title="Logistics Group" ng-disabled="false"></ui-field>
		</div>
		<div class="form rows subform" ng-class="{'min':!evr.logGroup.on}" ui-max-height>
			<ui-field type="toggle" class="no-label checkbox left" ng-model="evr.logGroup.ryd1" title="RYD1" ng-required="false" ng-disabled="false"></ui-field>
			<ui-field type="toggle" class="no-label checkbox left" ng-model="evr.logGroup.ryd2" title="RYD2" ng-required="false" ng-disabled="false"></ui-field>
			<ui-field type="toggle" class="no-label checkbox left" ng-model="evr.logGroup.ryd3" title="RYD3" ng-required="false" ng-disabled="false"></ui-field>
		</div>

		<div class="form rows">
			<ui-field type="toggle" class="no-label" ng-model="evr.scac.on" title="SCAC" ng-required="false" ng-disabled="false"></ui-field>
		</div>
		<div class="form rows subform" ng-class="{'min':!evr.scac.on}" ui-max-height>
			<ui-field type="toggle" class="no-label checkbox left" ng-model="evr.scac.rydd" title="RYDD" ng-required="false" ng-disabled="false"></ui-field>
			<ui-field type="toggle" class="no-label checkbox left" ng-model="evr.scac.ryd1" title="RYD2" ng-required="false" ng-disabled="false"></ui-field>
		</div>

		<div class="form rows">
			<ui-field type="toggle" class="no-label" ng-model="evr.equip.on" title="Equipment Type" ng-required="false" ng-disabled="false"></ui-field>
		</div>
		<div class="form rows subform" ng-class="{'min':!evr.equip.on}" ui-max-height>
			<ui-field type="toggle" class="no-label checkbox left" ng-model="evr.equip.s1" title="Equipment 1" ng-required="false" ng-disabled="false"></ui-field>
			<ui-field type="toggle" class="no-label checkbox left" ng-model="evr.equip.s2" title="Equipment 2" ng-required="false" ng-disabled="false"></ui-field>
		</div>

		<div class="form rows">
			<ui-field type="toggle" class="no-label" ng-model="evr.service.on" title="Service" ng-required="false" ng-disabled="false"></ui-field>
		</div>
		<div class="form rows subform" ng-class="{'min':!evr.service.on}" ui-max-height>
			<ui-field type="toggle" class="no-label checkbox left" ng-model="evr.service.s1" title="Service 1" ng-required="false" ng-disabled="false"></ui-field>
			<ui-field type="toggle" class="no-label checkbox left" ng-model="evr.service.s2" title="Service 2" ng-required="false" ng-disabled="false"></ui-field>
		</div>

		<div class="form rows">
			<ui-field type="toggle" class="no-label" ng-model="evr.cost.on" title="Cost Tolerance" ng-required="false" ng-disabled="false"></ui-field>
		</div>
		<div class="form rows subform" ng-class="{'min':!evr.cost.on}" ui-max-height>
			<ui-field type="number" ng-model="evr.cost.tolerance" label="Cost Tolerance" ng-required="false" ng-disabled="false"></ui-field>
		</div>

	</ui-section>

	<ui-section>
		<div class="form rows">
			<ui-field type="toggle" ng-model="evr.fuelProgram.on" class="no-label" title="Fuel Program" ng-disabled="false"></ui-field>
		</div>
		<div class="form rows subform" ng-class="{'min':!evr.fuelProgram.on}" ui-max-height>
			<ui-field type="toggle" class="no-label checkbox left" ng-model="evr.fuelProgram.fs" title="FS" ng-required="false" ng-disabled="false"></ui-field>
			<ui-field type="toggle" class="no-label checkbox left" ng-model="evr.fuelProgram.fc" title="FC" ng-required="false" ng-disabled="false"></ui-field>
			<ui-field type="toggle" class="no-label checkbox left" ng-model="evr.fuelProgram.fm" title="FM" ng-required="false" ng-disabled="false"></ui-field>

		</div>
		<div class="form rows">
			<ui-field type="toggle" ng-model="evr.fuelProgram.no" class="no-label" title="No Fuel Program" ng-disabled="false"></ui-field>
		</div>
	</ui-section>


	<ui-section>
		<div class="form">
			<ui-field type="toggle" ng-model="evr.dateRange.on" class="no-label" title="Date Range" ng-disabled="false"></ui-field>
		</div>
		<div class="form cols subform" ng-class="{'min':!evr.dateRange.on}" ui-max-height>
			<ui-field type="date" ng-model="evr.startDate" label="Start Date" ng-disabled="false"></ui-field>
			<ui-field type="date" ng-model="evr.endDate" label="End Date" ng-disabled="false"></ui-field>
		</div>
	</ui-section>





</div>