<ui-sidebar>
	<ui-sidebar-panel>
		<div class="rows form">
			<ui-menu-content>
				<ui-menu-group collapsed ng-repeat="menu in menuCategories" ui-max-height-cloak>
					<ui-header theme="ui-menu-header" ui-button ui-menu-collapse>
						<ui-header-body>
							<span ng-bind="menu.title"></span>
						</ui-header-body>
						<ui-header-controls>
							<div ui-menu-collapse-indicator></div>
						</ui-header-controls>
					</ui-header>
					<ui-menu-content ui-max-height>
						<ui-menu-item ng-repeat="item in menu.items" ui-sref="{{item.state?item.state:'blank'}}" ui-sref-active="active">
							<span ng-bind="item.title"></span>
						</ui-menu-item>
					</ui-menu-content>
				</ui-menu-group>
			</ui-menu-content>
		</div>
	</ui-sidebar-panel>
	<ui-sidebar-content>
		<ui-view></ui-view>
	</ui-sidebar-content>
</ui-sidebar>