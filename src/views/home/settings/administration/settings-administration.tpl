<ui-sidebar-nav>
	<nav class="sidebar-nav">
		<ui-sidebar-btn></ui-sidebar-btn>
		<a ui-sref="home.settings" ui-info="Administration" ui-sref-active="active">
			<i class="fa fa-gear"></i>
		</a>
		<a xui-sref="home.settings.users" ui-info="USers" xui-sref-active="active">
			<i class="fa fa-user"></i>
		</a>
	</nav>
	<ui-sidebar-nav-content>
		<ui-view></ui-view>
	</ui-sidebar-nav-content>
</ui-sidebar-nav>