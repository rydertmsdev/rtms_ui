<ui-sidebar-nav>
	<nav class="sidebar-nav">
		<ui-sidebar-btn></ui-sidebar-btn>
		<a ui-sref="home.settings.event-matching.rules" ui-info="Event Matching" ui-sref-active="active">
			<i class="ic ic-split"></i>
		</a>
		<a ui-sref="home.settings.event-matching.errors" ui-info="Error Handling" ui-sref-active="active">
			<i class="fa fa-exclamation-circle"></i>
		</a>
	</nav>
	<ui-sidebar-nav-content>
		<ui-view></ui-view>
	</ui-sidebar-nav-content>
</ui-sidebar-nav>
