<div class="form modal-full" ng-controller="settingsEventMatchingRuleDetailsModalCtrl">
	<ui-header theme="ui-modal-header">
		<ui-header-body>
			<div>
				<span>
					Rule 1 Details
				</span>
			</div>
		</ui-header-body>
		<ui-header-controls>
			<div ui-button title="Clear" ng-click="close()">
				<i class="fa fa-times"></i>
			</div>
		</ui-header-controls>
	</ui-header>
	<div class="modal-body overflow-y">
		<ui-section-container>
		<ui-section class="fade-in-out" ng-repeat="match in rule.matches">
			<ui-header theme="section">
				<ui-header-body>
					<div>
						Match {{($index+1)}} Detail
					</div>
				</ui-header-body>
				<ui-header-controls>
					<div ui-button ui-info="Delete Match">
						<i class="fa fa-trash"></i>
					</div>
				</ui-header-controls>
			</ui-header>
			<div class="form cols">
				<ui-field type="text" ng-model="matchLevel" label="Match Level" ng-required="true" ng-disabled="false"></ui-field>
			</div>
			<div ui-table="tableConfig" ui-table-columns="tableColumns" ui-table-data="match.matchFields">

			</div>
			<ui-header theme="ui-list-item-header">
				<ui-header-body>
				</ui-header-body>
				<ui-header-controls>
					<span class="subtitle">Priority: 99</span>
				</ui-header-controls>
				<ui-header-controls>
					<div ui-button ng-click="" ui-info="Move Down"><i class="fa fa-chevron-down"></i></div>
					<div ui-button ng-click="" ui-info="Move Up"><i class="fa fa-chevron-up"></i></div>
				</ui-header-controls>
			</ui-header>
		</ui-section>
		</ui-section-container>
	</div>
	<ui-header theme="ui-modal-footer">
		<ui-header-body>
			<div ui-button ng-click="newHandlingUnit()" class="bg-white">
				<span>New Match Detail</span>
			</div>
		</ui-header-body>
		<ui-header-controls>
			<div ui-button ng-click="close()" class="bg-gray">
				<span>Cancel</span>
			</div>
			<div ui-button type="submit" ng-click="moveItem()" class="bg-accent">
				<span>Save</span>
			</div>
		</ui-header-controls>
	</ui-header>
</div>