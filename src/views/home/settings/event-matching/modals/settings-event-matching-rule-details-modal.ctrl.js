App.controller('settingsEventMatchingRuleDetailsModalCtrl',['$scope', '$timeout', 'uiModalSvc', function($scope, $timeout, uiModalSvc){	
	//$scope.item=angular.copy(orderSvc.deliveryOrders[$scope.args.dIndex].handlingUnits[$scope.args.hIndex].items[$scope.args.index]);

	$scope.rule=angular.copy($scope.args.rule);



	$scope.tableConfig={
		selectable:true,
		rowClick:function(row){
			row.$$selected=!row.$$selected;
		}
	}

	$scope.tableColumns=[
	{
		title:"Event Field",
		key:"eventField",
		type:"field",
		field:{
			type:"text",
			required:false,
			disabled:false,
			className:"no-label",
		}
	},
	{
		title:"Event Qualifier",
		key:"eventQualifier",
		type:"field",
		field:{
			type:"text",
			required:false,
			disabled:false,
			className:"no-label",
		}
	},
	{
		title:"Match to Field",
		key:"matchToField",
		type:"field",
		field:{
			type:"text",
			required:false,
			disabled:false,
			className:"no-label",
		}
	},
	{
		title:"Match to Qualifier",
		key:"matchToQualifier",
		type:"field",
		field:{
			type:"text",
			required:false,
			disabled:false,
			className:"no-label",
		}
	},
	{
		title:"Remove Characters",
		key:"removeChars",
		type:"field",
		field:{
			type:"text",
			required:false,
			disabled:false,
			className:"no-label",
		}
	},
	];


	$scope.close=function(){
		uiModalSvc.hide();
	}

	$scope.save=function(){
		uiModalSvc.hide();
	}

}]);
