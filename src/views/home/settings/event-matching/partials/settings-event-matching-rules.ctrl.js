App.controller('settingsEventMatchingRules',['$scope', '$timeout', 'uiLoaderSvc', 'uiModalSvc', function($scope, $timeout, uiLoaderSvc, uiModalSvc){
	var filters={
		customerId:"XOM"
	}

	$scope.tableConfig={
		selectable:false,
		rowClick:function(row){
			row.$$selected=!row.$$selected;
		}
	}

	$scope.showRuleDetails=function($index){
		$scope.ruleDetailIndex=$scope.ruleDetailIndex==$index?-1:$index;
/*		uiModalSvc.show('settings-event-matching-rule-details-modal.tpl',{
			rule:rule,
			callback:function(){

			}
		});	*/
	}

/*$scope.tableActions=[
{
title:"Delete",
className:"bg-red",
iconClass:"fa fa-trash",
action:function(row){
console.log(row);
}
},
{
title:"Add",
className:"bg-green",
iconClass:"fa fa-plus",
action:function(row){
console.log(row);
}
},
{
title:"Details",
className:"text-accent",
iconClass:"fa fa-info",
action:function(row){
console.log(row);
}
},
]*/
/*	priority:"1",
	client:"XOM",
	scac:"RDWY",
	eventCode:"AB",
	mode:"LTL",
	division:"XOM1",
	logisticsGroup:"XOM5",
	applyToLevels:["S","ST","L"],
	*/
$scope.tableColumns=[
{
	title:"Customer ID",
	key:"client",
	type:"text",
	className:"text-center"
},
{
	title:"Event Code",
	key:"eventCode",
	type:"field",
	field:{
		type:"text",
		required:false,
		disabled:false,
		className:"no-label",
	}
},
{
	title:"Carrier SCAC",
	key:"scac",
	type:"field",
	field:{
		type:"text",
		required:false,
		disabled:false,
		className:"no-label",
	}
},
{
	title:"Mode",
	key:"mode",
	type:"field",
	field:{
		type:"text",
		required:false,
		disabled:false,
		className:"no-label",
	}
},
{
	title:"Division",
	key:"division",
	type:"field",
	field:{
		type:"text",
		required:false,
		disabled:false,
		className:"no-label",
	}
},
{
	title:"Logistics Group",
	key:"logisticsGroup",
	type:"field",
	field:{
		type:"text",
		required:false,
		disabled:false,
		className:"no-label",
	}
},
{
	title:"Apply to Levels",
	key:"applyToLevels",

	type:"field",
	field:{
		type:"text",
		required:false,
		disabled:false,
		className:"no-label",
	}
},
];

$scope.detailTableColumns=[
	{
		title:"Event Field",
		key:"eventField",
		type:"field",
		field:{
			type:"text",
			required:false,
			disabled:false,
			className:"no-label",
		}
	},
	{
		title:"Event Qualifier",
		key:"eventQualifier",
		type:"field",
		field:{
			type:"text",
			required:false,
			disabled:false,
			className:"no-label",
		}
	},
	{
		title:"Match to Field",
		key:"matchToField",
		type:"field",
		field:{
			type:"text",
			required:false,
			disabled:false,
			className:"no-label",
		}
	},
	{
		title:"Match to Qualifier",
		key:"matchToQualifier",
		type:"field",
		field:{
			type:"text",
			required:false,
			disabled:false,
			className:"no-label",
		}
	},
	{
		title:"Remove Characters",
		key:"removeChars",
		type:"field",
		field:{
			type:"text",
			required:false,
			disabled:false,
			className:"no-label",
		}
	},
	];

$scope.rules=[];

///////////////////////////////////////////////////
// TEST DATA
//

var testRule={
	priority:"1",
	client:"XOM",
	scac:"RDWY",
	eventCode:"AB",
	mode:"LTL",
	division:"XOM1",
	logisticsGroup:"XOM5",
	applyToLevels:["S","ST","L"],
	matches:[
	{
		matchLevel:"ST",
		priority:1,
		matchFields:[
		{
			enabled:true,
			eventField:"CUSTOMERNUM",
			eventQualifier:"",
			matchToField:"CUSTOMERNUM",
			matchToQualifier:"",
			removeChars:"-,.!@#$%"
		},
		{
			enabled:true,
			eventField:"EVENTCITY",
			eventQualifier:"",
			matchToField:"STOPCITY",
			matchToQualifier:"",
			removeChars:""
		}
		]
	},
	{
		matchLevel:"ST",
		priority:2,
		matchFields:[
		{
			enabled:true,
			eventField:"CUSTOMERNUM",
			eventQualifier:"",
			matchToField:"CUSTOMERNUM",
			matchToQualifier:"",
			removeChars:"-,.!@#$%"
		},
		{
			enabled:true,
			eventField:"EVENTCITY",
			eventQualifier:"",
			matchToField:"STOPCITY",
			matchToQualifier:"",
			removeChars:""
		}
		]
	}
	]
};

for (var i=0;i<10;i++){
	var rule=angular.copy(testRule)
	$scope.rules.push(rule)
}
}]);
