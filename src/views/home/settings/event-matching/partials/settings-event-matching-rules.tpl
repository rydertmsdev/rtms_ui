<ui-section ng-repeat="rule in rules">
	<ui-header theme="section">
		<ui-header-body>
			<span>
				Rule {{($index+1)}}
			</span>
		</ui-header-body>
		<ui-header-controls>		
			<span class="subtitle">Rule Priority: 99</span>
		</ui-header-controls>
		<ui-header-controls>
			<div ui-button ng-click="" ui-info="Move Rule Down"><i class="fa fa-arrow-down"></i></div>
			<div ui-button ng-click="" ui-info="Move Rule Up"><i class="fa fa-arrow-up"></i></div>
		</ui-header-controls>
	</ui-header>
	<div class="form cols">
		<ui-field type="text" ng-model="rule.client" label="Customer ID" ng-required="false" ng-disabled="false"></ui-field>
		<ui-field type="text" ng-model="rule.eventCode" label="Event Code" ng-required="false" ng-disabled="false"></ui-field>
		<ui-field type="text" ng-model="rule.scac" label="Carrier SCAC" ng-disabled="false"></ui-field>
		<ui-field type="text" ng-model="rule.mode" label="Mode" ng-disabled="false"></ui-field>
	</div>	
	<div class="form cols">
		<div class="form cols">
			<ui-field type="text" ng-model="rule.division" label="Division" ng-required="false" ng-disabled="false"></ui-field>
			<ui-field type="text" ng-model="rule.logisticsGroup" label="Logistics Group" ng-required="false" ng-disabled="false"></ui-field>
		</div>
		<ui-field type="text" ng-model="rule.applyToLevels" label="Apply to Levels" ng-disabled="false"></ui-field>
	</div>	
	<div class="subform" ui-max-height ng-class="{'min':$index!=ruleDetailIndex}">
		<ui-section>
			<ui-header theme="section">
				<ui-header-body>
					<div>
						References
					</div>
				</ui-header-body>
				<ui-header-controls>
				</ui-header-controls>
			</ui-header>
			<div class="form cols">
				<div class="form cols">
					<ui-field type="text" ng-model="rule.pro" label="PRO" ng-required="false" ng-disabled="false"></ui-field>
					<ui-field type="text" ng-model="rule.bol" label="BOL" ng-required="false" ng-disabled="false"></ui-field>
				</div>
				<ui-field type="text" ng-model="rule.po" label="PO" ng-disabled="false"></ui-field>
			</div>
		</ui-section>
		<ui-section ng-repeat="(matchIndex,match) in rule.matches">
			<ui-header theme="section">
				<ui-header-body>
					<div>
						Match {{(matchIndex+1)}} Detail
					</div>
				</ui-header-body>
				<ui-header-controls>		
					<span class="subtitle">Match Priority: 99</span>
				</ui-header-controls>
				<ui-header-controls>
					<div ui-button ng-click="" ui-info="Move Match Down"><i class="fa fa-arrow-down"></i></div>
					<div ui-button ng-click="" ui-info="Move Match Up"><i class="fa fa-arrow-up"></i></div>
				</ui-header-controls>
				<ui-header-controls>
					<div ui-button ui-info="Delete Match">
						<i class="fa fa-trash"></i>
					</div>
					<div ui-button ui-info="New Match">
						<i class="fa fa-plus"></i>
					</div>
				</ui-header-controls>
			</ui-header>
			<div class="form cols">
				<ui-field type="text" ng-model="matchLevel" label="Match Level" ng-required="true" ng-disabled="false"></ui-field>
			</div>
			<div ui-table="tableConfig" ui-table-columns="detailTableColumns" ui-table-data="match.matchFields">
			</div>
		</ui-section>
	</div>
	<ui-header theme="ui-list-item-header">
		<ui-header-body>
		</ui-header-body>
		<ui-header-controls>
			<div ui-button ui-info="Rule Details" ng-click="showRuleDetails($index,$event)">
				<i class="fa fa-info-circle"></i>
				<span>Details</span>
			</div>			
		</ui-header-controls>
	</ui-header>
</ui-section>
<ui-page-actions-spacer></ui-page-actions-spacer>
<ui-page-actions-container>
	<ui-page-actions>
		<div class="group">
			<div ui-button ui-page-action class="bg-eBlue" ui-info="New Rule" ng-click="">
				<i class="fa fa-plus"></i>
			</div>
		</div>
	</ui-page-actions>
</ui-page-actions-container>








