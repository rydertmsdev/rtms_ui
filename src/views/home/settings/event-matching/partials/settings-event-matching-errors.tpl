<ui-sidebar>
	<ui-sidebar-panel><div class="content form">
		<ui-header theme="ui-sidebar-header">
			<ui-header-body>
				<div>
					<span>
						Search
					</span>
				</div>
			</ui-header-body>
			<ui-header-controls>
				<div ui-button title="Clear" ng-click="clearFilters()">
					<i class="fa fa-times text-red"></i>
				</div>
				<div ui-button title="Apply" ng-click="doSearch()">
					<i class="fa fa-check text-green"></i>
				</div>
			</ui-header-controls>
		</ui-header>
		<div class="overflow-y">
			<ui-menu-group static>
			<ui-header theme="ui-menu-header" ui-button>
					<ui-header-body>
						<span>
							Error Search
						</span>
					</ui-header-body>
<!-- 				<ui-header-controls>
					<div ui-menu-collapse-indicator></div>
				</ui-header-controls> -->
			</ui-header>
			<ui-menu-content>
				<ui-field type="text" ng-value="filters.customerId" label="Customer ID" placeholder="---" ng-required="false" ng-disabled="false"></ui-field>
				<ui-field type="date" ng-model="filters.dateFrom" label="Create Date From" placeholder="---" ng-required="false" ng-disabled="false"></ui-field>	
				<ui-field type="date" ng-model="filters.dateTo" label="Create Date To" placeholder="---" ng-required="false" ng-disabled="false"></ui-field>	
				<ui-field type="text" ng-model="filters.stopSequence" label="Stop Sequence" placeholder="---" ng-required="false" ng-disabled="false"></ui-field>
				<ui-field type="text" ng-model="filters.eventCode" label="Event Code" placeholder="---" ng-required="false" ng-disabled="false"></ui-field>
				<ui-field type="text" ng-model="filters.carrierScac" label="Carrier SCAC" placeholder="---" ng-required="false" ng-disabled="false"></ui-field>
				<ui-field type="text" ng-model="filters.stopSequence" label="Source" placeholder="---" ng-required="false" ng-disabled="false"></ui-field>
			</ui-menu-content>
		</ui-menu-group>
	</div>
</div>
</ui-sidebar-panel>
<ui-sidebar-content>
	<ui-filters>
		<ui-filter>
			<div>
				<label>Customer ID</label>
				<span>XOM</span>
			</div>
			<div ui-button ui-filter-remove-btn>
				<i class="fa fa-times"></i>
			</div>
		</ui-filter>
	</ui-filters>
	<ui-section>
		<div ui-table="tableConfig" ui-table-columns="tableColumns" ui-table-data="tableData" ui-table-actions="tableActions">
		</div>
	</ui-section>
	<ui-page-actions-spacer></ui-page-actions-spacer>
	<ui-page-actions-container>
		<ui-page-actions>
			<div class="group">
				<div ui-button ui-page-action class="bg-gray" ui-info="Upload" ng-click="">
					<i class="fa fa-upload"></i>
				</div>
			</div>
			<div class="group">
				<div ui-button ui-page-action class="bg-accent" ui-info="Download" ng-click="">
					<i class="fa fa-download"></i>
				</div>
			</div>
			<div class="group">
				<div ui-button ui-page-action class="text-accent bg-white" ui-info="Submit">
					<span>Submit</span>
					<i class="fa fa-chevron-right"></i>
				</div>
			</div>
		</ui-page-actions>
	</ui-page-actions-container>


</ui-sidebar-content>
</ui-sidebar>






