App.controller('settingsEventMatchingErrorsCtrl',['$scope', '$timeout', 'uiLoaderSvc', function($scope, $timeout, uiLoaderSvc){
	var filters={
		customerId:"XOM"
	}

	$scope.tableConfig={
		selectable:true,
		rowClick:function(row){
			row.$$selected=!row.$$selected;
		}
	}

	/*$scope.tableActions=[
		{
			title:"Delete",
			className:"bg-red",
			iconClass:"fa fa-trash",
			action:function(row){
				console.log(row);
			}
		},
		{
			title:"Add",
			className:"bg-green",
			iconClass:"fa fa-plus",
			action:function(row){
				console.log(row);
			}
		},
		{
			title:"Details",
			className:"text-accent",
			iconClass:"fa fa-info",
			action:function(row){
				console.log(row);
			}
		},
	]*/

	$scope.tableColumns=[
	{
		title:"Error ID",
		key:"errorId",
		type:"text",
		className:"text-center"
	/*	type:"field",
		field:{
			type:"text",
			required:false,
			disabled:true,
			className:"no-label",
		}*/
	},
	{
		title:"SCAC",
		key:"scac",
		type:"text",
		className:"text-center"
	},
	{
		title:"Error",
		type:"rows",
		rows:[
			{
				title:"Error Message",
				key:"errorMessage",
				className:""
			},
			{
				title:"Event Code",
				key:"eventCode",
				className:"subtitle",
				prepend:"Event Code: "
			}
		]
	},
	{
		title:"Shipment",
		type:"rows",
		rows:[
			{
				title:"Shipment",
				key:"shipment",
				className:""
			},
			{
				title:"Load",
				key:"load",
				className:"subtitle",
				prepend:"Load #"
			}
		]
	},
	{
		title:"Stop Number",
		key:"stopNumber",
		type:"text",
		className:"text-center"
	},
	{
		title:"City",
		key:"city",
		type:"text",
		className:"text-center",
		hideMobile:true
	},
	{
		title:"State",
		key:"state",
		type:"text",
		className:"text-center",
		hideMobile:true
	},
	{
		title:"Country",
		key:"country",
		type:"text",
		className:"text-center",
		hideMobile:true
	},
	{
		title:"City, State, Country",
		key:"combinedCityState",
		type:"text",
		className:"text-center",
		hideDesktop:true
	},
	{
		title:"Transaction ID",
		key:"transactionId",
		type:"text",
		className:"text-center"
	},
	{
		title:"Source",
		key:"source",
		type:"text",
		className:"text-center"
	},
	];

	$scope.tableData=[
		{
			errorId:'1234',
			client:'XOM',
			scac:'RDWY',
			eventCode:'AB',
			errorMessage:'Event match not found',
			load:'123456',
			shipment:'123456',
			stopNumber:'1',
			city:'Ft. Worth',
			state:'TX',
			country:'US',
			combinedCityState:'Ft. Worth, TX, US',
			combinedCityState:'Ft. Worth, TX, US',
			transactionId:'123456',
			source:'RDWY'
		},
		{
			errorId:'1234',
			client:'XOM',
			scac:'RDWY',
			eventCode:'AB',
			errorMessage:'Event match not found',
			load:'123456',
			shipment:'123456',
			stopNumber:'1',
			city:'Ft. Worth',
			state:'TX',
			country:'US',
			combinedCityState:'Ft. Worth, TX, US',
			transactionId:'123456',
			source:'RDWY'
		},
		{
			errorId:'1234',
			client:'XOM',
			scac:'RDWY',
			eventCode:'AB',
			errorMessage:'Event match not found',
			load:'123456',
			shipment:'123456',
			stopNumber:'1',
			city:'Ft. Worth',
			state:'TX',
			country:'US',
			combinedCityState:'Ft. Worth, TX, US',
			transactionId:'123456',
			source:'RDWY'
		},
		{
			errorId:'1234',
			client:'XOM',
			scac:'RDWY',
			eventCode:'AB',
			errorMessage:'Event match not found',
			load:'123456',
			shipment:'123456',
			stopNumber:'1',
			city:'Ft. Worth',
			state:'TX',
			country:'US',
			combinedCityState:'Ft. Worth, TX, US',
			transactionId:'123456',
			source:'RDWY'
		},
		{
			errorId:'1234',
			client:'XOM',
			scac:'RDWY',
			eventCode:'AB',
			errorMessage:'Event match not found',
			load:'123456',
			shipment:'123456',
			stopNumber:'1',
			city:'Ft. Worth',
			state:'TX',
			country:'US',
			combinedCityState:'Ft. Worth, TX, US',
			transactionId:'123456',
			source:'RDWY'
		},
		{
			errorId:'1234',
			client:'XOM',
			scac:'RDWY',
			eventCode:'AB',
			errorMessage:'Event match not found',
			load:'123456',
			shipment:'123456',
			stopNumber:'1',
			city:'Ft. Worth',
			state:'TX',
			country:'US',
			combinedCityState:'Ft. Worth, TX, US',
			transactionId:'123456',
			source:'RDWY'
		},
		{
			errorId:'1234',
			client:'XOM',
			scac:'RDWY',
			eventCode:'AB',
			errorMessage:'Event match not found',
			load:'123456',
			shipment:'123456',
			stopNumber:'1',
			city:'Ft. Worth',
			state:'TX',
			country:'US',
			combinedCityState:'Ft. Worth, TX, US',
			transactionId:'123456',
			source:'RDWY'
		},
		{
			errorId:'1234',
			client:'XOM',
			scac:'RDWY',
			eventCode:'AB',
			errorMessage:'Event match not found',
			load:'123456',
			shipment:'123456',
			stopNumber:'1',
			city:'Ft. Worth',
			state:'TX',
			country:'US',
			combinedCityState:'Ft. Worth, TX, US',
			transactionId:'123456',
			source:'RDWY'
		},
		{
			errorId:'1234',
			client:'XOM',
			scac:'RDWY',
			eventCode:'AB',
			errorMessage:'Event match not found',
			load:'123456',
			shipment:'123456',
			stopNumber:'1',
			city:'Ft. Worth',
			state:'TX',
			country:'US',
			combinedCityState:'Ft. Worth, TX, US',
			transactionId:'123456',
			source:'RDWY'
		},
		{
			errorId:'1234',
			client:'XOM',
			scac:'RDWY',
			eventCode:'AB',
			errorMessage:'Event match not found',
			load:'123456',
			shipment:'123456',
			stopNumber:'1',
			city:'Ft. Worth',
			state:'TX',
			country:'US',
			combinedCityState:'Ft. Worth, TX, US',
			transactionId:'123456',
			source:'RDWY'
		},
		{
			errorId:'1234',
			client:'XOM',
			scac:'RDWY',
			eventCode:'AB',
			errorMessage:'Event match not found',
			load:'123456',
			shipment:'123456',
			stopNumber:'1',
			city:'Ft. Worth',
			state:'TX',
			country:'US',
			combinedCityState:'Ft. Worth, TX, US',
			transactionId:'123456',
			source:'RDWY'
		},
		{
			errorId:'1234',
			client:'XOM',
			scac:'RDWY',
			eventCode:'AB',
			errorMessage:'Event match not found',
			load:'123456',
			shipment:'123456',
			stopNumber:'1',
			city:'Ft. Worth',
			state:'TX',
			country:'US',
			combinedCityState:'Ft. Worth, TX, US',
			transactionId:'123456',
			source:'RDWY'
		},
		{
			errorId:'1234',
			client:'XOM',
			scac:'RDWY',
			eventCode:'AB',
			errorMessage:'Event match not found',
			load:'123456',
			shipment:'123456',
			stopNumber:'1',
			city:'Ft. Worth',
			state:'TX',
			country:'US',
			combinedCityState:'Ft. Worth, TX, US',
			transactionId:'123456',
			source:'RDWY'
		},
		{
			errorId:'1234',
			client:'XOM',
			scac:'RDWY',
			eventCode:'AB',
			errorMessage:'Event match not found',
			load:'123456',
			shipment:'123456',
			stopNumber:'1',
			city:'Ft. Worth',
			state:'TX',
			country:'US',
			combinedCityState:'Ft. Worth, TX, US',
			transactionId:'123456',
			source:'RDWY'
		},
		{
			errorId:'1234',
			client:'XOM',
			scac:'RDWY',
			eventCode:'AB',
			errorMessage:'Event match not found',
			load:'123456',
			shipment:'123456',
			stopNumber:'1',
			city:'Ft. Worth',
			state:'TX',
			country:'US',
			combinedCityState:'Ft. Worth, TX, US',
			transactionId:'123456',
			source:'RDWY'
		},
		{
			errorId:'1234',
			client:'XOM',
			scac:'RDWY',
			eventCode:'AB',
			errorMessage:'Event match not found',
			load:'123456',
			shipment:'123456',
			stopNumber:'1',
			city:'Ft. Worth',
			state:'TX',
			country:'US',
			combinedCityState:'Ft. Worth, TX, US',
			transactionId:'123456',
			source:'RDWY'
		},
		{
			errorId:'1234',
			client:'XOM',
			scac:'RDWY',
			eventCode:'AB',
			errorMessage:'Event match not found',
			load:'123456',
			shipment:'123456',
			stopNumber:'1',
			city:'Ft. Worth',
			state:'TX',
			country:'US',
			combinedCityState:'Ft. Worth, TX, US',
			transactionId:'123456',
			source:'RDWY'
		},
		{
			errorId:'1234',
			client:'XOM',
			scac:'RDWY',
			eventCode:'AB',
			errorMessage:'Event match not found',
			load:'123456',
			shipment:'123456',
			stopNumber:'1',
			city:'Ft. Worth',
			state:'TX',
			country:'US',
			combinedCityState:'Ft. Worth, TX, US',
			transactionId:'123456',
			source:'RDWY'
		},
		{
			errorId:'1234',
			client:'XOM',
			scac:'RDWY',
			eventCode:'AB',
			errorMessage:'Event match not found',
			load:'123456',
			shipment:'123456',
			stopNumber:'1',
			city:'Ft. Worth',
			state:'TX',
			country:'US',
			combinedCityState:'Ft. Worth, TX, US',
			transactionId:'123456',
			source:'RDWY'
		},
	]
}]);
