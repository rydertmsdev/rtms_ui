App.controller('headerCtrl',['$scope', '$state', 'orderSvc','permissionsSvc', function($scope, $state, orderSvc, permissionsSvc){
	$scope.$state=$state;
	$scope.showMenu=false;
	$scope.orderSvc=orderSvc;
	$scope.roles=permissionsSvc.roles;
	$scope.setPermissions=function(roleName){
		permissionsSvc.setRole(roleName);
	}
	$scope.permissionsSvc=permissionsSvc;

}]);
