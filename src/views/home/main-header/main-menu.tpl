<button ui-button permission="fulfillment" ui-sref="home.order-fulfillment">
	<i class="fa fa-download"></i>
	<span>Fulfillment</span>
</button>
<button ui-button permission="tracking" ui-sref="home.tracking">
	<i class="fa fa-crosshairs"></i>
	<span>Tracking</span>
</button>
<button ui-button permission="shipment">
	<i class="fa fa-truck"></i>
	<span>Shipment</span>
</button>
<button ui-button>
	<i class="fa fa-dollar"></i>
	<span>Financials</span>
</button>
<button ui-button>
	<i class="fa fa-sliders"></i>
	<span>Manage</span>
</button>
<button ui-button>
	<i class="fa fa-wrench"></i>
	<span>Tools</span>
</button>