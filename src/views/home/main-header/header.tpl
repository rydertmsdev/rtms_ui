<div class="main-header">
	<!-- <ui-header theme="main-header">
		<ui-header-body>
			<div class="branding">
				<img src="img/ryder-logo.svg">
			</div>
		</ui-header-body>
		<ui-header-controls>
			<div ui-button>
				<i class="fa fa-gear"></i>
				<div class="submenu">
					<button ui-button ng-click="orderSvc.save()">
						<i class="fa fa-save"></i>
						<span>Save Test Data</span>
					</button>
				</div>
			</div>
			<div ui-button>
				<i class="fa fa-user"></i>
				<div class="submenu">
					<button ui-button ng-repeat="(roleKey,role) in roles" ng-click="setPermissions(roleKey)">
						<i class="fa fa-user{{roleKey==permissionsSvc.activeRole?'':'-o'}}"></i>
						<span>{{role.name}}</span>
					</button>
				</div>
			</div>
			<div ui-button class="branding">
				<img src="img/grainger-logo.svg">
				<div class="submenu">
					<button ui-button class="branding">
						<img src="img/grainger-logo.svg" alt="">
					</button>
					<button ui-button class="branding">
						<img src="img/ng-logo.svg" alt="">
					</button>
					<button ui-button class="branding">
						<img src="img/mattel-logo.svg" alt="">
					</button>
				</div>
			</div>
		</ui-header-controls>
	</ui-header> -->

	<ui-header theme="accent">
		<ui-header-body>
			<div class="icon" style="margin: 15px 0 0 0; background: none">
				<i class="fa fa-download" ng-if="$state.includes('home.order-fulfillment')"></i>
				<!-- <i class="fa fa-crosshairs" ng-if="$state.includes('home.tracking')"></i> -->
			</div>
			<div style="font-size: 1.2em;">
				<span ng-if="$state.includes('home.order-fulfillment')">Order Fulfillment</span>
				<!-- <span ng-if="$state.includes('home.tracking')">Shipment Tracking</span> -->
			</div>
		</ui-header-body>
		<button ui-button class="branding" style="padding: 0">
				<img src="img/grainger-banner.png" alt="">
			</button>
<!-- 		<ui-header-controls ng-include="'main-menu.tpl'">

		</ui-header-controls> -->
	</ui-header>
</div>
