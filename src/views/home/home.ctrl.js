App.controller('homeCtrl',['$scope', '$state', 'uiSidebarSvc','permissionsSvc', 'uiResponsiveSvc', function($scope, $state, uiSidebarSvc, permissionsSvc, uiResponsiveSvc){
	$scope.$state=$state;
	$scope.uiSidebarSvc=uiSidebarSvc;
	$scope.uiResponsiveSvc=uiResponsiveSvc;
	
	$scope.showMenu=false;
	$scope.showSidebar=false;

	$scope.roles=permissionsSvc.roles;

	$scope.setPermissions=function(roleName){
		permissionsSvc.setRole(roleName);

	}
	$scope.permissionsSvc=permissionsSvc;

	$scope.toggleMenu=function(){
		$scope.showMenu=!$scope.showMenu;
	}

	$scope.toggleSidebar=function(){
		uiSidebarSvc.showSidebar=!uiSidebarSvc.showSidebar;
		$scope.showMenu=false;
	}

	$scope.closeMenus=function(){
		$scope.showMenu=false;
	}

	$scope.go=function(button,$event){
		if (button.children&&button.children.length>0){
			$event.preventDefault();
			return;
		}
		$state.go(button.state);
		$scope.showMenu=false;
	}

	$scope.menu=[
	{
		title:"Home",
		state:"home.dashboard",
	},
	{
		title:"Orders",
		state:"home.orders",
		permission:"fulfillment",
		children:[
		{
			title:"Order Fulfillment",
			state:"home.orders.fulfillment",
		},
		{
			title:"Location Search",
			state:"home.orders.location-search",
		},
		{
			title:"Item Search",
			state:"home.orders.item-search",
		}
		]
	},
	{
		title:"Tracking",
		state:"home.tracking",
		permission:"tracking"
	},
	{
		title:"Shipment",
		state:"home.shipment",
		permission:"shipment",
		children:[
		{
			title:"Create Shipment",
			state:"home.shipment.create",
		},
		]
	},
	{
		title:"Docks",
		state:"home.dock-management"
	},
	{
		title:"Settings",
		state:"home.settings",
		children:[
		{
			title:"Administration",
			state:"home.settings.administration",
		},
		{
			title:"Event Matching",
			state:"home.settings.event-matching",
		},
		]
	},
	]

	$scope.clients=[
	{
		title:"Northrop Grumman"
	},
	{
		title:"Grainger"
	},
	{
		title:"Mattel"
	},
	]

}]);
