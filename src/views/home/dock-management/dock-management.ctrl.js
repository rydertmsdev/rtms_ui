App.controller('dockManagementCtrl', ['$scope', '$state','$window','$timeout','uiScheduleSvc', function ($scope, $state, $window,$timeout,uiScheduleSvc) {
    $scope.$state = $state;
    $scope.hourly = false;
    $scope.uiScheduleSvc=uiScheduleSvc;
    $scope.editing=false;

    $scope.changeTimeScale = function () {
        $scope.hourly = !$scope.hourly;
    }

    $scope.editEvent = function (event) {
        angular.forEach($scope.docks, function (dock) {
            angular.forEach(dock.events, function (event) {
                event.$$editable = false;
            });
        });
        if (event) {
            $scope.editing=true;
            event.$$editable = true;
        }
    }

    $scope.finishEditing = function () {
        $scope.editing=false;
        $timeout.cancel($scope.editTimeout);
        $scope.editTimeout=$timeout(function(){
            $scope.editEvent();
        },350)
    }

    $scope.addEvent = function (dock, time) {
        var defaultDuration = 3600;
        var newEvent = {
            title: "New Event",
            startTime: time-uiScheduleSvc.tzOffset,
            endTime: time-uiScheduleSvc.tzOffset+defaultDuration,
            color: "#33ccff",
            $$visible:true
        }
        console.log(newEvent);
        $scope.docks[$scope.docks.indexOf(dock)].events.push(newEvent);
    }

    $scope.dragStart = function (dIndex, event) {
        $scope.showTrash = true;
        $scope.dragDIndex = dIndex;
        $scope.dragEvent = event;
    }

    $scope.dragStop = function () {
        $scope.showTrash = false;
        delete $scope.dragDIndex;
        delete $scope.dragEvent;
    }

    $scope.dropTrash = function () {
        $scope.showTrash = false;
        $scope.docks[$scope.dragDIndex].events.splice($scope.docks[$scope.dragDIndex].events.indexOf($scope.dragEvent), 1);
    }

    $scope.dropCell = function ($event, dock, time) {
        $scope.showTrash = false;

        var newEvent = angular.copy($scope.dragEvent);
        newEvent.startTime = time - uiScheduleSvc.tzOffset,
        newEvent.endTime = time - uiScheduleSvc.tzOffset + $scope.dragEvent.endTime - $scope.dragEvent.startTime;

        if (newEvent.endTime>uiScheduleSvc.selectedDate-uiScheduleSvc.tzOffset+86400){
            newEvent.endTime=uiScheduleSvc.selectedDate-uiScheduleSvc.tzOffset+86400;
        }

        dock.events.push(newEvent);

        if ($event&&($event.ctrlKey||$event.metaKey)) return;
        $scope.docks[$scope.dragDIndex].events.splice($scope.docks[$scope.dragDIndex].events.indexOf($scope.dragEvent), 1);
    }


    ///////////////////////////////////////////////////
    // Watchers
    //
    
    var selectedDateWatch = $scope.$watch(function(){
        return uiScheduleSvc.selectedDate
    }, function (newval, oldval) {
        angular.forEach($scope.docks, function (dock) {
            angular.forEach(dock.events, function (event) {
                event.$$visible = event.startTime >= newval - uiScheduleSvc.tzOffset && event.startTime < newval - uiScheduleSvc.tzOffset + 86400;
            });
        });
    })

    var watchDocks=$scope.$watch('docks',function(newval){
        if (!newval) return;
        saveEvents();
    },true)
    
    $scope.$on('$destroy', function () {
        selectedDateWatch();
        watchDocks();
    })

    ///////////////////////////////////////////////////
    // Data Init
    //
    

    //$window.localStorage.removeItem('scheduleEvents');
    
    var loadEvents=function(){
        return JSON.parse($window.localStorage.getItem('scheduleEvents'))||schedulerTestData
    }

    var saveEvents=function(){
        $window.localStorage.setItem('scheduleEvents',angular.toJson($scope.docks))
    }

    var initData=function(){
        $scope.docks = loadEvents();
        if (!$scope.docks||$scope.docks.length<1){
            $scope.docks=[];
            for (var i=0;i<10;i++){
                $scope.docks.push({
                    name:"Dock "+(i+1),
                    events:[]
                })
            }
        }
    }

    var initTime = function () {
        $scope.times = [];
        var tempTime = uiScheduleSvc.selectedDate;
        while (tempTime<=uiScheduleSvc.selectedDate+86400-1) {
            $scope.times.push(tempTime);
            tempTime+=15*60;
        }
    }

    initTime();
    initData();

}]).service('uiScheduleSvc', [function(){
    var svc=this;
    //svc.selectedDate=(new Date()).setHours(0, 0, 0, 0)/1000;
    svc.selectedDate=(new Date("May 12, 2017")).setHours(0, 0, 0, 0)/1000
    svc.tzOffset = (new Date()).getTimezoneOffset() * 60; 
}])
.directive('eventResizeHandle', ['$window','$timeout','uiScheduleSvc',function ($window,$timeout,uiScheduleSvc) {
    return {
        scope:{
            eventResizeHandle:"="
        },
        link:function(scope, element, attrs, ngModel) {
            var parent=element.closest("[event-resize]");
            var win=angular.element($window);
            var startY, startStyleHeight, startActualHeight, newHeight, ratio;
            var handleMove=function($event){
                newHeight=startActualHeight+($event.screenY-startY);
                if (newHeight<20) newHeight=20;
                parent.style.height=newHeight+'px';
            }
            var handleUp=function($event){
                var newDuration=newHeight/ratio;
                newDuration=Math.round(newDuration/60/15)*60*15;
                var newEndTime=scope.eventResizeHandle.startTime+newDuration;
                if (newEndTime>uiScheduleSvc.selectedDate-uiScheduleSvc.tzOffset+86400){
                    newEndTime=uiScheduleSvc.selectedDate-uiScheduleSvc.tzOffset+86400;
                }
                parent.style.height = (newEndTime - scope.eventResizeHandle.startTime) / 86400 * 100 + '%';
                $timeout(function(){
                    scope.eventResizeHandle.endTime=newEndTime;
                })
                win.off('mousemove',handleMove)
                win.off('mouseup',handleUp)
            }
            element.on("mousedown",function($event){
                startActualHeight=parent.clientHeight;
                ratio=parent.clientHeight/(scope.eventResizeHandle.endTime-scope.eventResizeHandle.startTime);
                startY=$event.screenY;
                $event.preventDefault();
                win.on('mousemove',handleMove)
                win.on('mouseup',handleUp)
            })

            scope.$on("$destroy",function(){
                element.off();
                win.off('mousemove',handleMove)
                win.off('mouseup',handleUp)
            })
        }
    }
}])
.directive('currentTime', ['$timeout','$interval','uiScheduleSvc',function ($timeout,$interval,uiScheduleSvc) {
    return {
        scope:{
            currentTime:"="
        },
        link:function(scope, element, attrs) {
            var newTop;
            var updatePosition=function(){
                var midnight=(new Date()).setHours(0,0,0,0);
                var now=new Date();
                newTop=(now-midnight) / 1000 / 86400 * 100 + '%';
                element[0].style.top=newTop
            }
            updatePosition();

            $timeout(function(){
                var scrollContainer=angular.element(element.closest(".ui-scroll-table-content"));
                if (scrollContainer){
                    scrollTarget=element[0].offsetTop-scrollContainer[0].clientHeight/2;
                    scrollContainer[0].scrollTop=scrollTarget;
                }
            })

            var timer=$interval(updatePosition,10000)

            scope.$on("$destroy",function(){
                $interval.cancel(timer)
            })
        }
    }
}]);

var schedulerTestData=[
  {
    "name": "Dock 1",
    "events": []
  },
  {
    "name": "Dock 2",
    "events": [
      {
        "title": "Incoming",
        "startTime": 1494583200,
        "endTime": 1494597600,
        "color": "#7977de",
        "details": "FEDEX\n#2429902-34289\n\nReceiving:\nParts Department"
      },
      {
        "title": "Incoming",
        "startTime": 1494601200,
        "endTime": 1494608400,
        "color": "#7977de",
        "details": "FEDEX\n#060300-29390\n\nReceiving:\nTools Department"
      }
    ]
  },
  {
    "name": "Dock 3",
    "events": [
      {
        "title": "UPS Pickup",
        "startTime": 1494604800,
        "endTime": 1494612000,
        "color": "#ffaa33",
        "details": "Reserved Every Day\n4pm-6pm"
      },
      {
        "title": "UPS Pickup",
        "startTime": 1494579600,
        "endTime": 1494586800,
        "color": "#ffaa33",
        "details": "Reserved Every Day\n9am-11am"
      },
      {
        "title": "Parts Dept.",
        "startTime": 1494590400,
        "endTime": 1494597600,
        "color": "#6fe083",
        "details": "Reserved by Parts department. Contact Bob Smith for details @ ext.2460"
      }
    ]
  },
  {
    "name": "Dock 4",
    "events": [
      {
        "title": "Parts Dept.",
        "startTime": 1494579600,
        "endTime": 1494586800,
        "color": "#6fe083",
        "details": "Reserved by Parts department. Contact Bob Smith for details @ ext.2460"
      },
      {
        "title": "Parts Dept.",
        "startTime": 1494594000,
        "endTime": 1494604800,
        "color": "#6fe083",
        "details": "Reserved by Parts department. Contact Bob Smith for details @ ext.2460"
      }
    ]
  },
  {
    "name": "Dock 5",
    "events": [
      {
        "title": "Parts Delivery",
        "startTime": 1494576000,
        "endTime": 1494594000,
        "color": "#ff5777",
        "details": "5 pallets\nForklift requested"
      }
    ]
  },
  {
    "name": "Dock 6",
    "events": []
  },
  {
    "name": "Dock 7",
    "events": [
      {
        "title": "DOCK CLOSED",
        "startTime": 1494547200,
        "endTime": 1494633600,
        "color": "#999",
        "details": "Scheduled maintenance"
      }
    ]
  },
  {
    "name": "Dock 8",
    "events": []
  },
  {
    "name": "Dock 9",
    "events": [
      {
        "title": "Parts Delivery (1/2)",
        "startTime": 1494572400,
        "endTime": 1494585000,
        "color": "#ff6259",
        "details": "1 trailer\nForklift requested"
      },
      {
        "title": "Parts Delivery (2/2)",
        "startTime": 1494594000,
        "endTime": 1494608400,
        "color": "#ff6259",
        "details": "1 trailer\nForklift requested"
      }
    ]
  },
  {
    "name": "Dock 10",
    "events": [
      {
        "title": "Trailer Storage",
        "startTime": 1494547200,
        "endTime": 1494633600,
        "color": "#33ccff",
        "details": "Lic. AB2590CS/CA"
      }
    ]
  }
];