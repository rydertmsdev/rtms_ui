<div class="dock-management-container">
    <div class="dock-management" ng-class="{'hourly':hourly,'editing':editing}" >
        <div class="overlay fade" ng-show="editing" ng-click="finishEditing()"></div>
        <ui-scroll-table>
            <corner>
                <i class="fa fa-clock-o"></i>
            </corner>
            <left-header>
                <div ng-repeat="(tIndex,time) in times" class="time-header" ng-class="{'quarter':((tIndex+1) % 4 != 1)}">
                    {{time*1000|date:"shortTime"}}
                </div>
            </left-header>
            <top-header>
                <div ng-repeat="dock in docks" class="dock-header">
                    <div>
                        {{dock.name}}
                    </div>
                </div>
            </top-header>
            <content>
                <div class="docks">
                    <div ng-repeat="(dIndex,dock) in docks" class="dock" style="position: relative;">
                        <div class="cell" 
                        ng-repeat="(tIndex,time) in times"
                        ui-drag-drop="dropCell($event, dock,time)"
                        ng-class="{'quarter':((tIndex+1) % 4 != 1)}"
                        ng-dblclick="addEvent(dock, time)">
                    </div>
                    <div class="event"
                    ng-dblclick="editEvent(event)"
                    event-resize
                    ui-drag
                    ui-drag-start="dragStart(dIndex,event)"
                    ui-drag-stop="dragStop()"
                    ng-repeat="(eIndex,event) in dock.events"
                    ng-if="event.$$visible"
                    ng-class="{'editable':event.$$editable}"
                    ng-style="{'background-color':event.color,'top':(event.startTime - uiScheduleSvc.selectedDate + uiScheduleSvc.tzOffset) / 86400 * 100 + '%','height':(event.endTime - event.startTime) / 86400 * 100 + '%'}">
                    <input type="text" class="title" ng-model="event.title" ng-disabled="!event.$$editable"/>
                    <textarea spellcheck="false" class="details details-textarea" ng-model="event.details" ng-disabled="!event.$$editable"></textarea>
                    <div class="details details-div" ng-bind-html="event.details"></div>
                    <button ui-info="Change Color" class="field-color" ui-color ng-model="event.color"></button>
                    <div event-resize-handle="event">
                    </div>
                </div>
            </div>
            <div class="current-time" current-time>
                {{currentTime}}
            </div>
        </div>
    </content>
</ui-scroll-table>
<ui-page-actions-container>
    <ui-page-actions>
        <div class="group" ng-show="showTrash" ui-drag-drop="dropTrash()">
            <div ui-button ui-page-action class="bg-red" ui-info="Delete Entry">
                <i class="fa fa-trash"></i>
            </div>
        </div>
        <div class="group" ui-drag-drop="dropTrash()">
            <div ui-button ui-page-action class="bg-accent" ui-info="Switch View" ng-click="changeTimeScale()">
                <div>
                    <i class="fa fa-clock-o"></i>
                    <span class="small">{{hourly?'1h':'15m'}}</span>
                </div>
            </div>
        </div>
        <div class="group">
            <div ui-button ui-page-action class="text-accent bg-white" ui-toggle="showDatePicker">
                <div>
                    <i class="fa fa-calendar"></i>
                    <span class="small" ng-bind-html="uiScheduleSvc.selectedDate*1000|date:'MMM dd'"></span>
                </div>
                <ui-field type="date" ng-model="uiScheduleSvc.selectedDate" class="no-input" ng-class="{'show-picker':showDatePicker}">
                </ui-field>
            </div>
        </div>
    </div>
</ui-page-actions>
    </ui-page-actions-container>



</div>
</div>