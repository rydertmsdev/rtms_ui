App.controller('ordersFulfillmentCtrl',['generateSearchQuery', '$scope', '$state', 'orderSvc' ,'codeListService', 'purchaseOrderSearchResult', 'purchaseOrderDetails', function(generateSearchQuery, $scope, $state, orderSvc, codeListService, purchaseOrderSearchResult, purchaseOrderDetails){

	$scope.$state=$state;
	$scope.orderSvc=orderSvc;

	if(codeListService.codeLists && codeListService.codeLists.SearchResultsFieldList){
		$scope.tableHeaders = JSON.parse(JSON.stringify(codeListService.codeLists.SearchResultsFieldList));
	}
	
	$scope.dynamicContent = [];
	$scope.searchContent = [];
	$scope.additionalSearchContent = [];
	
	if(generateSearchQuery.from_server_copy){
		$scope.dynamicContent = generateSearchQuery.from_server_copy.SearchFieldList;
	}

	angular.forEach($scope.dynamicContent, function(field, index) {
		if(field.sectionSeq == 0 || field.sectionSeq == null){
			$scope.searchContent.push(field);
		}
		else{
			$scope.additionalSearchContent.push(field);
		}
	});

}]);
