//Directive that returns an element which adds buttons on click which show an alert on click
App.directive("addContactButton", function(){
	return {
		restrict: "E",
		template: "<button addLocationContact>Click to add Contacts</button>"
	}
});

//Directive for adding buttons on click that show an alert on click
App.directive("addLocationContact", function($compile){
	return function(scope, element, attrs){
		element.bind("click", function(){
			scope.count++;
			angular.element(document.getElementById('locationContacts')).append($compile("<div><button class='btn btn-default' data-alert="+scope.count+">Show alert #"+scope.count+"</button></div>")(scope));
	});
	};
});