<div class="form modal-small ng-scope" ng-controller="ofPrintDocumentModalCtrl">
    <ui-header theme="ui-modal-header">
        <ui-header-body>
            <div><i class="fa fa-print"></i> <span>Print Documents</span></div>
        </ui-header-body>
        <ui-header-controls>
            <div ui-button="" title="Clear" ng-click="close()"><i class="fa fa-times"></i></div>
        </ui-header-controls>
    </ui-header>
    <div class="modal-body">
        <ui-list class="static">
            <ui-list-header>
                <li class="icon text-center"><span>Select</span></li>
                <li><span>Document</span>
                    <div ui-list-sort-btn=""></div>
                </li>
                <li class="icon">Print</li>
            </ui-list-header>
            <ui-list-body>
                <!-- ngRepeat: doc in documents -->
                <li ng-repeat="doc in documents" style="min-height: 54px" class="ng-scope">
                    <cell class="icon" ng-button="" ng-click="doc.$$selected=!doc.$$selected">
                        <div class="checkbox" ng-class="{'checked':doc.$$selected}"></div>
                    </cell>
                    <ui-list-content>
                        <cell>
                            <span ng-bind="::doc.docDesc" class="ng-binding"></span>
                        </cell>
                    </ui-list-content>
                    <cell class="icon">
                        <div ui-button="" ng-click="printDoc(doc)"><i class="fa fa-print"></i></div>
                    </cell>
                </li>
                <!-- end ngRepeat: doc in documents -->
            </ui-list-body>
        </ui-list>
    </div>
    <ui-header theme="ui-modal-footer">
        <ui-header-controls>
            <div ui-button="" ng-click="close()" class="bg-gray"><span>Cancel</span></div>
            <div ui-button="" class="bg-accent" ng-click="printAll()" ng-if="(documents|filter:{$$selected:true}).length<1">Print All</div>
            <div ui-button="" class="bg-accent" ng-click="printSelected()" ng-if="(documents|filter:{$$selected:true}).length>0">Print Selected</div>
        </ui-header-controls>
    </ui-header>
</div>
