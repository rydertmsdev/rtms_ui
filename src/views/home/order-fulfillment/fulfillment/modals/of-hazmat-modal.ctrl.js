App.controller('ofHazmatModalCtrl',['$scope', '$timeout', 'uiModalSvc', 'orderSvc', function($scope, $timeout, uiModalSvc, orderSvc){	
	$scope.item=angular.copy(orderSvc.deliveryOrders[$scope.args.dIndex].shipmentObject.handlingUnits[$scope.args.hIndex].items[$scope.args.index]);

	//$scope.itemReferences=angular.copy(orderSvc.deliveryOrders[$scope.args.dIndex].handlingUnits[$scope.args.hIndex].items[$scope.args.index].);

	$scope.hazArray = $scope.args.hazArray;

	angular.forEach($scope.hazArray, function(item, index) {
		if($scope.item.references){
			var refCode = '';
			angular.forEach($scope.item.references, function(ref, i) {
				if(ref.referenceCode == item.fieldName){
					refCode = ref.referenceNum;
				}
			});
			item["itemValue"] = refCode;
		}		
	});

	$scope.columns = columnize($scope.hazArray, Math.ceil($scope.hazArray.length/2));

	function columnize(input, cols) {
	  var arr = [];
	  for(i = 1; i < input.length; i++) {
	    var colIdx = i % cols;
	    // if(input[i].displayType == "readonly.checkbox" || input[i].displayType == "single checkbox"){
	    // 	arr[colIdx] = arr[colIdx] || [];
	    // 	arr[colIdx].unshift(input[i]);
	    // 	cols+2;
	    // }
	    // else{
	    	arr[colIdx] = arr[colIdx] || [];
	    	arr[colIdx].push(input[i]);
	    // }
	    
	  }
	  var tempArr = [input[0]]
	  arr.unshift(tempArr);
	  return arr;
	}

	$scope.close=function(){
		uiModalSvc.hide();
	}

	$scope.save=function(flag){
		if(flag){
			orderSvc.updateItem($scope.args.dIndex,$scope.args.hIndex,$scope.args.index,$scope.item);

			angular.forEach($scope.hazArray, function(name, index) {
				if(orderSvc.deliveryOrders[$scope.args.dIndex].shipmentObject.handlingUnits[$scope.args.hIndex].items[$scope.args.index].references){
					var refFlag = false;
					angular.forEach(orderSvc.deliveryOrders[$scope.args.dIndex].shipmentObject.handlingUnits[$scope.args.hIndex].items[$scope.args.index].references, function(ref, i) {
						if(name.fieldName == ref.referenceCode){
							refFlag = true;
							if(name.displayType == "dropdown"){
								orderSvc.deliveryOrders[$scope.args.dIndex].shipmentObject.handlingUnits[$scope.args.hIndex].items[$scope.args.index].references[i].referenceNum = name.itemValue.code;
							}
							else if((name.displayType == "readonly.checkbox") || (name.displayType == "single checkbox")){
								if(name.itemValue){
									name.itemValue = "Y";
								}
								else{
									name.itemValue = "N";
								}
								orderSvc.deliveryOrders[$scope.args.dIndex].shipmentObject.handlingUnits[$scope.args.hIndex].items[$scope.args.index].references[i].referenceNum = name.itemValue;
							}
							else{
								orderSvc.deliveryOrders[$scope.args.dIndex].shipmentObject.handlingUnits[$scope.args.hIndex].items[$scope.args.index].references[i].referenceNum = name.itemValue;
							}						
						}
					});
					if(!refFlag && !!name.itemValue){
						if(name.displayType == "dropdown"){
							var tempObj = {};
							tempObj["referenceCode"] = name.fieldName;
							tempObj["referenceNum"] = name.itemValue.code;
							orderSvc.deliveryOrders[$scope.args.dIndex].shipmentObject.handlingUnits[$scope.args.hIndex].items[$scope.args.index].references.push(tempObj);
						}
						else if((name.displayType == "readonly.checkbox") || (name.displayType == "single checkbox")){
							if(name.itemValue){
								name.itemValue = "Y";
							}
							else{
								name.itemValue = "N";
							}
							
							var tempObj = {};
							tempObj["referenceCode"] = name.fieldName;
							tempObj["referenceNum"] = name.itemValue;

							orderSvc.deliveryOrders[$scope.args.dIndex].shipmentObject.handlingUnits[$scope.args.hIndex].items[$scope.args.index].references.push(tempObj);
						}
						else{
							var tempObj = {};
							tempObj["referenceCode"] = name.fieldName;
							tempObj["referenceNum"] = name.itemValue;
							orderSvc.deliveryOrders[$scope.args.dIndex].shipmentObject.handlingUnits[$scope.args.hIndex].items[$scope.args.index].references.push(tempObj);
						}
					}
				}
			});

			uiModalSvc.hide();
		}
		else{
			//Form validation happens by angular
		}
	}
}]);
