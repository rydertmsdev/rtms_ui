App.controller('ofPoDetailsCtrl',['$scope', '$timeout', 'uiLoaderSvc', 'uiModalSvc', 'orderSvc', 'updatePODetails', 'codeListService', function($scope, $timeout, uiLoaderSvc, uiModalSvc, orderSvc, updatePODetails, codeListService){	
	$scope.po=$scope.args.po;
	$scope.OrderItemReferenceList = $scope.args.OrderItemReferenceList || [];
	$scope.PODetailsRolViewList = [];
	$scope.ReferenceList = [];

	if(codeListService.codeLists && codeListService.codeLists.PODetailsRolViewList){
		$scope.PODetailsRolViewList = codeListService.codeLists.PODetailsRolViewList;
	}

	if(codeListService.codeLists && codeListService.codeLists.ReferenceList){
		$scope.ReferenceList = codeListService.codeLists.ReferenceList;
	}

	function columnize(input, cols, fieldLength) {
	  var arr = [];
	  for(i = 0; i < input.length; i++) {
	  	if(input[i].required == "Y" || input[i].required == true){
	  		input[i].required = function(){return true};
	  	}
	  	else if(input[i].required == "N" || input[i].required == false || !(input[i].required)){
	  		input[i].required = function(){return false};
	  	}
	    var colIdx = Math.floor(i / fieldLength);
    	arr[colIdx] = arr[colIdx] || [];
    	arr[colIdx].push(input[i]);	    
	  }

	  for(j=0;j<arr.length;j++){
	  	if(arr[j].length < fieldLength){
	  		var tempVal = fieldLength - arr[j].length;
	  		for(var k=0; k<tempVal; k++){
	  			arr[j].push({});
	  		}
	  	}
	  }

	  return arr;
	}

	if($scope.po && $scope.po.orderItems){
		angular.forEach($scope.po.orderItems, function(orderItem, oi){
			orderItem.OrderItemsFieldsArr = [];
			if(orderItem.references){
				angular.forEach(orderItem.references, function(ref, r){
					angular.forEach($scope.OrderItemReferenceList, function(totalRef, rIndex){
						if(ref.referenceCode == totalRef.code && totalRef.displayType != "none"){
							orderItem.OrderItemsFieldsArr.push(ref);
						}
					});
				});
			}
			orderItem.columns = columnize(orderItem.OrderItemsFieldsArr, Math.ceil(orderItem.OrderItemsFieldsArr.length/4), 4);
		});
	}

	$scope.PODetailsRolViewListColumns = columnize($scope.PODetailsRolViewList, Math.ceil($scope.PODetailsRolViewList.length/4), 4);

	$scope.showItemsFlag = false;

	$scope.selectedRootOrder = orderSvc.selectedRootOrder;

	$scope.showItems = function(){
		$scope.showItemsFlag = !($scope.showItemsFlag);
	}

	$scope.updatePoDetails=function(){
		if($scope.po && $scope.po.orderItems){
			angular.forEach($scope.po.orderItems, function(orderItem, oi){
				if(orderItem.references){
					angular.forEach(orderItem.references, function(ref, r){
						ref.referenceNum = angular.isObject(ref.referenceNum) ? ref.referenceNum.code : ref.referenceNum;
					});
				}
			});
		}

		uiLoaderSvc.show();

		// $scope.po = {"createDt":1363531770000,"createId":"WWG","lastUpdateDt":1494251558000,"lastUpdateId":"WWG","orderComponentNum":1017649853,"customerNum":"WWG","freightTerms":"COLLECT","memo":"PLEASE PRINT CUSTOMER  to reference is 1173753273","orderNumber":"20130312411","qtyFulfilled":25,"qtyShipped":27,"quantity":0,"status":"FULFILLED","supplierPartyId":469878,"supplyordertypecd":"PURCHASE","totalLineItems":3,"urgent":0,"volume":0,"weight":1111,"orderItems":[{"createDt":1491344571000,"createId":"RYDERO","lastUpdateDt":1495752324000,"lastUpdateId":"RYDERO","orderComponentNum":1029020513,"parentOrderComponentNum":1017649853,"commodityCode":"DRY","description":"24 WHITE BOUFFANT CAP","dueDate":1359957660000,"height":0,"itemCode":"70VN56","itemNumber":"70VN56","length":0,"measurementUom":"null","orderNumber":"20130312411","qtyFulfilled":1,"qtyShipped":1,"quantity":1,"quantityUom":"EA","status":"FULFILLED","width":0,"customerNum":"WWG","lineItemNum":"00001","attributes":[],"references":[{"createDt":1491344722000,"createId":"RYDERO","lastUpdateDt":1491344722000,"lastUpdateId":"RYDERO","lineItemNum":"1","referenceCode":"CC","referenceNum":"11"},{"createDt":1491344722000,"createId":"RYDERO","lastUpdateDt":1491344722000,"lastUpdateId":"RYDERO","lineItemNum":"1","referenceCode":"HAZ","referenceNum":"HAZ"},{"createDt":1491344722000,"createId":"RYDERO","lastUpdateDt":1491344722000,"lastUpdateId":"RYDERO","lineItemNum":"1","referenceCode":"HAZM","referenceNum":"NO"},{"createDt":1491344722000,"createId":"RYDERO","lastUpdateDt":1491344722000,"lastUpdateId":"RYDERO","lineItemNum":"1","referenceCode":"P8","referenceNum":"11"},{"createDt":1491344722000,"createId":"RYDERO","lastUpdateDt":1491344722000,"lastUpdateId":"RYDERO","lineItemNum":"1","referenceCode":"P8","referenceNum":"23"},{"createDt":1491344722000,"createId":"RYDERO","lastUpdateDt":1491344722000,"lastUpdateId":"RYDERO","lineItemNum":"1","referenceCode":"POCN","referenceNum":"1017543054"},{"createDt":1491344722000,"createId":"RYDERO","lastUpdateDt":1491344722000,"lastUpdateId":"RYDERO","lineItemNum":"1","referenceCode":"PORD","referenceNum":"TST123"},{"createDt":1491344722000,"createId":"RYDERO","lastUpdateDt":1491344722000,"lastUpdateId":"RYDERO","lineItemNum":"1","referenceCode":"PP","referenceNum":"12"},{"createDt":1491344722000,"createId":"RYDERO","lastUpdateDt":1491344722000,"lastUpdateId":"RYDERO","lineItemNum":"1","referenceCode":"SORD","referenceNum":"84784745857"},{"createDt":1491344722000,"createId":"RYDERO","lastUpdateDt":1491344722000,"lastUpdateId":"RYDERO","lineItemNum":"1","referenceCode":"SORD","referenceNum":"test1"},{"createDt":1491344722000,"createId":"RYDERO","lastUpdateDt":1491344722000,"lastUpdateId":"RYDERO","lineItemNum":"1","referenceCode":"SORD","referenceNum":"test2"},{"createDt":1491344722000,"createId":"RYDERO","lastUpdateDt":1491344722000,"lastUpdateId":"RYDERO","lineItemNum":"1","referenceCode":"UNC","referenceNum":"11"}],"orderItemHistory":[],"enableLineCancel":"Y","type":"ORDER_ITEM","entityId":"1029020513","notAllocated":false,"allocated":true},{"createDt":1491344571000,"createId":"RYDERO","lastUpdateDt":1495752324000,"lastUpdateId":"RYDERO","orderComponentNum":1029020514,"parentOrderComponentNum":1017649853,"commodityCode":"DRY","description":"9X9 WHITE MAGIC WIPER","dueDate":1359957660000,"height":0,"itemCode":"72GV33","itemNumber":"72GV33","length":0,"measurementUom":"null","orderNumber":"20130312411","qtyFulfilled":10,"qtyShipped":10,"quantity":10,"quantityUom":"EA","status":"FULFILLED","width":0,"customerNum":"WWG","lineItemNum":"00002","attributes":[],"references":[{"createDt":1491344722000,"createId":"RYDERO","lastUpdateDt":1491344722000,"lastUpdateId":"RYDERO","lineItemNum":"1","referenceCode":"CC","referenceNum":"11"},{"createDt":1491344722000,"createId":"RYDERO","lastUpdateDt":1491344722000,"lastUpdateId":"RYDERO","lineItemNum":"1","referenceCode":"HAZ","referenceNum":"HAZ"},{"createDt":1491344722000,"createId":"RYDERO","lastUpdateDt":1491344722000,"lastUpdateId":"RYDERO","lineItemNum":"1","referenceCode":"HAZM","referenceNum":"NO"},{"createDt":1491344722000,"createId":"RYDERO","lastUpdateDt":1491344722000,"lastUpdateId":"RYDERO","lineItemNum":"1","referenceCode":"P8","referenceNum":"11"},{"createDt":1491344722000,"createId":"RYDERO","lastUpdateDt":1491344722000,"lastUpdateId":"RYDERO","lineItemNum":"1","referenceCode":"P8","referenceNum":"23"},{"createDt":1491344722000,"createId":"RYDERO","lastUpdateDt":1491344722000,"lastUpdateId":"RYDERO","lineItemNum":"1","referenceCode":"POCN","referenceNum":"1017543054"},{"createDt":1491344722000,"createId":"RYDERO","lastUpdateDt":1491344722000,"lastUpdateId":"RYDERO","lineItemNum":"1","referenceCode":"PORD","referenceNum":"TST123"},{"createDt":1491344722000,"createId":"RYDERO","lastUpdateDt":1491344722000,"lastUpdateId":"RYDERO","lineItemNum":"1","referenceCode":"PP","referenceNum":"12"},{"createDt":1491344722000,"createId":"RYDERO","lastUpdateDt":1491344722000,"lastUpdateId":"RYDERO","lineItemNum":"1","referenceCode":"SORD","referenceNum":"84784745857"},{"createDt":1491344722000,"createId":"RYDERO","lastUpdateDt":1491344722000,"lastUpdateId":"RYDERO","lineItemNum":"1","referenceCode":"SORD","referenceNum":"test1"},{"createDt":1491344722000,"createId":"RYDERO","lastUpdateDt":1491344722000,"lastUpdateId":"RYDERO","lineItemNum":"1","referenceCode":"SORD","referenceNum":"test2"},{"createDt":1491344722000,"createId":"RYDERO","lastUpdateDt":1491344722000,"lastUpdateId":"RYDERO","lineItemNum":"1","referenceCode":"UNC","referenceNum":"11"}],"orderItemHistory":[],"enableLineCancel":"Y","type":"ORDER_ITEM","entityId":"1029020514","notAllocated":false,"allocated":true},{"createDt":1491344571000,"createId":"RYDERO","lastUpdateDt":1495752324000,"lastUpdateId":"RYDERO","orderComponentNum":1029020515,"parentOrderComponentNum":1017649853,"commodityCode":"DRY","description":"SHOE COVERS SZ LG/UNIV CS/300 OLD # HTSC","dueDate":1359957660000,"height":0,"itemCode":"75PU23","itemNumber":"75PU23","length":0,"measurementUom":"null","orderNumber":"20130312411","qtyFulfilled":4,"qtyShipped":4,"quantity":4,"quantityUom":"EA","status":"FULFILLED","width":0,"customerNum":"WWG","lineItemNum":"00003","attributes":[],"references":[{"createDt":1491344722000,"createId":"RYDERO","lastUpdateDt":1491344722000,"lastUpdateId":"RYDERO","lineItemNum":"1","referenceCode":"CC","referenceNum":"11"},{"createDt":1491344722000,"createId":"RYDERO","lastUpdateDt":1491344722000,"lastUpdateId":"RYDERO","lineItemNum":"1","referenceCode":"HAZ","referenceNum":"HAZ"},{"createDt":1491344722000,"createId":"RYDERO","lastUpdateDt":1491344722000,"lastUpdateId":"RYDERO","lineItemNum":"1","referenceCode":"HAZM","referenceNum":"NO"},{"createDt":1491344722000,"createId":"RYDERO","lastUpdateDt":1491344722000,"lastUpdateId":"RYDERO","lineItemNum":"1","referenceCode":"P8","referenceNum":"11"},{"createDt":1491344722000,"createId":"RYDERO","lastUpdateDt":1491344722000,"lastUpdateId":"RYDERO","lineItemNum":"1","referenceCode":"P8","referenceNum":"23"},{"createDt":1491344722000,"createId":"RYDERO","lastUpdateDt":1491344722000,"lastUpdateId":"RYDERO","lineItemNum":"1","referenceCode":"POCN","referenceNum":"1017543054"},{"createDt":1491344722000,"createId":"RYDERO","lastUpdateDt":1491344722000,"lastUpdateId":"RYDERO","lineItemNum":"1","referenceCode":"PORD","referenceNum":"TST123"},{"createDt":1491344722000,"createId":"RYDERO","lastUpdateDt":1491344722000,"lastUpdateId":"RYDERO","lineItemNum":"1","referenceCode":"PP","referenceNum":"12"},{"createDt":1491344722000,"createId":"RYDERO","lastUpdateDt":1491344722000,"lastUpdateId":"RYDERO","lineItemNum":"1","referenceCode":"SORD","referenceNum":"84784745857"},{"createDt":1491344722000,"createId":"RYDERO","lastUpdateDt":1491344722000,"lastUpdateId":"RYDERO","lineItemNum":"1","referenceCode":"SORD","referenceNum":"test1"},{"createDt":1491344722000,"createId":"RYDERO","lastUpdateDt":1491344722000,"lastUpdateId":"RYDERO","lineItemNum":"1","referenceCode":"SORD","referenceNum":"test2"},{"createDt":1491344722000,"createId":"RYDERO","lastUpdateDt":1491344722000,"lastUpdateId":"RYDERO","lineItemNum":"1","referenceCode":"UNC","referenceNum":"11"}],"orderItemHistory":[],"enableLineCancel":"Y","type":"ORDER_ITEM","entityId":"1029020515","notAllocated":false,"allocated":true}],"attributes":[],"references":[{"createDt":1363531770000,"createId":"WWG","lastUpdateDt":1363531770000,"lastUpdateId":"WWG","referenceCode":"CUSP","referenceNum":"CUSP20130312411"},{"createDt":1363531770000,"createId":"WWG","lastUpdateDt":1363531770000,"lastUpdateId":"WWG","referenceCode":"DN","referenceNum":"DN20130312411"},{"createDt":1363531770000,"createId":"WWG","lastUpdateDt":1363531770000,"lastUpdateId":"WWG","referenceCode":"HAZ","referenceNum":"YES"},{"createDt":1363531770000,"createId":"WWG","lastUpdateDt":1363531770000,"lastUpdateId":"WWG","referenceCode":"MTYP","referenceNum":"SALE"},{"createDt":1363531770000,"createId":"WWG","lastUpdateDt":1414528501000,"lastUpdateId":"WWG","referenceCode":"OTYP","referenceNum":"SALE"},{"createDt":1363531770000,"createId":"WWG","lastUpdateDt":1363531770000,"lastUpdateId":"WWG","referenceCode":"PO","referenceNum":"PO20130312411"},{"createDt":1363531770000,"createId":"WWG","lastUpdateDt":1363531770000,"lastUpdateId":"WWG","referenceCode":"POSO","referenceNum":"POSO20130312411"},{"createDt":1414528713000,"createId":"WWG","lastUpdateDt":1414528713000,"lastUpdateId":"WWG","referenceCode":"PP","referenceNum":"10"},{"createDt":1363531770000,"createId":"WWG","lastUpdateDt":1363531770000,"lastUpdateId":"WWG","referenceCode":"REGN","referenceNum":"CANADA"},{"createDt":1363531770000,"createId":"WWG","lastUpdateDt":1363531770000,"lastUpdateId":"WWG","referenceCode":"SALG","referenceNum":"FIND"},{"createDt":1363531770000,"createId":"WWG","lastUpdateDt":1363531770000,"lastUpdateId":"WWG","referenceCode":"SORD","referenceNum":"SORD20130312411"},{"createDt":1363531770000,"createId":"WWG","lastUpdateDt":1363531770000,"lastUpdateId":"WWG","referenceCode":"TRAL","referenceNum":"DRY VAN"},{"createDt":1363531770000,"createId":"WWG","lastUpdateDt":1363531770000,"lastUpdateId":"WWG","referenceCode":"VEN","referenceNum":"TEKParent"}],"dates":[{"createDt":1363531770000,"createId":"RYDERO","lastUpdateDt":1363531770000,"lastUpdateId":"RYDERO","pk":{"orderComponentNum":1017649853,"dateTypeCd":"037"},"dateValue":1359525600000,"dateTmz":null,"dateConvention":null,"supplyOrder":null,"orderComponentNum":1017649853,"dateTypeCd":"037","id":null},{"createDt":1363531770000,"createId":"RYDERO","lastUpdateDt":1363531770000,"lastUpdateId":"RYDERO","pk":{"orderComponentNum":1017649853,"dateTypeCd":"063"},"dateValue":1359957600000,"dateTmz":null,"dateConvention":null,"supplyOrder":null,"orderComponentNum":1017649853,"dateTypeCd":"063","id":null}],"addresses":[{"createDt":1363531770000,"createId":"RYDERO","lastUpdateDt":1494028959000,"lastUpdateId":"RYDERO","pk":{"orderComponentNum":1017649853,"orderAddressTypeCd":19},"addrName1":"VP RACING FUELS INC","addrName2":null,"addrName3":null,"addr1":"7124 RICHTER ROAD","addr2":null,"city":"ELMENDORF","state":"TX","country":"USA","postalCode":"78112","entityId":"20001002","latitude":null,"longitude":null,"geoCodeOverride":null,"geoCodeCalc":null,"residential":null,"supplyOrder":null,"orderContacts":null,"id":{"orderComponentNum":1017649853,"orderAddressTypeCd":19},"orderComponentNum":1017649853,"orderAddressTypeCd":19},{"createDt":1363531770000,"createId":"RYDERO","lastUpdateDt":1494028959000,"lastUpdateId":"RYDERO","pk":{"orderComponentNum":1017649853,"orderAddressTypeCd":20},"addrName1":"WW GRAINGER ILLINOIS DC","addrName2":null,"addrName3":null,"addr1":"701 GRAINGER WAY","addr2":null,"city":"MINOOKA","state":"IL","country":"USA","postalCode":"60447","entityId":"005","latitude":null,"longitude":null,"geoCodeOverride":null,"geoCodeCalc":null,"residential":null,"supplyOrder":null,"orderContacts":null,"id":{"orderComponentNum":1017649853,"orderAddressTypeCd":20},"orderComponentNum":1017649853,"orderAddressTypeCd":20}],"enableReprocess":"Y","type":"SUPPLY_ORDER","entityId":"1017649853","statusDesc":"Fulfilled"}
		updatePODetails.update(orderSvc.module, $scope.po).then(function(resp){
			uiLoaderSvc.hide();
			$scope.close();
			uiModalSvc.show('ok-cancel-modal.tpl',{
				title:"Success",
				icon: "fa",
				text:"PO details updated.",
				callback:function(){
				}
			});

		}, function(err){
			uiLoaderSvc.hide();

			//User not authenticated (403) redirecting to login page
			if(err.status == 403){
				logoutService.logout();
			}
			uiModalSvc.show('ok-cancel-modal.tpl',{
				title:"Error",
				icon: "fa fa-exclamation-triangle",
				text:"Cannot update PO details at this time, please try again.",
				callback:function(){
				}
			});
		});

	}

	$scope.close=function(){
		uiModalSvc.hide();
	}
}]);
