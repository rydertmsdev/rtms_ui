<div class="form modal-small" style="display: inline-block;" ng-controller="ofNewHandlingUnitModalCtrl">
<form name="newHandlingUnitForm" ng-submit="accept(newHandlingUnitForm, newHandlingUnitForm.$valid, newHandlingUnitForm.input.$valid)">
	<ui-header theme="ui-modal-header">
		<ui-header-body>
			<div>
				<i class="fa fa-cube"></i>
				<span ng-if="args.mode=='edit'">Edit Handling Unit</span>
				<span ng-if="args.mode=='split'">Create Handling Unit and Split Item</span>
				<span ng-if="args.mode=='move'">Create Handling Unit and Move Item</span>
				<span ng-if="args.mode=='new'">New Handling Unit</span>
			</div>
		</ui-header-body>
		<ui-header-controls>
			<button ui-button title="Clear" ng-click="close()">
				<i class="fa fa-times"></i>
			</button>
		</ui-header-controls>
	</ui-header>
	<div class="modal-body">
		<!-- <div class="cols">
			<ui-field type="text" ng-model="handlingUnit.name" label="Handling Unit Name" ng-required="true" ng-disabled="false"></ui-field>
		</div> -->
		<ui-field type="select" placeholder="Handling Unit Look up" ng-model="handlingUnitName" prop="name" options="handlingUnitLists" class="no-label address-lookup" label="Look up" ng-required="false" ng-disabled="false" refresh="getHandlingUnits" refresh-args="{}" on-select="selectedHandlingUnit" selected-args="{}"></ui-field>
		<!-- <div class="cols" ng-repeat="twoItems in columns" ng-if="handlingUnit.quantityUom == 'PLT' || handlingUnit.quantityUom.code == 'PLT'">
			<div  ng-repeat="hu in twoItems" >	
				<ui-field type="text" ng-if="hu.displayType == 'freeform text' && hu.fieldName == 'description'" ng-model="handlingUnit[hu.fieldName]" label="{{hu.displayName}}" ng-required="hu.required()" ng-disabled="false"></ui-field>	
				<ui-field type="select" ng-if="hu.displayType == 'dropdown' && hu.fieldName == 'quantityUom'" options="containerTypesList" prop="name" ng-model="handlingUnit[hu.fieldName]" label="{{hu.displayName}}" ng-required="hu.required()" ng-disabled="hu.displayType == 'readonly' ? true : false"></ui-field>
			</div>
		</div>

		<div class="cols" ng-repeat="twoItems in columns" ng-if="handlingUnit.quantityUom != 'PLT' && handlingUnit.quantityUom.code != 'PLT'"> -->
		<div class="cols" ng-repeat="twoItems in columns">
			<div  ng-repeat="hu in twoItems" ng-init="handlingUnit[hu.fieldName] = handlingUnit[hu.fieldName] || ''">				
				<ui-field type="text" ng-if="hu.displayType == 'readonly'" options="hu.options" ng-model="handlingUnit[hu.fieldName]" label="{{hu.displayName}}" ng-disabled="true"></ui-field>
				<ui-field type="text" ng-if="hu.displayType == 'freeform text' && hu.fieldName != 'volume' && hu.fieldName != 'length' && hu.fieldName != 'weight' && hu.fieldName != 'height'" ng-model="handlingUnit[hu.fieldName]" label="{{hu.displayName}}" ng-required="hu.required()" ng-disabled="false"></ui-field>
				<ui-field type="text" ng-if="hu.displayType == 'freeform text' && hu.fieldName == 'volume'" ng-model="handlingUnit[hu.fieldName]" label="{{hu.displayName}}" ng-required="hu.required()" ng-disabled="true"></ui-field>
				<ui-field type="text" ng-if="hu.displayType == 'freeform text' && hu.fieldName == 'weight'" ng-model="handlingUnit[hu.fieldName]" label="{{hu.displayName}}" ng-required="hu.required()" ng-disabled="false" ng-change="resetFieldValue(this)"></ui-field>
				<ui-field type="text" ng-if="hu.displayType == 'freeform text' && hu.fieldName == 'length'" ng-model="handlingUnit[hu.fieldName]" label="{{hu.displayName}}" ng-required="hu.required()" ng-disabled="false" ng-change="resetFieldValue(this)"></ui-field>
				<ui-field type="text" ng-if="hu.displayType == 'freeform text' && hu.fieldName == 'height'" ng-model="handlingUnit[hu.fieldName]" label="{{hu.displayName}}" ng-required="hu.required()" ng-disabled="false" ng-change="resetFieldValue(this)"></ui-field>
				<ui-field type="select" ng-if="hu.displayType == 'dropdown' && hu.fieldName == 'quantityUom'" options="containerTypesList" prop="name" ng-model="handlingUnit[hu.fieldName]" label="{{hu.displayName}}" ng-required="hu.required()" ng-disabled="hu.displayType == 'readonly' ? true : false" ></ui-field>
				<ui-field type="select" ng-if="hu.displayType == 'dropdown' && hu.fieldName == 'dimensionsUom'" options="hu.options" prop="name" ng-model="handlingUnit[hu.fieldName]" label="{{hu.displayName}}" ng-required="hu.required()" ng-disabled="hu.displayType == 'readonly' ? true : false" ng-change="dimensionsChange()"></ui-field>
				<ui-field type="select" ng-if="hu.displayType == 'dropdown' && hu.fieldName != 'quantityUom' && hu.fieldName != 'dimensionsUom'" options="hu.options" prop="name" ng-model="handlingUnit[hu.fieldName]" label="{{hu.displayName}}" ng-required="hu.required()" ng-disabled="hu.displayType == 'readonly' ? true : false" ></ui-field>
				
			</div>
		</div>
		<div class="cols" ng-if="configList.OF_UI_HUNIT_SAVE_MASTER_DATA && configList.OF_UI_HUNIT_SAVE_MASTER_DATA == 'Y'">
			<ui-field type="toggle" class="no-label" ng-model="handlingUnit.saveToMasterTable" hover="Save data to master table" title="Save as master record" ng-required="false" ng-disabled="false"></ui-field>
		</div>		


<!-- 		<div class="cols">
			<ui-field type="text" ng-model="handlingUnit.length" label="Length" ng-required="false" ng-disabled="false"></ui-field>
			<ui-field type="text" ng-model="handlingUnit.width" label="Width" ng-required="false" ng-disabled="false"></ui-field>
		</div>
		<div class="cols">
			<ui-field type="text" ng-model="handlingUnit.height" label="Height" ng-required="false" ng-disabled="false"></ui-field>
			<ui-field type="select" options="uoms" ng-model="handlingUnit.uom" label="UOM" ng-required="false" ng-disabled="false"></ui-field>
		</div>
		<div class="cols">
			<ui-field type="text" ng-model="handlingUnit.weight" label="Weight" ng-required="false" ng-disabled="false"></ui-field>
		</div>
		<div class="cols form-condensed">
			<ui-field type="text" ng-model="handlingUnit.volume" label="Volume" ng-required="false" ng-disabled="true" placeholder="---"></ui-field>
			<ui-field type="text" ng-model="handlingUnit.volumeUom" label="Volume UOM" ng-required="false" ng-disabled="true"  placeholder="---"></ui-field>
		</div> -->
	</div>
	<ui-header theme="ui-modal-footer">
		<ui-header-controls>
			<button ui-button ng-click="close()" class="bg-gray">
				<span>Cancel</span>
			</button>
			<button ui-button type="submit" class="bg-accent" ng-disabled="!handlingUnit.name" >
				<span ng-if="args.mode=='edit'">Save</span>
				<span ng-if="args.mode=='split'">Create and Split</span>
				<span ng-if="args.mode=='move'">Create and Move</span>
				<span ng-if="args.mode=='new'">Create</span>
			</button>
		</ui-header-controls>
	</ui-header>
	</form>
</div>