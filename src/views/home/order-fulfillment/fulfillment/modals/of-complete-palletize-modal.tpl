<div class="modal-small" ng-controller="ofCompletePalletizeModalCtrl">
	<ui-header theme="ui-modal-header">
		<ui-header-body>
			<div>
				<i class="fa fa-info-circle"></i>
				<span>
					Palletizing Available
				</span>
			</div>
		</ui-header-body>
		<ui-header-controls>
			<button ui-button ng-click="close()">
				<i class="fa fa-times"></i>
			</button>
		</ui-header-controls>
	</ui-header>
	<div class="modal-body form">
			<div class="text">
				You may be able to reduce shipping costs by palletizing your shipment.
				If you choose not to palletize, this action will be logged and reviewed.
			</div>
			<ui-field type="select" ng-model="canPalletize" options="palletizeOptions" label="Is your location capable of palletizing this shipment?"></ui-field>
			<div class="cols" ng-if="canPalletize=='Yes'">
				<ui-field type="text" ng-model="palletPositions" label="Pallet Positions" ng-required="true" ng-disabled="false"></ui-field>
				<ui-field type="date" ng-model="readyOnDock" label="Ready on Dock" ng-required="true" ng-disabled="false"></ui-field>
			</div>
	</div>
	<ui-header theme="ui-modal-footer">
		<ui-header-controls ng-if="canPalletize=='Yes'">
			<button ui-button ng-click="close()" class="bg-gray">
				<span>Decline</span>
			</button>
			<button ui-button type="submit" ng-click="close()" class="bg-accent">
				<span>Accept</span>
			</button>
		</ui-header-controls>
		<ui-header-controls ng-if="canPalletize=='No'">
			<button ui-button type="submit" ng-click="close()" class="bg-accent">
				<span>Decline</span>
			</button>
		</ui-header-controls>
	</ui-header>
</div>