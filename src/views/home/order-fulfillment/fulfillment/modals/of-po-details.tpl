<div class="form modal-large" style="display: inline-block;" ng-controller="ofPoDetailsCtrl">
	<ui-header theme="ui-modal-header">
		<ui-header-body>
			<div class="skew" >
				<i class="fa fa-dollar"></i>
				<span>Purchase Order {{po.orderNumber}}</span>
			</div>
		</ui-header-body>
		<ui-header-controls class="inset">
			<button ui-button ng-click="poDetailsEditable=false;close()"><i class="fa fa-times"></i></button>
		</ui-header-controls>
	</ui-header>
	<div class="modal-body overflow-y">
		<div class="form" xng-class="{'form-condensed':!poDetailsEditable}">
			<div class="cols">
				<ui-field type="text" label="Order Number" ng-disabled="!poDetailsEditable" ng-model="po.orderNumber"></ui-field>
				<!-- <ui-field type="text" label="On-Dock Date" ng-disabled="!poDetailsEditable" ng-model="po.lines[0].executeFrom"></ui-field>
				<ui-field type="text" label="Supplier Number" ng-disabled="!poDetailsEditable" ng-model="po.supplierId"></ui-field>
				<ui-field type="text" label="Supplier Name" ng-disabled="!poDetailsEditable" ng-model="po.supplierName"></ui-field> -->
			</div>
			<!-- <div class="cols">
				<ui-field type="select" options="$parent.filters.psa.options" label="Premium Service Authorization" ng-disabled="!poDetailsEditable" ng-model="po.psa"></ui-field>
				<ui-field type="select" options="$parent.filters.sector.options" label="Sector" ng-disabled="!poDetailsEditable" ng-model="po.sector"></ui-field>
				<ui-field></ui-field>
				<ui-field></ui-field>
			</div> -->
			
			<div class="cols">
				<div class="rows">
					<ui-header theme="subsection">
						<ui-header-body>
							<div>
								<span>Origin</span>
							</div>
						</ui-header-body>
					</ui-header>
					<div ng-repeat="origin in po.addresses" ng-if="origin.orderAddressTypeCd == '19'">
						<div class="cols" >
							<ui-field type="text" label="Origin Id" ng-disabled="true" ng-model="origin.entityId"></ui-field>	
							<div></div>
						</div>
						<div class="cols" >
							<ui-field type="text" label="Name" ng-disabled="true" ng-model="origin.addrName1"></ui-field>	
							<ui-field type="text" label="Address" ng-disabled="true" ng-model="origin.addr1"></ui-field>	
						</div>
						<div class="cols">
							<ui-field type="text" label="City" ng-disabled="true" ng-model="origin.city"></ui-field>	
							<ui-field type="text" label="State or Province" ng-disabled="true" ng-model="origin.state"></ui-field>	
						</div>
						<div class="cols">
							<ui-field type="text" label="Postal Code" ng-disabled="true" ng-model="origin.postalCode"></ui-field>	
							<ui-field type="text" label="Country" ng-disabled="true" ng-model="origin.country"></ui-field>	
						</div>
					</div>
				</div>
				<div class="rows">
					<ui-header theme="subsection">
						<ui-header-body>
							<div>
								<span>Destination</span>
							</div>
						</ui-header-body>
					</ui-header>
					<div ng-repeat="destination in po.addresses" ng-if="destination.orderAddressTypeCd == '20'">
						<div class="cols" >
							<ui-field type="text" label="Destination Id" ng-disabled="true" ng-model="destination.entityId"></ui-field>	
							<div></div>
						</div>
						<div class="cols">
							<ui-field type="text" label="Name" ng-disabled="true" ng-model="destination.addrName1"></ui-field>	
							<ui-field type="text" label="Address" ng-disabled="true" ng-model="destination.addr1"></ui-field>	
						</div>
						<div class="cols">
							<ui-field type="text" label="City" ng-disabled="true" ng-model="destination.city"></ui-field>	
							<ui-field type="text" label="State or Province" ng-disabled="true" ng-model="destination.state"></ui-field>	
						</div>
						<div class="cols">
							<ui-field type="text" label="Postal Code" ng-disabled="true" ng-model="destination.postalCode"></ui-field>	
							<ui-field type="text" label="Country" ng-disabled="true" ng-model="destination.country"></ui-field>	
						</div>
					</div>
				</div>
			</div>
			<ui-header theme="subsection">
				<ui-header-body>
					<div>
						<span>References</span>
					</div>
				</ui-header-body>
			</ui-header>
			<div class="badges" style="margin: 10px 0;">
				<div ng-repeat="refCL in ReferenceList">
					<div class="badge" ng-repeat="ref in po.references" ng-if="refCL.code == ref.referenceCode">
						<span>{{refCL.name}}</span>
						<i class="inset">
							{{ref.referenceNum}}
						</i>
					</div>
				</div>
			</div>
			<ui-header theme="subsection">
				<ui-header-body>
					<div>
						<span>Item Details</span>
					</div>
				</ui-header-body>
			</ui-header>
			<div class="item" ng-repeat="item in po.orderItems" ng-show= "item.orderComponentNum == selectedRootOrder">
				<div class="cols">
					<ui-field type="text" label="Part Number" ng-disabled="true" ng-model="item.itemNumber"></ui-field>
					<ui-field type="text" label="Item Description" ng-disabled="true" ng-model="item.description"></ui-field>
					<!-- <ui-field type="text" label="Terms" ng-disabled="true" ng-model="po.incoTerms"></ui-field> -->
					<div></div>
					<div></div>
				</div>
				<div class="cols" ng-repeat="fourColumns in PODetailsRolViewListColumns">
					<div ng-repeat="column in fourColumns">
						<ui-field type="text" ng-model="item[column.fieldName]" label="{{column.displayName}}" ng-disabled="true"></ui-field>
						<!-- <ui-field type="text" ng-if="column.displayType == 'freeform text'" ng-model="item[column.fieldName]" label="{{column.displayName}}" ng-required="false" ng-disabled="true"></ui-field>
						<ui-field type="select" ng-if="column.displayType == 'dropdown'" options="column.options" prop="name" ng-model="item[column.fieldName]" label="{{column.displayName}}" ng-required="false" ng-disabled="true" ></ui-field> -->
					</div>
				</div>
				<ui-header theme="subsectionNoBackground">
					<ui-header-body>
						<div>
							<span>Item References:</span>
						</div>
					</ui-header-body>
				</ui-header>
				<div class="" style="margin-top: 0px;">					
					<div class="cols" ng-repeat="twoColumns in item.columns" >
						<div ng-repeat="ref in twoColumns">
							<div ng-repeat="refCL in OrderItemReferenceList" ng-if="ref.referenceCode == refCL.code">
								<ui-field type="text" ng-if="refCL.displayType == 'readonly'" ng-model="ref.referenceNum" label="{{refCL.name}}" ng-disabled="true"></ui-field>
								<ui-field type="text" ng-if="refCL.displayType == 'freeform text'" ng-model="ref.referenceNum" label="{{refCL.name}}" ng-required="false" ng-disabled="!poDetailsEditable"></ui-field>
								<ui-field type="select" ng-if="refCL.displayType == 'dropdown'" options="refCL.qualifierCodeValues" prop="name" ng-model="ref.referenceNum" label="{{refCL.name}}" ng-required="false" ng-disabled="!poDetailsEditable" ></ui-field>
								<ui-field ng-if="refCL.displayType=='single checkbox'" type="toggle" class="no-label" ng-model="ref.referenceNum" hover="{{refCL.name}}" title="{{refCL.name}}" ng-required="false" ng-disabled="!poDetailsEditable"></ui-field>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div style="padding: 10px">
				<button ui-button="" type="submit" ng-click="showItems()" class="bg-accent">
					<span ng-show="!showItemsFlag">Show more line items</span>
					<span ng-show="showItemsFlag">Show fewer line items</span>
				</button>
			</div>

			<div class="item" ng-repeat="item in po.orderItems" ng-show="item.orderComponentNum != selectedRootOrder && showItemsFlag">
				<div class="cols">
					<ui-field type="text" label="Part Number" ng-disabled="true" ng-model="item.itemNumber"></ui-field>
					<ui-field type="text" label="Item Description" ng-disabled="true" ng-model="item.description"></ui-field>
					<div></div>
					<div></div>
				</div>
				<div class="cols" ng-repeat="fourColumns in PODetailsRolViewListColumns">
					<div ng-repeat="column in fourColumns">
						<ui-field type="text" ng-model="item[column.fieldName]" label="{{column.displayName}}" ng-disabled="true"></ui-field>
						<!-- <ui-field type="text" ng-if="column.displayType == 'freeform text'" ng-model="item[column.fieldName]" label="{{column.displayName}}" ng-required="false" ng-disabled="true"></ui-field>
						<ui-field type="select" ng-if="column.displayType == 'dropdown'" options="column.options" prop="name" ng-model="item[column.fieldName]" label="{{column.displayName}}" ng-required="false" ng-disabled="true" ></ui-field> -->
					</div>
				</div>
				<ui-header theme="subsectionNoBackground">
					<ui-header-body>
						<div>
							<span>Item References:</span>
						</div>
					</ui-header-body>
				</ui-header>
				<div class="" style="margin-top: 0px;">					
					<div class="cols" ng-repeat="twoColumns in item.columns" >
						<div ng-repeat="ref in twoColumns">
							<div ng-repeat="refCL in OrderItemReferenceList" ng-if="ref.referenceCode == refCL.code">
								<ui-field type="text" ng-if="refCL.displayType == 'readonly'" ng-model="ref.referenceNum" label="{{refCL.name}}" ng-disabled="true"></ui-field>
								<ui-field type="text" ng-if="refCL.displayType == 'freeform text'" ng-model="ref.referenceNum" label="{{refCL.name}}" ng-required="false" ng-disabled="!poDetailsEditable"></ui-field>
								<ui-field type="select" ng-if="refCL.displayType == 'dropdown'" options="refCL.qualifierCodeValues" prop="name" ng-model="ref.referenceNum" label="{{refCL.name}}" ng-required="false" ng-disabled="!poDetailsEditable" ></ui-field>
								<ui-field ng-if="refCL.displayType=='single checkbox'" type="toggle" class="no-label" ng-model="ref.referenceNum" hover="{{refCL.name}}" title="{{refCL.name}}" ng-required="false" ng-disabled="!poDetailsEditable"></ui-field>
							</div>
						</div>
					</div>
				</div>
				<hr>
			</div>
		</div>




	</div>
	<ui-header theme="ui-modal-footer">
		<ui-header-controls>
			<button ui-button class="bg-gray" ng-class="{'active':poDetailsEditable}" ng-click="poDetailsEditable=!poDetailsEditable" ng-show="!poDetailsEditable" permission="fulfillment.po.details.edit">
				<span>Edit</span>
			</button>
			<button ui-button class="bg-gray" ng-click="poDetailsEditable=!poDetailsEditable" ng-show="poDetailsEditable">
				<span>Cancel</span>
			</button>
			<button ui-button class="bg-accent" ng-click="poDetailsEditable=!poDetailsEditable; updatePoDetails()" ng-show="poDetailsEditable">
				<span>Save</span>
			</button>
			<button ui-button type="submit" ng-click="close()" class="bg-accent" ng-hide="poDetailsEditable">
				<span>Close</span>
			</button>
		</ui-header-controls>
	</ui-header>
</div>