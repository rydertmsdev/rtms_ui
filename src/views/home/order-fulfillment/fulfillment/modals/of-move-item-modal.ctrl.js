App.controller('ofMoveItemModalCtrl',['$scope', '$timeout', 'uiModalSvc', 'orderSvc', function($scope, $timeout, uiModalSvc, orderSvc){	
	$scope.modalOptions=orderSvc.deliveryOrders[$scope.args.dIndex].shipmentObject.handlingUnits;
	$scope.targetHandlingUnit=orderSvc.deliveryOrders[$scope.args.dIndex].shipmentObject.$$lastTargetHandlingUnit;

	$scope.palletTypeFlag = orderSvc.palletTypeFlag;

	$scope.close=function(){
		uiModalSvc.hide();
	}
	$scope.moveItem=function(){
		orderSvc.moveItem($scope.args.dIndex,$scope.args.hIndex,$scope.args.index,$scope.targetHandlingUnit)
		$scope.close();
	}
	$scope.newHandlingUnit=function(){
		uiModalSvc.show('of-new-handling-unit-modal.tpl',{
			dIndex:dIndex,
			sourceHIndex:$scope.args.hIndex,
			sourceIndex:$scope.args.index,
			mode:'move'
		});	
	}
}]);
