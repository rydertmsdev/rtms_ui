<div class="modal-small" ng-controller="ofMoveItemModalCtrl">
	<ui-header theme="ui-modal-header">
		<ui-header-body>
			<div>
				<i class="fa fa-arrow-right"></i>
				<span>
					Move Item
				</span>
			</div>
		</ui-header-body>
		<ui-header-controls>
			<button ui-button title="Clear" ng-click="close()">
				<i class="fa fa-times"></i>
			</button>
		</ui-header-controls>
	</ui-header>
	<div class="modal-body">
		<form class="form">
			<ui-field type="select" prop="name" options="modalOptions" ng-model="targetHandlingUnit" label="Handling Unit" ng-required="true" ng-disabled="false"></ui-field>
		</form>
	</div>
	<ui-header theme="ui-modal-footer">
		<ui-header-body>
			<span>
				<button ui-button ng-click="newHandlingUnit()" class="bg-white">
					<i class="fa fa-plus"></i>
					<span>New Handling Unit</span>
				</button>
			</span>
		</ui-header-body>
		<ui-header-controls>
			<button ui-button ng-click="close()" class="bg-gray">
				<span>Cancel</span>
			</button>
			<button ui-button type="submit" ng-click="moveItem()" class="bg-accent" ng-disabled="!targetHandlingUnit">
				<span>Move</span>
			</button>
		</ui-header-controls>
	</ui-header>
</div>