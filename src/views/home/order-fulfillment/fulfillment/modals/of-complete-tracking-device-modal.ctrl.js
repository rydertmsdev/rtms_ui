App.controller('ofCompleteTrackingDeviceModalCtrl',['$scope', '$timeout', 'uiModalSvc', 'orderSvc', function($scope, $timeout, uiModalSvc, orderSvc){	

	$scope.hIndex = $scope.args.hIndex;
	$scope.order=orderSvc.selectedDelivery();
	$scope.trackingDeviceId = orderSvc.selectedDelivery().shipmentObject.handlingUnits[$scope.hIndex].trackingId;

	$scope.close=function(){
		uiModalSvc.hide();
	}

	$scope.save=function(){
		$scope.order.shipmentObject.handlingUnits[$scope.hIndex]["trackingId"] = $scope.trackingDeviceId;
		uiModalSvc.hide();
	}

}]);