App.controller('ofSerializationModalCtrl',['$scope', '$timeout', 'uiModalSvc', 'orderSvc', function($scope, $timeout, uiModalSvc, orderSvc){


	$scope.close=function(){
		$scope.errorFlag = false;
		uiModalSvc.hide();
	}

	$scope.errorFlag = false;
	$scope.requiredFlag = false;

	$scope.item=orderSvc.deliveryOrders[$scope.args.dIndex].shipmentObject.handlingUnits[$scope.args.hIndex].items[$scope.args.index];
	// if (!$scope.item.serialization){
		$scope.item.serialization=new Array(parseInt($scope.item.quantity));
	// }

	if($scope.item && $scope.item.references){
		angular.forEach($scope.item.references, function(ref, rIndex){
			if(ref.referenceCode == "SER"){
				$scope.requiredFlag = true;
			}
		});
	}

	$scope.save=function(flag){
		if(flag){
			if(!hasDuplicates($scope.item.serArray)){
				// angular.forEach($scope.item.serArray, function(ser, index){
				// 	var testObj = {};
				// 	testObj["referenceCode"] = "SER";
				// 	testObj["referenceNum"] = ser;
				// 	orderSvc.deliveryOrders[$scope.args.dIndex].shipmentObject.handlingUnits[$scope.args.hIndex].items[$scope.args.index].references.push(testObj);
				// });
				$scope.close();
			}
			else{
				$scope.errorFlag = true;
				$(".modal-small").scrollTop(0);
			}
		}
		else{
			$(".modal-small").scrollTop(0);
		}
	}

	function hasDuplicates(array) {
	    var valuesSoFar = Object.create(null);
	    for (var i = 0; i < array.length; ++i) {
	        var value = array[i];
	        if (value in valuesSoFar) {
	            return true;
	        }
	        valuesSoFar[value] = true;
	    }
	    return false;
	}

}]);
