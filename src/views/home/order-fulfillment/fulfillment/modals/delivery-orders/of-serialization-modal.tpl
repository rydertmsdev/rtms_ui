

<div class="form modal-small" ng-controller="ofSerializationModalCtrl">

	<ui-header theme="ui-modal-header">
		<ui-header-body>
			<div>
				<span>Serialization</span>
			</div>
		</ui-header-body>
		<ui-header-controls>
			<div ui-button title="Clear" ng-click="close()">
				<i class="fa fa-times"></i>
			</div>
		</ui-header-controls>
	</ui-header>
	<form ui-width="100%" id="serializationForm" name="serialization">
		<div class="modal-body overflow-y">

			<ui-list class="static">		
				<cell class="ui-alert-body" ng-if="requiredFlag" ui-width="100%">
					<i class="fa text-accent fa-info"></i>
					<span class="text">
						All serialization codes are mandatory.
					</span>
				</cell>	
				<cell class="ui-alert-body" ng-if="errorFlag" ui-width="100%">
					<i class="fa text-accent fa-exclamation-triangle"></i>
					<span class="text">
						Please check for duplicate's and retry saving.
					</span>
				</cell>
				<ui-list-header>
					<li ui-width="54px" class="text-center">
						<span>Index</span>
					</li>
					<li>		
						<span>Serial Number</span>
					</li>
				</ui-list-header>
				<ui-list-body>
					<li ng-repeat="(index,ser) in item.serialization track by $index" style="min-height: 54px;">
						<ui-list-content>
							<cell class="media-tablet-hide text-center" ui-width="54px">
								{{index + 1}}
							</cell>
							<cell class="text-center">
								<label class="media-tablet-show">{{index + 1}}</label>
								<ui-field type="text" ng-model="item.serArray[$index]" class="inline" ng-required="requiredFlag"></ui-field>
							</cell>
						</ui-list-content>
					</li>
				</ui-list-body>
			</ui-list>
		</div>
		<ui-header theme="ui-modal-footer">
			<ui-header-controls>
				<div ui-button ng-click="close()" class="bg-gray">
					<span>Cancel</span>
				</div>
				<button ui-button type="submit" ng-click="save(serialization.$valid)" class="bg-accent">
					<span>Save</span>
				</button>
			</ui-header-controls>
		</ui-header>
	</form>
</div>
