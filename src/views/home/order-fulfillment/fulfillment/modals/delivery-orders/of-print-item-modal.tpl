<div class="form modal-small" ng-controller="ofPrintItemModalCtrl">
	<ui-header theme="ui-modal-header">
		<ui-header-body>
			<div>
				<span>Print Item</span>
			</div>
		</ui-header-body>
		<ui-header-controls>
			<div ui-button title="Clear" ng-click="close()">
				<i class="fa fa-times"></i>
			</div>
		</ui-header-controls>
	</ui-header>
	<div class="modal-body">
		<ui-field type="select" label="Print Size" options="printOptions" ng-model="printOption" class="inline"></ui-field>
	</div>
	<ui-header theme="ui-modal-footer">
		<ui-header-controls>
			<div ui-button ng-click="close()" class="bg-gray">
				<span>Cancel</span>
			</div>
			<div ui-button type="submit" ng-click="close()" class="bg-accent">
				<span>Print</span>
			</div>
		</ui-header-controls>
	</ui-header>
</div>