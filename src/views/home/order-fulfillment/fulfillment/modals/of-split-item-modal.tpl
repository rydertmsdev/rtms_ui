<div class="modal-small" style="display: inline-block;" ng-controller="ofSplitItemModalCtrl">
	<ui-header theme="ui-modal-header">
		<ui-header-body>
			<div>
				<i class="fa fa-share-alt"></i>
				<span>
					Split Item
				</span>
			</div>
		</ui-header-body>
		<ui-header-controls>
			<button ui-button title="Clear" ng-click="close()">
				<i class="fa fa-times"></i>
			</button>
		</ui-header-controls>
	</ui-header>
	<div class="modal-body form">
		<div class="cols" ng-if="palletTypeFlag">
			<ui-field type="toggle" ng-model="splitToOwn" label="Own Handling Units" title="Split this item into individual handling units" ng-required="false" ng-disabled="false"></ui-field>
		</div>
		<div class="cols" ng-hide="!!splitToOwn">
			<ui-field type="text" ng-model="targetQuantity" label="Quantity" ng-required="true" ng-disabled="!!splitToOwn"></ui-field>
			<ui-field type="select" prop="name" options="modalOptions" ng-model="targetHandlingUnit" label="Handling Unit" ng-required="true" ng-disabled="!!splitToOwn"></ui-field>
		</div>
	</div>
	<ui-header theme="ui-modal-footer">
		<ui-header-body>
			<span>
				<button ui-button ng-click="newHandlingUnit()" class="bg-white" ng-disabled="!targetQuantity" ng-hide="!!splitToOwn">
					<i class="fa fa-plus"></i>
					<span>New Handling Unit</span>
				</button>
			</span>
		</ui-header-body>
		<ui-header-controls>
			<button ui-button ng-click="close()" class="bg-gray">
				<span>Cancel</span>
			</button>
			<button ui-button type="submit" ng-click="splitItem()" class="bg-accent" ng-disabled="(!targetHandlingUnit||!targetQuantity)&&!splitToOwn">
				<span>Split</span>
			</button>
		</ui-header-controls>
	</ui-header>
</div>