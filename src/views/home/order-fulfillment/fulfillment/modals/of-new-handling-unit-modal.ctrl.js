App.controller('ofNewHandlingUnitModalCtrl',['$scope', '$timeout', 'uiModalSvc', 'uiLoaderSvc', 'orderSvc', 'codeListService', '$state', 'SaveHandlingUnit', '$rootScope', 'handlingUnitLookUpService', function($scope, $timeout, uiModalSvc, uiLoaderSvc, orderSvc, codeListService, $state, SaveHandlingUnit, $rootScope, handlingUnitLookUpService){
	$scope.handlingUnitTypes=["Box","Pallet","Container","Other"];
	$scope.uoms=["Feet","Inches","Meters","Centimeters"];
	$scope.configList;
	$scope.handlingUnitLists = [{"code":"", "name": ""}];
	$scope.handlingUnitName = "";

	$scope.getHandlingUnits=function(text, args){
		if(text.length > 2){

			var requestObj ={};

			requestObj["name"] = text;

			uiLoaderSvc.show();
			handlingUnitLookUpService.getResults(orderSvc.module, requestObj).then(function(resp){
				uiLoaderSvc.hide();

				if(resp.data && resp.data.content){
					$scope.handlingUnitLists = resp.data.content;
				}			
			},
			function(data){
				uiLoaderSvc.hide();
				console.log("Service Error - ", data);

				//User not authenticated (403) redirecting to login page
				if(data.status == 403){
					logoutService.logout();
				}
			});
		}
	}

	$scope.selectedHandlingUnit=function(item, args) {
		angular.forEach($scope.handlingUnitFieldsArray, function(hField, hInd){
			$scope.handlingUnit[hField.fieldName] = item[hField.fieldName];
		});
		$scope.handlingUnit["description"] = item.name;
		$scope.handlingUnit["weight"] = item.netWeight;
		$scope.handlingUnit["id"] = item.id;
	}

	$scope.resetFieldValue=function(event){
		if(isNaN($scope.handlingUnit.weight)){
			$scope.handlingUnit.weight = "";
		}
		else if(isNaN($scope.handlingUnit.height)){
			$scope.handlingUnit.height = "";
		}
		else if(isNaN($scope.handlingUnit.length)){
			$scope.handlingUnit.length = "";
		}
	}

	if(codeListService.codeLists && codeListService.codeLists.CustomerConfigList[0]){
		$scope.configList = codeListService.codeLists.CustomerConfigList[0];
	}

	$scope.vUoms={
		"Feet":"Cubic Feet",
		"Inches":"Cubic Inches",
		"Meters":"Cubic Meters",
		"Centimeters":"Cubic Centimeters",
	}
	$scope.containerTypesList = [];
	if(orderSvc && orderSvc.containerTypesList){
		angular.forEach(orderSvc.containerTypesList, function(item, i) {
			$scope.containerTypesList.push(item);
		});
	}

	angular.forEach($scope.containerTypesList, function(item, i) {
		if(item.defaultValue && orderSvc.deliveryOrders[dIndex].shipmentObject.handlingUnits[$scope.args.hIndex] && !orderSvc.deliveryOrders[dIndex].shipmentObject.handlingUnits[$scope.args.hIndex].quantityUom){
			orderSvc.deliveryOrders[dIndex].shipmentObject.handlingUnits[$scope.args.hIndex].quantityUom = item.code;
		}
	});

	$scope.configList;

	if(codeListService.codeLists && codeListService.codeLists.CustomerConfigList[0]){
		$scope.configList = codeListService.codeLists.CustomerConfigList[0];
	}

	// var configContainerType = $scope.configList.OF_UI_CONTAINER_TYPES_UNMIXABLE;

	$scope.handlingUnitFieldsArray = [];

	if ($scope.args.mode=='move'){
		$scope.handlingUnit={
			name:"Handling Unit " + (orderSvc.deliveryOrders[dIndex].shipmentObject.handlingUnits.length+1) + " - " +orderSvc.selectedDelivery().shipmentObject.handlingUnits[$scope.args.sourceHIndex].items[$scope.args.sourceIndex].description
		};

		if(codeListService.codeLists){
			$scope.handlingUnitFieldsArray = codeListService.codeLists.OrderDetailsHandlingUnitsRolViewList;
		}

		assignScopeValues($scope.handlingUnit.name);
		$scope.handlingUnit.quantityUom = orderSvc.deliveryOrders[dIndex].shipmentObject.handlingUnits[$scope.args.sourceHIndex].quantityUom;

	}	
	else if ($scope.args.mode=='split'){
		
		if(codeListService.codeLists){
			$scope.handlingUnitFieldsArray = codeListService.codeLists.OrderDetailsHandlingUnitsRolViewList;
		}

		$scope.handlingUnit={
			name:"Handling Unit " + (orderSvc.deliveryOrders[dIndex].shipmentObject.handlingUnits.length+1) + " - " +orderSvc.selectedDelivery().shipmentObject.handlingUnits[$scope.args.sourceHIndex].items[$scope.args.sourceIndex].description,
		};

		assignScopeValues($scope.handlingUnit.name);
		$scope.handlingUnit.quantityUom = orderSvc.deliveryOrders[dIndex].shipmentObject.handlingUnits[$scope.args.sourceHIndex].quantityUom;

	}
	else if ($scope.args.mode=='edit'){
		if(codeListService.codeLists){
			$scope.handlingUnitFieldsArray = codeListService.codeLists.OrderDetailsHandlingUnitsRolViewList;
		}
		
		$scope.handlingUnit=angular.copy(orderSvc.deliveryOrders[$scope.args.dIndex].shipmentObject.handlingUnits[$scope.args.hIndex]);
		$scope.handlingUnit.description = orderSvc.deliveryOrders[dIndex].shipmentObject.handlingUnits[$scope.args.hIndex].description || orderSvc.deliveryOrders[dIndex].shipmentObject.handlingUnits[$scope.args.hIndex].name;

	}
	else if ($scope.args.mode=='new'){
		$scope.handlingUnit={
			name:"Handling Unit " + (orderSvc.deliveryOrders[dIndex].shipmentObject.handlingUnits.length+1)
		};

		assignScopeValues($scope.handlingUnit.name);

		if(codeListService.codeLists){
			$scope.handlingUnitFieldsArray = codeListService.codeLists.OrderDetailsHandlingUnitsRolViewList;
		}
	}
	
	$scope.handlingUnit.saveToMasterTable = true;

	function assignScopeValues(name){
		$scope.handlingUnit.description = name;
		var huConfigTypeFlag = false;

		// angular.forEach(orderSvc.deliveryOrders[dIndex].shipmentObject.handlingUnits, function(hu, index) {
		// 	if(!!configContainerType && hu.quantityUom && hu.quantityUom.match(RegExp(configContainerType))){
		// 		huConfigTypeFlag = true;
		// 	}
		// });

		// configContainerType = configContainerType.split("|");

		// if(configContainerType && huConfigTypeFlag){
		// 	$scope.handlingUnit.quantityUom = configContainerType[0];
		// }
		// else{
		// 	angular.forEach($scope.containerTypesList, function(item, i) {
		// 		if(item.defaultValue){
		// 			$scope.handlingUnit.quantityUom = item.code;
		// 		}
		// 	});
		// }
	}

	function columnize(input, cols, fieldLength) {
	  	var arr = [];
	  	for(i = 0; i < input.length; i++) {
		  	if(input[i].required == "Y" || input[i].required == true){
		  		input[i].required = function(){return true};
		  	}
		  	else if(input[i].required == "N" || input[i].required == false || !(input[i].required)){
		  		input[i].required = function(){return false};
		  	}
		    var colIdx = Math.floor(i / fieldLength);
	    	arr[colIdx] = arr[colIdx] || [];
	    	arr[colIdx].push(input[i]);	    
	  	}

	    for(j=0;j<arr.length;j++){
	  		if(arr[j].length < fieldLength){
	  			var tempVal = fieldLength - arr[j].length;
	  			for(var k=0; k<tempVal; k++){
	  				arr[j].push({});
	  			}
	  		}
	  	}

	  return arr;
	}

	$scope.huDependencyList = [];
	$scope.dependencyList = [];

	if(codeListService.codeLists && codeListService.codeLists.FieldDependencyList){
		$scope.dependencyList = codeListService.codeLists.FieldDependencyList;
	}

	angular.forEach($scope.dependencyList, function(list, index) {
		if(list.component == "OF_HUNIT"){
			$scope.huDependencyList = list.parents;
		}
	});

	// angular.forEach($scope.huDependencyList, function(item, index) {
	// 	var mapping = {};
	// 	if(item.regexChildrenMap){
	// 		mapping = item.regexChildrenMap;
	// 	}
		
	// 	var regex = "";
	// 	$scope.processFieldsArr = [];

	// 	angular.forEach(mapping, function(value, key) {
	// 	 	regex = key;
	// 	 	$scope.processFieldsArr = value;
	// 	});

	// 	var code = item.code;

	// 	angular.forEach($scope.handlingUnitFieldsArray, function(huField, i) {

	// 		angular.forEach($scope.processFieldsArr, function(depField, j) {
	// 			if(huField.fieldName == depField.code){
	// 				huField.fieldType = depField.type;
	// 				huField.required = function(){
	// 					if(!regex || !$scope.handlingUnit[code]){
	// 						return false;
	// 					}
	// 					if(angular.isObject($scope.handlingUnit[code])){
	// 						return RegExp(regex).test($scope.handlingUnit[code].code);
	// 					}
	// 					else{
	// 						return RegExp(regex).test($scope.handlingUnit[code]);
	// 					}
	// 				};
	// 				if(depField.disabled){
	// 					if(huField.displayType == "readonly"){
	// 						huField.displayType = "freeform text";
	// 					}								
	// 				}
	// 				else{
	// 					huField.displayType == "readonly"
	// 				}							
	// 			}
	// 		});
	// 	});
	// });

	$scope.columns = columnize($scope.handlingUnitFieldsArray, Math.ceil($scope.handlingUnitFieldsArray.length/2), 2);

	// var removePalletFromListFlag = false;
	// var removePalletFromListIndex;

	// if($scope.handlingUnit.quantityUom != "PLT" && (orderSvc.selectedDelivery().handlingUnits.length > 1 || $scope.args.hIndex != 0)){
	// 	angular.forEach($scope.containerTypesList, function(option, index) {
	// 		if(option.code == 'PLT'){
	// 			removePalletFromListFlag = true;
	// 			removePalletFromListIndex = index;
	// 		}
	// 	});

	// 	if(removePalletFromListFlag && (removePalletFromListIndex > -1)){
	// 		$scope.containerTypesList.splice(removePalletFromListIndex, 1);
	// 	}
	// }

	$scope.$watch(function(){
		return $scope.handlingUnit.length*$scope.handlingUnit.width*$scope.handlingUnit.height
	},function(val){
		$scope.handlingUnit.volume=val;
	});	

	$scope.$watch(function(){
		return $scope.handlingUnit.uom;
	},function(val){
		$scope.handlingUnit.volumeUom=$scope.vUoms[val];
	});

	$scope.close=function(){
		uiModalSvc.hide();
	}

	$scope.accept=function(flag){
		if(flag){
			if ($scope.args.mode=='move'){
				var newHandlingUnit=orderSvc.addHandlingUnit($scope.args.dIndex,$scope.handlingUnit)-1;
				orderSvc.moveItem($scope.args.dIndex,$scope.args.sourceHIndex,$scope.args.sourceIndex,orderSvc.deliveryOrders[dIndex].shipmentObject.handlingUnits[newHandlingUnit])
			}	
			else if ($scope.args.mode=='split'){
				var newHandlingUnit=orderSvc.addHandlingUnit($scope.args.dIndex,$scope.handlingUnit)-1;
				orderSvc.splitItem($scope.args.dIndex,$scope.args.sourceHIndex,$scope.args.sourceIndex,orderSvc.deliveryOrders[dIndex].shipmentObject.handlingUnits[newHandlingUnit], $scope.args.targetQuantity)
			}
			else if ($scope.args.mode=='edit'){
				// orderSvc.updateHandlingUnit($scope.args.dIndex,$scope.args.hIndex,$scope.handlingUnit);

				angular.forEach($scope.handlingUnitFieldsArray, function(name, index) {
					if(name.displayType == "freeform text" && $scope.handlingUnit[name.fieldName]){
						if(name.fieldName == 'description'){
							orderSvc.deliveryOrders[dIndex].shipmentObject.handlingUnits[$scope.args.hIndex][name.fieldName] =	$scope.handlingUnit[name.fieldName];
							orderSvc.deliveryOrders[dIndex].shipmentObject.handlingUnits[$scope.args.hIndex]['name'] =	$scope.handlingUnit[name.fieldName];
						}
						else{
							orderSvc.deliveryOrders[dIndex].shipmentObject.handlingUnits[$scope.args.hIndex][name.fieldName] =	$scope.handlingUnit[name.fieldName];
						}
					}
					else if (name.displayType == "dropdown" && $scope.handlingUnit[name.fieldName]){
						if(angular.isObject($scope.handlingUnit[name.fieldName]))
							orderSvc.deliveryOrders[dIndex].shipmentObject.handlingUnits[$scope.args.hIndex][name.fieldName] =	$scope.handlingUnit[name.fieldName].code;
						else
							orderSvc.deliveryOrders[dIndex].shipmentObject.handlingUnits[$scope.args.hIndex][name.fieldName] =	$scope.handlingUnit[name.fieldName];
					}
					else{
						//TODO: For any other display types
					}
				});
			}
			else if ($scope.args.mode=='new'){
				orderSvc.addHandlingUnit($scope.args.dIndex,$scope.handlingUnit);

			}

			$scope.huObj = orderSvc.selectedDelivery().shipmentObject.handlingUnits;
			orderSvc.selectedDelivery().totalWeight = 0;

			orderSvc.palletTypeFlag = true;
			orderSvc.selectedDelivery().totalWeight = 0;
			orderSvc.selectedDelivery().totalVolume = 0;

			angular.forEach($scope.huObj, function(hu,index) {
				if(!!hu.weight){
					orderSvc.selectedDelivery().totalWeight += parseInt(orderSvc.selectedDelivery().shipmentObject.handlingUnits[index].weight);
				}

				if(!!hu.volume){
					orderSvc.selectedDelivery().totalVolume += parseInt(orderSvc.selectedDelivery().shipmentObject.handlingUnits[index].volume);
				}

				// if(hu.quantityUom == "PLT"){
				// 	orderSvc.palletTypeFlag = false;
				// 	orderSvc.selectedDelivery().totalWeight = "";
				// 	orderSvc.selectedDelivery().totalVolume = "";
				// }
			});	

			// if($scope.handlingUnit && $scope.handlingUnit["quantityUom"]){
			// 	if(angular.isObject($scope.handlingUnit["quantityUom"]) && angular.isObject($scope.handlingUnit["quantityUom"]).code == "PLT"){

			// 	}
			// 	else{

			// 	}
			// }

			// orderSvc.selectedDelivery().totalWeight += parseInt($scope.handlingUnit.weight);
			if($scope.handlingUnit.saveToMasterTable){
				if(!!orderSvc.selectedDelivery().origin.entityId){
					var reqObj = {};

					reqObj["customernum"] = $rootScope.userProfile.data.content.customerNum || "",
					reqObj["id"] = $scope.handlingUnit.id || "";
					reqObj["vendorCd"] = orderSvc.selectedDelivery().origin.entityId;
					reqObj["name"] = $scope.handlingUnit.description.code || $scope.handlingUnit.description || "";
					reqObj["netWeight"] = $scope.handlingUnit.weight || "";

					angular.forEach($scope.handlingUnitFieldsArray, function(hField, hInd){
						if(hField.displayType == "dropdown" && angular.isObject($scope.handlingUnit[hField.fieldName])){
							reqObj[hField.fieldName] = $scope.handlingUnit[hField.fieldName].code;
							if(hField.fieldName == "quantityUom"){
								reqObj["typeCode"] = $scope.handlingUnit.quantityUom.code || "PLT";
							}
						}else{
							reqObj[hField.fieldName] = $scope.handlingUnit[hField.fieldName];
							if(hField.fieldName == "quantityUom"){
								reqObj["typeCode"] = $scope.handlingUnit.quantityUom || "PLT";
							}
						}
					});

					uiLoaderSvc.show();

					SaveHandlingUnit.create("order", reqObj).then(function(resp){
						uiLoaderSvc.hide();
						uiModalSvc.show('ok-cancel-modal.tpl',{
							title:"Success",
							icon: "fa fa-check",
							text: "Handling Unit saved.",
							callback:function(){
								//Do nothing on OK button.
							}
						});
					}, function(err){
						uiLoaderSvc.hide();

						uiModalSvc.show('ok-cancel-modal.tpl',{
							title:"Error",
							icon: "fa fa-exclamation-triangle",
							text: "Handling Unit cannot be saved at this time, please try again.",
							callback:function(){
								//Do nothing on OK button.
							}
						});
					});
				}
				else{
					uiModalSvc.show('ok-cancel-modal.tpl',{
						title:"Error",
						icon: "fa fa-exclamation-triangle",
						text: "Origin is required to save a master record for Handling Unit.",
						callback:function(){
							//Do nothing on OK button.
						}
					});
				}
			}



			$scope.close();
		}
		else{
			//TODO: Form validation handled by Angular
		}
	}

	$scope.dimensionsChange = function(){
		if(!!$scope.handlingUnit["dimensionsUom"])
		{
			var tempVal = "CU "+$scope.handlingUnit["dimensionsUom"].name;
			$scope.handlingUnit["volumeUom"] = tempVal;
		}
	}

	// $scope.containerTypeChange=function(){

	// 	if($scope.configList){
	// 		var configContainerType = $scope.configList.OF_UI_CONTAINER_TYPES_UNMIXABLE;

	// 		var containerTypeFlag = true;

	// 		if(!!configContainerType){
	// 			if(!!$scope.handlingUnit.quantityUom && angular.isObject($scope.handlingUnit.quantityUom)){
	// 				// orderSvc.selectedDelivery().shipmentObject.handlingUnits.forEach((cont, i) => {
	// 					// if(!!cont.quantityUom){
	// 					// 	if(cont.quantityUom.match(RegExp(configContainerType))){
	// 					// 		containerTypeFlag = false;
	// 					// 	}
	// 					// }
	// 					if($scope.handlingUnit.quantityUom.code.match(RegExp(configContainerType))){
	// 						containerTypeFlag = false;
	// 					}
	// 				// });
	// 			}
	// 			else{
	// 				containerTypeFlag = true; // If user havne't selected container type for any HU, then containerType is undefined and we 
	// 			}

	// 			if(!containerTypeFlag){
	// 				configContainerType = configContainerType.split("|");

	// 				uiModalSvc.show('ok-cancel-modal.tpl',{
	// 				title:"Warning",
	// 				icon: "fa fa-exclamation-triangle",
	// 				text:"Carton handling units cannot be combined with any other handling unit types. Do you wish to change all handling unit types for this Shipment to "+configContainerType[0]+"? If not, please create a separate fulfillment for this handling unit.",
	// 				callback:function(){
							
	// 						uiModalSvc.show('of-new-handling-unit-modal.tpl',{
	// 							dIndex:dIndex,
	// 							hIndex:$scope.args.hIndex,
	// 							mode:'edit'
	// 						});

	// 						angular.forEach(orderSvc.selectedDelivery().shipmentObject.handlingUnits, function(cont, i) {
	// 							cont.quantityUom = configContainerType[0];
	// 						});	
	// 					}
	// 				});							
	// 			}
	// 			else{
	// 				// This means user haven't selected any container which is equals to OF_UI_CONTAINER_TYPES_UNMIXABLE
	// 			}
	// 		}
	// 		else{
	// 			//If the config value is null or empty - do nothing
	// 		}
	// 	}
	// }


}]);
