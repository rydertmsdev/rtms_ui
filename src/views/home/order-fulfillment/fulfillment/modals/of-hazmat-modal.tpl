<div class="modal-small" ng-controller="ofHazmatModalCtrl">
<form name="hazmatModal" ui-width="100%" ng-submit="save(hazmatModal.$valid)">
	<ui-header theme="ui-modal-header">
		<ui-header-body>
			<div>
				<i class="ic-nuclear"></i>
				<span>
					Hazmat
				</span>
			</div>
		</ui-header-body>
		<ui-header-controls>
			<button ui-button title="Clear" ng-click="close()">
				<i class="fa fa-times"></i>
			</button>
		</ui-header-controls>
	</ui-header>
	<div class="modal-body form">
		<!-- Assuming all the hazmat fieldType will be REF and not from PROP, so that condition is not handles here. -->
		<div class="cols" ng-repeat="twoItems in columns">
			<div  ng-repeat="haz in twoItems" >

				<ui-field type="toggle" ng-if="(haz.displayType == 'readonly.checkbox') || (haz.displayType == 'single checkbox')" ng-model="haz.itemValue" label="Hazmat" title="{{haz.displayName}}" ng-required="haz.required == 'Y' ? true : false" ng-disabled="haz.displayType == 'readonly.checkbox' ? true : false"></ui-field>
			
				<ui-field type="text" ng-if="haz.displayType == 'readonly'" ng-model="haz.itemValue" label="{{haz.displayName}}" ng-disabled="true"></ui-field>

				<ui-field type="text" ng-if="haz.displayType == 'freeform text'" ng-model="haz.itemValue" label="{{haz.displayName}}" ng-required="haz.required == 'Y' ? true : false" ng-disabled="false"></ui-field>

				<ui-field type="select" ng-if="haz.displayType == 'dropdown'" prop="name" options="haz.options" ng-model="haz.itemValue" label="{{haz.displayName}}" ng-required="haz.required == 'Y' ? true : false" ng-disabled="haz.displayType == 'readonly' ? true : false"></ui-field>

			</div>
		</div>

<!-- 		<div class="cols">
			<ui-field type="toggle" ng-model="item.hazmat" label="Hazmat" title="This item falls under hazardous classification" ng-required="false" ng-disabled="false"></ui-field>
		</div>
		<div class="rows" ng-show="!!item.hazmat">
		<div class="cols" ng-hide="!!splitToOwn">
			<ui-field type="text" ng-model="item.hazmatUnCode" label="UN Code" ng-required="false"></ui-field>
			<ui-field type="text" ng-model="item.hazmatClass" label="Hazardous Class" ng-required="false"></ui-field>
		</div>
		<div class="cols">
			<ui-field type="text" ng-model="item.packGroup" label="Packaging Group" ng-required="false"></ui-field>
			<ui-field type="text" ng-model="item.packGroup" label="ERG Page Number" ng-required="false"></ui-field>
		</div>
		<ui-field type="text" ng-model="item.hazmatPropShipName" label="Proper Shipping Name" ng-required="false"></ui-field>
		</div> -->
	</div>
	<ui-header theme="ui-modal-footer">
		<ui-header-controls>
			<button ui-button ng-click="close()" class="bg-gray">
				<span>Cancel</span>
			</button>
			<button ui-button type="submit" class="bg-accent">
				<span>Save</span>
			</button>
		</ui-header-controls>
	</ui-header>
	</form>
</div>