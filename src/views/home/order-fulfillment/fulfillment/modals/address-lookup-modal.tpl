<div class="form modal-large" style="display: inline-block;" ng-controller="ofAddressLookupCtrl">
	<ui-header theme="ui-modal-header">
		<ui-header-body>
			<div class="skew" >
				<span>Address Lookup</span>
			</div>
		</ui-header-body>
		<ui-header-controls class="inset">
			<button ui-button ng-click="close();"><i class="fa fa-times"></i></button>
		</ui-header-controls>
	</ui-header>
	<div class="modal-body overflow-y">
		<div class="form">
			<ui-header theme="subsection">
				<ui-header-body>
					<div class="width100">
						<span>
							Search For:
						</span>
					</div>
				</ui-header-body>
			</ui-header>
			<div class="cols">				
				<div class="rows">
					<div class="cols">
						<ui-field type="text" ng-model="addressId" label="Address ID" ng-required="false" ng-disabled="false"></ui-field>
					</div>
					
					<div class="cols">
						<ui-field type="text" ng-model="addressLine1" label="Address Line 1" ng-required="false" ng-disabled="false"></ui-field>
					</div>
					
					<div class="cols">
						<ui-field type="text" ng-model="city" label="City" ng-required="false" ng-disabled="false"></ui-field>
					</div>
					
					<div class="cols">
						<ui-field type="select" code="" options="countriesList" ng-model="country" label="Country" placeholder="" ng-required="false" ng-disabled="false"></ui-field>
					</div>
					
					<div class="cols">
						<ui-field type="text" ng-model="contactName" label="Contact Name" ng-required="false" ng-disabled="false"></ui-field>
					</div>
					
					<div class="cols">
						<ui-field type="text" ng-model="timeZone" label="Time Zone" ng-required="false" ng-disabled="false"></ui-field>
					</div>
				</div>
				<div class="rows">
					
					<div>
						<ui-field type="text" ng-model="addressName" label="Address Name" ng-required="false" ng-disabled="false"></ui-field>
					</div>
					
					<div>
						<ui-field type="text" ng-model="addressLine2" label="Address Line 2" ng-required="false" ng-disabled="false"></ui-field>
					</div>
					
					<div>
						<ui-field type="select" options="statesList" ng-model="state" label="State" ng-required="false" ng-disabled="false" placeholder=""></ui-field>
					</div>
					
					<div>
						<ui-field type="text" ng-model="postalCode" label="Postal Code" ng-required="false" ng-disabled="false"></ui-field>
					</div>
					
					<div>
						<ui-field type="text" ng-model="contactPhone" label="Contact Phone" ng-required="false" ng-disabled="false"></ui-field>
					</div>
					
				</div>
			</div>
			<ui-header theme="ui-modal-footer">
				<ui-header-controls>
					<button ui-button class="bg-accent" ng-click="getAddressResults('19')" >
						<span>Search</span>
					</button>
					<button ui-button type="submit" ng-click="clear()" class="bg-gray" >
						<span>Clear</span>
					</button>
				</ui-header-controls>
			</ui-header>
		</div>

		<div ng-if="searchResults.length == 0 && showNoResultFlag" style="padding: 0 10px 10px 10px; border: solid 2px #ccc">
			<div class="no-results">
				No results to show.
			</div>
		</div>

		<ui-list class="overflow-y" ng-if="searchResults.length > 0">			
			<ui-list-header>
				<li class="icon text-center">
					<span>Select</span>
				</li>
				<li ng-repeat="heading in tableHeading" code="{{heading.code}}" ng-class="{true:'width125', false:''}[heading.code == 'description']">
					<span>{{heading.name}}</span>
					<!-- <div ui-list-sort-btn ng-click="sortResults(heading.code, $event, 'filtering')">
						<i class="fa fa-sort"></i>
					</div> -->
				</li>				
			</ui-list-header>
			<ui-list-body>
				<li ng-repeat="al in searchResults" ng-class="{'selected':al.$$selected}" ng-click="selectItem(al)">
					<div class="icon">
						<div class="checkbox" ng-class="{'checked':al.$$selected}"></div>
					</div>

					<div ng-repeat="heading in tableHeading" code="{{heading.code}}" ng-class="">
						<span class="title" ui-info="{{al[heading.code]}}">{{al[heading.code]}}</span>		
					</div>
				</li>
			</ui-list-body>
		</ui-list>
	</div>
	<ui-header theme="ui-modal-footer" ng-if="searchResults.length > 0">
		<ui-header-controls>
			<!-- <button ui-button class="bg-accent" ng-click="" >
				<span>Apply</span>
			</button> -->
			<button ui-button type="submit" ng-click="close()" class="bg-gray" ng-hide="poDetailsEditable">
				<span>Cancel</span>
			</button>
		</ui-header-controls>
	</ui-header>
</div>