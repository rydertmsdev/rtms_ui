App.controller('ofCompletePalletizeModalCtrl',['$scope', '$timeout', 'uiModalSvc', 'orderSvc', function($scope, $timeout, uiModalSvc, orderSvc){	

	$scope.hIndex = $scope.args.hIndex;
	$scope.order=orderSvc.selectedDelivery();
	$scope.trackingDeviceId;

	$scope.close=function(){
		uiModalSvc.hide();
	}

	$scope.save=function(){
		$scope.order.shipmentObject.handlingUnit[$scope.hIndex].trackingId = $scope.trackingDeviceId;
		uiModalSvc.hide();
	}

}]);