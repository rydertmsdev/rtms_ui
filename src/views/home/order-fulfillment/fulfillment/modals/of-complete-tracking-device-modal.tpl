<div class="modal-small" ng-controller="ofCompleteTrackingDeviceModalCtrl">
	<ui-header theme="ui-modal-header">
		<ui-header-body>
			<div>
				<i class="fa fa-crosshairs"></i>
				<span>
					Add Tracking Device
				</span>
			</div>
		</ui-header-body>
		<ui-header-controls>
			<button ui-button ng-click="close()">
				<i class="fa fa-times"></i>
			</button>
		</ui-header-controls>
	</ui-header>
	<ui-list>			
		<ui-list-body>
			<li style="min-height: 54px;">
				<div class="text-center">
					<ui-field type="text" label="Enter tracking device ID for this Handling Unit" ng-model="trackingDeviceId" class=""></ui-field>
				</div>
			</li>
		</ui-list-body>
	</ui-list>
	<ui-header theme="ui-modal-footer">
		<ui-header-controls>
			<button ui-button ng-click="close()" class="bg-gray">
				<span>Cancel</span>
			</button>
			<button ui-button type="submit" ng-click="save()" class="bg-accent">
				<span>Save</span>
			</button>
		</ui-header-controls>
	</ui-header>
</div>




