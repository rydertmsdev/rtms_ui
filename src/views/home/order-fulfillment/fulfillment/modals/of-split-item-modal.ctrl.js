App.controller('ofSplitItemModalCtrl',['$scope', '$timeout', 'uiModalSvc', 'orderSvc', function($scope, $timeout, uiModalSvc, orderSvc){	
	$scope.modalOptions=orderSvc.deliveryOrders[$scope.args.dIndex].handlingUnits;
	$scope.targetHandlingUnit=orderSvc.deliveryOrders[$scope.args.dIndex].shipmentObject.$$lastTargetHandlingUnit;

	$scope.palletTypeFlag = orderSvc.palletTypeFlag;

	$scope.item=orderSvc.deliveryOrders[$scope.args.dIndex].handlingUnits[$scope.args.hIndex].items[$scope.args.index];

	$scope.close=function(){
		uiModalSvc.hide();
	}

	$scope.splitItem=function(){
		if (!!$scope.splitToOwn){
			orderSvc.splitItemToOwn($scope.args.dIndex,$scope.args.hIndex,$scope.args.index);
		}
		else{
			orderSvc.splitItem($scope.args.dIndex,$scope.args.hIndex,$scope.args.index,$scope.targetHandlingUnit,$scope.targetQuantity);
		}
		$scope.close();
	}

	$scope.newHandlingUnit=function(){
		uiModalSvc.show('of-new-handling-unit-modal.tpl',{
			dIndex:dIndex,
			sourceHIndex:$scope.args.hIndex,
			sourceIndex:$scope.args.index,
			mode:'split',
			targetQuantity:$scope.targetQuantity
		});	
	}


}]);
