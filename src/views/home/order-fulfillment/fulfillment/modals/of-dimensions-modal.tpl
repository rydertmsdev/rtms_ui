<div class="modal-small" style="display: inline-block;" ng-controller="ofDimensionsModalCtrl">
<form name="dimensionForm" ng-submit="save(dimensionForm.$valid)">
	<ui-header theme="ui-modal-header">
		<ui-header-body>
			<div>
				<i class="ic-measure"></i>
				<span>
					Item Dimensions
				</span>
			</div>
		</ui-header-body>
		<ui-header-controls>
			<button ui-button title="Clear" ng-click="close()">
				<i class="fa fa-times"></i>
			</button>
		</ui-header-controls>
	</ui-header>
	<div class="modal-body form">
	<!-- Assuming all the dimensions fieldType will be PROP and not from REF, so that condition is not handles here. -->
		<div class="cols" ng-repeat="twoItems in columns">
			<div  ng-repeat="dim in twoItems">
				<ui-field type="text" ng-if="dim.displayType == 'freeform text'" ng-model="item[dim.fieldName]" label="{{dim.displayName}}" ng-required="dim.required == 'Y' ? true : false" ng-disabled="false"></ui-field>
				<ui-field type="text" ng-if="dim.displayType == 'readonly'" ng-model="item[dim.fieldName]" label="{{dim.displayName}}" ng-required="dim.required == 'Y' ? true : false" ng-disabled="true"></ui-field>
				<ui-field type="select" ng-if="dim.displayType == 'dropdown'" prop="name" options="dim.options" ng-model="item[dim.fieldName]" label="{{dim.displayName}}" ng-required="dim.required == 'Y' ? true : false" ng-disabled="dim.displayType == 'readonly' ? true : false"></ui-field>
			</div>
		</div>
		<div class="cols" ng-if="configList.OF_UI_ITEM_SAVE_MASTER_DATA && configList.OF_UI_ITEM_SAVE_MASTER_DATA == 'Y'">
			<ui-field type="toggle" class="no-label" ng-model="saveToMasterTable" hover="Save data to master table" title="Save as master record" ng-required="false" ng-disabled="false"></ui-field>
		</div>

		<!-- <div class="cols">
			<ui-field type="text" ng-model="item.length" label="Length" ng-required="false" ng-disabled="false"></ui-field>
			<ui-field type="text" ng-model="item.width" label="Width" ng-required="false" ng-disabled="false"></ui-field>
		</div>
		<div class="cols">
			<ui-field type="text" ng-model="item.height" label="Height" ng-required="false" ng-disabled="false"></ui-field>
			<ui-field type="select" options="uoms" ng-model="item.uom" label="UOM" ng-required="false" ng-disabled="false"></ui-field>
		</div>
		<div class="cols form-condensed">
			<ui-field type="text" ng-model="item.volume" label="Volume" ng-required="false" ng-disabled="true" placeholder="---"></ui-field>
			<ui-field type="text" ng-model="item.volumeUom" label="Volume UOM" ng-required="false" ng-disabled="true"  placeholder="---"></ui-field>
		</div> -->
	</div>
	<ui-header theme="ui-modal-footer">
		<ui-header-controls>
			<button ui-button ng-click="close()" class="bg-gray">
				<span>Cancel</span>
			</button>
			<button ui-button type="submit" class="bg-accent">
				<span>Save</span>
			</button>
		</ui-header-controls>
	</ui-header>
	</form>
</div>