App.controller("ofPrintDocumentModalCtrl", ["$scope", "$timeout", "uiLoaderSvc" ,"uiModalSvc", "orderSvc", '$window', 'getFulfilledDocuments', function($scope, $timeout, uiLoaderSvc, uiModalSvc, orderSvc, $window, getFulfilledDocuments) {
    $scope.close = function() {
        uiModalSvc.hide();
    }

    $scope.documents = [];

    if(orderSvc.shipmentCreateObj && orderSvc.shipmentCreateObj.docs){
        $scope.documents= orderSvc.shipmentCreateObj.docs;
    }

    $scope.printDoc=function(doc){
        uiModalSvc.hide();
        // $window.open(doc.docUrl);

         printDocuments([doc.docId]);
    }

    $scope.printAll=function(){
        uiModalSvc.hide(); 

        $scope.printAllArr = [];

        angular.forEach($scope.documents, function(doc, i) {
            $scope.printAllArr.push(doc.docId);
        });  

        printDocuments($scope.printAllArr);
    }

    $scope.printSelected=function(){
        uiModalSvc.hide();
    
        $scope.printSelectedArr = [];

        angular.forEach($scope.documents, function(doc, i) {
            if(doc.$$selected){
                $scope.printSelectedArr.push(doc.docId);
            }
        });  

        printDocuments($scope.printSelectedArr);
    }

    function printDocuments(docArr){
        var reqObj = {};
        if(orderSvc.shipmentCreateObj){
            reqObj["shipmentNum"] = orderSvc.shipmentCreateObj.shipmentNum;
        }
        else{
            reqObj["shipmentNum"] = "";   
        }

        reqObj["docIds"] = docArr;
        // reqObj["fetchDocs"] = true;
        // reqObj["mergeDocs"] = true;

        var docList = "";
        angular.forEach(docArr, function(doc, index) {
            docList += "&docId="+doc;
        });

        var url = "shipmentNum="+reqObj.shipmentNum+""+docList;

        $window.open("/ryderonline/rtms/service/"+orderSvc.module+"/fulfilledDocContent?"+url);

        // uiLoaderSvc.show();

        // getFulfilledDocuments.getResults(orderSvc.module, url).then(function(resp){
        //     uiLoaderSvc.hide();
        //     if(resp.data.content && resp.data.content[0].docUrl){
        //         $window.open(resp.data.content[0].docUrl);
        //     }
        //     else{
        //         console.log("Cannot Print: FulfilledDocuments Success, but print document is NULL")
        //     }
        // }, function(err){
        //     uiLoaderSvc.hide();
        //     console.log("FulfilledDocuments service err..");
        // });
    }

}
]);