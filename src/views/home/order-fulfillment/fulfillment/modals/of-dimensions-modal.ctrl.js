App.controller('ofDimensionsModalCtrl',['$scope', '$rootScope', 'uiLoaderSvc', '$timeout', 'uiModalSvc', 'orderSvc', 'ItemMasterService', 'codeListService', function($scope, $rootScope, uiLoaderSvc, $timeout, uiModalSvc, orderSvc, ItemMasterService, codeListService){	
	$scope.item=angular.copy(orderSvc.deliveryOrders[$scope.args.dIndex].shipmentObject.handlingUnits[$scope.args.hIndex].items[$scope.args.index]);

	$scope.dimArray = $scope.args.dimArray;

	$scope.saveToMasterTable = true;

	$scope.columns = columnize($scope.dimArray, Math.ceil($scope.dimArray.length/2));

	$scope.configList;
	if(codeListService.codeLists && codeListService.codeLists.CustomerConfigList[0]){
		$scope.configList = codeListService.codeLists.CustomerConfigList[0];
	}

	function columnize(input, cols) {
	  var arr = [];
	  for(i = 0; i < input.length; i++) {
	    var colIdx = i % cols;
	    arr[colIdx] = arr[colIdx] || [];
	    arr[colIdx].push(input[i]);
	  }
	  return arr;
	}

	$scope.close=function(){
		uiModalSvc.hide();
	}


	$scope.save=function(flag){
		if(flag){
			var reqObj = {};
			angular.forEach($scope.dimArray, function(name, index) {
				if(name.displayType == "freeform text"){
					orderSvc.deliveryOrders[$scope.args.dIndex].shipmentObject.handlingUnits[$scope.args.hIndex].items[$scope.args.index][name.fieldName] =	$scope.item[name.fieldName];
					reqObj[name.fieldName] = $scope.item[name.fieldName];
				}
				else if (name.displayType == "dropdown"){
					orderSvc.deliveryOrders[$scope.args.dIndex].shipmentObject.handlingUnits[$scope.args.hIndex].items[$scope.args.index][name.fieldName] =	$scope.item[name.fieldName].code;
					if(name.fieldName == "dimensionsUom"){
						reqObj["dimensionsUOM"] = $scope.item[name.fieldName].code;
					}
					else if(name.fieldName == "weightUom"){
						reqObj["weightUOM"] = $scope.item[name.fieldName].code;
					}else if(name.fieldName == "volumeUom"){
						reqObj["volumeUOM"] = $scope.item[name.fieldName].code;
					}
					else{
						reqObj[name.fieldName] = $scope.item[name.fieldName].code;
					}
				}
				else{
					//TODO: For any other display types
				}
			});

			if($scope.saveToMasterTable){
				if(!!orderSvc.selectedDelivery().origin.entityId){
					reqObj["customernum"] = $rootScope.userProfile.data.content.customerNum || "",
					reqObj["itemCode"] = orderSvc.deliveryOrders[$scope.args.dIndex].shipmentObject.handlingUnits[$scope.args.hIndex].items[$scope.args.index].itemCode;
					reqObj["vendorCd"] = orderSvc.selectedDelivery().origin.entityId;

					uiLoaderSvc.show();

					ItemMasterService.createItem("itemmaster/v1", reqObj, 'updateFromOF').then(function(resp){
						uiLoaderSvc.hide();
						uiModalSvc.show('ok-cancel-modal.tpl',{
							title:"Success",
							icon: "fa fa-check",
							text: "Dimensions saved for this item.",
							callback:function(){
								//Do nothing on OK button.
							}
						});
					}, function(err){
						uiLoaderSvc.hide();

						uiModalSvc.show('ok-cancel-modal.tpl',{
							title:"Error",
							icon: "fa fa-exclamation-triangle",
							text: "Dimensions cannot be saved at this time, please try again.",
							callback:function(){
								//Do nothing on OK button.
							}
						});
					});
				}
				else{
					uiModalSvc.show('ok-cancel-modal.tpl',{
						title:"Error",
						icon: "fa fa-exclamation-triangle",
						text: "Origin is required to save a master record for Item dimensions.",
						callback:function(){
							//Do nothing on OK button.
						}
					});
				}
			}

		}
		else{
			//Form Validation will be handled by Angular
		}

		uiModalSvc.hide();
	}

}]);
