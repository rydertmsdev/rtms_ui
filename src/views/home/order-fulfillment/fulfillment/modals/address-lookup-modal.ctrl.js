App.controller('ofAddressLookupCtrl',['$scope', '$timeout', 'uiModalSvc', 'uiLoaderSvc', 'orderSvc', 'addressLookupService', 'getLocationDependencies', function($scope, $timeout, uiModalSvc, uiLoaderSvc, orderSvc, addressLookupService, getLocationDependencies){	

	$scope.countriesList = orderSvc.countriesList;
	$scope.statesList = orderSvc.statesList;

	$scope.searchResults = [];
	$scope.showNoResultFlag = false;
	
	$scope.addressLine1;
	$scope.addressLine2;
	$scope.contactName;
	$scope.city;
	$scope.state;
	$scope.country;
	$scope.timeZone;	
	$scope.postalCode;
	$scope.contactPhone;

	if($scope.args.type == "19"){
		$scope.addressId = orderSvc.selectedDelivery().origin.entityId.name || "";
		$scope.addressName = orderSvc.selectedDelivery().origin.name.name || "";
	}
	else if($scope.args.type == "20"){
		$scope.addressId = orderSvc.selectedDelivery().destination.entityId.name || "";
		$scope.addressName = orderSvc.selectedDelivery().destination.name.name || "";
	}

	var loadRequestObj={};
	//Commenting code as ALM requested not to search by default unless they hit Search button on Address lookup pop up
	// if(!!$scope.addressName || !!$scope.addressId){

	// 	if(!!$scope.addressId){
	// 		loadRequestObj["code"] = $scope.addressId;
	// 	}

	// 	if(!!$scope.addressName){
	// 		loadRequestObj["name"] = $scope.addressName;
	// 	}

	// 	loadRequestObj["type"] = $scope.args.type;

	// 	if(!angular.equals(loadRequestObj, {})){
	// 		uiLoaderSvc.show();
	// 		addressLookupService.getResults(orderSvc.module, loadRequestObj).then(function(resp){
	// 			uiLoaderSvc.hide();
	// 			$scope.showNoResultFlag = true;
	// 			if(resp.data && resp.data.content){
	// 				$scope.searchResults = resp.data.content;
	// 			}			
	// 		},
	// 		function(err){
	// 			uiLoaderSvc.hide();
	// 			console.log("Service error on address look up", err);
	// 		});
	// 	}
	// }


	$scope.tableHeading = [
			{
				name: "Address Id",
				code: "code"
			},
			{
				name: "Address Name",
				code: "name"
			},
			{
				name: "Address Line 1",
				code: "address1"
			},
			{
				name: "Address Line 2",
				code: "address2"
			},
			{
				name: "City",
				code: "city"
			},
			{
				name: "State/Province",
				code: "state"
			},
			{
				name: "Postal Code",
				code: "postalCode"
			},
			{
				name: "Country",
				code: "country"
			},
			{
				name: "Contact Name",
				code: "contactname"
			},
			{
				name: "Contact Phone",
				code: "contactPhone"
			},
			{
				name: "Time Zone",
				code: "timeZone"
			},
			{
				name: "Type Code",
				code: "type"
			}
		];

	$scope.selectItem=function(item){
		item.$$selected = true;
		uiLoaderSvc.show();

		if(item.type == "19"){
			orderSvc.selectedDelivery().origin.id = item.type;
	        orderSvc.selectedDelivery().origin.name = {'name' : item.name};
	        orderSvc.selectedDelivery().origin.address = item.address1;
	        orderSvc.selectedDelivery().origin.address2 = item.address2;
	        orderSvc.selectedDelivery().origin.city = item.city;
	        orderSvc.selectedDelivery().origin.sau = {'name' : item.state};
	        orderSvc.selectedDelivery().origin.postalCode = item.postalCode;
	        orderSvc.selectedDelivery().origin.country = {'name' : item.country};
	        orderSvc.selectedDelivery().origin.entityId = {'name' : item.code };
	    }
	    else if(item.type == "20"){
	    	orderSvc.selectedDelivery().destination.id = item.type;
	        orderSvc.selectedDelivery().destination.name = {'name' : item.name};
	        orderSvc.selectedDelivery().destination.address = item.address1;
	        orderSvc.selectedDelivery().destination.address2 = item.address2;
	        orderSvc.selectedDelivery().destination.city = item.city;
	        orderSvc.selectedDelivery().destination.sau = {'name' : item.state};
	        orderSvc.selectedDelivery().destination.postalCode = item.postalCode;
	        orderSvc.selectedDelivery().destination.country = {'name' : item.country};
	        orderSvc.selectedDelivery().destination.entityId = {'name' : item.code };
	    }

	    if(orderSvc.selectedDelivery().shipmentObject){
	        getLocationDependencies.getResults(orderSvc.module, orderSvc.selectedShipmentUpdated()).then(function(locResp){
				if(locResp.data.content[0]){
					orderSvc.containerTypesList =	locResp.data.content[0].qualifierCodeValues;
				}
			},
			function(err){
				uiLoaderSvc.hide();
				console.log("Service error on address look up", err);
			});
	    }

		$timeout(function () {
	        uiLoaderSvc.hide();
	        uiModalSvc.hide();
	    }, 500);
	}

	$scope.close=function(){
		uiModalSvc.hide();
	}

	$scope.clear=function(){		
		$scope.addressId = "";
		$scope.addressLine1 = "";
		$scope.addressLine2 = "";
		$scope.contactName = "";
		$scope.timeZone = "";
		$scope.addressName = "";
		$scope.postalCode = "";
		$scope.contactPhone = "";
		$scope.city = "";
		$scope.state = "";
		$scope.country = "";
	}

	$scope.getAddressResults=function(type){
		if(!$scope.addressId && !$scope.addressLine1 && !$scope.addressLine2 && !$scope.contactName && !$scope.timeZone && !$scope.addressName && !$scope.postalCode && !$scope.contactPhone && !$scope.city && !$scope.state && !$scope.country){
			uiModalSvc.show('ok-cancel-modal.tpl',{
				title:"Message",
				text:"An empty search with no search criteria is not allowed. Please enter a value.",
				callback:function(){
					uiModalSvc.hide();
				}
			});
		}
		else{
			
			//call search service for address lookup
			console.log("request for address lookup - ", $scope.addressId);

			uiLoaderSvc.show();

			requestObj = {};

			requestObj.address1 = $scope.addressLine1;
			requestObj.address2 = $scope.addressLine2;
			requestObj.city = $scope.city;
			requestObj.code = $scope.addressId;
			requestObj.contactPhone = $scope.contactPhone;
			requestObj.contactName = $scope.contactName;
			
			if(!!$scope.country){
				requestObj.country = $scope.country.code;
			}
			
			// requestObj.locationTypeCd = orderSvc.selectedDelivery().origin.entityId;
			requestObj.name = $scope.addressName;
			requestObj.postalCode = $scope.postalCode;
			if($scope.state){
				requestObj.state = $scope.state.code;
			}
			
			requestObj.timeZone = $scope.timeZone;
			requestObj.type = type;
			// requestObj.addressTypeIds =orderSvc.selectedDelivery().origin.addrName1;

			addressLookupService.getResults(orderSvc.module, requestObj).then(function(resp){
				uiLoaderSvc.hide();
				$scope.showNoResultFlag = true;
				if(resp.data && resp.data.content){
					$scope.searchResults = resp.data.content;
				}
			});
		}		
	}
}]);
