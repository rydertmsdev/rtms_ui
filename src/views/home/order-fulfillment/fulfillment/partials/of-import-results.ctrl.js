App.controller('ofImportResultsCtrl',['$scope', '$timeout','orderSvc', function($scope, $timeout, orderSvc){
	
	///////////////////////////////////////////////////
	// Dropzone default config
	//

	$scope.dzOptions = {
		url : 'https://angular-file-upload-cors-srv.appspot.com/upload',
		//paramName : 'photo',
		maxFilesize : '10',
		//acceptedFiles : 'image/jpeg, images/jpg, image/png',
		addRemoveLinks : false,
		dictDefaultMessage:"Drag files here to upload"
	};
	
	
	//Handle events for dropzone
	//Visit http://www.dropzonejs.com/#events for more events
	$scope.dzCallbacks = {
		'addedfile' : function(file){
			console.log(file);
			$scope.newFile = file;
		},
		'success' : function(file, xhr){
			console.log(file, xhr);
		},
	};
	
	//Apply methods for dropzone
	//Visit http://www.dropzonejs.com/#dropzone-methods for more methods
	$scope.dzMethods = {};
	$scope.removeNewFile = function(){
		$scope.dzMethods.removeFile($scope.newFile);
	}
}]);
