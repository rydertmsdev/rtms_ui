<ui-section>
	<ng-dropzone class="dropzone" options="dzOptions" callbacks="dzCallbacks" methods="dzMethods"></ng-dropzone>
</ui-section>
<ui-section>
	<ui-list>			
		<ui-list-header>
			<li style="min-width: 120px">
				<span>File</span>
			</li>	
			<li style="max-width: 120px" class="text-center">Status</li>
		</ui-list-header>
		<ui-list-body>
			<li>
				<ui-list-content>
					<cell class="text rows"  style="min-width: 120px">
						<span class="title" xng-bind="::po.orderNumber">File name</span>
						<span class="subtitle" xng-bind="::('Line '+po.lines[0].lineNumber)">4/7/2017 11:17am</span>
					</cell>
					<cell style="max-width: 120px;">
						<b class="bg-orange">Uploading</b>
					</cell>
				</ui-list-content>
			</li>
			<li>
				<ui-list-content>
					<cell class="text rows"  style="min-width: 120px">
						<span class="title" xng-bind="::po.orderNumber">File name</span>
						<span class="subtitle" xng-bind="::('Line '+po.lines[0].lineNumber)">4/7/2017 11:17am</span>
					</cell>
					<cell style="max-width: 120px;">
						<b class="bg-green">Uploaded</b>
					</cell>
				</ui-list-content>
			</li>
		</ui-list-body>
	</ui-list>
</ui-section>
<ui-page-actions-spacer></ui-page-actions-spacer>
<ui-page-actions-container>
	<ui-page-actions>
		<div class="group">
			<div ui-button ui-page-action class="text-accent bg-white" ui-info="Download Template">
				<span>Download Template</span>
				<i class="fa fa-arrow-down"></i>
			</div>
		</div>
	</ui-page-actions>
</ui-page-actions-container>



