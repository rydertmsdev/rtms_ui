App.controller('ofDeliveryOrdersSidebarCtrl',['$rootScope', '$scope', '$state', '$timeout', 'uiModalSvc', 'orderSvc', function($rootScope, $scope, $state, $timeout, uiModalSvc, orderSvc){
	$scope.orderSvc=orderSvc;

	$scope.selectDelivery=function(dIndex){
		orderSvc.dIndex=dIndex;
		orderSvc.showDetails=true;
		if($state.current.name == "home.order-fulfillment.delivery-details"){
			$state.reload();
		}
		else{
			$state.go('home.order-fulfillment.delivery-details');
		}
	}

	$scope.$colors=$colors;
}]);


