<div class="form form-dark overflow-y">
	<div class="text text-center" ng-show="orderSvc.deliveryOrders.length==0">
		Select items from search results to create Shipments
	</div>
	<div class="rows">
		<ui-menu-group ng-repeat="(dIndex, dOrder) in orderSvc.deliveryOrders track by $index" ng-click="selectDelivery(dIndex)" ui-button>
			<ui-header theme="ui-menu-header" ng-class="{'faded':orderSvc.dIndex!=dIndex}">
				<ui-header-background ng-style="{'background-color':$colors($index,10)}">
				</ui-header-background>
				<ui-header-body>
					<div>
						<span>
							{{dOrder.name}}
						</span>
					</div>
				</ui-header-body>
				<ui-header-controls>
					<div class="indicator" ng-show="dIndex==orderSvc.dIndex">
						<i class="fa fa-chevron-right"></i>
					</div>
				</ui-header-controls>
			</ui-header>
			<ui-menu-group-content>
				<ui-header ng-if ="!(dOrder.visitedFlag)" theme="ui-menu-item" ng-repeat="hu in dOrder.handlingUnits">
					<ui-header-body>
						<div>
							<i class="fa fa-cube" style="font-size: 0.8em;"></i>
							<span>
								{{hu.name}}
							</span>
						</div>
					</ui-header-body>
					<ui-header-controls>
						<span class="icon" ng-show="hu.items.length>0"><b ui-animate-change="hu.items.length" ui-animate-change-class="highlight">{{hu.items.length}}</b></span>
					</ui-header-controls>
				</ui-header>
				<ui-header ng-if ="(dOrder.visitedFlag)" theme="ui-menu-item" ng-repeat="hu in dOrder.shipmentObject.handlingUnits">
					<ui-header-body>
						<div>
							<i class="fa fa-cube" style="font-size: 0.8em;"></i>
							<span>
								{{hu.name}}
							</span>
						</div>
					</ui-header-body>
					<ui-header-controls>
						<span class="icon" ng-show="hu.items.length>0"><b ui-animate-change="hu.items.length" ui-animate-change-class="highlight">{{hu.items.length}}</b></span>
					</ui-header-controls>
				</ui-header>
			</ui-menu-group-content>
		</ui-menu-group>
	</div>
</div>