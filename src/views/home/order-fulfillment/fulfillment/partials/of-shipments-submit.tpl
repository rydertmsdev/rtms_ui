
<ui-section>
	<ui-header theme="section">
		<ui-header-body>
			<div>
				Submit Order
			</div>
		</ui-header-body>
		<ui-header-controls ng-show="showLabels">
			<div ui-button ng-click="addTracking();">
				<i class="fa fa-crosshairs"></i>
				<span>Add Tracking Device</span>
			</div>
		</ui-header-controls>
		<ui-header-controls>
			<div class="form form-condensed">
				<ui-field type="toggle" ng-model="addlReview" class="no-label" title="Request Additional Review"></ui-field>
			</div>
		</ui-header-controls>
	</ui-header>
	<div class="form"  style="position: relative;">
		<div class="scenario">
			<div class="icon">
				<i class="fa fa-gear"></i>
			</div>
			<ul>
				<li ng-click="scenario1()">
					Scenario 1 - No Palletization Options
				</li>
				<li ng-click="scenario2()">
					Scenario 2 - Palletization Options
				</li>
				<li ng-click="scenario3()">
					Scenario 3 - Additional Processing
				</li>
			</ul>
		</div>
		<div class="order-complete">
			<ul>
				<li class="order-complete-section" ng-repeat="step in steps" ng-class="{'active':step.status==1,'complete':step.status==2}">
					<span class="icon">
						<i class="fa fa-check"></i>
					</span>
					<span class="text">{{step.name}}</span>
				</li>
			</ul>
			<div class="message" ng-show="!!processing">
				<div class="text">
					<div class="title">This shipment requires additional processing</div>
					<div class="subtitle">You will be notified once processing is completed</div>
				</div>
			</div>
		</div>
	</div>

</ui-section>

<ui-section  ng-show="showLabels">
	<ui-header theme="section">
		<ui-header-body>
			<div>
				<span>
					Shipping Documents
				</span>
			</div>
		</ui-header-body>
		<ui-header-controls>
			<div ui-button ng-click="openPrint()">
				<i class="fa fa-print"></i>
			</div>
		</ui-header-controls>
	</ui-header>
	<div class="form rows">
		<div class="shipping-label text-center">
			<div class="sample-document">
				<img src="img/sample-label.png" alt="">
			</div>
		</div>
	</ui-section>
