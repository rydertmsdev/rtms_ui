App.controller('ofShipmentsCtrl',['$scope', '$state', '$timeout', '$window', 'uiLoaderSvc', 'uiModalSvc', 'uiAlertSvc', 'orderSvc', 'codeListService', 'addressLookupService', 'deliveryOrderDetails', 'createShipment', 'getLocationDependencies' , 'logoutService', 'shipmentValidations', function($scope, $state, $timeout, $window, uiLoaderSvc, uiModalSvc, uiAlertSvc, orderSvc, codeListService, addressLookupService, deliveryOrderDetails, createShipment, getLocationDependencies, logoutService, shipmentValidations){
	$scope.orderSvc=orderSvc;

	///////////////////////////////////////////////////
	// INIT
	//
	if ($state.params.uuid){
		// orderSvc.selectDelivery($state.params.uuid);
		$state.go('home.orders.fulfillment');
	}
	else{
		$state.go('home.orders.fulfillment.shipments.details',{uuid:orderSvc.deliveryOrders[orderSvc.dIndex].uuid});
	}

	///////////////////////////////////////////////////
	// SIDEBAR
	//
	

	$scope.selectDelivery=function(dOrder){
		orderSvc.selectDelivery(dOrder.uuid);
		$state.go('home.orders.fulfillment.shipments.details',{uuid:dOrder.uuid});
		if ($scope.uiResponsiveSvc.isMobile||$scope.uiResponsiveSvc.isTablet){
			$scope.uiSidebarSvc.showSidebar=false;
		}
		orderSvc.selectedDelivery().totalVolume = 0;
		$scope.huObj = orderSvc.selectedDelivery().handlingUnits;

		initializeShipmentCreationObject();
	}

	$scope.$colors=$colors;

	///////////////////////////////////////////////////
	// DETAILS
	//
	$scope.today=Date.now()/1000;

	$scope.statesList = orderSvc.statesList;
	$scope.countriesList = orderSvc.countriesList;
	if(orderSvc.selectedDelivery()){
		orderSvc.selectedDelivery().totalVolume = 0;
	}

	$scope.regex = "/^[0-9]*$/";
	
	if(orderSvc.selectedDelivery()){
		$scope.huObj = orderSvc.selectedDelivery().handlingUnits;
	}

	$scope.configList;
	$scope.FieldValidationList = [];
	$scope.handlingUnitFieldsArray = [];

	if(codeListService.codeLists && codeListService.codeLists.CustomerConfigList[0]){
		$scope.configList = codeListService.codeLists.CustomerConfigList[0];
	}

	if(codeListService.codeLists && codeListService.codeLists.FieldValidationList){
		$scope.FieldValidationList = codeListService.codeLists.FieldValidationList;
	}

	if(codeListService.codeLists){
		$scope.handlingUnitFieldsArray = codeListService.codeLists.OrderDetailsHandlingUnitsRolViewList;
	}

	$scope.getLocations=function(text, args){

		// console.log(angular.element(event.target));

		// $scope.$apply();

		if(text.length > 2){

			var requestObj ={};

			if(args.name == 'location'){
				requestObj["code"] = text+"%";
			}

			if(args.name == 'name'){
				requestObj["name"] = "%"+text+"%";
			}

			requestObj["type"] = args.code;

			uiLoaderSvc.show();
			addressLookupService.getResults(orderSvc.module, requestObj).then(function(resp){
				uiLoaderSvc.hide();

				if(resp.data && resp.data.content){
					if(args.name == 'location' && args.code == "19"){
						if(resp.data.content.length == 0){
							$scope.originIds = [{'code' : ''}];
						}
						else{
							$scope.originIds = resp.data.content;
						}
					}
					if(args.name == 'location' && args.code == "20"){
						if(resp.data.content.length == 0){
							$scope.destinationIds = [{'code' : ''}];
						}
						else{
							$scope.destinationIds = resp.data.content;
						}
					}
					if(args.name == 'name' && args.code == "19"){
						if(resp.data.content.length == 0){
							$scope.originNames = [{'name' : ''}];
						}
						else{
							$scope.originNames = resp.data.content;
						}
					}
					if(args.name == 'name' && args.code == "20"){
						if(resp.data.content.length == 0){
							$scope.destinationNames = [{'name' : ''}];
						}
						else{
							$scope.destinationNames = resp.data.content;
						}
					}
				}			
			},
			function(data){
				uiLoaderSvc.hide();
				console.log("Service Error - ", data);

				//User not authenticated (403) redirecting to login page
				if(data.status == 403){
					logoutService.logout();
				}
			});
			// console.log(orderSvc.selectedDelivery().origin.id);
		}
	}

	if($scope.configList && $scope.configList.OF_UI_HEADER_BANNER){
		uiAlertSvc.alerts = [];
		uiAlertSvc.show({
			icon: 'fa fa-info',
			style: 'bg-gray',
			//label: 'Alert',
			text : $scope.configList.OF_UI_HEADER_BANNER,
			static: false
		});
	}

	if(!orderSvc.selectedDelivery().visitedFlag){

		$scope.originIds = [{'code' : ''}];
		$scope.originNames = [{'name' : ''}];

		$scope.destinationIds = [{'code' : orderSvc.selectedDelivery().destination.entityId}];
		$scope.destinationNames = [{'name' : orderSvc.selectedDelivery().destination.name}];
	}
	else if(orderSvc.selectedDelivery().visitedFlag){

		$scope.originIds = [''];
		$scope.originNames = [''];

		$scope.destinationIds = [orderSvc.selectedDelivery().destination.entityId];
		$scope.destinationNames = [orderSvc.selectedDelivery().destination.name];

		//making serice call to get the default location object if any.

		if(orderSvc.selectedDelivery().shipmentObject.originAddressId){
        	var locObj = {'name':'location', 'code':'19'};
        	$scope.getLocations(orderSvc.selectedDelivery().shipmentObject.originAddressId, locObj);
        }

        if(orderSvc.selectedDelivery().shipmentObject.originName){
        	var locObj = {'name':'name', 'code':'19'};
        	$scope.getLocations(orderSvc.selectedDelivery().shipmentObject.originName, locObj);
        }
	}

	function initializeShipmentCreationObject(){

		//if the user have not visited this delivery order details page, visitedFlag will be false and making a service call to get shipment object
		if(!orderSvc.selectedDelivery().visitedFlag){

			$scope.reqObj = {};

			$scope.arrayOfOrderComponentNum = [];
			angular.forEach($scope.huObj, function(hu,index) {
				angular.forEach(hu.items, function(item, i) {
					// if(!!item.quantity){
						$scope.arrayOfOrderComponentNum.push(orderSvc.selectedDelivery().handlingUnits[index].items[i].orderComponentNum);
					// }
				});
			});

			$scope.reqObj["orderItemOcns"] = $scope.arrayOfOrderComponentNum;

			$scope.shipmentObjectReferences = [];

			uiLoaderSvc.show();

			deliveryOrderDetails.getResults(orderSvc.module, $scope.reqObj).then(function(resp){
				uiLoaderSvc.hide();
				if(resp.data.content){
			        orderSvc.selectedDelivery().origin.name = {'name' : resp.data.content.originName};
			        orderSvc.selectedDelivery().origin.address = resp.data.content.originAddr;
			        orderSvc.selectedDelivery().origin.address2 = resp.data.content.originAddr2;
			        orderSvc.selectedDelivery().origin.city = resp.data.content.originCity;
			        orderSvc.selectedDelivery().origin.sau =  resp.data.content.originState;
			        orderSvc.selectedDelivery().origin.postalCode = resp.data.content.originPostalCode;
			        orderSvc.selectedDelivery().origin.country = resp.data.content.originCountry;
			        orderSvc.selectedDelivery().origin.entityId = resp.data.content.originAddressId;
			        orderSvc.selectedDelivery().destination.name = {'name' : resp.data.content.destName};
			        orderSvc.selectedDelivery().destination.address = resp.data.content.destAddr;
			        orderSvc.selectedDelivery().destination.address2 = resp.data.content.destAddr2;
			        orderSvc.selectedDelivery().destination.city = resp.data.content.destCity;
			        orderSvc.selectedDelivery().destination.sau = resp.data.content.destState;
			        orderSvc.selectedDelivery().destination.postalCode = resp.data.content.destPostalCode;
			        orderSvc.selectedDelivery().destination.country = resp.data.content.destCountry;
			        orderSvc.selectedDelivery().destination.entityId = resp.data.content.destAddressId;

			        orderSvc.selectedDelivery().shipmentObject = resp.data.content;		
			        if(orderSvc.selectedDelivery().shipmentObject.requestShipFromDt)        
			        	orderSvc.selectedDelivery().shipmentObject.requestShipFromDt = orderSvc.selectedDelivery().shipmentObject.requestShipFromDt / 1000;
			        orderSvc.selectedDelivery().visitedFlag = true;
			        orderSvc.selectedDelivery().shipmentObject.handlingUnits[0].name = "Handling Unit 1";

			        // if(resp.data.content.originAddressId){

			        // 	var locObj = {'name':'location', 'code':'19'};
			        // 	$scope.getLocations(resp.data.content.originAddressId, locObj);
			        // }

			        // if(resp.data.content.originName){
			        // 	var locObj = {'name':'name', 'code':'19'};
			        // 	$scope.getLocations(resp.data.content.originName, locObj);
			        // }

			        angular.forEach($scope.shipmentSummaryArr, function(item, index) {
						if(item.fieldType == "REF" && $scope.shipmentObjectReferences){
							var refCode = '';
							angular.forEach($scope.shipmentObjectReferences, function(ref, i) {
								if(ref.referenceCode == item.fieldName){
									refCode = ref.referenceNum;
								}
							});
							item["itemValue"] = refCode;
						}
						else if(item.fieldType == "PROP" && orderSvc.selectedDelivery().shipmentObject){
							item["itemValue"] = orderSvc.selectedDelivery().shipmentObject[item.fieldName];
						}
					});

					angular.forEach($scope.additionalInfoArr, function(item, index) {
						if(item.fieldType == "CHRG" && orderSvc.selectedDelivery().shipmentObject && orderSvc.selectedDelivery().shipmentObject.chargeOverrides){
							var refCode = '';
							angular.forEach(orderSvc.selectedDelivery().shipmentObject.chargeOverrides, function(ref, i) {
								// angular.forEach(ref, function(value, key) {
									if(ref.chargeCd == item.fieldName){
										refCode = true;
									}
									else{
										refCode = false;
									}
								// });				
							});
							item["itemValue"] = refCode;
						}
						else if(item.fieldType == "REF" && orderSvc.selectedDelivery().shipmentObject && orderSvc.selectedDelivery().shipmentObject.references){
							var refCode = '';
							angular.forEach(orderSvc.selectedDelivery().shipmentObject.references, function(ref, i) {
								// angular.forEach(ref, function(value, key) {
									if(ref.referenceCode == item.fieldName){
										refCode = ref.referenceNum;
									}
								// });				
							});
							item["itemValue"] = refCode;
						}
						else if(item.fieldType == "PROP" && orderSvc.selectedDelivery().shipmentObject){
							item["itemValue"] = orderSvc.selectedDelivery().shipmentObject[item.fieldName];
						}
					});

					//Add validation for COuntry of origin field based on shipment DROP value - This has to happen on every fulfillTemplate call
					validationAfterFulfillTemplateCall();
					//Commenting for Northrop Grumman
					// getLocationDependencies.getResults(orderSvc.module, orderSvc.selectedDelivery().shipmentObject).then(function(locResp){
					// 	if(locResp.data && locResp.data.content && locResp.data.content[0]){
					// 		orderSvc.containerTypesList =	locResp.data.content[0].qualifierCodeValues;

					// 		if(orderSvc.selectedDelivery() && orderSvc.selectedDelivery().shipmentObject){
					// 		    angular.forEach(orderSvc.selectedDelivery().shipmentObject.handlingUnits, function(hu, h) {
					// 		     	if(!hu.quantityUom){
					// 		     		angular.forEach(orderSvc.containerTypesList, function(cont, c) {
					// 		     			if(cont.defaultValue){
					// 							hu.quantityUom = cont.code;
					// 						}
					// 		     		});
					// 		     	}

					// 		     	if(hu.quantityUom == "PLT"){
					// 		     		orderSvc.palletTypeFlag = false;
					// 		     	}
					// 		    });
					// 		}
					// 	}
					// 	else{
							angular.forEach(codeLists.OrderDetailsHandlingUnitsRolViewList, function(huField, huI){
								if(huField.fieldName == "quantityUom"){
									orderSvc.containerTypesList = huField.options;
								}
							});

							if(orderSvc.selectedDelivery() && orderSvc.selectedDelivery().shipmentObject){
							    angular.forEach(orderSvc.selectedDelivery().shipmentObject.handlingUnits, function(hu, h) {
							     	if(!hu.quantityUom){
							     		angular.forEach(orderSvc.containerTypesList, function(cont, c) {
							     			if(cont.defaultValue){
												hu.quantityUom = cont.code;
											}
							     		});
							     	}

							     	// if(hu.quantityUom == "PLT"){
							     	// 	orderSvc.palletTypeFlag = false;
							     	// }
							    });
							}
					// 	}
					// });
			    }
			},
			function(data) {
				uiLoaderSvc.hide();
	        	console.log("Service Error - ", data);

	        	//User not authenticated (403) redirecting to login page
				if(data.status == 403){
					logoutService.logout();
				}
				else{
					uiModalSvc.show('ok-cancel-modal.tpl',{
						title:"Error",
						icon: "fa fa-exclamation-triangle",
						close: 'true',
						text:"Cannot create a shipment at this time, please try to create a new shipment.",
						callback:function(){
							$state.go("home.orders.fulfillment.search");
						}
					});	
				}
	   		 });
		}
	}

	initializeShipmentCreationObject();

	if(orderSvc.selectedDelivery().shipmentObject && orderSvc.selectedDelivery().shipmentObject.references){
		$scope.shipmentObjectReferences = orderSvc.selectedDelivery().shipmentObject.references;
	}

	//Below code is to get HU table headers and column details array, dimension , hazmat fields array objects to show on UI
	$scope.itemsHeader = [];
	$scope.dimensionFieldsListArr = [];
	$scope.hazmatFieldsListArr = [];

	//Code is to split the service response into ShipmentSummary, AddtionalInfo and Acknowledgement objects - Objects will be used to render UI
	$scope.shipmentSummaryArr = [];
	$scope.additionalInfoArr = [];
	$scope.acknowledgementArr = [];

	$scope.OrderDetailsShipmentRolViewList = [];
	$scope.OrderDetailsItemsRolViewList = [];
	$scope.dependencyList = [];
	$scope.shipmentDependencyList = [];

	if(codeListService.codeLists && codeListService.codeLists.OrderDetailsShipmentRolViewList){
		$scope.OrderDetailsShipmentRolViewList = codeListService.codeLists.OrderDetailsShipmentRolViewList;
	}

	if(codeListService.codeLists && codeListService.codeLists.OrderDetailsItemsRolViewList){
		$scope.OrderDetailsItemsRolViewList = codeListService.codeLists.OrderDetailsItemsRolViewList;
	}
	
	if(codeListService.codeLists && codeListService.codeLists.FieldDependencyList){
		$scope.dependencyList = codeListService.codeLists.FieldDependencyList;
	}

	angular.forEach($scope.dependencyList, function(list, index) {
		if(list.component == "OF_SHIPMENT"){
			$scope.shipmentDependencyList = list.parents;
		}
	});

	// orderSvc.selectedDelivery().handlingUnits[0].quantityUom = "PLT";

	function validationAfterFulfillTemplateCall(){
		angular.forEach($scope.shipmentDependencyList, function(item, index) {
			var mapping = {};
			if(item.regexChildrenMap){
				mapping = item.regexChildrenMap;
			}
			
			var regex = "";
			$scope.processFieldsArr = [];

			angular.forEach(mapping, function(value, key) {
			 	regex = key;
			 	$scope.processFieldsArr = value;
			});

			var code = item.code;

			angular.forEach($scope.OrderDetailsItemsRolViewList, function(shipmentField, i) {

				angular.forEach($scope.processFieldsArr, function(depField, j) {
					if(shipmentField.fieldName == depField.code){
						shipmentField.fieldType = depField.type;
						shipmentField.required = "N";

					 	if(!regex || !$scope.OrderDetailsItemsRolViewList){
					 		shipmentField.required = 'N';
					 	}

					 	var codeArr = [];
						
					 	if(code){
					 		codeArr = code.split(".");
					 	}

						if(codeArr.length == 1){							
							var tempFlag = "N";
							angular.forEach($scope.OrderDetailsShipmentRolViewList, function(tempField, j) {
								if(angular.isObject(tempField.itemValue)){
									tempField.itemValue = tempField.itemValue.code;
								}
								if((tempField.fieldName == code) && RegExp(regex).test(tempField.itemValue)){
									if(depField.required){
										tempFlag = "Y";
									}
									else{
										tempFlag = "N";
									}								
								}
							});
							shipmentField.required = tempFlag;						
						}
					
						else{
							//Not handled for any codeArr length greater than 2 - Ex: handlingUnits.items.references 
							console.log("Not handeled for any codeArr length greater than 2");
							return false;
						}						
					}
				});
			});
		});
	}

	validationAfterFulfillTemplateCall();

		angular.forEach($scope.shipmentDependencyList, function(item, index) {
		var mapping = {};
		if(item.regexChildrenMap){
			mapping = item.regexChildrenMap;
		}
		
		var regex = "";
		$scope.processFieldsArr = [];

		angular.forEach(mapping, function(value, key) {
		 	regex = key;
		 	$scope.processFieldsArr = value;
		});

		var code = item.code;

		angular.forEach($scope.OrderDetailsShipmentRolViewList, function(shipmentField, i) {

			angular.forEach($scope.processFieldsArr, function(depField, j) {
				if(shipmentField.fieldName == depField.code){
					shipmentField.fieldType = depField.type;
					shipmentField.required = function(column){
						// console.log(regex, $scope, code);

						if(!regex || !$scope.OrderDetailsShipmentRolViewList){
							return false;
						}

						var codeArr = [];
						
						if(code){
							codeArr = code.split(".");
						}

						if(codeArr.length == 1){
							if(shipmentField.displayType == "single checkbox" || shipmentField.displayType == "hidden"){
								var tempFlag = false;
								angular.forEach($scope.OrderDetailsShipmentRolViewList, function(tempField, j) {
									if((tempField.fieldName == code) && (tempField.itemValue)){
										tempFlag = depField.required;
									}
								});
								return tempFlag;
							}
							else{
								var tempFlag = true;
								angular.forEach($scope.OrderDetailsShipmentRolViewList, function(tempField, j) {
									if(angular.isObject(tempField.itemValue)){
										tempField.itemValue = tempField.itemValue.code;
									}
									if((tempField.fieldName == code) && RegExp(regex).test(tempField.itemValue)){
										tempFlag = depField.required;
									}
								});
								return tempFlag;
							}
						}
						else if(codeArr.length == 2){
							var tempFlag = false;
							if(orderSvc.selectedDelivery() && orderSvc.selectedDelivery().shipmentObject){
								angular.forEach(orderSvc.selectedDelivery().shipmentObject.handlingUnits, function(huItem, h) {
									if(RegExp(regex).test(huItem[codeArr[1]])){
										tempFlag = depField.required;
									}
								});
							}
							return tempFlag;
						}
						else{
							//Not handled for any codeArr length greater than 2 - Ex: handlingUnits.items.references 
							console.log("Not handeled for any codeArr length greater than 2");
							return false;
						}
						
					};
					shipmentField.disabled = function(){
						// console.log(regex, $scope, code);
						if(!regex || !$scope.OrderDetailsShipmentRolViewList){
							return false;
						}
						var codeArr = [];
						
						if(code){
							codeArr = code.split(".");
						}
						if(codeArr.length == 1){
							if(shipmentField.displayType == "single checkbox"){
								var tempFlag = true;
								angular.forEach($scope.OrderDetailsShipmentRolViewList, function(tempField, j) {
									if((tempField.fieldName == code) && (tempField.itemValue)){
										tempFlag = depField.disabled;
									}
								});
								return tempFlag;
							}
							else{
								var tempFlag = true;
								angular.forEach($scope.OrderDetailsShipmentRolViewList, function(tempField, j) {
									if(tempField.displayType == "single checkbox"){
										if((tempField.fieldName == code) && (tempField.itemValue)){
											tempFlag = depField.disabled;
										}
									}else{
										if(angular.isObject(tempField.itemValue)){
											tempField.itemValue = tempField.itemValue.code;
										}
										if((tempField.fieldName == code) && RegExp(regex).test(tempField.itemValue)){
											tempFlag = depField.disabled;
										}
									}
								});
								return tempFlag;
							}
						}
						else if(codeArr.length == 2){
							var tempFlag = false;
							if(orderSvc.selectedDelivery() && orderSvc.selectedDelivery().shipmentObject){
								angular.forEach(orderSvc.selectedDelivery().shipmentObject.handlingUnits, function(huItem, h) {
									if(RegExp(regex).test(huItem[codeArr[1]])){
										tempFlag = depField.disabled;
									}
								});
							}
							return tempFlag;
						}
						else{
							//Not handled for any codeArr length greater than 2 - Ex: handlingUnits.items.references 
							console.log("Not handeled for any codeArr length greater than 2");
							return false;
						}
					};						
				}
			});
		});
	});

	angular.forEach($scope.OrderDetailsItemsRolViewList, function(item, index) {
		if(item.sectionSeq == 1){
			$scope.itemsHeader.push(item);
		}
		else if(item.sectionSeq == 2){
			$scope.dimensionFieldsListArr.push(item);
		}
		else if(item.sectionSeq == 3){
			$scope.hazmatFieldsListArr.push(item);
		}
		else{
			//Other sequence number or undefined sequenceNum is not handled
		}
	});


	angular.forEach($scope.OrderDetailsShipmentRolViewList, function(item, index) {
		if(item.sectionSeq == 1){
			$scope.shipmentSummaryArr.push(item);
		}
		else if(item.sectionSeq == 2){
			$scope.additionalInfoArr.push(item);
		}
		else if(item.sectionSeq == 3){
			$scope.acknowledgementArr.push(item);
		}
		else{
			//Other sequence number or undefined sequenceNum is not handled
		}
	});

	//Columnize function to split the array of fields in to fixed number of array of arrays for view.
	function columnize(input, cols, fieldLength) {
	  var arr = [];
	  for(i = 0; i < input.length; i++) {
	  	
	  	if(input[i].required == "Y" || input[i].required == true){
	  		input[i].required = function(){return true};
	  	}
	  	else if(input[i].required == "N" || input[i].required == false || !(input[i].required)){
	  		input[i].required = function(){return false};
	  	}
	  	
	  	if(input[i].disabled == "Y" || input[i].disabled == true){
	  		input[i].disabled = function(){return true};
	  	}
	  	else if(input[i].disabled == "N" || input[i].disabled == false || !(input[i].disabled)){
	  		if(input[i].displayType == 'readonly' || input[i].displayType == 'hidden') {
	  			input[i].disabled = function(){return true};	
	  		}
	  		else{
	  			input[i].disabled = function(){return false};
	  		}	  		
	  	}

	  	if(input[i].displayType != 'textarea' && input[i].displayType != 'hidden' && input[i].displayType != 'none'){
		    var colIdx = Math.floor(i / fieldLength);
	    	arr[colIdx] = arr[colIdx] || [];
	    	arr[colIdx].push(input[i]);
	    }
	    else if(input[i].displayType == 'hidden' && input[i].fieldType == 'CHRG'){
	    	arr[cols] = arr[cols] || [];
	    	arr[cols].push(input[i]);
	    }
	    else if(input[i].displayType == 'hidden' && input[i].fieldType == 'REF'){
	    	arr[cols+1] = arr[cols+1] || [];
	    	arr[cols+1].push(input[i]);
	    }
	  }
	  return arr;
	}

	//Calling columnize function and the numbers (4 or 5) are the fields that we want to show on view per row
	$scope.shipmentSummaryColumns = columnize($scope.shipmentSummaryArr, Math.ceil($scope.shipmentSummaryArr.length/4), 4);
	$scope.additionalInfoColumns = columnize($scope.additionalInfoArr, Math.ceil($scope.additionalInfoArr.length/5), 5);
	$scope.handlingUnitFieldsArrayColumns = columnize($scope.handlingUnitFieldsArray, Math.ceil($scope.handlingUnitFieldsArray.length/2), 2);

	orderSvc.selectedDelivery().totalItems = 0;
	orderSvc.selectedDelivery().totalPallets;
	orderSvc.selectedDelivery().totalWeight = "";
	orderSvc.selectedDelivery().totalVolume = "";

	//Calculation Total items and updating the model for it. 
	angular.forEach($scope.huObj, function(hu,index) {
		angular.forEach(hu.items, function(item, i) {
			if(!!item.quantity && orderSvc.selectedDelivery().shipmentObject && orderSvc.selectedDelivery().shipmentObject.handlingUnits[index] && orderSvc.selectedDelivery().shipmentObject.handlingUnits[index].items[i] && orderSvc.selectedDelivery().shipmentObject.handlingUnits[index].items[i].quantity){
				orderSvc.selectedDelivery().totalItems += parseInt(orderSvc.selectedDelivery().shipmentObject.handlingUnits[index].items[i].quantity);
			}
		});
	});

	$scope.redefineTotalItems=function(event, item){
		if(isNaN(item.quantity)){
			item.quantity = "";
		}
		else{
			if($scope.configList && $scope.configList.OF_UI_OVERFULFILL_ALLOWED == "N"){
				var reqQty;
				var fulQty;
				angular.forEach(item.references, function(ref, index) {
					if(ref.referenceCode == "RQTY"){
						reqQty = ref.referenceNum;
					}

					if(ref.referenceCode == "FQTY"){
						fulQty = ref.referenceNum;
					}
				});
				if(parseInt(item.quantity) > parseInt(reqQty - fulQty)){
					uiModalSvc.show('ok-cancel-modal.tpl',{
						title:"Error",
						icon: "fa fa-exclamation-triangle",
						close: 'true',
						text:"Planned quantity cannot be greater remaining quantity, do you wish to change planned quantity to remaining quantity?",
						callback:function(){
							item.quantity= parseInt(reqQty - fulQty);
						}
					});	
					item.quantity= "";
				}
			}

			orderSvc.selectedDelivery().totalItems = 0;
			if(orderSvc.selectedDelivery().shipmentObject){
			    angular.forEach(orderSvc.selectedDelivery().shipmentObject.handlingUnits, function(hu,index) {
					angular.forEach(hu.items, function(item, i) {
						if(!!item.quantity){
							orderSvc.selectedDelivery().totalItems += parseInt(item.quantity);
						}
					});
				});
			}
		}
	}

	$scope.validationErrorPP = false;

	$scope.defineTotalVolume=function(){
		angular.forEach($scope.shipmentSummaryArr, function(item, index) {
			if(item.fieldName == "PP" && item.itemValue){
				if(RegExp("^([1-9]|1[0-9]|2[0-6])$").test(item.itemValue)){
					$scope.validationErrorPP = false;
					orderSvc.selectedDelivery().totalVolume = parseInt(item.itemValue) * 100;
				}
				else{
					$scope.validationErrorPP = true;
					item.itemValue = "";
					orderSvc.selectedDelivery().totalVolume = "";
					angular.element(document.querySelector('ui-field[label = "Pallet Positions"] input'))[0].focus();
				}
			}
		});
	}

	$scope.selectedLocation=function(item, args){

		if(args.code == "19"){
			orderSvc.selectedDelivery().origin.id = item.type;
	        orderSvc.selectedDelivery().origin.name = {'name' : item.name};
	        orderSvc.selectedDelivery().origin.address = item.address1;
	        orderSvc.selectedDelivery().origin.address2 = item.address2;
	        orderSvc.selectedDelivery().origin.city = item.city;
	        orderSvc.selectedDelivery().origin.sau =  item.state;
	        orderSvc.selectedDelivery().origin.postalCode = item.postalCode;
	        orderSvc.selectedDelivery().origin.country = item.country;
	        orderSvc.selectedDelivery().origin.entityId =  item.code;
		}

		if(args.code == "20"){
			orderSvc.selectedDelivery().destination.id = item.type;
	        orderSvc.selectedDelivery().destination.name = {'name' : item.name};
	        orderSvc.selectedDelivery().destination.address = item.address1;
	        orderSvc.selectedDelivery().destination.address2 = item.address2;
	        orderSvc.selectedDelivery().destination.city = item.city;
	        orderSvc.selectedDelivery().destination.sau = item.state;
	        orderSvc.selectedDelivery().destination.postalCode = item.postalCode;
	        orderSvc.selectedDelivery().destination.country = item.country;
	        orderSvc.selectedDelivery().destination.entityId = item.code;
		}
		//Commented for NG
		// if(orderSvc.selectedDelivery().shipmentObject){
			
		// 	getLocationDependencies.getResults(orderSvc.module, getUpdatedShipmentObj()).then(function(locResp){
		// 		if(locResp.data.content && locResp.data.content[0]){
		// 			orderSvc.containerTypesList =	locResp.data.content[0].qualifierCodeValues;
		// 		}
		// 	});
		// }

		// $scope.$apply();
	}

	$scope.splitItem=function(hIndex,index){
		orderSvc.splitItem(index);
	}

	$scope.splitItemToOwn=function(hIndex){
		dIndex=orderSvc.dIndex;
		orderSvc.splitItemToOwn(dIndex,hIndex);
	}

	$scope.removeItem=function(hIndex,index, item){
		dIndex=orderSvc.dIndex;

		if(item.lineItemBreakdownNum){

			angular.forEach($scope.huObj, function(hu, h) {
				angular.forEach(hu.items, function(itemVal, i) {
					var lineItem = itemVal.orderNumber+"_"+itemVal.lineItemNum;

					if(lineItem == item.lineItemBreakdownNum){
						index = i;
					}
				});
			});
		}
		// if(orderSvc.deliveryOrders[dIndex] && orderSvc.deliveryOrders[dIndex].shipmentObject){
		// 	var removeIndex = orderSvc.selectedSearchOrders.indexOf(orderSvc.deliveryOrders[dIndex].shipmentObject.handlingUnits[hIndex].items[index].lineItemBreakdownNum);
		// 	if(removeIndex >= 0)
		// 		orderSvc.selectedSearchOrders.splice(removeIndex, 1);
		// }
		if(index > -1)
			orderSvc.removeItem(dIndex,hIndex,index);
		else
			console.log("Cannot remove item from Handling Unit");
	}

	$scope.deleteDeliveryOrder=function(){
		dIndex=orderSvc.dIndex;
		uiModalSvc.show('ok-cancel-modal.tpl',{
			title:"Delete Order",
			close: 'true',
			icon: "fa fa-exclamation-triangle",
			text:"Are you sure you want delete "+orderSvc.deliveryOrders[dIndex].name+"?",
			callback:function(){
				angular.forEach(orderSvc.deliveryOrders[dIndex].shipmentObject.handlingUnits, function(hu, h) {
					angular.forEach(hu.items, function(item, j) {
						var removeIndex = orderSvc.selectedSearchOrders.indexOf(item.lineItemBreakdownNum);
						if(removeIndex >= 0)
							orderSvc.selectedSearchOrders.splice(removeIndex, 1);
					});
				});
				orderSvc.deleteDeliveryOrder(dIndex);

				// if (orderSvc.deliveryOrders.length==0){
				$state.go('home.orders.fulfillment');
				// }
			}
		});	
	}

	$scope.deleteHandlingUnit=function(hIndex){
		dIndex=orderSvc.dIndex;
		uiModalSvc.show('ok-cancel-modal.tpl',{
			title:"Delete Handling Unit",
			close: 'true',
			icon: "fa fa-exclamation-triangle",
			text:"Are you sure you want delete "+orderSvc.deliveryOrders[dIndex].handlingUnits[hIndex].name+"?",
			callback:function(){
				angular.forEach(orderSvc.deliveryOrders[dIndex].shipmentObject.handlingUnits[hIndex].items, function(item, j) {
					var removeIndex = orderSvc.selectedSearchOrders.indexOf(item.lineItemBreakdownNum);
					if(removeIndex >= 0)
						orderSvc.selectedSearchOrders.splice(removeIndex, 1);
				});
				orderSvc.deleteHandlingUnit(dIndex,hIndex);	
			}
		});	
	}

	$scope.addHandlingUnit=function(){
		dIndex=orderSvc.dIndex;
		$scope.uiModalSvc.show('of-new-handling-unit-modal.tpl',{
			dIndex:dIndex,
			mode:'new'
		});
	}

	// $scope.handlingUnitInfo=function(hIndex){
	// 	if ($scope.handlingUnitInfoIndex==hIndex){
	// 		$scope.handlingUnitInfoIndex=-1;
	// 	}
	// 	else{
	// 		$scope.handlingUnitInfoIndex=hIndex;
	// 	}

	// 	/*dIndex=orderSvc.dIndex;
	// 	$scope.uiModalSvc.show('of-new-handling-unit-modal.tpl',{
	// 		dIndex:dIndex,
	// 		hIndex:hIndex,
	// 		mode:'edit'
	// 	});	*/
	// }

	$scope.handlingUnitInfo=function(hIndex){
		dIndex=orderSvc.dIndex;
		uiModalSvc.show('of-new-handling-unit-modal.tpl',{
			dIndex:dIndex,
			hIndex:hIndex,
			mode:'edit'
		});	
	}

	$scope.splitItem=function(hIndex,index){
		dIndex=orderSvc.dIndex;
		$scope.uiModalSvc.show('of-split-item-modal.tpl',{
			dIndex:dIndex,
			hIndex:hIndex,
			index:index
		});
	}	

	$scope.moveItem=function(hIndex,index){
		dIndex=orderSvc.dIndex;
		uiModalSvc.show('of-move-item-modal.tpl',{
			dIndex:dIndex,
			hIndex:hIndex,
			index:index
		});
	}

	$scope.clearPlanned=function(){
		dIndex=orderSvc.dIndex;
		uiModalSvc.show('ok-cancel-modal.tpl',{
			title:"Clear Planned",
			close: 'true',
			icon: "fa fa-exclamation-triangle",
			text:"Are you sure you want to clear all planned quantities?",
			callback:function(){
				orderSvc.clearPlanned(dIndex);
			}
		});
	}

	$scope.openHazmat=function(hIndex,index){
		dIndex=orderSvc.dIndex;
		$scope.uiModalSvc.show('of-hazmat-modal.tpl',{
			dIndex:dIndex,
			hIndex:hIndex,
			index:index,
			hazArray: $scope.hazmatFieldsListArr
		});
	}

	$scope.openDims=function(hIndex,index){
		dIndex=orderSvc.dIndex;
		uiModalSvc.show('of-dimensions-modal.tpl',{
			dIndex:dIndex,
			hIndex:hIndex,
			index:index,
			dimArray: $scope.dimensionFieldsListArr
		});
	}

	$scope.openSerialization=function(hIndex,index){
		dIndex=orderSvc.dIndex;
		$scope.tempItem=orderSvc.deliveryOrders[dIndex].shipmentObject.handlingUnits[hIndex].items[index];
		$scope.tempItem["serArray"] = orderSvc.deliveryOrders[dIndex].shipmentObject.handlingUnits[hIndex].items[index].serArray || [];
		$scope.uiModalSvc.show('of-serialization-modal.tpl',{
			dIndex:dIndex,
			hIndex:hIndex,
			index:index
		});
	}

	$scope.openPrint=function(hIndex,index){
		dIndex=orderSvc.dIndex;
		$scope.uiModalSvc.show('of-print-item-modal.tpl',{
			dIndex:dIndex,
			hIndex:hIndex,
			index:index
		});
	}

	$scope.openAddressLookup=function(){
		dIndex=orderSvc.dIndex;
		$scope.uiModalSvc.show('address-lookup-modal.tpl',{});
	}

	$scope.addresslookUp=function(name){
		dIndex=orderSvc.dIndex;
		uiModalSvc.show('address-lookup-modal.tpl',{
			type: name
		});	
	}

	$scope.searchTest="ElectroLux Chicago RDC"
	$scope.optionsTest=[
			"Northrop Grumman Corporation",
			"Warehouse #1199",
			"L-3 Communications Integrated"
	]

	///////////////////////////////////////////////////
	// SUBMIT
	//
	
	$scope.steps=[
	{
		name:"Submitting Request",
		status:2
	},
	{
		name:"Processing Data",
		status:1
	},
	{
		name:"Reviewing Shipments",
		status:0
	},
	{
		name:"Creating Labels",
		status:0
	}
	]

	$scope.addTracking=function(hIndex){
		dIndex=orderSvc.dIndex;
		$scope.uiModalSvc.show('of-complete-tracking-device-modal.tpl',{
			dIndex:dIndex,
			hIndex:hIndex
		});
	}

	$scope.resetScenarios=function(){
		$scope.showLabels=false;
		$scope.processing=false;
		$scope.steps[0].status=2;
		$scope.steps[1].status=1;
		$scope.steps[2].status=0;
		$scope.steps[3].status=0;
	}

	$scope.scenario1=function(){
		$scope.resetScenarios();
		$scope.scenario=1;
		$scope.uiLoaderSvc.show();
		$timeout(function(){
			$scope.steps[1].status=2;
			$scope.steps[2].status=2;
			$scope.steps[3].status=2;
			$scope.showLabels=true;
			$scope.uiLoaderSvc.hide();
		},2000)
	}

	$scope.scenario2=function(){
		$scope.resetScenarios();
		$scope.scenario=2;
		$scope.uiLoaderSvc.show();
		$timeout(function(){
			$scope.steps[1].status=2;
			$scope.steps[2].status=1;
			$scope.steps[3].status=0;
			$scope.uiModalSvc.show('of-complete-palletize-modal.tpl',{
				callback:function(){
					$scope.uiLoaderSvc.show();
					$timeout(function(){
						$scope.steps[2].status=2;
						$scope.steps[3].status=2;
						$scope.showLabels=true;
						$scope.uiLoaderSvc.hide();
					},2000)
				}
			});
			$scope.uiLoaderSvc.hide();
		},2000)
	}

	$scope.scenario3=function(){
		$scope.resetScenarios();
		$scope.scenario=3;
		$scope.uiLoaderSvc.show();
		$timeout(function(){
			$scope.steps[1].status=2;
			$scope.steps[2].status=1;
			$scope.steps[3].status=0;
			$scope.showPalletModal=true;
			$scope.processing=true;
			$scope.uiLoaderSvc.hide();
		},2000)
	}

	$scope.openPrint=function(hIndex,index){
		dIndex=orderSvc.dIndex;
		$scope.uiModalSvc.show('of-print-document-modal.tpl',{});
	}

	
	function processResults(tempArr) {
		angular.forEach(tempArr, function(item, index) {
			if(item.displayType == "dropdown" && angular.isObject(item.itemValue)){
				item.itemValue = item.itemValue.code;
			}

			if(item.displayType == "time" && angular.isObject(item.itemValue)){
				item.itemValue = item.itemValue.name;
			}


        	if(item.fieldType == "PROP"){
        		if(item.displayType == 'single checkbox' && item.displayName == "Flatbed" && item.itemValue){
        			orderSvc.selectedDelivery().shipmentObject[item.fieldName] = item.options[0].code;
        		}
        		else if(item.code == "equipmentType" && !item.itemValue){
					orderSvc.selectedDelivery().shipmentObject[item.fieldName] = "";
        		}
        		else{
					orderSvc.selectedDelivery().shipmentObject[item.fieldName] = item.itemValue;
        		}
			}
			else if(item.fieldType == "REF"){
				if(orderSvc.selectedDelivery().shipmentObject && orderSvc.selectedDelivery().shipmentObject.references){
					var refFlag = false;
					angular.forEach(orderSvc.selectedDelivery().shipmentObject.references, function(ref, i) {
						if(ref.referenceCode == item.fieldName){
							refFlag = true;
							orderSvc.selectedDelivery().shipmentObject.references[i].referenceNum = item.itemValue;
						}
					});
					if(!refFlag && !!item.itemValue){
						var tempObj = {};
						tempObj["referenceCode"] = item.fieldName;
						tempObj["referenceNum"] = item.itemValue;
						orderSvc.selectedDelivery().shipmentObject.references.push(tempObj);
					}
				}
			}
			else if(item.fieldType == "CHRG"){
				if(orderSvc.selectedDelivery().shipmentObject && orderSvc.selectedDelivery().shipmentObject.chargeOverrides){
					var chargeFlag = false;
					angular.forEach(orderSvc.selectedDelivery().shipmentObject.chargeOverrides, function(ref, i) {
						if(ref.chargeCd == item.fieldName){
							chargeFlag = true;
							if(item.itemValue){								
								orderSvc.selectedDelivery().shipmentObject.chargeOverrides[i].chargeCd = item.fieldName;
							}
							else if(!item.itemValue){
								orderSvc.selectedDelivery().shipmentObject.chargeOverrides.splice(i,1);
							}
						}
					});
					if(!chargeFlag && item.itemValue){
						var tempObj = {};
						tempObj["chargeCd"] = item.fieldName;
						orderSvc.selectedDelivery().shipmentObject.chargeOverrides.push(tempObj);
					}
				}
			}
			else{
				//do for other field types
			}
        });
	}
	
	function getUpdatedShipmentObj() {
		processResults($scope.shipmentSummaryArr);
		processResults($scope.additionalInfoArr);
		processResults($scope.acknowledgementArr);
		
		var shipmentObj = orderSvc.selectedShipmentUpdated();
		shipmentObj.weight = orderSvc.selectedDelivery().totalWeight;
		shipmentObj.volume = orderSvc.selectedDelivery().totalVolume;

        angular.forEach(shipmentObj.handlingUnits, function(hu, h) {
         	if(!hu.quantityUom){
         		angular.forEach(orderSvc.containerTypesList, function(cont, c) {
         			if(cont.defaultValue){
						hu.quantityUom = cont.code;
					}
         		});
         	}

         	angular.forEach($scope.handlingUnitFieldsArray, function(huField, hIndex){
         		if(huField.displayType == "dropdown" && angular.isObject(hu[huField.fieldName])){
         			hu[huField.fieldName] = hu[huField.fieldName].code;
         		}
         	});

         	angular.forEach(hu.items, function(item, itemIndex){
         		var tempItem = item;
	         	angular.forEach(item.serArray, function(ser, index){
				 	if(!!ser){
					 	var testObj = {};
					 	testObj["referenceCode"] = "SER";
					 	testObj["referenceNum"] = ser;
					 	item["references"].push(testObj);
					}	
				});
			});
        });
        return shipmentObj;
	}

	function submitShipment() {
			dIndex=orderSvc.dIndex;
			getUpdatedShipmentObj();
	        afterValidationSubmit();
	}

	function afterValidationSubmit(){
		if(orderSvc.selectedDelivery().shipmentObject){
			$scope.tempArr = [];

			angular.forEach(orderSvc.selectedDelivery().shipmentObject.handlingUnits, function(hu, hIndex){
				angular.forEach(hu.items, function(item, itemIndex){
					angular.forEach(item.references, function(ref, rIndex){
						if(ref.referenceCode == "SER"){
							if(!item.serArray || item.serArray.length != item.quantity){
								$scope.tempArr.push("Serialization codes are missing for Handling Unit: <strong>"+hu.name+"</strong> and for Item: <strong>"+item.description+"</strong>");
							}
						}
					});
				});
			});

			if($scope.tempArr.length > 0){
				$(".root-view").scrollTop(0);
			}
			else{
				if(orderSvc.selectedDelivery().shipmentObject.requestShipFromDt){
		        	var readyDate = new Date(orderSvc.selectedDelivery().shipmentObject.requestShipFromDt * 1000);
		        	var todayDate = new Date();

		        	var timeDiff = readyDate.getTime() - todayDate.getTime();
					var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
					if(diffDays < $scope.configList.OF_UI_SHIP_FROM_DATE_CUTOFF_DAYS_MIN || diffDays > $scope.configList.OF_UI_SHIP_FROM_DATE_CUTOFF_DAYS_MAX){
						var alertText;
						if(diffDays < $scope.configList.OF_UI_SHIP_FROM_DATE_CUTOFF_DAYS_MIN){
							alertText = "Ready to Pickup date cannot be before Today's date."
						}
						else{
							alertText = "Ready to pick up date cannot be more than "+scope.configList.OF_UI_SHIP_FROM_DATE_CUTOFF_DAYS_MAX+ " days from Today, Please modify to continue."
						}
						uiModalSvc.show('ok-cancel-modal.tpl',{
						title:"Alert",
						icon: "fa fa-exclamation-triangle",
						text: alertText,
						callback:function(){
								angular.element(document.querySelector('ui-field[label = "Ready to Pickup"] input'))[0].focus();
							}
						});	
					}
					else{
						submitShipmentService();
					}
		        }
		        else{
		        	submitShipmentService();
		        }
		    }
	    }
	}

	function submitShipmentService(){
		uiLoaderSvc.show();

		var reqObj = {};

		reqObj["shipment"] = orderSvc.selectedDelivery().shipmentObject;
		reqObj["saveDefaultOrigin"] = orderSvc.selectedDelivery().setAsDefaultOrigin || false;
		reqObj["saveDefaultDestination"] = orderSvc.selectedDelivery().setAsDefaultDestination || false;

		createShipment.create(orderSvc.module, reqObj).then(function(resp){
			uiLoaderSvc.hide();
			
			//removeing items from selectedSearchOrders from search results page
			dIndex=orderSvc.dIndex;
			if(orderSvc.deliveryOrders[dIndex] && orderSvc.deliveryOrders[dIndex].shipmentObject){
				angular.forEach(orderSvc.deliveryOrders[dIndex].shipmentObject.handlingUnits, function(handlingUnit, hIndex) {
					angular.forEach(handlingUnit.items, function(itemDetail, iIndex) {
						var removeIndex = orderSvc.selectedSearchOrders.indexOf(orderSvc.deliveryOrders[dIndex].shipmentObject.handlingUnits[hIndex].items[iIndex].lineItemBreakdownNum);
						if(removeIndex >= 0)
							orderSvc.selectedSearchOrders.splice(removeIndex, 1);
						});
				});
			}

			if(resp.data.content){
				orderSvc.shipmentCreateObj = resp.data.content;
				//Removing the Delivery order from the list once the shipment is created.
				angular.forEach(orderSvc.deliveryOrders[dIndex].handlingUnits, function(hu, h) {
					angular.forEach(hu.items, function(item, j) {
						var removeIndex = orderSvc.selectedSearchOrders.indexOf(item.lineItemBreakdownNum);
						if(removeIndex >= 0)
							orderSvc.selectedSearchOrders.splice(removeIndex, 1);
					});
					orderSvc.deliveryOrders[dIndex].handlingUnits.splice(h, 1);
					orderSvc.deliveryOrders[dIndex].shipmentObject.handlingUnits.splice(h, 1);
				});
				orderSvc.deliveryOrders.splice(dIndex, 1);
			}

			$state.go('home.order-fulfillment.delivery-submit');
		},
		function(err){
			uiLoaderSvc.hide();
			console.log("Service Error - ", err);

			//User not authenticated (403) redirecting to login page
			if(err.err.status == 403){
				logoutService.logout();
			}
			else if(err.err.status == 400){
				uiModalSvc.show('ok-cancel-modal.tpl',{
					title:"Error",
					icon: "fa fa-exclamation-triangle",
					html: "There was a system error, please check with application support.",
					callback:function(){
						//Do nothing on OK button.
					}
				});
			}
			else{

				var errMessage = "<ul>"
				for(i=0; i<err.err.data.messages.length; i++){
					errMessage += "<li>"+ err.err.data.messages[i].message+"</li>";
				}
				errMessage += "</ul>";

				uiModalSvc.show('ok-cancel-modal.tpl',{
					title:"Error",
					icon: "fa fa-exclamation-triangle",
					html: errMessage,
					callback:function(){
						//Do nothing on OK button.
					}
				});
			}
		});
	}

	$scope.submitOrder=function(flag){
		if(flag){
			// uiLoaderSvc.show();
			submitShipment();
			// shipmentValidations.getResults(orderSvc.module, getUpdatedShipmentObj()).then(function(resp){
			// 	uiLoaderSvc.hide();

			// 	if(resp.data.content && resp.data.content.length > 0){
			// 		if(resp.data.content[0].level == "ERROR"){
			// 			uiModalSvc.show('ok-cancel-modal.tpl',{
			// 				title:"Error",
			// 				icon: "fa fa-exclamation-triangle",
			// 				html: resp.data.content[0].msg,
			// 				callback:function(){
			// 					//Do nothing on OK button.
			// 				}
			// 			});
			// 		}
			// 		else{
			// 			uiModalSvc.show('ok-cancel-modal.tpl',{
			// 				title:"Warning",
			// 				close: "true",
			// 				icon: "fa fa-exclamation-triangle",
			// 				html: resp.data.content[0].msg,
			// 				callback:function(){
			// 					submitShipment();
			// 				}
			// 			});
			// 		}
			// 	}
			// 	else{
			// 		submitShipment();
			// 	}				
			// },
			// function(err){
			// 	uiLoaderSvc.hide();
			// 	console.log("Service Error - ", err);

			// 	//User not authenticated (403) redirecting to login page
			// 	if(err.err.status == 403){
			// 		logoutService.logout();
			// 	}
			// 	else if(err.err.status == 400){
			// 		uiModalSvc.show('ok-cancel-modal.tpl',{
			// 			title:"Error",
			// 			icon: "fa fa-exclamation-triangle",
			// 			html: "There was a system error, please check with application support.",
			// 			callback:function(){
			// 				//Do nothing on OK button.
			// 			}
			// 		});
			// 	}
			// 	else{

			// 		var errMessage = "<ul>"
			// 		for(i=0; i<err.err.data.messages.length; i++){
			// 			errMessage += "<li>"+ err.err.data.messages[i].message+"</li>";
			// 		}
			// 		errMessage += "</ul>";

			// 		uiModalSvc.show('ok-cancel-modal.tpl',{
			// 			title:"Error",
			// 			icon: "fa fa-exclamation-triangle",
			// 			html: errMessage,
			// 			callback:function(){
			// 				//Do nothing on OK button.
			// 			}
			// 		});
			// 	}
			// });

			// Code to check if the quantity is zero and throw error accordingly

			// var resultFlag = true;
			// if(orderSvc.selectedDelivery() && orderSvc.selectedDelivery().shipmentObject && orderSvc.selectedDelivery().shipmentObject.handlingUnits){
			// 	orderSvc.selectedDelivery().shipmentObject.handlingUnits.forEach((hu, i) => {
			// 		hu.items.forEach((item, index) => {
			// 			if(!item.quantity || item.quantity == "0"){
			// 				resultFlag = false;
			// 			}
			// 		});
			// 	});
			// }

			// if(resultFlag){
			// 	submitShipment();
			// }
			// else{
			// 	uiModalSvc.show('ok-cancel-modal.tpl',{
			// 	title:"Alert",
			// 	icon: "fa fa-exclamation-triangle",
			// 	text:"Planned quanity cannot be '0', please modify and resubmit for shipment process",
			// 	callback:function(){
			// 			//Don't do anything
			// 		}
			// 	});	
			// }



			
		// 	if($scope.configList){
		// 		var configContainerType = $scope.configList.OF_UI_CONTAINER_TYPES_UNMIXABLE;

		// 		var containerTypeFlag = true;

		// 		if(!!configContainerType){
				
		// 			orderSvc.selectedDelivery().shipmentObject.handlingUnits.forEach((cont, i) => {
		// 				if(!!cont.quantityUom){
		// 					if(!cont.quantityUom.match(RegExp(configContainerType))){
		// 						containerTypeFlag = false;
		// 					}
		// 				}
		// 				else{
		// 					containerTypeFlag = false; // If user havne't selected container type for any HU, then containerType is undefined and we 
		// 				}
		// 			});

		// 			if(!containerTypeFlag){
		// 				configContainerType = configContainerType.split("|");

		// 				uiModalSvc.show('ok-cancel-modal.tpl',{
		// 				title:"Warning",
		// 				text:"Carton handling units cannot be combined with any other handling unit types. Do you wish to change all handling unit types for this Delivery Order to "+configContainerType[0]+"? If not, please create a separate fulfillment for this handling unit.",
		// 				callback:function(){
		// 						orderSvc.selectedDelivery().shipmentObject.handlingUnits.forEach((cont, i) => {
		// 							cont.quantityUom = configContainerType[0];
		// 						});

		// 						submitShipment();
		// 					}
		// 				});							
		// 			}
		// 			else{
		// 				///submit to create shipment service when all the HUs container type is equals to OF_UI_CONTAINER_TYPES_UNMIXABLE
		// 				submitShipment();
		// 			}
		// 		}
		// 		else{
		// 			//submit to create shipment service when OF_UI_CONTAINER_TYPES_UNMIXABLE is undefined/null from config list
		// 			submitShipment();
		// 		}
		// 	}
		// 	else{
		// 		//submit to create shipment service even though we do not have config list from codeList call
		// 		submitShipment();
		// 	}			
		}
		else{
			//TODO: to show validation errors on page
		}		
	}

	$scope.$colors=$colors;
}]);