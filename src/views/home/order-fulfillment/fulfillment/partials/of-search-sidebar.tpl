<div class="form form-dark">
	<div class="rows">
		<ui-header theme="ui-sidebar-header">
			<ui-header-body>
				<div>
					<span>
						Search
					</span>
				</div>
			</ui-header-body>
			<ui-header-controls>
				<button ui-button title="Clear" ng-click="clearFilters()">
					<i class="fa fa-times text-red"></i>
				</button>
				<button ui-button title="Apply" ng-click="doSearch()">
					<i class="fa fa-check text-green"></i>
				</button>
			</ui-header-controls>
		</ui-header>
		<ui-menu-group>
			<ui-menu-content>
				<dynamic-template data="searchContent"></dynamic-template>
				<!-- <ui-field type="select" options="filters.sector.options" ng-model="filters.sector.value" label="Sector" placeholder="---" ng-required="false" ng-disabled="false"></ui-field>
				<ui-field type="date" ng-model="dateTest" label="On-Dock Date" placeholder="---" ng-required="false" ng-disabled="false"></ui-field>	
				<ui-field type="multi" options="filters.orderNumber.options" ng-model="filters.orderNumber.value" label="Order Number" placeholder="---" ng-required="false" ng-disabled="false"></ui-field>
				<ui-field type="multi" options="filters.supplierName.options" ng-model="filters.supplierName.value" label="Supplier Name" placeholder="---" ng-required="false" ng-disabled="false"></ui-field>
				<ui-field type="text" ng-model="filters.itemDescription.value" label="Description" placeholder="---" ng-required="false" ng-disabled="false"></ui-field> -->
				<ui-header theme="ui-menu-header"  class="" ng-if="additionalSearchContent.length > 0">
					<ui-header-body>
						<span>Additional Filters</span>
					</ui-header-body>
				</ui-header>
				<dynamic-template data="additionalSearchContent"></dynamic-template>
			</ui-menu-content>
		</ui-menu-group>
		<!-- <ui-menu-group collapsed>
			<ui-header theme="ui-menu-header">
				<ui-header-body>
					<div>
						<span>
						Additional Filters
						</span>
					</div>
				</ui-header-body>
				<ui-header-controls>
					<button ui-button ui-menu-collapse>
						<i class="fa fa-chevron-{{collapsed?'down':'up'}}"></i>
					</button>
				</ui-header-controls>
			</ui-header>
			<ui-menu-content ui-max-height>
				<ui-field type="select" options="filters.psa.options" ng-model="filters.psa.value" label="PSA" placeholder="---" ng-required="false" ng-disabled="false"></ui-field>
				<ui-field type="select" options="filters.origin.options" ng-model="filters.origin.value" label="Origin" placeholder="---" ng-required="false" ng-disabled="false"></ui-field>
				<ui-field type="select" options="filters.destination.options" ng-model="filters.destination.value" label="Destination" placeholder="---" ng-required="false" ng-disabled="false"></ui-field>
			</ui-menu-content>
		</ui-menu-group> -->
	</div>
</div>