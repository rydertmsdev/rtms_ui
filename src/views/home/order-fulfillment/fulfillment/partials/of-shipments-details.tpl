<form name="deliveryDetailsForm">
	<ui-alert-container></ui-alert-container>
	<ui-section class="errorContainer bg-accent" ng-if="tempArr.length > 0">
		<cell class="ui-alert-body" ng-repeat="err in tempArr" ui-width="100%" style="padding:10px">
			<i class="fa text-white fa-exclamation-triangle"></i>
			<span class="text text-white" ng-bind-html="err"></span>
		</cell>
	</ui-section>
	<div class="cols">		
		<ui-section>
			<ui-header theme="section">
				<ui-header-body>
					<div>
						<span>
							Origin
						</span>
					</div>
				</ui-header-body>
				<ui-header-controls hide-actions>
					<div ui-button ui-header-actions-btn  class="accent">
						<i class="ic-dot-3"></i>
					</div>
					<div ui-button title="Set as my default address" class="text-eBlue" ng-click="orderSvc.selectedDelivery().homeAddress=!orderSvc.selectedDelivery().homeAddress">
						<i class="fa fa-check"></i>
						<span>{{orderSvc.selectedDelivery().setAsDefaultOrigin?'Unset':'Set'}} as Default</span>
					</div>
					<div ui-button title="Address Book" class="text-orange" ng-click="addresslookUp(19)">
						<i class="fa fa-address-book"></i>
						<span>Address Book</span>
					</div>
				</ui-header-controls>
			</ui-header>
			<div class="form address-form" ng-class="{'home-location':orderSvc.selectedDelivery().homeAddress}">
				<ui-field type="select" placeholder="Address Look up" ng-model="orderSvc.selectedDelivery().origin.name" prop="name" options="originIds" class="no-label address-lookup" ng-required="true" ng-disabled="false" refresh="getLocations" refresh-args="{'name':'location', 'code':'19'}" on-select="selectedLocation" selected-args="{'name': orderSvc.selectedDelivery().origin.entityId.name, 'code':'19'}"></ui-field>
				<div class="cols">
					<div class="form-very-condensed">
						<ui-field type="text" ng-value="orderSvc.selectedDelivery().origin.address" class="no-label" ng-required="true" ng-disabled="true"></ui-field>
						<ui-field type="text" ng-value="orderSvc.selectedDelivery().origin.city+', '+orderSvc.selectedDelivery().origin.sau + ' ' + orderSvc.selectedDelivery().origin.postalCode" class="no-label" ng-required="true" ng-disabled="true"></ui-field>
						<ui-field type="text" ng-value="orderSvc.selectedDelivery().origin.country" class="no-label" ng-required="true" ng-disabled="true"></ui-field>
					</div>
				</div>
				<div class="address-form-id">
					ID: {{orderSvc.selectedDelivery().origin.entityId}}
				</div>
			</div>
		</ui-section>

		<ui-section>
			<ui-header theme="section">
				<ui-header-body>
					<div>
						<span>
							Destination
						</span>
					</div>
				</ui-header-body>
				<ui-header-controls hide-actions>
					<div ui-button ui-header-actions-btn  class="accent">
						<i class="ic-dot-3"></i>
					</div>
					<div ui-button title="Address Book" class="text-orange" ng-click="addresslookUp(20)">
						<i class="fa fa-address-book"></i>
						<span>Address Book</span>
					</div>
				</ui-header-controls>
			</ui-header>
			<div class="form address-form">
				<ui-field type="select" ng-model="orderSvc.selectedDelivery().destination.name" prop="name"  options="optionsTest" class="no-label address-lookup" ng-required="true" ng-disabled="true"></ui-field>
				<div class="cols">
					<div class="form-very-condensed">
						<ui-field type="text" ng-value="orderSvc.selectedDelivery().destination.address" class="no-label" ng-required="true" ng-disabled="true"></ui-field>
						<ui-field type="text" ng-value="orderSvc.selectedDelivery().destination.city+', '+orderSvc.selectedDelivery().destination.sau + ' ' + orderSvc.selectedDelivery().destination.postalCode" class="no-label" ng-required="true" ng-disabled="true"></ui-field>
						<ui-field type="text" ng-value="orderSvc.selectedDelivery().destination.country" class="no-label" ng-required="true" ng-disabled="true"></ui-field>
					</div>
				</div>
				<div class="address-form-id">
					ID: {{orderSvc.selectedDelivery().destination.entityId}}
				</div>
			</div>
		</ui-section>
	</div>

	<ui-section ng-repeat="(hIndex,unit) in orderSvc.selectedDelivery().shipmentObject.handlingUnits track by $index">
		<ui-header theme="section" class="no-border">
			<ui-header-body>
				<div>
					<span>
						{{orderSvc.selectedDelivery().shipmentObject.handlingUnits[hIndex].description || orderSvc.selectedDelivery().handlingUnits[hIndex].name}}
					</span>
				</div>
			</ui-header-body>
			<ui-header-controls>
				<div ui-button ng-click="handlingUnitInfo(hIndex)" tabindex="-1">
					<i class="fa fa-info"></i>
				</div>
			</ui-header-controls>
			<ui-header-controls hide-actions>
				<div ui-button ui-header-actions-btn  class="accent">
					<i class="ic-dot-3"></i>
				</div>
				<div ui-button ng-click="addTracking(hIndex);">
					<i class="fa fa-crosshairs"></i>
					<span>Add Tracking Device</span>
				</div>
				<div ui-button ng-click="clearPlanned()">
					<i class="fa fa-undo text-blue"></i>
					<span class="media-tablet-hide">Clear Planned</span>
				</div>
				<div ui-button ng-click="splitItemToOwn(hIndex)">
					<i class="fa fa-share-alt text-eBlue"></i>
					<span class="media-tablet-hide">Split All</span>
				</div>
				<div ui-button ng-click="deleteHandlingUnit(hIndex)">
					<i class="fa fa-times text-red"></i>
					<span class="media-tablet-hide">Delete</span>
				</div>
			</ui-header-controls>
		</ui-header>
		<!-- <div class="form subform of-hu-subform" ng-class="{'min':hIndex!=handlingUnitInfoIndex}" ui-max-height>
			<div class="cols" ng-repeat="twoItems in handlingUnitFieldsArray" ng-if="unit.quantityUom == 'PLT' || unit.quantityUom.code == 'PLT'">
				<ui-field type="text" ng-if="twoItems.displayType == 'freeform text' && twoItems.fieldName == 'description'" ng-model="unit[twoItems.fieldName]" label="{{twoItems.displayName}}" ng-required="twoItems.required()" ng-disabled="false"></ui-field>	
				<ui-field type="select" ng-if="twoItems.displayType == 'dropdown' && twoItems.fieldName == 'quantityUom'" prop="name" options="orderSvc.containerTypesList" ng-model="unit[twoItems.fieldName]" label="{{twoItems.displayName}}" ng-required="twoItems.required()" ng-disabled="twoItems.displayType == 'readonly' ? true : false"></ui-field>
			</div>

			<div class="cols" ng-repeat="twoItems in handlingUnitFieldsArrayColumns" ng-if="unit.quantityUom != 'PLT' && unit.quantityUom.code != 'PLT'">
				<div  ng-repeat="hu in twoItems" ng-init="unit[hu.fieldName] = unit[hu.fieldName] || ''">				
					<ui-field type="text" ng-if="hu.displayType == 'readonly'" options="hu.options" ng-model="unit[hu.fieldName]" label="{{hu.displayName}}" ng-disabled="true"></ui-field>
					<ui-field type="text" ng-if="hu.displayType == 'freeform text' && hu.fieldName != 'volume'" ng-model="unit[hu.fieldName]" label="{{hu.displayName}}" ng-required="hu.required()" ng-disabled="false"></ui-field>
					<ui-field type="text" ng-if="hu.displayType == 'freeform text' && hu.fieldName == 'volume'" ng-model="unit[hu.fieldName]" label="{{hu.displayName}}" ng-required="hu.required()" ng-disabled="true"></ui-field>
					<ui-field type="select" ng-if="hu.displayType == 'dropdown' && hu.fieldName == 'quantityUom'" prop="name" options="orderSvc.containerTypesList" ng-model="unit[hu.fieldName]" label="{{hu.displayName}}" ng-required="hu.required()" ng-disabled="hu.displayType == 'readonly' ? true : false" ></ui-field>
					<ui-field type="select" ng-if="hu.displayType == 'dropdown' && hu.fieldName == 'dimensionsUom'" prop="name" options="hu.options" ng-model="unit[hu.fieldName]" label="{{hu.displayName}}" ng-required="hu.required()" ng-disabled="hu.displayType == 'readonly' ? true : false" ng-change="dimensionsChange()"></ui-field>
					<ui-field type="select" ng-if="hu.displayType == 'dropdown' && hu.fieldName != 'quantityUom' && hu.fieldName != 'dimensionsUom'" prop="name" options="hu.options" ng-model="unit[hu.fieldName]" label="{{hu.displayName}}" ng-required="hu.required()" ng-disabled="hu.displayType == 'readonly' ? true : false" ></ui-field>
					
				</div>
			</div>
		</div> -->
		<ui-list class="static">
			<ui-list-header>
				<li ui-width="110px">
					<span>Purchase Order</span>
				</li>
				
				<li ng-repeat="item in itemsHeader" class="text-center" ng-if="item.code != 'itemCode'" ng-class="{true:'width200', false:''}[item.code == 'description']">
					<span class="huTableHeader">{{item.displayName}}</span>
				</li>
				<!-- <li class="icon">Dims.</li> -->
				<li class="icon">Hazmat</li>
				<!-- <li class="icon">Split</li>
				<li class="icon">Move</li>
				<li class="icon">Remove</li> -->
				<li class="ui-list-actions-btn-header">

				</li>
			</ui-list-header>
							<ui-list-empty ng-show="unit.items.length==0">
					No items
				</ui-list-empty>
				<ui-list-body>
					<li ng-repeat="(index,item) in unit.items track by $index">
						<ui-list-content>
							<cell class="text rows" ui-width="110px" ui-info="{{item.lineItemBreakdownNum | split:'_':0}}">
								<div class="row">
									<label>Order Number</label>
									<span class="title">{{item.lineItemBreakdownNum | split:'_':0}}</span>
								</div>
								<div class="row">
									<label>Line Number</label>
									<span class="subtitle">Line {{item.lineItemBreakdownNum | split:'_':1}}</span>
								</div>
							</cell>
<!-- 						<div class="rows"  style="max-width: 110px">

							<ui-field type="text" ng-model="item.orderNumber" class="inline"></ui-field>
						</div>
						<div class="rows"  style="max-width: 110px">
							<ui-field type="text" ng-model="item.orderNumber" class="inline"></ui-field>

						</div>
						<div class="rows" style="min-width: 25%">
							<span class="title">{{item.lines[0].itemDescription}}</span>
							<span class="subtitle">#{{item.lines[0].itemNumber||' ---'}}</span>
						</div>
						<div class="text-center" style="max-width: 110px">
							<span>Tools</span>
						</div>
						<div class="text-center" style="max-width: 110px">
							<span>{{item.lines[0].executeFrom}}</span>
						</div>
						<div class="text-center" style="max-width: 54px">
							{{item.quantity}}
						</div>
						<div class="text-center" style="max-width: 54px">
							<ui-field type="text" ng-model="item.quantity" class="" ng-change="redefineTotalItems()"></ui-field>
						</div> -->

						<cell ng-repeat="column in itemsHeader" class="text-center" ng-if="column.code != 'itemCode'" ng-init="name = column.fieldName" ng-class="{true:'row width200', false:'row'}[column.code == 'description']">
							<span ng-if="column.displayType == 'freeform text'">
								<span ng-if="column.fieldType == 'REF'" >
									<span ng-repeat="itemDetail in item.references" ng-if="itemDetail.referenceCode == name">
										<ui-field type="text" ng-model="itemDetail.referenceNum" class="" ng-disabled="false" ng-required="column.required == 'Y'" ng-change="changeAllValues(this, item)"></ui-field>
									</span>
								</span>
								<span ng-if="column.fieldType == 'PROP'" >				
									<ui-field ng-if="name == 'quantity'" type="text" ng-model="item[name]" class="" ng-disabled="false" ng-required="column.required == 'Y'" ng-change="redefineTotalItems(this, item)" ></ui-field>
									<ui-field ng-if="name != 'quantity'" type="text" ng-model="item[name]" class="" ng-disabled="false" ng-required="column.required == 'Y'" ></ui-field>
								</span>
							</span>

							<span ng-if="column.displayType == 'readonly'">
								<span ng-if="column.fieldName == 'description'">
									<div class="rows" ui-info="{{item[name]}}">
										<span class="title"> {{item[name]}}</span>
										<span class="subtitle">Item Code: {{item['itemCode']}}</span >
									</div>
								</span>
								<span ng-if="column.fieldName != 'description' && column.fieldName != 'itemCode'">
									<span ng-if="column.fieldType == 'REF'">
										<span ng-repeat="itemDetail in item.references" ng-if="itemDetail.referenceCode == name" ui-info="{{itemDetail.referenceNum}}">
											<ui-field type="text" ng-model="itemDetail.referenceNum" class="" ng-disabled="true"></ui-field>
										</span>
									</span>
									<span ng-if="column.fieldType == 'PROP'" ui-info="{{item[name]}}">
										<ui-field type="text" ng-model="item[name]" class="" ng-disabled="true"></ui-field >
									</span>
								</span>
							</span>
						</cell>
						
						<ui-list-static-actions class="media-tablet-hide">
							<cell class="icon" ng-click="openHazmat(hIndex,index)" title="Hazmat">
								<div ui-button ng-class="{'text-orange':item.hazmat,'text-lgray':!item.hazmat}">
									<i class="fa fa-exclamation-triangle"></i>
								</div>
							</cell>
						</ui-list-static-actions>
						<!-- <div class="icon">
							<span ui-button ng-click="openDims(hIndex,index)" tabindex="-1">
								<i class="ic ic-measure text-accent"></i>
							</span>
						</div>
						<div class="icon">
							<span ui-button ng-click="openHazmat(hIndex,index)" tabindex="-1" class="text-lgray">
								<div class="hazmat-indicator" ng-class="{'active':item.hazmat}">
									<i class="ic-nuclear" ng-class="{'text-dgray':item.hazmat}"></i>
								</div>
							</span>
						</div>
						<div class="icon">
							<span ui-button ng-click="splitItem(hIndex,index)" tabindex="-1">
								<i class="fa fa-share-alt text-eBlue"></i>
							</span>
						</div>
						<div class="icon">
							<span ui-button ng-click="moveItem(hIndex,index)" tabindex="-1">
								<i class="fa fa-arrow-right text-orange"></i>
							</span>
						</div>
						<div class="icon">
							<span ui-button ng-click="removeItem(hIndex,index)" tabindex="-1">
								<i class="fa fa-times text-red"></i>
							</span>
						</div>
 -->
 					</ui-list-content>
					<ui-list-actions-btn ui-button title="More Actions">
						<i class="fa fa-ellipsis-h"></i>
					</ui-list-actions-btn>

					<ui-list-actions>
<!-- 							<div class="media-tablet-show-flex" ui-button ng-click="openHazmat(hIndex,index)" ng-class="{'bg-orange':item.hazmat,'bg-lgray':!item.hazmat}">
							<i class="fa fa-exclamation-triangle"></i>
						</div> -->
						<!-- <div ui-button ng-click="openHazmat(hIndex,index)" tabindex="-1" class="text-lgray media-tablet-show-flex" ng-class="{'bg-orange':item.hazmat,'bg-lgray':!item.hazmat}">
								<div class="hazmat-indicator" ng-class="{'active':item.hazmat}">
									<i class="ic-nuclear" ng-class="{'text-dgray':item.hazmat}"></i>
								</div>
								<i class="fa fa-exclamation-triangle"></i>
						</div>	 -->	
						<div ui-info="Serialization" ng-if="configList.OF_UI_ITEM_SERIALIZATION && configList.OF_UI_ITEM_SERIALIZATION == 'Y'" class="bg-gray" ui-button ng-click="openSerialization(hIndex,index)">
							<i class="fa fa-barcode"></i>
						</div>					
						<div title="Dimensions" class="bg-accent" ui-button ng-click="openDims(hIndex,index)">
							<i class="ic ic-measure"></i>
						</div>

						<div title="Split" class="bg-eBlue" ui-button ng-click="splitItem(hIndex,index)">
							<i class="fa fa-share-alt"></i>
						</div>
						<div title="Move" class="bg-orange" ui-button ng-click="moveItem(hIndex,index)">
							<i class="fa fa-arrow-right"></i>
						</div>
						<div ui-info="Print" class="bg-gray media-tablet-show-flex" ui-button ng-click="printItem(hIndex,index)" >
							<i class="fa fa-print"></i>
						</div>
						<div title="Remove" class="bg-red" ui-button ng-click="removeItem(hIndex,index, item)" >
							<i class="fa fa-times"></i>
						</div>
					</ui-list-actions>


				</li>
			</ui-list-body>		
		</ui-list>
	</ui-section>



	<ui-section ng-if="shipmentSummaryArr.length > 0">
		<ui-header theme="section">
			<ui-header-body>
				<div>
					<span>
						Shipment Summary
					</span>
				</div>
			</ui-header-body>
		</ui-header>
		<div class="form cols form-condensed">
			<ui-field type="text" ng-model="orderSvc.selectedDelivery().handlingUnits.length" label="Total Handling Units" ng-disabled="true"></ui-field>
			<ui-field type="text" ng-model="orderSvc.selectedDelivery().totalItems" label="Total Items" ng-disabled="true"></ui-field>
			<!-- <ui-field type="text" ng-model="orderSvc.selectedDelivery().totalVolume" label="Total Volume" ng-disabled="true"></ui-field>
			<ui-field type="text" ng-model="orderSvc.selectedDelivery().totalWeight" label="Total Weight" ng-disabled="orderSvc.palletTypeFlag"></ui-field> -->
		</div>	
		<ui-section-divider></ui-section-divider>

		<div class="form">
			<div ng-repeat="fourColumn in shipmentSummaryColumns" class="cols">
				<!-- <span > -->
					<!-- <span ng-if="column.fieldType == 'REF'"> -->
					<div class="" ng-repeat="column in fourColumn">
						<ui-field ng-if="column.displayType == 'time'" type="time" ng-model="column.itemValue" label="{{column.displayName}}" class="inline" ng-disabled="orderSvc.palletTypeFlag" ng-required="column.required()"></ui-field>							
						<ui-field ng-if="column.displayType == 'freeform text' && column.fieldName == 'PP'" type="text" ng-model="column.itemValue" label="{{column.displayName}}" class="" ng-disabled="orderSvc.palletTypeFlag" ng-required="column.required()" ng-change="defineTotalVolume()"></ui-field>
						<ui-field ng-if="column.displayType == 'freeform text' && column.fieldName == 'weight'" type="text" ng-model="orderSvc.selectedDelivery().totalWeight" label="{{column.displayName}}" class="" ng-disabled="orderSvc.palletTypeFlag" ng-required="column.required()"></ui-field>
						<ui-field ng-if="column.displayType == 'freeform text' && column.fieldName == 'volume'" type="text" ng-model="orderSvc.selectedDelivery().totalVolume" label="{{column.displayName}}" class="" ng-disabled="orderSvc.palletTypeFlag" ng-required="column.required()"></ui-field>
						<ui-field ng-if="column.displayType == 'freeform text' && column.fieldName == 'QTY'" type="text" ng-model="column.itemValue" label="{{column.displayName}}" class="" ng-disabled="orderSvc.palletTypeFlag" ng-required="column.required()"></ui-field>
						<ui-field ng-if="column.displayType == 'freeform text' && column.fieldName != 'PP' && column.fieldName != 'QTY' && column.fieldName != 'weight' && column.fieldName != 'volume'" type="text" ng-model="column.itemValue" label="{{column.displayName}}" class="" ng-disabled="column.disabled()" ng-required="column.required()"></ui-field>
						<ui-field ng-if="column.displayType == 'readonly'"  type="text" ng-model="column.itemValue" label="{{column.displayName}}" class="" ng-disabled="true"></ui-field>
						<ui-field ng-if="column.displayType == 'dropdown'" type="select" ng-model="column.itemValue" label="{{column.displayName}}" options="column.options" class="" ng-disabled="column.disabled()" ng-required="column.required()" ></ui-field>
						<ui-field ng-if="column.displayType == 'datetime' && column.fieldName == 'requestShipFromDt'" type="date" ng-model="column.itemValue" label="{{column.displayName}}" ng-disabled="column.disabled()" ng-required="column.required()" ></ui-field>
						<ui-field ng-if="column.displayType == 'datetime' && column.fieldName != 'requestShipFromDt'" type="date" ng-model="column.itemValue" label="{{column.displayName}}" ng-disabled="column.disabled()" ng-required="column.required()" ></ui-field>
						<div class="customErrorMessage" ng-if="column.displayType == 'freeform text' && column.fieldName == 'PP' && validationErrorPP" >Pallets Positions should be Numeric and  between 1 and 26</div>
					<!-- 	<div class="customErrorMessage" ng-if="column.displayType == 'freeform text' && column.fieldName == 'QTY' && validationErrorQTY" >Pallet Positions cannot exceed Pallets</div> -->
					</div>
			</div>
		</div>


		<ui-section-divider></ui-section-divider>
		<div class="form cols" ng-repeat="column in shipmentSummaryArr">
			<ui-field type="text" ng-if="column.displayType == 'textarea' " ng-model="column.itemValue" label="{{column.displayName}}" ng-required="false" ng-disabled="false"></ui-field>
		</div>
	</ui-section>

	<ui-section  ng-if="additionalInfoArr.length > 0">
		<ui-header theme="section">
			<ui-header-body>
				<div>
					<span>
						Additional Options
					</span>
				</div>
			</ui-header-body>
		</ui-header>

<!-- 			<div class="form cols">
			<div class="rows">
				<div class="cols">
					<ui-field type="text" ng-model="orderSvc.selectedDelivery().totalHandlingUnits" label="Packing Slip Number" ng-required="true" ng-disabled="false"></ui-field>
				</div>	
			</div>	
			<div class="rows">
				<div class="cols">
					<ui-field type="select" ng-model="orderSvc.selectedDelivery().equipment" label="Equipment" ng-required="false" ng-disabled="false"></ui-field>
				</div>	
			</div>	
		</div> -->
		<div class="form cols" ng-repeat="fourColumn in additionalInfoColumns track by $index" ng-if="fourColumn">
			<div class="" ng-repeat="column in fourColumn track by $index" >
				<ui-field ng-if="column.displayType=='single checkbox'" type="toggle" class="no-label" ng-model="column.itemValue" hover="{{column.hoverText}}" title="{{column.displayName}}" ng-required="column.required()" ng-disabled="column.disabled()"></ui-field>

				<ui-field ng-if="column.displayType=='hidden' && !column.disabled() && column.fieldType == 'CHRG'" type="toggle" class="no-label" ng-model="column.itemValue" hover="{{column.hoverText}}" title="{{column.displayName}}" ng-required="column.required()" ng-disabled="column.disabled()"></ui-field>

				<ui-field ng-if="column.displayType=='hidden' && !column.disabled() && column.fieldType == 'REF'" type="text" class="" ng-model="column.itemValue" label="{{column.displayName}}" ng-required="column.required()" ng-disabled="column.disabled()"></ui-field>

				<ui-field ng-if="column.displayType == 'dropdown'" prop="name" type="select" ng-model="column.itemValue" label="{{column.displayName}}" options="column.options" class="" ng-disabled="column.disabled()" ng-required="column.required()" ></ui-field>
				
				<!-- <ui-field type="toggle" class="no-label" ng-model="orderSvc.selectedDelivery().insideDelivery" title="Inside Delivery" ng-required="false" ng-disabled="false"></ui-field>
				<ui-field type="toggle" class="no-label" ng-model="orderSvc.selectedDelivery().flatbed" title="Flatbed" ng-required="false" ng-disabled="false"></ui-field>
				<ui-field type="toggle" class="no-label" ng-model="orderSvc.selectedDelivery().liftgate" title="Liftgate" ng-required="false" ng-disabled="false"></ui-field> -->
				<!-- <ui-field type="toggle" class="no-label" ng-model="orderSvc.selectedDelivery().constantSurveillance" title="Constant Surveillance" ng-required="false" ng-disabled="false"></ui-field> -->
				<!-- <ui-field type="toggle" class="no-label" ng-model="orderSvc.selectedDelivery().temperatureControlled" title="Temperature Controlled" ng-required="false" ng-disabled="false"></ui-field> -->
			</div>
		</div>
	</ui-section>

	<ui-section ng-if="acknowledgementArr.length > 0">
		<ui-header theme="section" >
			<ui-header-body>
				<div>
					<span> Required Acknowledgments</span>
				</div>
			</ui-header-body>
		</ui-header>
		<div class="form rows" ng-repeat="field in acknowledgementArr">
			<ui-header theme="subsection" ng-if="field.displayType == 'subtitle'">
				<ui-header-body>
					<div>
						<span ng-bind-html="field.displayName"></span>
					</div>
				</ui-header-body>
			</ui-header>
			<div class="text" ng-if="field.displayType == 'label'" ng-bind-html="field.displayName"></div>
			<textarea disabled="disabled" class="disclaimer" ng-if="field.displayType == 'label.textarea'" ng-bind-html="field.displayName">								
			</textarea>
		</div>
		<div class="cols">
			<div class="" ng-repeat="column in acknowledgementArr">
				<ui-field ng-if="column.displayType == 'freeform text'" type="text" ng-model="column.itemValue"  label="{{column.displayName}}" ng-required="column.required == 'Y' ? true : false" ng-disabled="column.disabled == 'Y' ? true : false"></ui-field>
				<ui-field ng-if="column.displayType == 'readonly'" type="text" ng-model="column.itemValue" label="{{column.displayName}}" ng-disabled="true"></ui-field>
				<ui-field ng-if="column.displayType == 'datetime'" type="date" ng-model="column.itemValue" label="{{column.displayName}}" ng-required="column.required == 'Y' ? true : false" ng-disabled="column.disabled == 'Y' ? true : false"></ui-field>
			</div>
		</div>
	</ui-section>
	<ui-page-actions-spacer></ui-page-actions-spacer>
	<ui-page-actions-container>
		<ui-page-actions>
			<div class="group">
				<div ui-button ui-page-action class="bg-red" ui-info="Delete Order" ng-click="deleteDeliveryOrder();">
					<i class="fa fa-trash"></i>
				</div>
			</div>

			<div class="group">
				<div ui-button ui-page-action class="bg-eBlue" ui-info="Add Handling Unit" ng-click="addHandlingUnit()" >
					<div>
						<i class="fa fa-cube"></i>
					</div>
				</div>
			</div>
			<div class="group">
				<button type="submit" ui-button ui-page-action class="text-accent bg-white" ui-info="Submit Order" ng-click="submitOrder(deliveryDetailsForm.$valid);">
					<span>Submit</span>
					<i class="fa fa-chevron-right"></i>
				</button>
			</div>
		</ui-page-actions>
	</ui-page-actions-container>
</form>