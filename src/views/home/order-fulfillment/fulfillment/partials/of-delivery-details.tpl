<form name="deliveryDetailsForm12">
	<ui-header theme="page">
		<ui-header-body>
			<div>
				<span>
					{{orderSvc.selectedDelivery().name}}
				</span>
			</div>
		</ui-header-body>
		<ui-header-controls>
			<button ui-button ng-click="submitOrderOld(deliveryDetailsForm12.$valid)">
			<!-- <button ui-button type="submit" > -->
				<span>Submit</span>
				<i class="fa fa-chevron-right accent"></i>
			</button>
		</ui-header-controls>
	</ui-header>

	

	<div class="overflow-y" ui-section-container>
		<ui-alert-container></ui-alert-container>
		<ui-section>
			<ui-header theme="section">
				<ui-header-body>
					<div>
						<i class="fa fa-info"></i>
						<span>
							Shipment Details
						</span>
					</div>
				</ui-header-body>
				<ui-header-controls>
					<span ui-button ng-click="deleteDeliveryOrder()">
						<i class="fa fa-times text-red"></i>
						<span>Delete Order</span>
					</span>
				</ui-header-controls>
			</ui-header>

			<div class="form">
				<div class="cols">
					<div class="rows">
						<ui-header theme="subsection">
							<ui-header-body>
								<div class="width100">
									<span>
										Origin
									</span>
								</div>
							</ui-header-body> 
						</ui-header>
						<div class="cols">
							<ui-field type="select" options="originIds" ng-model="orderSvc.selectedDelivery().origin.entityId" prop="code" label="Location ID" ng-required="true" ng-disabled="false" refresh="getLocations" refresh-args="{'name':'location', 'code':'19'}" on-select="selectedLocation" selected-args="{'name': orderSvc.selectedDelivery().origin.entityId.name, 'code':'19'}"></ui-field>
				
							<ui-field type="select" options="originNames" ng-model="orderSvc.selectedDelivery().origin.name" label="Address Name" ng-required="true" ng-disabled="false"  refresh="getLocations" refresh-args="{'name':'name', 'code':'19'}" on-select="selectedLocation" selected-args="{'name': orderSvc.selectedDelivery().origin.name.name, 'code':'19'}"></ui-field>
						</div>
						<div class="cols">
							<ui-field type="text" ng-model="orderSvc.selectedDelivery().origin.address" label="Address" ng-required="true" ng-disabled="false"></ui-field>
							<ui-field type="text" ng-model="orderSvc.selectedDelivery().origin.address2" label="Address (Additional)" ng-required="false" ng-disabled="false"></ui-field>
						</div>
						<div class="cols">
							<ui-field type="text" ng-model="orderSvc.selectedDelivery().origin.city" label="City" ng-required="true" ng-disabled="false"></ui-field>
							<ui-field type="text" ng-model="orderSvc.selectedDelivery().origin.postalCode" label="Postal Code" ng-required="true" ng-disabled="false"></ui-field>
						</div>
						<div class="cols">
							<ui-field type="select" options="statesList" ng-model="orderSvc.selectedDelivery().origin.sau" label="State/Province" ng-required="true" ng-disabled="false"></ui-field>
							<ui-field type="select" options="countriesList" ng-model="orderSvc.selectedDelivery().origin.country" label="Country" ng-required="true" ng-disabled="false" ></ui-field>
						</div>
						<div class="cols">
							<span ui-button ng-click="addresslookUp(19)" class="align-right">
								<i class="fa fa-search"></i>
								<span>Address lookup</span>
							</span>
						</div>
						<div class="cols">
							<ui-field type="toggle" class="no-label" ng-model="orderSvc.selectedDelivery().setAsDefaultOrigin" title="Default Shipping Location" ng-required="false" ng-disabled="false"></ui-field>
						</div>
					</div>
					<div class="rows">
						<ui-header theme="subsection">
							<ui-header-body>
								<div>
									<span>
										Destination
									</span>
								</div>
							</ui-header-body>
						</ui-header>
						<div class="cols">
							<ui-field type="select" options="destinationIds" prop="code" ng-model="orderSvc.selectedDelivery().destination.entityId" label="Location ID" ng-required="false" ng-disabled="true" refresh="getLocations" refresh-args="{'name':'location', 'code':'20'}" on-select="selectedLocation" selected-args="{'name': orderSvc.selectedDelivery().destination.entityId.name, 'code':'20'}"></ui-field>

							<ui-field type="select" options="destinationNames" ng-model="orderSvc.selectedDelivery().destination.name" label="Address Name" ng-required="false" ng-disabled="true" refresh="getLocations" refresh-args="{'name':'location', 'code':'20'}" on-select="selectedLocation" selected-args="{'name': orderSvc.selectedDelivery().destination.name.name, 'code':'20'}"></ui-field>
						</div>
						<div class="cols">
							<ui-field type="text" ng-model="orderSvc.selectedDelivery().destination.address" label="Address" ng-required="false" ng-disabled="true"></ui-field>
							<ui-field type="text" ng-model="orderSvc.selectedDelivery().destination.address2" label="Address (Additional)" ng-required="false" ng-disabled="true"></ui-field>
						</div>
						<div class="cols">
							<ui-field type="text" ng-model="orderSvc.selectedDelivery().destination.city" label="City" ng-required="false" ng-disabled="true"></ui-field>
							<ui-field type="text" ng-model="orderSvc.selectedDelivery().destination.postalCode" label="Postal Code" ng-required="false" ng-disabled="true"></ui-field>
						</div>
						<div class="cols">
							<ui-field type="select" options="statesList" ng-model="orderSvc.selectedDelivery().destination.sau" label="State/Province" ng-required="false" ng-disabled="true"></ui-field>
							<ui-field type="select" options="countriesList" ng-model="orderSvc.selectedDelivery().destination.country" label="Country" ng-required="false" ng-disabled="true" ></ui-field>
						</div>
						<div class="cols">
							<!-- <span ui-button ng-click="addresslookUp(20)" class="align-right" disabled="disabled">
							 -->
							<span ui-button class="align-right" disabled="disabled">
								<i class="fa fa-search"></i>
								<span>Address lookup</span>
							</span>
						</div>
						<div class="cols">
							<ui-field type="toggle" class="no-label" ng-model="orderSvc.selectedDelivery().setAsDefaultDestination" title="Default Shipping Location" ng-required="false" ng-disabled="true"></ui-field>
						</div>
					</div>
				</div>
			</div>
		</ui-section>

		<ui-section>

			<ui-header theme="section">
				<ui-header-body>
					<div>
						<i class="fa fa-cube"></i>
						<span>
							Packaging
						</span>
					</div>
				</ui-header-body>
				<ui-header-controls hide-actions>
					<div ui-button ui-header-actions-btn  class="accent">
						<i class="fa fa-ellipsis-h"></i>
					</div>
					<div ui-button ng-click="clearPlanned()">
						<i class="fa fa-undo text-blue"></i>
						<span class="media-tablet-hide">Clear Planned</span>
					</div>
					<div ng-if="orderSvc.customerName != 'WWG'" ui-button ng-click="">
						<i class="fa fa-share-alt text-eBlue"></i>
						<span class="media-tablet-hide">Split All</span>
					</div>
					<div ui-button ng-click="addHandlingUnit()" ng-if="orderSvc.palletTypeFlag">
						<i class="fa fa-plus text-green"></i>
						<span class="media-tablet-hide">New Handling Unit</span>
					</div>
				</ui-header-controls>
			</ui-header>

			<ui-list ng-repeat="(hIndex,unit) in orderSvc.selectedDelivery().shipmentObject.handlingUnits track by $index">			
				<ui-header theme="subsection">
					<ui-header-body>
						<div>
							<span>
								Contents of {{orderSvc.selectedDelivery().shipmentObject.handlingUnits[hIndex].name}}
							</span>
						</div>
					</ui-header-body>
					<ui-header-controls>
						<span ui-button ng-click="handlingUnitInfo(hIndex)" tabindex="-1">
							<i class="fa fa-info"></i>
						</span>
						<span ui-button ng-click="deleteHandlingUnit(hIndex)" tabindex="-1">
							<i class="fa fa-times"></i>
						</span>
					</ui-header-controls>
				</ui-header>
				<ui-list-header>
					<li ui-width="110px">
						<span>Purchase Order</span>
					</li>
					
					<li ng-repeat="item in itemsHeader" class="text-center" ng-if="item.code != 'itemCode'" ng-class="{true:'width200', false:''}[item.code == 'description']">
						<span class="huTableHeader">{{item.displayName}}</span>
					</li>
					<!-- <li class="icon">Dims.</li> -->
					<li class="icon">Hazmat</li>
					<!-- <li class="icon">Split</li>
					<li class="icon">Move</li>
					<li class="icon">Remove</li> -->
					<li class="ui-list-actions-btn-header">

					</li>
				</ui-list-header>
				<ui-list-empty ng-show="unit.items.length==0">
					No items
				</ui-list-empty>
				<ui-list-body>
					<li ng-repeat="(index,item) in unit.items track by $index">
						<ui-list-content>
							<cell class="text rows" ui-width="110px" ui-info="{{item.lineItemBreakdownNum | split:'_':0}}">
								<div class="row">
									<label>Order Number</label>
									<span class="title">{{item.lineItemBreakdownNum | split:'_':0}}</span>
								</div>
								<div class="row">
									<label>Line Number</label>
									<span class="subtitle">Line {{item.lineItemBreakdownNum | split:'_':1}}</span>
								</div>
							</cell>
<!-- 						<div class="rows"  style="max-width: 110px">

							<ui-field type="text" ng-model="item.orderNumber" class="inline"></ui-field>
						</div>
						<div class="rows"  style="max-width: 110px">
							<ui-field type="text" ng-model="item.orderNumber" class="inline"></ui-field>

						</div>
						<div class="rows" style="min-width: 25%">
							<span class="title">{{item.lines[0].itemDescription}}</span>
							<span class="subtitle">#{{item.lines[0].itemNumber||' ---'}}</span>
						</div>
						<div class="text-center" style="max-width: 110px">
							<span>Tools</span>
						</div>
						<div class="text-center" style="max-width: 110px">
							<span>{{item.lines[0].executeFrom}}</span>
						</div>
						<div class="text-center" style="max-width: 54px">
							{{item.quantity}}
						</div>
						<div class="text-center" style="max-width: 54px">
							<ui-field type="text" ng-model="item.quantity" class="inline text-center" ng-change="redefineTotalItems()"></ui-field>
						</div> -->

						<cell ng-repeat="column in itemsHeader" class="text-center" ng-if="column.code != 'itemCode'" ng-init="name = column.fieldName" ng-class="{true:'row width200', false:'row'}[column.code == 'description']">
							<span ng-if="column.displayType == 'freeform text'">
								<span ng-if="column.fieldType == 'REF'" >
									<span ng-repeat="itemDetail in item.references" ng-if="itemDetail.referenceCode == name">
										<ui-field type="text" ng-model="itemDetail.referenceNum" class="inline text-center" ng-disabled="false" ng-required="column.required == 'Y'" ng-change="changeAllValues(this, item)"></ui-field>
									</span>
								</span>
								<span ng-if="column.fieldType == 'PROP'" >				
									<ui-field ng-if="name == 'quantity'" type="text" ng-model="item[name]" class="inline text-center" ng-disabled="false" ng-required="column.required == 'Y'" ng-change="redefineTotalItems(this, item)" ng-click="selectAllText(item)"></ui-field>
									<ui-field ng-if="name != 'quantity'" type="text" ng-model="item[name]" class="inline text-center" ng-disabled="false" ng-required="column.required == 'Y'" ></ui-field>
								</span>
							</span>

							<span ng-if="column.displayType == 'readonly'">
								<span ng-if="column.fieldName == 'description'">
									<div class="rows" ui-info="{{item[name]}}">
										<span class="title"> {{item[name]}}</span>
										<span class="subtitle">Item Code: {{item['itemCode']}}</span >
									</div>
								</span>
								<span ng-if="column.fieldName != 'description' && column.fieldName != 'itemCode'">
									<span ng-if="column.fieldType == 'REF'">
										<span ng-repeat="itemDetail in item.references" ng-if="itemDetail.referenceCode == name" ui-info="{{itemDetail.referenceNum}}">
											<ui-field type="text" ng-model="itemDetail.referenceNum" class="inline text-center" ng-disabled="true"></ui-field>
										</span>
									</span>
									<span ng-if="column.fieldType == 'PROP'" ui-info="{{item[name]}}">
										<ui-field type="text" ng-model="item[name]" class="inline text-center" ng-disabled="true"></ui-field >
									</span>
								</span>
							</span>
						</cell>
						
						<ui-list-static-actions class="media-tablet-hide">
							<cell class="icon" ng-click="openHazmat(hIndex,index)" title="Hazmat">
								<div ui-button ng-class="{'text-orange':item.hazmat,'text-lgray':!item.hazmat}">
									<i class="fa fa-exclamation-triangle"></i>
								</div>
							</cell>
						</ui-list-static-actions>
						<!-- <div class="icon">
							<span ui-button ng-click="openDims(hIndex,index)" tabindex="-1">
								<i class="ic ic-measure text-accent"></i>
							</span>
						</div>
						<div class="icon">
							<span ui-button ng-click="openHazmat(hIndex,index)" tabindex="-1" class="text-lgray">
								<div class="hazmat-indicator" ng-class="{'active':item.hazmat}">
									<i class="ic-nuclear" ng-class="{'text-dgray':item.hazmat}"></i>
								</div>
							</span>
						</div>
						<div class="icon">
							<span ui-button ng-click="splitItem(hIndex,index)" tabindex="-1">
								<i class="fa fa-share-alt text-eBlue"></i>
							</span>
						</div>
						<div class="icon">
							<span ui-button ng-click="moveItem(hIndex,index)" tabindex="-1">
								<i class="fa fa-arrow-right text-orange"></i>
							</span>
						</div>
						<div class="icon">
							<span ui-button ng-click="removeItem(hIndex,index)" tabindex="-1">
								<i class="fa fa-times text-red"></i>
							</span>
						</div>
 -->
 					</ui-list-content>
						<ui-list-actions-btn ui-button title="More Actions">
							<i class="fa fa-ellipsis-h"></i>
						</ui-list-actions-btn>

						<ui-list-actions>
<!-- 							<div class="media-tablet-show-flex" ui-button ng-click="openHazmat(hIndex,index)" ng-class="{'bg-orange':item.hazmat,'bg-lgray':!item.hazmat}">
								<i class="fa fa-exclamation-triangle"></i>
							</div> -->
							<!-- <div ui-button ng-click="openHazmat(hIndex,index)" tabindex="-1" class="text-lgray media-tablet-show-flex" ng-class="{'bg-orange':item.hazmat,'bg-lgray':!item.hazmat}">
									<div class="hazmat-indicator" ng-class="{'active':item.hazmat}">
										<i class="ic-nuclear" ng-class="{'text-dgray':item.hazmat}"></i>
									</div>
									<i class="fa fa-exclamation-triangle"></i>
							</div>	 -->						
							<div title="Dimensions" class="bg-accent" ui-button ng-click="openDims(hIndex,index)">
								<i class="ic ic-measure"></i>
							</div>

							<div title="Split" class="bg-eBlue" ui-button ng-click="splitItem(hIndex,index)">
								<i class="fa fa-share-alt"></i>
							</div>
							<div title="Move" class="bg-orange" ui-button ng-click="moveItem(hIndex,index)">
								<i class="fa fa-arrow-right"></i>
							</div>
							<div title="Remove" class="bg-red" ui-button ng-click="removeItem(hIndex,index, item)" >
								<i class="fa fa-times"></i>
							</div>
						</ui-list-actions>


					</li>
				</ui-list-body>
			</ui-list>
		</ui-section>


		<ui-section ng-if="shipmentSummaryArr.length > 0">
			<ui-header theme="section">
				<ui-header-body>
					<div>
						<i class="fa fa-truck"></i>
						<span>
							Shipment Summary
						</span>
					</div>
				</ui-header-body>
			</ui-header>
			<div class="form cols form-condensed">
				<ui-field type="text" ng-model="orderSvc.selectedDelivery().handlingUnits.length" label="Total Handling Units" ng-disabled="true"></ui-field>
				<ui-field type="text" ng-model="orderSvc.selectedDelivery().totalItems" label="Total Items" ng-disabled="true"></ui-field>
				<!-- <ui-field type="text" ng-model="orderSvc.selectedDelivery().totalVolume" label="Total Volume" ng-disabled="true"></ui-field>
				<ui-field type="text" ng-model="orderSvc.selectedDelivery().totalWeight" label="Total Weight" ng-disabled="orderSvc.palletTypeFlag"></ui-field> -->
			</div>	
			<ui-section-divider></ui-section-divider>

			<div class="form">
				<div ng-repeat="fourColumn in shipmentSummaryColumns" class="text-center cols">
					<!-- <span > -->
						<!-- <span ng-if="column.fieldType == 'REF'"> -->
						<div class="" ng-repeat="column in fourColumn">
							<ui-field ng-if="column.displayType == 'time'" type="time" ng-model="column.itemValue" label="{{column.displayName}}" class="inline" ng-disabled="orderSvc.palletTypeFlag" ng-required="column.required()"></ui-field>							
							<ui-field ng-if="column.displayType == 'freeform text' && column.fieldName == 'PP'" type="text" ng-model="column.itemValue" label="{{column.displayName}}" class="inline text-center" ng-disabled="orderSvc.palletTypeFlag" ng-required="column.required()" ng-change="defineTotalVolume()"></ui-field>
							<ui-field ng-if="column.displayType == 'freeform text' && column.fieldName == 'weight'" type="text" ng-model="orderSvc.selectedDelivery().totalWeight" label="{{column.displayName}}" class="inline text-center" ng-disabled="orderSvc.palletTypeFlag" ng-required="column.required()"></ui-field>
							<ui-field ng-if="column.displayType == 'freeform text' && column.fieldName == 'volume'" type="text" ng-model="orderSvc.selectedDelivery().totalVolume" label="{{column.displayName}}" class="inline text-center" ng-disabled="orderSvc.palletTypeFlag" ng-required="column.required()"></ui-field>
							<ui-field ng-if="column.displayType == 'freeform text' && column.fieldName == 'QTY'" type="text" ng-model="column.itemValue" label="{{column.displayName}}" class="inline text-center" ng-disabled="orderSvc.palletTypeFlag" ng-required="column.required()"></ui-field>
							<ui-field ng-if="column.displayType == 'freeform text' && column.fieldName != 'PP' && column.fieldName != 'QTY' && column.fieldName != 'weight' && column.fieldName != 'volume'" type="text" ng-model="column.itemValue" label="{{column.displayName}}" class="inline text-center" ng-disabled="column.disabled()" ng-required="column.required()"></ui-field>
							<ui-field ng-if="column.displayType == 'readonly'"  type="text" ng-model="column.itemValue" label="{{column.displayName}}" class="inline text-center" ng-disabled="true"></ui-field>
							<ui-field ng-if="column.displayType == 'dropdown'" type="select" ng-model="column.itemValue" label="{{column.displayName}}" options="column.options" class="inline text-center" ng-disabled="column.disabled()" ng-required="column.required()" ></ui-field>
							<ui-field ng-if="column.displayType == 'datetime' && column.fieldName == 'requestShipFromDt'" type="date" ng-model="column.itemValue" label="{{column.displayName}}" ng-disabled="column.disabled()" ng-required="column.required()" ></ui-field>
							<ui-field ng-if="column.displayType == 'datetime' && column.fieldName != 'requestShipFromDt'" type="date" ng-model="column.itemValue" label="{{column.displayName}}" ng-disabled="column.disabled()" ng-required="column.required()" ></ui-field>
							<div class="customErrorMessage" ng-if="column.displayType == 'freeform text' && column.fieldName == 'PP' && validationErrorPP" >Pallets Positions should be Numeric and  between 1 and 26</div>
						<!-- 	<div class="customErrorMessage" ng-if="column.displayType == 'freeform text' && column.fieldName == 'QTY' && validationErrorQTY" >Pallet Positions cannot exceed Pallets</div> -->
						</div>
				</div>
			</div>


			<ui-section-divider></ui-section-divider>
			<div class="form cols" ng-repeat="column in shipmentSummaryArr">
				<ui-field type="text" ng-if="column.displayType == 'textarea' " ng-model="column.itemValue" label="{{column.displayName}}" ng-required="false" ng-disabled="false"></ui-field>
			</div>
		</ui-section>

		<ui-section  ng-if="additionalInfoArr.length > 0">
			<ui-header theme="section">
				<ui-header-body>
					<div>
						<i class="fa fa-asterisk"></i>
						<span>
							Additional Options
						</span>
					</div>
				</ui-header-body>
			</ui-header>

<!-- 			<div class="form cols">
				<div class="rows">
					<div class="cols">
						<ui-field type="text" ng-model="orderSvc.selectedDelivery().totalHandlingUnits" label="Packing Slip Number" ng-required="true" ng-disabled="false"></ui-field>
					</div>	
				</div>	
				<div class="rows">
					<div class="cols">
						<ui-field type="select" ng-model="orderSvc.selectedDelivery().equipment" label="Equipment" ng-required="false" ng-disabled="false"></ui-field>
					</div>	
				</div>	
			</div> -->
			<div class="form cols" ng-repeat="fourColumn in additionalInfoColumns" ng-if="fourColumn">
				<div class="" ng-repeat="column in fourColumn" >
					<ui-field ng-if="column.displayType=='single checkbox'" type="toggle" class="no-label" ng-model="column.itemValue" hover="{{column.hoverText}}" title="{{column.displayName}}" ng-required="column.required()" ng-disabled="column.disabled()"></ui-field>

					<ui-field ng-if="column.displayType=='hidden' && !column.disabled() && column.fieldType == 'CHRG'" type="toggle" class="no-label" ng-model="column.itemValue" hover="{{column.hoverText}}" title="{{column.displayName}}" ng-required="column.required()" ng-disabled="column.disabled()"></ui-field>

					<ui-field ng-if="column.displayType=='hidden' && !column.disabled() && column.fieldType == 'REF'" type="text" class="inline text-center" ng-model="column.itemValue" label="{{column.displayName}}" ng-required="column.required()" ng-disabled="column.disabled()"></ui-field>
					
					<!-- <ui-field type="toggle" class="no-label" ng-model="orderSvc.selectedDelivery().insideDelivery" title="Inside Delivery" ng-required="false" ng-disabled="false"></ui-field>
					<ui-field type="toggle" class="no-label" ng-model="orderSvc.selectedDelivery().flatbed" title="Flatbed" ng-required="false" ng-disabled="false"></ui-field>
					<ui-field type="toggle" class="no-label" ng-model="orderSvc.selectedDelivery().liftgate" title="Liftgate" ng-required="false" ng-disabled="false"></ui-field> -->
					<!-- <ui-field type="toggle" class="no-label" ng-model="orderSvc.selectedDelivery().constantSurveillance" title="Constant Surveillance" ng-required="false" ng-disabled="false"></ui-field> -->
					<!-- <ui-field type="toggle" class="no-label" ng-model="orderSvc.selectedDelivery().temperatureControlled" title="Temperature Controlled" ng-required="false" ng-disabled="false"></ui-field> -->
				</div>
			</div>
		</ui-section>

		<ui-section  ng-if="acknowledgementArr.length > 0">
			<ui-header theme="section">
				<ui-header-body>
					<div>
						<i class="fa fa-exclamation-circle"></i>
						<span>
							Required Acknowledgments
						</span>
					</div>
				</ui-header-body>
			</ui-header>
			<div class="form rows">
				<ui-header theme="subsection">
					<ui-header-body>
						<div>
							<span>
								Hazardous Materials
							</span>
						</div>
					</ui-header-body>
				</ui-header>
				<div class="text">By entering your initials below, you certify that you have read and agree to the terms presented in the following agreement:</div>
				<textarea disabled="disabled" class="disclaimer">						
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus vitae lorem elementum, facilisis metus eu, pellentesque erat. Fusce ultrices accumsan odio vitae consequat. Donec aliquam mauris hendrerit mi iaculis commodo. Phasellus id magna diam. Pellentesque ac nulla elit. Aliquam vitae sollicitudin odio. Donec vel nisi at urna vestibulum sodales. Nam finibus velit sed diam mattis suscipit.

					Etiam sed velit eget tortor egestas varius sit amet id ex. Curabitur posuere est at nibh vulputate facilisis. Maecenas ut quam quis ex malesuada dignissim ut vel nisi. Pellentesque rhoncus diam nec tellus efficitur scelerisque. Pellentesque ut augue mi. Phasellus fermentum vel leo eu imperdiet. Integer convallis aliquam dolor, at mollis dui congue vel. Praesent posuere eros luctus gravida mattis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vivamus volutpat euismod erat, vitae luctus quam sollicitudin nec. Aenean nulla libero, sodales non dignissim sit amet, consequat sit amet justo. Mauris sed vehicula nisl, in imperdiet felis. Suspendisse pretium lobortis tristique. Maecenas luctus interdum enim a egestas. Proin a bibendum nisl, vel ultrices erat. Suspendisse sodales pretium lorem.

					Mauris vel consectetur leo, quis commodo lectus. Curabitur eget ultrices dolor. Nullam at risus dolor. Integer nulla eros, sodales in nibh ac, congue tincidunt ex. Vivamus eleifend ut libero nec lacinia. Vestibulum at facilisis velit. Morbi ultrices dictum elit, id rhoncus nunc imperdiet nec. Donec sit amet nisl augue. Donec hendrerit vel risus id blandit. Donec quis enim viverra, pharetra ligula ac, condimentum purus.

					Nunc a vulputate dui. Mauris aliquet consectetur leo, at efficitur ex consectetur non. Aliquam placerat ante vitae diam venenatis, sed commodo felis finibus. Donec dui nibh, blandit a elit quis, lacinia ultrices lacus. Praesent a pellentesque augue. Proin molestie feugiat lacus et molestie. Aliquam eget libero aliquet, blandit lorem eget, tincidunt enim. Proin placerat placerat risus, sit amet sagittis leo mollis in. Sed convallis nunc ac nibh dignissim, vel cursus nisi lacinia. Sed cursus arcu in commodo accumsan. Donec fringilla sollicitudin nisl, id feugiat nisl lobortis at. Phasellus ultrices turpis mi, ac gravida magna varius ac. Suspendisse mollis rhoncus odio, eu viverra quam. Curabitur a enim non nunc eleifend pharetra sit amet at ex. Morbi enim erat, scelerisque pulvinar volutpat eu, sollicitudin eget sem. Suspendisse blandit, ligula eget interdum accumsan, quam elit dictum nisi, sit amet commodo lorem mauris ac nunc.

					Vivamus fermentum tellus nec interdum facilisis. Aenean auctor ut arcu quis congue. Nulla aliquam felis fringilla, sollicitudin dolor in, consequat lorem. In maximus fringilla nunc ut vulputate. Proin interdum lacinia luctus. Vestibulum auctor, lectus non imperdiet consequat, purus massa elementum metus, a maximus felis neque ullamcorper arcu. Quisque orci sem, bibendum quis massa quis, lacinia consectetur nisi. Quisque sed nibh ut nulla molestie dictum ac ut dolor.									
				</textarea>
				<div class="cols">
					<div class="" ng-repeat="column in acknowledgementArr">
						<ui-field ng-if="column.displayType == 'freeform text'" type="text" ng-model="column.itemValue"  label="{{column.displayName}}" ng-required="column.required == 'Y' ? true : false" ng-disabled="column.disabled == 'Y' ? true : false"></ui-field>
						<ui-field ng-if="column.displayType == 'readonly'" type="text" ng-model="column.itemValue" label="{{column.displayName}}" ng-disabled="true"></ui-field>
						<ui-field ng-if="column.displayType == 'datetime'" type="date" ng-model="column.itemValue" label="{{column.displayName}}" ng-required="column.required == 'Y' ? true : false" ng-disabled="column.disabled == 'Y' ? true : false"></ui-field>
					</div>
				</div>
			</div>
		</ui-section>

	</div>
</form>