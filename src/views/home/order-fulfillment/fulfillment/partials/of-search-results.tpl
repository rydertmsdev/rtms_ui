<ui-sidebar>
	<ui-sidebar-panel>
		<div class="content form">
			<ui-header theme="ui-sidebar-header">
				<ui-header-body>
					<div>
						<span>
							Search
						</span>
					</div>
				</ui-header-body>
				<ui-header-controls>
					<div ui-button title="Clear" ng-click="clearFilters()">
						<i class="fa fa-times text-red"></i>
					</div>
					<div ui-button title="Apply" ng-click="doSearch()">
						<i class="fa fa-check text-green"></i>
					</div>
				</ui-header-controls>
			</ui-header>
			<div>
				<ui-menu-group static>
					<ui-header theme="ui-menu-header" ui-button ui-menu-collapse>
						<ui-header-body>
							<span>
								General
							</span>
						</ui-header-body>
						<ui-header-controls>
							<div ui-menu-collapse-indicator></div>
						</ui-header-controls>
					</ui-header>
					<ui-menu-content ui-max-height>
						<!-- <ui-field type="select" refresh="lookupFilter()" options="filters.sector.options" ng-model="filters.sector.value" label="Sector" placeholder="-" ng-required="false" ng-disabled="false"></ui-field>
						<ui-field type="multi" options="filters.orderNumber.options" ng-model="filters.orderNumber.value" label="Order Number" placeholder="-" ng-required="false" ng-disabled="false"></ui-field>
						<ui-field type="text" ng-model="filters.lineItem.value" label="Line Item" placeholder="-" ng-required="false" ng-disabled="false"></ui-field> -->
						<dynamic-template data="searchContent"></dynamic-template>
					</ui-menu-content>
				</ui-menu-group>
				<ui-menu-group collapsed ng-if="additionalSearchContent.length > 0">
					<ui-header theme="ui-menu-header" ui-button ui-menu-collapse>
						<ui-header-body>
							<span>
								Additional Filters
							</span>
						</ui-header-body>
						<ui-header-controls>
							<div ui-menu-collapse-indicator></div>
						</ui-header-controls>
					</ui-header>
					<ui-menu-content ui-max-height>
						<!-- <ui-field type="date" ng-model="dateTest" label="On-Dock Date" placeholder="---" ng-required="false" ng-disabled="false"></ui-field>	
						<ui-field type="multi" options="filters.supplierName.options" ng-model="filters.supplierName.value" label="Supplier Name" placeholder="---" ng-required="false" ng-disabled="false"></ui-field>
						<ui-field type="text" ng-model="filters.itemDescription.value" label="Description" placeholder="---" ng-required="false" ng-disabled="false"></ui-field>
						<ui-field type="select" options="filters.psa.options" ng-model="filters.psa.value" label="PSA" placeholder="---" ng-required="false" ng-disabled="false"></ui-field>
						<ui-field type="select" options="filters.origin.options" ng-model="filters.origin.value" label="Origin" placeholder="---" ng-required="false" ng-disabled="false"></ui-field>
						<ui-field type="select" options="filters.destination.options" ng-model="filters.destination.value" label="Destination" placeholder="---" ng-required="false" ng-disabled="false"></ui-field>
						<ui-field type="toggle" ng-model="filters.actionable.value" class="no-label checkbox" title="Actionable" placeholder="---" ng-required="false" ng-disabled="false"></ui-field> -->
						<dynamic-template data="additionalSearchContent"></dynamic-template>
					</ui-menu-content>
				</ui-menu-group>
			</div>
		</div>
	</ui-sidebar-panel>
	<ui-sidebar-content>
		<ui-alert-container></ui-alert-container>
<!-- 		<ui-filters>
			<ui-filter>
				<div>
					<label>Sector</label>
					<span> MS - Mission Systems</span>
				</div>
				<div ui-button ui-filter-remove-btn>
					<i class="fa fa-times"></i>
				</div>
			</ui-filter>
			<ui-filter>
				<div>
					<label>On-Dock Date</label>
					<span>May 25, 2017</span>
				</div>
				<div ui-button ui-filter-remove-btn>
					<i class="fa fa-times"></i>
				</div>
			</ui-filter>
		</ui-filters> -->
		<ui-section>
			<ui-list>	
				<ui-list-header>
					<li class="icon text-center">
						<span>Select</span>
					</li>
					<li ng-repeat="heading in tableHeaders" code="{{heading.code}}" ng-class="{true:'width250', false:''}[heading.code == 'description']" ng-if="heading.displayType != 'image' && !heading.options">
						<span ui-width="80%" class="inline-block">{{heading.name}}</span>
						<div ui-list-sort-btn ng-click="sortResults(heading.code, $event, 'filtering')">
							<i class="fa fa-sort"></i>
						</div>
					</li>
					<li ng-repeat="heading in tableHeaders" code="{{heading.code}}" ui-width="50px" ng-if="heading.displayType != 'image' && heading.options">
						<i ng-if="heading.options" class="{{heading.layoutProperties.cssClasses}}"></i>
						<div ui-list-sort-btn ng-click="sortResults(heading.code, $event, 'filtering')">
							<i class="fa fa-sort"></i>
						</div>
					</li>
					<li ng-repeat="heading in tableHeaders" code="{{heading.code}}" ui-width="50px" ng-class="ui-list-flags-header" ng-if="heading.displayType == 'image'">
						<i class="{{heading.layoutProperties.cssClasses}}"></i>
						<div ui-list-sort-btn ng-click="sortResults(heading.code, $event, 'filtering')">
							<i class="fa fa-sort"></i>
						</div>
					</li>
					<li class="icon">Details</li>
				</ui-list-header>		
				<ui-list-body>
					<li ng-if="orderSvc.searchResults.length == 0">
						<div class="no-results">
							No results to show.
						</div>
					</li>
					<!-- <li ng-repeat="po in ::svc.searchResults | limitTo:100 " ng-click="::selectItem(po)"> -->
					<li ng-repeat="po in orderSvc.searchResults" ng-class="{'selected':po.$$selected}" ng-click="(((po.supplyOrder.status != 'CLOSED') || (po.status != 'CLOSED')) && po.actionable) ? selectItem(po) : '' ">
						<cell class="icon">
							<div class="checkbox" ng-class="{'checked':po.$$selected}" ng-if="(((po.supplyOrder.status != 'CLOSED') || (po.status != 'CLOSED')) && po.actionable)"></div>
						</cell>
						<ui-list-content>
							<cell ng-repeat="heading in tableHeaders" code="{{heading.code}}" ng-class="{true:'rows width250', false:'rows text'}[heading.code == 'description']" ng-if="heading.displayType != 'image' && !heading.options">
								<div ng-if="heading.displayType != 'date' && heading.displayType != 'datetime'">
									<span class="title" ui-info="{{po.resultObject[heading.code]}}">{{po.resultObject[heading.code]}}</span>			
									<span  ng-if="heading.code == 'orderNumber'" class="subtitle">Line {{po.lineItemNum}}</span>
									<span  ng-if="heading.code == 'description'" class="subtitle">Item Code: {{po.itemCode}}</span>
								</div>
								<div ng-if="heading.displayType == 'date'">
									<span class="title">{{po.resultObject[heading.code] | parseDates: heading.displayType}}</span>
								</div>		
							</cell>
							<ui-list-flags ng-repeat="heading in tableHeaders" code="{{heading.code}}" ui-width="50px" ng-class="" ng-if="heading.displayType != 'image' && heading.options">
								<div class="title" ng-class="selectClassName(heading.options, po.resultObject[heading.code])" ui-info="{{heading.name}}">{{heading.name}}</div>
							</ui-list-flags>
							<ui-list-flags ng-repeat="heading in tableHeaders" code="{{heading.code}}" ui-width="50px" ng-class="" ng-if="heading.displayType == 'image'">
								<div ng-class="selectClassName(heading.options, po.resultObject[heading.code])" >
									<!-- <i class="{{heading.layoutProperties.cssClasses}}"></i> -->
								</div>
							</ui-list-flags>
							<!-- <cell class="text rows" ui-width="120px">
								<div class="row">
									<label>Order Number</label>
									<span class="title" ng-bind="::po.orderNumber"></span>
								</div>
								<div class="row">
									<label>Line Number</label>
									<span class="subtitle" ng-bind="::('Line '+po.lines[0].lineNumber)"></span>
								</div>
							</cell>
							<cell class="text" ui-width="120px">
								<label>Sector</label>
								<span ng-bind="::po.sector"></span>
							</cell>
							<cell class="text">
								<label>Destination</label><span ng-bind="::po.lines[0].locations.destination.name"></span>
							</cell>
							<cell class="text rows" ui-width="150px">
								<div class="row">
									<label>Item Description</label>
									<span class="title" ng-bind="::po.lines[0].itemDescription"></span>
								</div>
								<div class="row">
									<label>Item Number</label>
									<span class="subtitle" ng-bind="::('#'+po.lines[0].itemNumber||' ---')"></span>
								</div>
							</cell>
							<cell class="text text-center" ui-width="54px">
								<label>Quantity</label><span ng-bind="::po.quantity"></span>
							</cell>
							<cell class="text text-center" ui-width="120px">
								<label>On-Dock Date</label><span ng-bind="::po.lines[0].executeFrom"></span>
							</cell> -->
						</ui-list-content>
						<cell class="icon">
							<div ui-button ng-click="showPoDetails(po,$event)" ui-info="View PO Details">
								<i class="fa fa-info accent"></i>
							</div>
						</cell>
					</li>
				</ui-list-body>
			</ui-list>
		</ui-section>
		<ui-page-actions-spacer ng-if="orderSvc.searchResults.length > 0"></ui-page-actions-spacer>
		<ui-page-actions-container ng-if="orderSvc.searchResults.length > 0">
			<ui-page-actions>
				<div class="group">

					<div ui-button ui-page-action class="bg-lgray" ui-info="Previous Page" ng-if="orderSvc.index > 1" ng-click="sortResults(null, $event, 'previous')">
						<i class="fa fa-chevron-left"></i>
					</div>
					<div ui-button ui-page-action class="bg-lgray" ui-info="Previous Page" ng-if="orderSvc.index == 1" disabled="disabled">
						<i class="fa fa-chevron-left"></i>
					</div>
					<div class="text">
						<span>{{orderSvc.index}} / {{orderSvc.pageTotal}}</span>
					</div>
					<div ui-button ui-page-action class="bg-accent"  ui-info="Next Page" ng-if="(orderSvc.index < orderSvc.pageTotal) || ((orderSvc.index == orderSvc.pageTotal) && orderSvc.recordsCountExceeded)" ng-click="sortResults(null, $event, 'next')">
						<i class="fa fa-chevron-right"></i>
					</div>
					<div ui-button ui-page-action class="bg-accent"  ui-info="Next Page" ng-if="(orderSvc.index == orderSvc.pageTotal) && !orderSvc.recordsCountExceeded" disabled="disabled">
						<i class="fa fa-chevron-right"></i>
					</div>
				</div>
			</ui-page-actions>
		</ui-page-actions-container>
	</ui-sidebar-content>
</ui-sidebar>







