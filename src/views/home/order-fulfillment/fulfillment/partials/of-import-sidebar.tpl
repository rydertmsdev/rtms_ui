<!-- <div class="content">
	<ui-file-import >
		<i class="fa fa-file-excel-o"></i>
		<span>
			Click here or drag and drop a file to import
		</span>
	</ui-file-import>
</div>
<div class="footer">
	<button ui-button class="bg-accent" style="margin-bottom: 10px;border-radius: 3px;">
		<span>Download Template</span>
		<i class="fa fa-download"></i>
	</button>
</div> -->

<div class="content" ng-controller="ofImportSidebarCtrl">
	<ng-dropzone class="dropzone" options="dzOptions" callbacks="dzCallbacks" methods="dzMethods"></ng-dropzone>
</div>
<div class="footer">
	<span class="bg-accent downloadTemplate">
		<a download="Template.xlsx" target="_blank" href="../../..{{orderSvc.uploadFileTemplate}}">Download Template</a>
		<i class="fa fa-download"></i>
	</span>

</div>