App.controller('ofImportSidebarCtrl',['$rootScope', '$scope', '$state', 'uiLoaderSvc', 'orderSvc', 'logoutService', function($rootScope, $scope, $state, uiLoaderSvc, orderSvc, logoutService){
	$scope.dzOptions = {
		url : '/ryderonline/rtms/service/order/fulfillUpload',
		// maxFilesize : '10',
		acceptedFiles : '.XLS, .xlsm, .ms-excel, .xlsx, .csv, .xltx, .xltm',
		addRemoveLinks : true,
		'bytes' : '20',
		dictDefaultMessage:"Click here or drag and drop files to upload"
	};
	
	
	//Handle events for dropzone
	//Visit http://www.dropzonejs.com/#events for more events
	$scope.dzCallbacks = {
		'addedfile' : function(file){
			// console.log(file);
			$scope.newFile = file;
			uiLoaderSvc.show();
		},
		'success' : function(file, xhr){
			uiLoaderSvc.hide();
			console.log(file, xhr);
			orderSvc.importResults = xhr.content.resultMessages;
			$state.go('home.order-fulfillment.file-upload');
		},
		'error' : function(err, msg){
			uiLoaderSvc.hide();
			console.log("Service Errored");

			//User not authenticated (403) redirecting to login page
			if(data.status == 403){
				logoutService.logout();
			}
			else{
				$state.go('home.order-fulfillment.file-upload');
			}			
		}
	};
	
	//Apply methods for dropzone
	//Visit http://www.dropzonejs.com/#dropzone-methods for more methods
	$scope.dzMethods = {};
	$scope.removeNewFile = function(){
		$scope.dzMethods.removeFile($scope.newFile);
	}

}]);


