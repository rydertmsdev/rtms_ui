	<ui-header theme="page">
		<ui-header-body>
			<div>
				<span>
					{{orderSvc.selectedDelivery().name}}
				</span>
			</div>
		</ui-header-body>
		<!-- <ui-header-controls>
			<div class="form form-condensed">
				<ui-field type="toggle" ng-model="addlReview" class="no-label" title="Request Additional Review"></ui-field>
			</div>
		</ui-header-controls> -->
	</ui-header>
	<div class="overflow-y" ui-section-container>
		<ui-section>
			<div class="order-complete">
				<ul>
					<li class="order-complete-section" ng-class="complete">
						<span class="icon">
							<i class="fa fa-check"></i>
						</span>
						<span class="text">Shipment request submitted</span>
					</li>
					<li class="order-complete-section" ng-class="complete">
						<span class="icon">
							<i class="fa fa-info"></i>
						</span>
						<span class="text">Shipment Number - <a href="{{shipmentUrl}}" target="_new" >{{shipmentNumber}}</a></span>
					</li>
					<li class="order-complete-section" ng-class="complete">
						<span class="icon">
							<i class="fa fa-info"></i>
						</span>
						<span class="text">Load Number  - <a href="{{loadUrl}}" target="_new"> {{loadNumber}}</a></span>
					</li>
					<li class="order-complete-section" ng-class="complete">
						<span class="icon">
							<i class="fa fa-info"></i>
						</span>
						<span class="text">Unique ID  - <a href="{{loadUrl}}" target="_new"> {{uniqueId}}</a></span>
					</li>
				</ul>
			</div>
			<!-- <ui-header theme="section">
				<ui-header-body>
					<div  ng-show="!showLabels">
						<i class="fa fa-ellipsis-h"></i>
						<span>
							Please wait while your order is processing
						</span>
					</div>
					<div ng-show="showLabels">
						<i class="fa fa-check"></i>
						<span ng-show="showLabels">
							Complete
						</span>
					</div>
				</ui-header-body>
				<ui-header-controls ng-show="showLabels">
					<button ui-button ng-click="addTracking();">
						<i class="fa fa-crosshairs"></i>
						<span>Add Tracking Device</span>
					</button>
				</ui-header-controls>
			</ui-header>
			<div class="form"  style="position: relative;">
				<div class="scenario">
					<div class="icon">
						<i class="fa fa-gear"></i>
					</div>
					<ul>
						<li ng-click="scenario1()">
							Scenario 1 - No Palletization Options
						</li>
						<li ng-click="scenario2()">
							Scenario 2 - Palletization Options
						</li>
						<li ng-click="scenario3()">
							Scenario 3 - Additional Processing
						</li>
					</ul>
				</div>
				<div class="order-complete">
					<ul>
						<li class="order-complete-section" ng-repeat="step in steps" ng-class="{'active':step.status==1,'complete':step.status==2}">
							<span class="icon">
								<i class="fa fa-check"></i>
							</span>
							<span class="text">{{step.name}}</span>
						</li>
					</ul>
					<div class="message" ng-show="!!processing">
						<div class="text">
							<div class="title">This shipment requires additional processing</div>
							<div class="subtitle">You will be notified once processing is completed</div>
						</div>
					</div>
				</div>
			</div> -->

		</ui-section>

		<ui-section  ng-show="documents.length > 0">
			<ui-header theme="section">
				<ui-header-body>
					<div>
						<i class="fa fa-exclamation-circle"></i>
						<span>
							Shipping Documents
						</span>
					</div>
				</ui-header-body>
				<ui-header-controls>
					<button ui-button ng-click="printDiv()">
						<i class="fa fa-print"></i>
						<span>Print</span>
					</button>
				</ui-header-controls>
			</ui-header>
<!-- 			<div class="form rows" id="printableDiv">
				<div class="shipping-label text-center" ng-repeat="doc in documents">
					<div style="padding: 10px">{{doc.docDesc}}</div>
					<div class="sample-document"> -->
					<!-- <html>
						<body>
						    <object data="http://www.pdf995.com/samples/pdf.pdf" type="{{doc.contentType}}">
						        <embed src="http://www.pdf995.com/samples/pdf.pdf" type="{{doc.contentType}}" />
						    </object>
						</body>
					</html> -->
						<!-- <img src="img/sample-label.png" alt=""> -->
<!-- 						<embed
    type="application/pdf"
    src="http://www.pdf995.com/samples/pdf.pdf"
    id="pdfDocument"
    width="100%"
    height="100%" /> -->
						<!-- <iframe src="{{doc.docUrl}}" width="80%" height="100%" >
					</div>
				</div>
			</div> -->
		</ui-section>



		</div>