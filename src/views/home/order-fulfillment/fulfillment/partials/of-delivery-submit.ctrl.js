App.controller('ofDeliverySubmitCtrl',['$scope', '$state', '$timeout', 'uiLoaderSvc', 'uiModalSvc', 'orderSvc', function($scope, $state, $timeout, uiLoaderSvc, uiModalSvc, orderSvc){
	$scope.orderSvc=orderSvc;

	$scope.steps=[
	{
		name:"Submitting Request",
		status:2
	},
	{
		name:"Processing Data",
		status:1
	},
	{
		name:"Reviewing Shipments",
		status:0
	},
	{
		name:"Creating Labels",
		status:0
	}
	]

	$scope.shipmentNumber = "";
	$scope.shipmentUrl = "";
	$scope.loadNumber = "";
	$scope.loadUrl = "";
	$scope.documents = [];
	$scope.uniqueId = "";


	if(orderSvc.shipmentCreateObj && Object.keys(orderSvc.shipmentCreateObj).length != 0){
		$scope.shipmentNumber = orderSvc.shipmentCreateObj.shipmentNum;
		$scope.shipmentUrl = orderSvc.shipmentCreateObj.shipmentUrl;
		$scope.loadNumber = orderSvc.shipmentCreateObj.loadNum;
		$scope.loadUrl = orderSvc.shipmentCreateObj.loadUrl;
		$scope.documents = orderSvc.shipmentCreateObj.docs;
		$scope.uniqueId = orderSvc.shipmentCreateObj.uniqueId;
	}
	else{
		console.log("Shipment create object is empty");
	}

	$scope.printDiv = function() {
		uiModalSvc.show('print-modal.tpl',{
			callback:function(){
				// uiLoaderSvc.show();
				// $timeout(function(){
				// 	$scope.steps[2].status=2;
				// 	$scope.steps[3].status=2;
				// 	$scope.showLabels=true;
				// 	uiLoaderSvc.hide();
				// },2000)
			}
		});	
	} 
	$scope.addTracking=function(){
		uiModalSvc.show('of-complete-tracking-device-modal.tpl',{});
	}

	$scope.resetScenarios=function(){
		$scope.showLabels=false;
		$scope.processing=false;
		$scope.steps[0].status=2;
		$scope.steps[1].status=1;
		$scope.steps[2].status=0;
		$scope.steps[3].status=0;
	}

	$scope.scenario1=function(){
		$scope.resetScenarios();
		$scope.scenario=1;
		uiLoaderSvc.show();
		$timeout(function(){
			$scope.steps[1].status=2;
			$scope.steps[2].status=2;
			$scope.steps[3].status=2;
			$scope.showLabels=true;
			uiLoaderSvc.hide();
		},2000)
	}

	$scope.scenario2=function(){
		$scope.resetScenarios();
		$scope.scenario=2;
		uiLoaderSvc.show();
		$timeout(function(){
			$scope.steps[1].status=2;
			$scope.steps[2].status=1;
			$scope.steps[3].status=0;
			uiModalSvc.show('of-complete-palletize-modal.tpl',{
				callback:function(){
					uiLoaderSvc.show();
					$timeout(function(){
						$scope.steps[2].status=2;
						$scope.steps[3].status=2;
						$scope.showLabels=true;
						uiLoaderSvc.hide();
					},2000)
				}
			});
			uiLoaderSvc.hide();
		},2000)
	}

	$scope.scenario3=function(){
		$scope.resetScenarios();
		$scope.scenario=3;
		uiLoaderSvc.show();
		$timeout(function(){
			$scope.steps[1].status=2;
			$scope.steps[2].status=1;
			$scope.steps[3].status=0;
			$scope.showPalletModal=true;
			$scope.processing=true;
			uiLoaderSvc.hide();
		},2000)
	}
}]);
