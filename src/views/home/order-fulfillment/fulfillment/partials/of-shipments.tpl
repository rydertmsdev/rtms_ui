<ui-sidebar>
	<ui-sidebar-panel>
		<div class="content form">
			<ui-menu-group class="of-do-menu-group" ng-repeat="(dIndex, dOrder) in orderSvc.deliveryOrders track by $index" ng-click="selectDelivery(dOrder)" ui-button ng-class="{'active':dIndex==orderSvc.dIndex}">
				<ui-header theme="ui-menu-header">
					<abs-background ng-style="{'background-color':$colors($index,10)}"></abs-background>
					<ui-header-body>
						<div>
							<span>
								{{dOrder.name}}
							</span>
						</div>
					</ui-header-body>
					<ui-header-controls>
						<div ui-menu-active-indicator ng-show="dIndex==orderSvc.dIndex"></div>
					</ui-header-controls>
				</ui-header>
				<ui-menu-content>
					<div class="delivery-sidebar-details">
						<li>
							<div>
								<span class="header">
									Pieces
								</span>
								<span class="detail" ng-if="!orderSvc.deliveryOrders[dIndex].visitedFlag">
									{{dOrder.handlingUnits[0].items.length}}
								</span>
								<span class="detail" ng-if="orderSvc.deliveryOrders[dIndex].visitedFlag">
									{{orderSvc.selectedDelivery().totalItems}}
								</span>
							</div>
						</li>
						<li>
							<div>
								<span class="header">
									Weight
								</span>
								<span class="detail" ng-if="!orderSvc.deliveryOrders[dIndex].visitedFlag">
									{{dOrder.handlingUnits[0].weight}}
								</span>
								<span class="detail" ng-if="orderSvc.deliveryOrders[dIndex].visitedFlag">
									{{orderSvc.selectedDelivery().totalWeight}}
								</span>
							</div>
						</li>
						<li>
							<div>
								<span class="header">
									Volume
								</span>
								<span class="detail" ng-if="!orderSvc.deliveryOrders[dIndex].visitedFlag">
									{{dOrder.handlingUnits[0].volume}}
								</span>
								<span class="detail" ng-if="orderSvc.deliveryOrders[dIndex].visitedFlag">
									{{orderSvc.selectedDelivery().totalVolume}}
								</span>
							</div>
						</li>
						<li>
							<div>
								<span class="header">
									Handling Units
								</span>
								<span class="detail" ng-if="!orderSvc.deliveryOrders[dIndex].visitedFlag">
									{{dOrder.handlingUnits.length}}
								</span>
								<span class="detail" ng-if="orderSvc.deliveryOrders[dIndex].visitedFlag">
									{{dOrder.handlingUnits.length}}
								</span>
							</div>
						</li>
					</div>
				</ui-menu-content>
			</ui-menu-group>
		</div>
	</ui-sidebar-panel>
	<ui-sidebar-content>
		<ui-view></ui-view>
	</ui-sidebar-content>
</ui-sidebar>