<ui-sidebar-nav>
	<nav class="sidebar-nav">
		<ui-sidebar-btn></ui-sidebar-btn>
		<a ui-sref="home.orders.fulfillment.search" ui-info="Order Search" ui-sref-active="active">
			<i class="fa fa-search"></i>
		</a>
		<a ui-sref="home.orders.fulfillment.shipments.details" ui-info="Shipments" ng-disabled="orderSvc.deliveryOrders.length<1"  ui-sref-active="active">
			<i class="fa fa-cube"></i>
			<b ng-if="orderSvc.deliveryOrders.length>0" ng-bind-html="orderSvc.deliveryOrders.length" ui-animate-change="orderSvc.deliveryOrders.length" ui-animate-change-class="highlight"></b>
		</a>
		<a class="media-tablet-hide" ui-sref="home.orders.fulfillment.import" ui-info="Import" ui-sref-active="active"><i class="fa fa-download"></i></a>
	</nav>
	<ui-sidebar-nav-content>
		<ui-view></ui-view>
	</ui-sidebar-nav-content>
</ui-sidebar-nav>

