<div class="form modal-large" ng-controller="ordersItemSearchCtrl">
	<ui-header theme="ui-modal-header">
		<ui-header-body>
			<div class="skew" >
				<span>Edit Item</span>
			</div>
		</ui-header-body>
		<ui-header-controls class="inset">
			<button ui-button ng-click="close();"><i class="fa fa-times"></i></button>
		</ui-header-controls>
	</ui-header>
	<div class="modal-body overflow-y">
		<div class="form">
			<form name="itemDetailsForm">
				<ui-section>
					<div class="cols">
						<ui-field type="text" ng-model="itemUpdateDetails['itemCode']" label="Part Number" ng-required="true"></ui-field>
						<ui-field type="text" label="Description" ng-model="itemUpdateDetails['description']" ng-required="true"></ui-field>
						<ui-field type="text" label="Item Id" ng-model="itemUpdateDetails['id']" ng-required="true" ng-disabled="true"></ui-field>
						<div></div>
					</div>
					<div class="cols" ng-repeat="fourColumns in itemCreateFieldsColumns">
						<div  ng-repeat="createField in fourColumns">				
							<ui-field type="text" ng-if="createField.displayType == 'readonly'"  ng-model="itemUpdateDetails[createField.fieldName]" label="{{createField.displayName}}" ng-disabled="true"></ui-field>
							<ui-field type="text" ng-if="createField.displayType == 'freeform text'" ng-model="itemUpdateDetails[createField.fieldName]" label="{{createField.displayName}}" ng-required="createField.required()" ng-disabled="false"></ui-field>
							<ui-field type="select" ng-if="createField.displayType == 'dropdown'" options="createField.options" prop="name" ng-model="itemUpdateDetails[createField.fieldName]" label="{{createField.displayName}}" ng-required="createField.required()" ng-disabled="createField.displayType == 'readonly' ? true : false" ></ui-field>	
							<ui-field type="text" ng-if="createField.displayType == 'textarea' " ng-model="itemUpdateDetails[createField.fieldName]" label="{{createField.displayName}}" ng-required="createField.required()" ng-disabled="false"></ui-field>
							<ui-field ng-if="createField.displayType=='single checkbox'" type="toggle" class="no-label" ng-model="itemUpdateDetails[createField.fieldName]" ui-info="{{createField.hoverText}}" title="{{createField.displayName}}" ng-required="createField.required()" ng-disabled="createField.disabled()"></ui-field>
							<ui-field ng-if="createField.displayType=='single checkbox '" type="toggle" class="no-label" ng-model="itemUpdateDetails[createField.fieldName]" ui-info="{{createField.hoverText}}" title="{{createField.displayName}}" ng-required="createField.required()" ng-disabled="createField.disabled()"></ui-field>
						</div>
					</div>
				</ui-section>
				<ui-header theme="ui-modal-footer">
					<ui-header-controls>
						<button ui-button="" ng-click="close()" class="text-accent bg-white">
							<span>Cancel</span>
						</button>
						<button ui-button="" type="submit" ng-click="submitItem(itemDetailsForm.$valid, 'update')" class="bg-accent text-white">
							<span>Update</span>
						</button>
					</ui-header-controls>	
				</ui-header>
			</form>	
		</div>
	</div>
</div>