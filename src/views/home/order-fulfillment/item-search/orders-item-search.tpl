<ui-sidebar-nav style="height: 100%; overflow-y: auto;">
	<nav class="sidebar-nav">
		<ui-sidebar-btn></ui-sidebar-btn>
		<a ng-click="itemSearch()" ui-info="Item Search" ui-sref-active="active">
			<i class="fa fa-search"></i>
		</a>
		<a ui-info="Add Items" ui-sref-active="active" ng-click="addItems()">
			<i class="fa fa-plus"></i>
		</a>
	</nav>
	<ui-sidebar-nav-content ng-if="!showAddItem">
		<ui-sidebar>
			<ui-sidebar-panel>
				<div class="content form">
					<ui-header theme="ui-sidebar-header">
						<ui-header-body>
							<div>
								<span>
									Search
								</span>
							</div>
						</ui-header-body>
						<ui-header-controls>
							<div ui-button title="Clear" ng-click="clearFilters()">
								<i class="fa fa-times text-red"></i>
							</div>
							<div ui-button title="Apply" ng-click="doItemSearch(null)">
								<i class="fa fa-check text-green"></i>
							</div>
						</ui-header-controls>
					</ui-header>
					<div>
						<ui-menu-group no-overflow>
							<ui-header theme="ui-menu-header">
								<ui-header-body>
									<span>
										Filters
									</span>
								</ui-header-body>
							</ui-header>
							<ui-menu-content ui-max-height>
								<!-- <ui-field type="text" ng-model="filters.partNumber.value" label="Part Number" placeholder="-" ng-required="false" ng-disabled="false"></ui-field>
								<ui-field type="text" ng-model="filters.description.value" label="Description" placeholder="-" ng-required="false" ng-disabled="false"></ui-field> -->
								<dynamic-template data="searchContent"></dynamic-template>
								<!-- 
								<ui-field type="text" ng-model="filters.upc.value" label="Barcode" placeholder="---" ng-required="false" ng-disabled="false"></ui-field>
								<ui-field type="text" ng-model="filters.commodity.value" label="Commodity" placeholder="---" ng-required="false" ng-disabled="false"></ui-field>
								<ui-field type="text" ng-model="filters.category.value" label="Category" placeholder="---" ng-required="false" ng-disabled="false"></ui-field>
								<ui-field type="text" ng-model="filters.countryOfOrigin.value" label="Country Of Origin" placeholder="---" ng-required="false" ng-disabled="false"></ui-field>
								<ui-field type="select" options="filters.hazmat.options" prop="name" ng-model="filters.hazmat.value" label="Hazmat" placeholder="---" ng-required="false" ng-disabled="false"></ui-field> -->
							</ui-menu-content>
						</ui-menu-group>
					</div>
				</div>
			</ui-sidebar-panel>
			<ui-sidebar-content>
				<ui-alert-container></ui-alert-container>
				<!-- <ui-filters>
					<ui-filter>
						<div>
							<label>Country</label>
							<span>US</span>
						</div>
						<div ui-button ui-filter-remove-btn>
							<i class="fa fa-times"></i>
						</div>
					</ui-filter>
				</ui-filters> -->
				<div ng-if="itemMasterSvc.searchResults.length == 0">
					<ui-section>
						<ui-list class="static">
							<ui-list-body>
								<li>
									<div class="no-results">
										No results to show.
									</div>
								</li>
							</ui-list-body>
						</ui-list>
					</ui-section>
				</div>
				<div ng-if="itemMasterSvc.searchResults.length > 0">
					<ui-section ng-repeat="result in itemMasterSvc.searchResults">
						<ui-list class="static">
						<ui-list-body>
						<li>
							<ui-list-content>
								<cell class="text rows">
									<ui-header theme="section">
										<ui-header-body>
											<div class="rows">
												<span>
													{{result.description}}
												</span>
												<span class="subtitle">{{result.itemCode}}</span>
											</div>
										</ui-header-body>
										<!-- <ui-header-controls ng-if="uiResponsiveSvc.isDesktop">
											<div class="rows">
											<barcode type="code128b" string="{{result.upc}}" options="barcodeOptions" render="img"></barcode>
											<div class="subtitle">UPC {{result.upc}}</div>
											</div>
										</ui-header-controls> -->
									</ui-header>
								</cell>
							</ui-list-content>
							<ui-list-actions-btn ui-button ui-info="More Actions">
								<i class="ic-dot-3"></i>
							</ui-list-actions-btn>

							<ui-list-actions>
								<div ui-info="Edit" class="bg-eBlue" ui-button ng-click="editItem(result, $index)">
									<i class="fa fa-edit"></i>
								</div>
								<div ui-info="Delete" class="bg-red" ui-button ng-click="deleteItem(result.id, $index)" >
									<i class="fa fa-trash"></i>
								</div>
							</ui-list-actions>
						</li>
						<li>
							<ui-list-content>
								<cell class="text rows">
									<div class="cols" ng-repeat="fourColumns in SearchResultsFieldListColumns">
										<div  ng-repeat="searchField in fourColumns">				
											<ui-field type="text" ng-model="result[searchField.fieldName]" label="{{searchField.displayName}}" ng-required="false" ng-disabled="true"></ui-field>	
										</div>
									</div>
								</cell>
							</ui-list-content>
						</li>
					</ui-list-body>
					</ui-list>
					</ui-section>
				</div>
				<ui-page-actions-spacer></ui-page-actions-spacer>
				<ui-page-actions-container ng-if="itemMasterSvc.searchResults.length > 0">
					<ui-page-actions>
						<div class="group">

							<div ui-button ui-page-action class="bg-lgray" ui-info="Previous Page" ng-if="itemMasterSvc.index > 1" ng-click="doItemSearch('previous')">
								<i class="fa fa-chevron-left"></i>
							</div>
							<div ui-button ui-page-action class="bg-lgray" ui-info="Previous Page" ng-if="itemMasterSvc.index == 1" disabled="disabled">
								<i class="fa fa-chevron-left"></i>
							</div>
							<div class="text">
								<span>{{itemMasterSvc.index}} / {{itemMasterSvc.pageTotal}}</span>
							</div>
							<div ui-button ui-page-action class="bg-accent"  ui-info="Next Page" ng-if="(itemMasterSvc.index < itemMasterSvc.pageTotal) || ((itemMasterSvc.index == itemMasterSvc.pageTotal) && itemMasterSvc.recordsCountExceeded)" ng-click="doItemSearch('next')">
								<i class="fa fa-chevron-right"></i>
							</div>
							<div ui-button ui-page-action class="bg-accent"  ui-info="Next Page" ng-if="(itemMasterSvc.index == itemMasterSvc.pageTotal) && !itemMasterSvc.recordsCountExceeded" disabled="disabled">
								<i class="fa fa-chevron-right"></i>
							</div>
						</div>
					</ui-page-actions>
				</ui-page-actions-container>
			</ui-sidebar-content>
		</ui-sidebar>
	</ui-sidebar-nav-content>
	<ui-sidebar-nav-content ng-if="showAddItem">
		<form name="itemDetailsForm">
			<ui-sidebar-content >
				<ui-section>
					<ui-header theme="section">
						<ui-header-body>
							<span class="ng-binding">Add Item</span>
						</ui-header-body>
					</ui-header>
					<div class="cols">
						<ui-field type="text" ng-model="createItemFieldValues['itemCode']" label="Part Number" ng-required="true"></ui-field>
						<ui-field type="text" label="Description" ng-model="createItemFieldValues['description']" ng-required="true"></ui-field>
					</div>
					<div class="cols" ng-repeat="fourColumns in itemCreateFieldsColumns">
						<div  ng-repeat="createField in fourColumns">				
							<ui-field type="text" ng-if="createField.displayType == 'readonly'"  ng-model="createItemFieldValues[createField.fieldName]" label="{{createField.displayName}}" ng-disabled="true"></ui-field>
							<ui-field type="text" ng-if="createField.displayType == 'freeform text'" ng-model="createItemFieldValues[createField.fieldName]" label="{{createField.displayName}}" ng-required="createField.required()" ng-disabled="false"></ui-field>
							<ui-field type="select" ng-if="createField.displayType == 'dropdown'" options="createField.options" prop="name" ng-model="createItemFieldValues[createField.fieldName]" label="{{createField.displayName}}" ng-required="createField.required()" ng-disabled="createField.displayType == 'readonly' ? true : false" ></ui-field>	
							<ui-field type="text" ng-if="createField.displayType == 'textarea' " ng-model="createItemFieldValues[createField.fieldName]" label="{{createField.displayName}}" ng-required="createField.required()" ng-disabled="false"></ui-field>	
							<ui-field ng-if="createField.displayType=='single checkbox'" type="toggle" class="no-label" ng-model="createItemFieldValues[createField.fieldName]" ui-info="{{createField.hoverText}}" title="{{createField.displayName}}" ng-required="createField.required()" ng-disabled="createField.disabled()"></ui-field>
							<ui-field ng-if="createField.displayType=='single checkbox '" type="toggle" class="no-label" ng-model="createItemFieldValues[createField.fieldName]" ui-info="{{createField.hoverText}}" title="{{createField.displayName}}" ng-required="createField.required()" ng-disabled="createField.disabled()"></ui-field>					
						</div>
					</div>
				</ui-section>

				<ui-page-actions-spacer class="ng-scope"></ui-page-actions-spacer>
				<ui-page-actions-container>
					<ui-page-actions>
						<div class="group">
							<button type="submit" ui-button ui-page-action class="text-accent bg-white" ui-info="Submit Item Details" ng-click="submitItem(itemDetailsForm.$valid, 'add')">
								<span>Submit</span>
								<i class="fa fa-chevron-right"></i>
							</button>
						</div>
					</ui-page-actions>
				</ui-page-actions-container>
			</ui-sidebar-content>
		</form>		
	<ui-sidebar-nav-content>
</ui-sidebar-nav>

