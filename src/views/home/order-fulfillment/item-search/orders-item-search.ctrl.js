App.controller('ordersItemSearchCtrl',['$scope', '$timeout','$rootScope','$compile', 'uiModalSvc','uiLoaderSvc','logoutService','codeListService','generateSearchQuery','itemMasterSvc', 'purchaseOrderSearchResult','ItemMasterService', function($scope, $timeout,$rootScope,$compile,uiModalSvc,uiLoaderSvc,logoutService,codeListService,generateSearchQuery,itemMasterSvc, purchaseOrderSearchResult, ItemMasterService){

$scope.filters={
	country:{
		value:"US"
	},
	hazmat:{
		options:[
			"Hazmat",
			"Non-Hazmat"
		]
	}
}

$scope.codeLists = [];
$scope.dynamicContent = [];
$scope.searchContent = [];
$scope.createItemFieldValues = {};
$scope.itemMasterSvc = itemMasterSvc;
$scope.itemUpdateDetails = itemMasterSvc.itemUpdateDetails;
$scope.SearchResultsFieldList = [];
itemMasterSvc.searchResults = [];

if(generateSearchQuery.from_server_copy){
	$scope.dynamicContent = generateSearchQuery.from_server_copy.SearchFieldList;
}

angular.forEach($scope.dynamicContent, function(field, index) {
	if(field.sectionSeq == 1 || field.sectionSeq == null){
		$scope.searchContent.push(field);
	}
	else{
		$scope.additionalSearchContent.push(field);
	}
});

if(codeListService.codeLists){
	$scope.codeLists = codeListService.codeLists;
}

if(codeLists.SearchResultsFieldList){
	$scope.SearchResultsFieldList = codeLists.SearchResultsFieldList;
}

$scope.itemCreateFields = [];

if($scope.codeLists.ItemEditFieldsList){
	$scope.itemCreateFields = $scope.codeLists.ItemEditFieldsList;
}

function columnize(input, cols, fieldLength) {
	var arr = [];
	for(i = 0; i < input.length; i++) {
	  	
	  	if(input[i].required == "Y" || input[i].required == true){
	  		input[i].required = function(){return true};
	  	}
	  	else if(input[i].required == "N" || input[i].required == false || !(input[i].required)){
	  		input[i].required = function(){return false};
	  	}
	  	
	  	if(input[i].disabled == "Y" || input[i].disabled == true){
	  		input[i].disabled = function(){return true};
	  	}
	  	else if(input[i].disabled == "N" || input[i].disabled == false || !(input[i].disabled)){
	  		if(input[i].displayType == 'readonly' || input[i].displayType == 'hidden') {
	  			input[i].disabled = function(){return true};	
	  		}
	  		else{
	  			input[i].disabled = function(){return false};
	  		}	  		
	  	}

	  	if(input[i].displayType != 'hidden' && input[i].displayType != 'none'){
		    var colIdx = Math.floor(i / fieldLength);
	    	arr[colIdx] = arr[colIdx] || [];
	    	arr[colIdx].push(input[i]);
	    }
	    else if(input[i].displayType == 'hidden' && input[i].fieldType == 'CHRG'){
	    	arr[cols] = arr[cols] || [];
	    	arr[cols].push(input[i]);
	    }
	    else if(input[i].displayType == 'hidden' && input[i].fieldType == 'REF'){
	    	arr[cols+1] = arr[cols+1] || [];
	    	arr[cols+1].push(input[i]);
	    }
	}
	return arr;
}

var tempArr = $scope.itemCreateFields;

angular.forEach(tempArr, function(item, index){
	if(item.displayType == "readonly"){
		tempArr.splice(index, 1);
	}
});

$scope.itemCreateFieldsColumns = columnize(tempArr, Math.ceil(tempArr.length/4), 4);
$scope.SearchResultsFieldListColumns = columnize($scope.SearchResultsFieldList, Math.ceil($scope.SearchResultsFieldList.length/4), 4);

$scope.showAddItem = false;

$scope.submitItem=function(flag, type){
	if(flag){

		if(type == "update"){
			$scope.createItemFieldValues = $scope.itemUpdateDetails;
		}

		angular.forEach($scope.itemCreateFields, function(fieldSet, i){
			if(fieldSet.displayType == "dropdown" && angular.isObject($scope.createItemFieldValues[fieldSet.fieldName])){
				$scope.createItemFieldValues[fieldSet.fieldName] = $scope.createItemFieldValues[fieldSet.fieldName] ? $scope.createItemFieldValues[fieldSet.fieldName].code : "";		
			}
		});

		console.log($scope.createItemFieldValues);	
		$scope.createItemFieldValues["customernum"] = $rootScope.userProfile.data.content.customerNum || ""; 

		uiLoaderSvc.show();
		ItemMasterService.createItem("itemmaster/v1", $scope.createItemFieldValues, type).then(function(data){
			uiLoaderSvc.hide();
			// if(type=="update"){
			// 	uiModalSvc.hide();
			// }
			var message = "New Item created.";

			if(type=="update"){
				message = "Item Updated";
			}

			uiModalSvc.show('ok-cancel-modal.tpl',{
				title:"Success",
				icon: "fa fa-check",
				text: message,
				callback:function(){
					$scope.locationSearch();
				}
			});
			
		}, function(err){
			uiLoaderSvc.hide();
			console.log("Service Error - ", err);

			//User not authenticated (403) redirecting to login page
			if(err.status == 403){
				logoutService.logout();
			}
			else{
				var errMessage = "<ul>"
		        for(i=0; i<err.data.messages.length; i++){
		            errMessage += "<li>"+ err.data.messages[i].message+"</li>";
		        }
		          errMessage += "</ul>";

				uiModalSvc.show('ok-cancel-modal.tpl',{
					title:"Error",
					icon: "fa fa-exclamation-triangle",
					html: errMessage ? errMessage : "Something went wrong, please try again.",
					callback:function(){
						//Do nothing on OK button.
					}
				});
			}
		});
	}
}

$scope.locationSearch=function(){
	$scope.showAddItem = false;
}

$scope.categoryOptions=[{"name":"CAT1", "code":""}, {"name":"CAT2", "code":""}, {"name":"CAT3", "code":""}];
$scope.commodityOptions=[{"name":"100", "code":""}, {"name":"125", "code":""}, {"name":"150", "code":""}, {"name":"175", "code":""}];
$scope.countryOptions=[{"name":"United States", "code":""}, {"name":"United Kingdon", "code":""}, {"name":"Mexico", "code":""}, {"name":"Canada", "code":""}];
$scope.hazmatOptions=[{"name":"Yes", "code":"Y"}, {"name":"No", "code":"N"}];
$scope.dimensionsOptions=[{"name":"Length", "code":"L"}, {"name":"Width", "code":"W"}, {"name":"Height", "code":"H"}, {"name":"Weight", "code":"W"}];
$scope.requirementOptions=[{"name":"Temperature Control", "code":"TC"}, {"name":"Pilot Accessible", "code":"PA"}, {"name":"Escort", "code":"E"}, {"name":"Tracking", "code":"T"}];

$scope.addItems=function(){
	$scope.showAddItem = true;
	$scope.createItemFieldValues = {};
}

$scope.itemSearch=function(){
	$scope.showAddItem = false;
}

$scope.deleteItem=function(id, index){
	$scope.uiModalSvc.show('ok-cancel-modal.tpl',{
		title:"Delete Location",
		text:"Are you sure you want delete this Item",
		callback:function(){
			var reqObj = {"id": id, "customernum": $rootScope.userProfile.data.content.customerNum};
			uiLoaderSvc.show();
			ItemMasterService.deleteItem("itemmaster/v1", reqObj).then(function(data){
				uiLoaderSvc.hide();
				uiModalSvc.show('ok-cancel-modal.tpl',{
					title:"Success",
					icon: "fa fa-check",
					text: "Item successfully deleted.",
					callback:function(){
						//Do nothing on OK button.
					}
				});
				itemMasterSvc.searchResults.splice(index, 1);
			},function(err){
				uiLoaderSvc.hide();
				var errMessage = err.data.errorDetails;

				uiModalSvc.show('ok-cancel-modal.tpl',{
					title:"Error",
					icon: "fa fa-exclamation-triangle",
					text: errMessage || "Something went wrong, please try again.",
					callback:function(){
						//Do nothing on OK button.
					}
				});
			});
		}
	});	
}

$scope.editItem=function(result, index){
	itemMasterSvc.itemUpdateDetails = itemMasterSvc.searchResults[index];
	$scope.uiModalSvc.show('item-edit-modal.tpl',{
		
	});
}

$scope.close=function(){
	uiModalSvc.hide();
}

$scope.doItemSearch=function(type){

	uiLoaderSvc.show();

	$scope.searchCriteria = generateSearchQuery.searchOrderQueryResp();
	$scope.searchCriteria.customernum = $rootScope.userProfile.data.content.customerNum;
	// $scope.searchCriteria.status = "NOT_ALLOCATED";

	$scope.requestObject = {};
	$scope.requestObject.currentPage = itemMasterSvc.index;
	$scope.requestObject.sortBy = null;
	$scope.requestObject.sortOrder = null;
	$scope.requestObject.pagingEnabled = true;

	$scope.requestObject.pageSize = itemMasterSvc.pageSize;
	$scope.requestObject.rowsPerBatch = itemMasterSvc.rowsPerbatch;
	$scope.requestObject.searchCriteriaObject = $scope.searchCriteria;

	if(type == "doublePrevious"){
		var totalNumOfPages = itemMasterSvc.rowsPerbatch/itemMasterSvc.pageSize ;
		$scope.requestObject.currentPage = (Math.floor((itemMasterSvc.index - 1)/totalNumOfPages)*totalNumOfPages)-totalNumOfPages+1;	
	}
	else if(type == "previous"){
		$scope.requestObject.currentPage = itemMasterSvc.index - 1;
	}
	else if(type == "next"){
		$scope.requestObject.currentPage = itemMasterSvc.index + 1;
	}
	else if(type == "doubleNext"){
		var totalNumOfPages = itemMasterSvc.rowsPerbatch/itemMasterSvc.pageSize;
		//$scope.requestObject.currentPage = itemMasterSvc.pageTotal + 1;
		$scope.requestObject.currentPage = (Math.floor((itemMasterSvc.index + (totalNumOfPages - 1))/totalNumOfPages)*totalNumOfPages)+1;	
	}
	else{
		$scope.requestObject.currentPage = 1;	
	}

	// var filter = [{ "by": "orderNumber", "order": "ASC"}, { "by": "lineItemNum", "order": "ASC"}];
	// $scope.requestObject.sortByList = filter;

	// console.log("Request Object  ---   ", JSON.stringify($scope.requestObject));
	var orders = ItemMasterService.getResults("itemmaster/v1", $scope.requestObject).then(function(resp){
		uiLoaderSvc.hide();

		if(resp.data && resp.data.content){
			// itemMasterSvc.searchResults = getProcessedSearchResults.processResults(resp.data.content);	
			itemMasterSvc.index = $scope.requestObject.currentPage;
			itemMasterSvc.searchResults = resp.data.content.searchResultsList || [];
			itemMasterSvc.pageTotal = Math.ceil(resp.data.content.totalRecords/$scope.requestObject.rowsPerBatch);
			itemMasterSvc.recordsCount = resp.data.recordsCount;
			// itemMasterSvc.recordsCountExceeded = resp.data.recordsCountExceeded;
		}		
	},
	function(data){
		uiLoaderSvc.hide();
		itemMasterSvc.searchResults = [];
		console.log("Service Error - ", data);
		
		//User not authenticated (403) redirecting to login page
		if(data.status == 403){
			logoutService.logout();
		}
	});

	itemMasterSvc.selectAllFlag = false;	

	$rootScope.$emit("searchClicked");
	// $state.reload();
	// itemMasterSvc.search().then(function(){
	// 	uiLoaderSvc.hide();
	// });
}

$scope.clearFilters=function(){
	generateSearchQuery.clearValues();
}


$scope.barcodeOptions = {
	width: 1,
	height:20,
	quite: 10,
	displayValue: false,
	font: "monospace",
	textAlign: "center",
	fontSize: 10,
	backgroundColor: "",
	lineColor: "#000"
}


}]);
