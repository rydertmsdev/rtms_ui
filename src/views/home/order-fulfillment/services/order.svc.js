///////////////////////////////////////////////////
// test stuff
//

var shuffle=function(array) {
  //array.slice(0, 100);
  var currentIndex = array.length, temporaryValue, randomIndex;
  while (0 !== currentIndex) {
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }
  return array;
}

///////////////////////////////////////////////////
// Service
//

App.service('UserProfileService',
  /**
   * Fetches the user profile details
   */
  function UserProfileService($rootScope, $http, $filter) {

    /**
     * Fetch the user profile
     */

    this.fetchUserProfile = function(module, url) {
      // var data = {"changeToCustomerNum" : customerName};
      var errorMessage;
      if(!url){
        url = "userProfile";
      }
      // if($filter('isInvalidString')(module)){
      //  throw new Error($translate.instant('error.empty.module.name'));
      // }
      
      return $http.get("/ryderonline/rtms/service/"+module+"/"+url);
      
    };

});

/* Fetches the codeList service details */
App.service('codeListService', function codeListService($rootScope, $http, $filter, $q) {

    /* Fetch the code List result  */

    this.codeLists = {};

    this.codeListsGetCall = function (module) {
      
      var deferred = $q.defer();

      $http.get("/ryderonline/rtms/service/"+module+"/codeLists")
      .then( function(response) {
              this.codeLists = response.data.content;
              deferred.resolve(response);
          }, function(errResp) {
               deferred.reject(errResp);
          });
      return deferred.promise;
    }

    this.codeListsPostCall = function (module, data, url) {
      
      var deferred = $q.defer();

      if(!url){
        url = "codeLists";
      }

      $http.post("/ryderonline/rtms/service/"+module+"/"+url, data)
      .then( function(response) {
              this.codeLists = response.data.content;
              deferred.resolve(response);
          }, function(errResp) {
               deferred.reject(errResp);
          });
      return deferred.promise;
    }
});

/* Fetches the purchase order search result */
App.service('purchaseOrderSearchResult', function purchaseOrderSearchResult($rootScope, $http, $filter, $q) {

    /* Get the search result  */

    this.getResults = function (module, data) {
      
      var deferred = $q.defer();

      $http.post("/ryderonline/rtms/service/"+module+"/orderItemsList", data)
      .then( function(response) {
               deferred.resolve(response);
          }, function(errResp) {
               deferred.reject(errResp);
          });
      return deferred.promise;
    }
});

App.service('addressLookupService', function addressLookupService($rootScope, $http, $filter, $q) {

    /* Get location search result  */

    this.getResults = function (module, data) {
      
      var deferred = $q.defer();

      $http.post("/ryderonline/rtms/service/"+module+"/locations", data)
      .then( function(response) {
               deferred.resolve(response);
          }, function(errResp) {
               deferred.reject(errResp);
          });
      return deferred.promise;
    }
});

App.service('handlingUnitLookUpService', function handlingUnitLookUpService($rootScope, $http, $filter, $q) {

    /* Get location search result  */

    this.getResults = function (module, data) {
      
      var deferred = $q.defer();

      $http.post("/ryderonline/rtms/service/"+module+"/orders/huMasterData/lookup", data)
      .then( function(response) {
               deferred.resolve(response);
          }, function(errResp) {
               deferred.reject(errResp);
          });
      return deferred.promise;
    }
});

App.service('SaveHandlingUnit', function SaveHandlingUnit($rootScope, $http, $filter, $q) {

    /* Get location search result  */

    this.create = function (module, data) {
      
      var deferred = $q.defer();

      $http.post("/ryderonline/rtms/service/"+module+"/orders/huMasterData/save/", data)
      .then( function(response) {
               deferred.resolve(response);
          }, function(errResp) {
               deferred.reject(errResp);
          });
      return deferred.promise;
    }
});

App.service('getLocationDependencies', function getLocationDependencies($rootScope, $http, $filter, $q) {

    /* Get the search result  */

    this.getResults = function (module, obj) {
      
      var deferred = $q.defer();

      $http.post("/ryderonline/rtms/service/"+module+"/shipmentFieldDependencies", obj)
      .then( function(response) {
               deferred.resolve(response);
          }, function(errResp) {
               deferred.reject(errResp);
          });
      return deferred.promise;
    }
});

App.service('shipmentValidations', function shipmentValidations($rootScope, $http, $filter, $q) {

    /* Get the search result  */

    this.getResults = function (module, obj) {
      
      var deferred = $q.defer();

      $http.post("/ryderonline/rtms/service/"+module+"/shipmentValidations", obj)
      .then( function(response) {
               deferred.resolve(response);
          }, function(errResp) {
               deferred.reject(errResp);
          });
      return deferred.promise;
    }
});

App.service('getFulfilledDocuments', function getFulfilledDocuments($rootScope, $http, $filter, $q) {

    /* Get the print document */
    //Not using this service as this is replaced with fulfilledDocContent service which we call directly from print controller

    this.getResults = function (module, data) {
      
      var deferred = $q.defer();

      $http.post("/ryderonline/rtms/service/"+module+"/fulfilledDocuments", data)
      .then( function(response) {
               deferred.resolve(response);
          }, function(errResp) {
               deferred.reject(errResp);
          });
      return deferred.promise;
    }
});


App.service('generateSearchQuery', function generateSearchQuery($rootScope){
  var _self = this;
  var map = {};
  var codes = [];

  this.searchQuery = function(result){
    
    _self.from_server_copy = JSON.parse( JSON.stringify( result.data.content ) );
    var object_keys_received = Object.keys( _self.from_server_copy );
    var uuid = 0;
    _self.values = {};
    

    angular.forEach(object_keys_received, function( key ) {
    // if ( (key == "ReferenceList") || (key == "OrderItemReferenceList") || (key == "SearchFieldList")) {
     if (key == "SearchFieldList") {
      var tempKey = key;
        angular.forEach(_self.from_server_copy[ key ], function( val ) {
          
          var code = val.fieldName;

          // if(tempKey == "ReferenceList"){
          //   code =  "supplyOrder.references[referenceCode='"+val.code+"'].referenceNum";
          // }

          // if(tempKey == "OrderItemReferenceList"){
          //   code =  "references[referenceCode='"+val.code+"'].referenceNum";
          // }
          var tempArray = code.split(".");
            if(tempArray.length <= 1 && code != "statusList") {
              code = code;
            }
            
            codes.push( code );

            
            map[ code ] = uuid;
            val.uuid = uuid;
            uuid++;
        } )
    } else {
      //don't do anything

            // _self.from_server_copy[ key ].uuid = uuid;
            // codes.push( _self.from_server_copy[ key ].code );
            // map[ _self.from_server_copy[ key ].code ] = uuid;
            // uuid++;
        }
    } )

    // setting values for uuids
    for ( var i = 0; i < uuid; i++ ) {
        _self.values[ i ] = "";//Math.random();
    }

    
    // console.log( 'Output generated to output.txt' );

  }

  this.searchOrderQueryResp = function(){
    var sendObj = {};
    
    angular.forEach(codes, function ( code ) {
        var prop_array = code.split( '.' );
        var stored_val_for_code = _self.values[ map[ code ] ];

        if(angular.isObject(stored_val_for_code) && !(angular.isArray(stored_val_for_code))){
          stored_val_for_code = stored_val_for_code.code;
        }

        if(angular.isArray(stored_val_for_code)){
          var tempArr = [];
          angular.forEach(stored_val_for_code, function( item ) {
            tempArr.push(item.code);
          })
          stored_val_for_code = tempArr;          

          // if(tempArr.length > 0)
          //   stored_val_for_code = tempArr;
          // else
          //   stored_val_for_code = [];
        }

        if(!!stored_val_for_code){
          var currentProp = sendObj;
          angular.forEach(prop_array, function( prop, ind ) {
              // console.log(prop,ind)
              store_empty_or_val = ind == ( prop_array.length - 1 ) ? stored_val_for_code : {};
              var arrayDetails = _self.findArrayType( prop );
              if(store_empty_or_val === true){ store_empty_or_val = 'Y'}
              currentProp =  ( arrayDetails.matched ) ? _self.forArray( currentProp,arrayDetails  ) :_self.forNotArray( currentProp, prop, store_empty_or_val ); 
          } )
        }
    } );

   // console.log("result----",sendObj);
   return sendObj;
  }

    this.forArray = function(  currentProp, arrayDetails ) {
      if ( !currentProp[ arrayDetails.propname ] ) {
          currentProp[ arrayDetails.propname ] = [];
      }
      currentProp[ arrayDetails.propname ].push( arrayDetails.matchValues );
      var index = currentProp[ arrayDetails.propname ].length - 1;
      return currentProp = currentProp[ arrayDetails.propname ][ index ];
    }

    this.forNotArray = function(  currentProp, prop, store_empty_or_val ) {
        if ( !currentProp[ prop ] ) {

          currentProp[ prop ] = store_empty_or_val;
          
        }
        return currentProp = currentProp[ prop ];
    }



    // find if the prop is an array type and extract info
    this.findArrayType = function( prop ) {
        var matched = prop.match( /\[(.*)\]/ );
        if ( matched && matched[ 0 ] ) {
            var propname = prop.split( '[' )[ 0 ];
            var matchedArr = matched[ 1 ].replace( /'/g, '' )
                .split( "=" );
            var returnObj = {
                matched: true,
                propname: propname
            };
            returnObj.matchValues = {};
            returnObj.matchValues[ matchedArr[ 0 ] ] = matchedArr[ 1 ]
            return ( returnObj )
        } else return ( {
            matched: false,
        } )
    }

    this.clearValues = function() {
      var tempObj = Object.keys(_self.values); 

      angular.forEach(tempObj, function(key) {
        _self.values[key] = "";
      })
    }
 
});

/* Fetches the purchase order details */
App.service('purchaseOrderDetails', function purchaseOrderDetails($rootScope, $http, $filter, $q) {

    /* Get the search result  */

    this.getResults = function (module, id) {
      
      var deferred = $q.defer();

      $http.get("/ryderonline/rtms/service/"+module+"/orders/"+id)
      .then( function(response) {
               deferred.resolve(response);
          }, function(errResp) {
               deferred.reject(errResp);
          });
      return deferred.promise;
    }
});


/* Fetches the delivery order details */
App.service('deliveryOrderDetails', function deliveryOrderDetails($rootScope, $http, $filter, $q) {

    /* Get the search result  */

    this.getResults = function (module, obj) {
      
      var deferred = $q.defer();

      $http.post("/ryderonline/rtms/service/"+module+"/fulfillTemplate", obj)
      .then( function(response) {
               deferred.resolve(response);
          }, function(errResp) {
               deferred.reject(errResp);
          });
      return deferred.promise;
    }
});

/* Submit PO for shipment creation service */
App.service('createShipment', function createShipment($rootScope, $http, $filter, $q) {

    /* Get the search result  */

    this.create = function (module, obj) {
      
      var deferred = $q.defer();

      $http.post("/ryderonline/rtms/service/"+module+"/fulfillShipment", obj)
      .then( function(response) {
               deferred.resolve(response);
          }, function(errResp) {
               deferred.reject({ message: "Really bad", err : errResp });
          });
      return deferred.promise;
    }
});

/* Update PO details */
App.service('updatePODetails', function updatePODetails($rootScope, $http, $filter, $q) {

    /* Get the search result  */

    this.update = function (module, obj) {
      
      var deferred = $q.defer();

      $http.post("/ryderonline/rtms/service/"+module+"/orders/update", obj)
      .then( function(response) {
               deferred.resolve(response);
          }, function(errResp) {
               deferred.reject({ message: "Really bad", err : errResp });
          });
      return deferred.promise;
    }
});

App.service('getProcessedSearchResults', ['codeListService', 'orderSvc', function getProcessedSearchResults(codeListService, orderSvc){

  this.processResults = function(tempResultObj){
    var tableHeadingobj = codeListService.codeLists.SearchResultsFieldList;

    var resultObj = [];
    
    angular.forEach(tempResultObj, function(record, index) {
        resultObj.push({});
        angular.forEach(tableHeadingobj, function(val) {    
          var code = val.code;
          var tempArray = code.split(".");
          if(tempArray.length <= 1) {
            resultObj[index][code] =  tempResultObj[index][code];
          }

          if(tempArray.length > 1) {
            angular.forEach(tempArray, function( prop, ind ) {
              //supplyOrder.addresses[addressesTypeId='20'].addr1
              //firstIndex = supplyOrder, lastIndex = addr1, tempArray[1] will have "addresses[addressesTypeId='20']"

              

              if(tempArray.length == 2){
                firstIndex = "";
                middleIndex = tempArray[0];
                lastIndex = tempArray[1];
              }
              else{
                firstIndex = tempArray[0];
                 middleIndex = tempArray[1]
                lastIndex = tempArray[2];
              }

               if(tempArray.length == 2 && middleIndex == "supplyOrder"){
                  resultObj[index][code] =  tempResultObj[index][middleIndex][lastIndex];
               }
               else{
                //Below code will split the values and get the array values as a object
                var matched = middleIndex.match( /\[(.*)\]/ );
                var propname = middleIndex.split( '[' )[ 0 ];
                  var matchedArr = matched[ 1 ].replace( /'/g, '' )
                      .split( "=" );
                  var returnObj = {
                      matched: true,
                      propname: propname
                  };
                  returnObj.matchValues = {};
                  returnObj.matchValues[ matchedArr[ 0 ] ] = matchedArr[ 1 ]
                  
                  if(tempArray.length == 2){
                    var addrObj = tempResultObj[index][returnObj.propname];
                  }
                  else{
                    var addrObj = tempResultObj[index][firstIndex][returnObj.propname]; 
                  }

                  if(!addrObj){
                    resultObj[index][code] = "";
                  }
                  else{
                    angular.forEach(addrObj, function(addr, i) {
                      var val1 = addr[matchedArr[0]];
                      var val2 = matchedArr[ 1 ];

                      if(val1 == val2){
                        resultObj[index][code] = addr[lastIndex]; 
                      }
                    });
                  }
                }
            });
          }

        });

      var tempFlagSelected = false;  
      angular.forEach(orderSvc.selectedSearchOrders, function(order, i) {
        var orderComponentNum = record.orderNumber+"_"+record.lineItemNum;       
        if(orderSvc.selectedSearchOrders[i] == orderComponentNum){
          tempFlagSelected = true; 
        }
      });


        if(tempFlagSelected){
          tempResultObj[index].$$selected = true;
        }
        else{
          tempResultObj[index].$$selected = false;
        }

        tempResultObj[index]["resultObject"] = resultObj[index];
    });
    return tempResultObj;
  }
}]);

App.service('logoutService', ['uiModalSvc', '$window', function(uiModalSvc, $window){
  this.logout = function(){
    uiModalSvc.show('ok-cancel-modal.tpl',{
      title:"Warning",
      html: "For your security, you have been automatically logged out due to inactivity. Please log in again.",
      callback:function(){
        var url = "../../rydertrac/splash";
        $window.open(url, '_self');
      }
    });
  }
}]);

App.filter('parseDates', function() {
    return function(val, type) {
      if(!!val){
        var date = new Date(val);

        var month =  date.getMonth() +1;
        if(month < 10)
          month = "0"+month;
        var day = date.getDate();
        var year = date.getFullYear();
        var hours = date. getHours();
        var minutes = date.getMinutes();
        var seconds = date.getSeconds();

        if(type == 'date')
        {
          date = month+"/"+day+"/"+year;
        }

        else if(type == 'datetime'){
          date = month+"/"+day+"/"+year+" "+hours+":"+minutes+":"+seconds;
        }

        return date;
      }
      else{
        return '';
      }
   }
});

App.filter('split', function() {
    return function(input, splitChar, splitIndex) {
        // do some bounds checking here to ensure it has that index
        if(input){
          return input.split(splitChar)[splitIndex];  
        }
        else{
          return;
        }
        
    }
});

App.service('orderSvc', [
  '$rootScope',
  '$window', 
  '$q', 
  '$timeout', 
  '$filter', 
  'uiLoaderSvc', 
  'uiModalSvc', 
  'testData', 
  'containsFilter', 
  'filterByFilter', 
  'codeListService',
  '$state',
  function ($rootScope, $window, $q, $timeout, $filter, uiLoaderSvc, uiModalSvc, testData, containsFilter, filterByFilter, codeListService, $state) {
    var svc=this;

    svc.module = "order";
    svc.doInc = 0;

    // svc.searchResults=shuffle(testData.purchaseOrders)||[];
    svc.searchResults= [];
    svc.index = 1;
    svc.pageTotal = "";
    svc.recordsCount = "";
    svc.pageSize = 50;
    svc.rowsPerbatch = 500;
    svc.selectedSearchOrders = [];
    if($rootScope.userProfile)
      svc.customerName = $rootScope.userProfile.data.content.customerNum;
    svc.importResults = [];

    svc.palletTypeFlag = true;

    //To store the container types based on locationDependencies service call
    svc.containerTypesList = [];

    svc.selectAllFlag = false;

    //Object create to store shipmnet creation object
    svc.shipmentCreateObj = {};

    svc.uploadFileTemplate = "";

    if(codeListService.codeLists && codeListService.codeLists.CustomerConfigList && codeListService.codeLists.CustomerConfigList[0]){
      svc.uploadFileTemplate = codeListService.codeLists.CustomerConfigList[0].OF_UI_UPLOAD_TEMPLATE;
    } 

   svc.statesList = [];
   svc.countriesList = [];
    
    if(codeListService.codeLists){
      if(codeListService.codeLists.StateList){
       svc.statesList = codeListService.codeLists.StateList;  
      }

      if(codeListService.codeLists.CountryList){
       svc.countriesList = codeListService.codeLists.CountryList;
      }
      
    }

    //flag to show double next button based on service result
    svc.recordsCountExceeded = false;

    //selected root orderComponentNum used to compare the line items on order details modal popup
    svc.selectedRootOrder = "";

    svc.save=function(){
     $window.localStorage.setItem('deliveryOrders',angular.toJson(svc.deliveryOrders));
   }  

   svc.load=function(){
     svc.deliveryOrders=JSON.parse($window.localStorage.getItem('deliveryOrders'))||[];
   }

   svc.load();

   if(codeListService.codeLists.SearchResultsFieldList)
    svc.tableHeaders = JSON.parse(JSON.stringify(codeListService.codeLists.SearchResultsFieldList));


  ///////////////////////////////////////////////////
  // Prototypes
  //

  svc.DeliveryOrder=function(param){
    this.uuid=$uuid();
    this.name=param.name;
    this.origin=param.origin;
    this.destination=param.destination;
    this.sector=param.sector;
    this.visitedFlag=param.visitedFlag;
    this.handlingUnits=[];
    this.references=param.references;
    this.key = param.key; 
  }

  svc.HandlingUnit=function(param){
    this.uuid=$uuid();
    this.name=param.name;
    this.type=param.type;
    this.length=param.length;
    this.width=param.width;
    this.height=param.height;
    this.weight=param.weight;
    this.uom=param.uom;
    this.volume=param.volume;
    this.quantityUom=param.quantityUom;
    this.volumeUom=param.volumeuom;
    this.items=[];
  }

  ///////////////////////////////////////////////////
  // Functions
  //

  svc.dIndex=0;
  //svc.showDetails=true;

  svc.search=function(filters){
    var dfd=$q.defer();
    uiLoaderSvc.show();
    //get data from the server
    $timeout(function(){
      svc.searchResults=shuffle(testData.purchaseOrders)||[];
      uiLoaderSvc.hide();
      dfd.resolve();
    },1000)
    return dfd.promise;
  }

  svc.createDeliveryOrder=function(deliveryOrder){
    if (!svc.deliveryOrders) svc.deliveryOrders=[];
    return svc.deliveryOrders.push(deliveryOrder);
  }

  svc.deleteDeliveryOrder=function(index){
    svc.dIndex>0?svc.dIndex--:svc.dIndex=0;
    return svc.deliveryOrders.splice(index,1);
  }

  svc.deleteAllDeliveryOrders=function(){
    svc.deliveryOrders.length=0;
  }

  svc.addHandlingUnit=function(dIndex,newHandlingUnit){
    var handlingUnit=new svc.HandlingUnit(newHandlingUnit);
    if(angular.isObject(handlingUnit.quantityUom)){
      handlingUnit.quantityUom = handlingUnit.quantityUom.code;
    }
    if(svc.deliveryOrders[dIndex] && svc.deliveryOrders[dIndex].visitedFlag){
      svc.deliveryOrders[dIndex].shipmentObject.handlingUnits.push(handlingUnit) 
    }
    return svc.deliveryOrders[dIndex].handlingUnits.push(handlingUnit)
  }  

  svc.updateHandlingUnit=function(dIndex,hIndex,handlingUnit){
    svc.deliveryOrders[dIndex].shipmentObject.handlingUnits.splice(hIndex,1,handlingUnit);
    svc.deliveryOrders[dIndex].handlingUnits.splice(hIndex,1,handlingUnit)
  }  

  svc.deleteHandlingUnit=function(dIndex,hIndex){
    svc.deliveryOrders[dIndex].handlingUnits.splice(hIndex,1)
    if(svc.deliveryOrders[dIndex].shipmentObject){
      svc.deliveryOrders[dIndex].shipmentObject.handlingUnits.splice(hIndex,1)
      if(svc.deliveryOrders[dIndex].shipmentObject.handlingUnits.length == 0){
        svc.deleteDeliveryOrder(dIndex);
        $state.go('home.orders.fulfillment');
      }
    }
  }

  svc.updateItem=function(dIndex,hIndex,index,item){
    svc.deliveryOrders[dIndex].shipmentObject.handlingUnits[hIndex].items.splice(index,1,item);
    svc.deliveryOrders[dIndex].handlingUnits[hIndex].items.splice(index,1,item);
  }

  svc.removeItem=function(dIndex,hIndex,index){
    // This is for svc.deliveryOrders[dIndex].shipmentObject
    svc.deliveryOrders[dIndex].shipmentObject.handlingUnits[hIndex].items.splice(index,1);
    svc.deliveryOrders[dIndex].handlingUnits[hIndex].items.splice(index,1);
    //TODO: remove $$selected, also if split item is removed, need to check if others are present and combine count
    //remove item from selectedSearchResults
    if(svc.deliveryOrders[dIndex].shipmentObject.handlingUnits[hIndex].items[index]){
      //var lineItemBreakdownNum = svc.deliveryOrders[dIndex].shipmentObject.handlingUnits[hIndex].items[index].lineItemBreakdownNum.split("_");
      var tempIndex = svc.selectedSearchOrders.indexOf(svc.deliveryOrders[dIndex].shipmentObject.handlingUnits[hIndex].items[index].lineItemBreakdownNum);
      svc.deliveryOrders[dIndex].handlingUnits[hIndex].items[index].$$selected = false;
      if(tempIndex >= 0)
        svc.selectedSearchOrders.splice(tempIndex, 1);

      var sIndex = svc.selectedSearchOrders.indexOf(svc.deliveryOrders[dIndex].shipmentObject.handlingUnits[hIndex].items[index].lineItemBreakdownNum);
      svc.deliveryOrders[dIndex].shipmentObject.handlingUnits[hIndex].items[index].$$selected = false;      
      if(sIndex >= 0)
        svc.selectedSearchOrders.splice(sIndex, 1);
    } 

    if(svc.deliveryOrders[dIndex].shipmentObject.handlingUnits.length == 1 && svc.deliveryOrders[dIndex].shipmentObject.handlingUnits[0].items.length == 0){
      svc.deleteHandlingUnit(dIndex, 0);
      //svc.deleteDeliveryOrder(dIndex);
     }
     else{
       angular.forEach(svc.deliveryOrders[dIndex].shipmentObject.handlingUnits, function(hu, index) {
         if(hu.items.length <= 0){
            svc.deleteHandlingUnit(dIndex, index);
          }
       });
     }
  }

  svc.splitItem=function(item){
    var splitItem=angular.copy(item);
    svc.selectedItems.splice(svc.selectedItems.indexOf(item)+1,0,splitItem)
  }

  svc.splitItemToOwn=function(dIndex,hIndex){
    //svc.deliveryOrders[dIndex].shipmentObject
    angular.forEach(svc.deliveryOrders[dIndex].shipmentObject.handlingUnits[hIndex].items, function(item, index){
      for (var i=0;i<svc.deliveryOrders[dIndex].shipmentObject.handlingUnits[hIndex].items[index].quantity;i++){
        var newIndex=svc.addHandlingUnit(dIndex,{
          name: svc.deliveryOrders[dIndex].shipmentObject.handlingUnits[hIndex].items[index].description + " ("+(i+1)+")",
        }) ;
        var item=angular.copy(svc.deliveryOrders[dIndex].shipmentObject.handlingUnits[hIndex].items[index]);
        item.quantity=1;
        svc.deliveryOrders[dIndex].shipmentObject.handlingUnits[newIndex-1].items.push(item);
        if(svc.deliveryOrders[dIndex].shipmentObject.handlingUnits[hIndex].items[index].serArray){
          svc.deliveryOrders[dIndex].shipmentObject.handlingUnits[newIndex-1].items[0]["serArray"] = [];
          svc.deliveryOrders[dIndex].shipmentObject.handlingUnits[newIndex-1].items[0]["serArray"].push(svc.deliveryOrders[dIndex].shipmentObject.handlingUnits[hIndex].items[index].serArray[i]);
        }
      }
    });  
    svc.deliveryOrders[dIndex].shipmentObject.handlingUnits.splice(hIndex, 1);
  }

  svc.clearPlanned=function(dIndex){
    angular.forEach(svc.deliveryOrders[dIndex].shipmentObject.handlingUnits,function(hUnit){
        angular.forEach(hUnit.items,function(item){
          delete item.quantity;
      })
    });

    angular.forEach(svc.deliveryOrders[dIndex].handlingUnits,function(hUnit){
        angular.forEach(hUnit.items,function(item){
          delete item.quantity;
      })
    });

  }

  svc.addItem=function(dIndex,hIndex,item){
   if (!containsFilter(svc.deliveryOrders[dIndex].handlingUnits[hIndex].items,item)){
      var itemClone=angular.copy(item);
      itemClone.quantity=itemClone.quantity;
      svc.deliveryOrders[dIndex].handlingUnits[hIndex].items.push(itemClone);
    }
    else{
     //already added
   }
 }

svc.selectDelivery=function(uuid){
  var match=false;
  svc.deliveryOrders.filter(function(order,index) {
    if(order.uuid === uuid){
      svc.dIndex=index;
      match=true;
      return;
    };
  });
  if (!match){
    $state.go("home.orders.fulfillment.search");
  }
}

 svc.selectItem=function(item){

    var dIndex=-1;

    var addrObj = item.supplyOrder.addresses;
    var originObj = {};
    var destObj = {};
    var references = item.references;
    var key = "";

    angular.forEach(addrObj, function(addr) {
      if(addr.orderAddressTypeCd == "19"){
        originObj.id = addr.id.orderAddressTypeCd;
        originObj.name = addr.addrName1,
        originObj.address = addr.addr1,
        originObj.address2 = addr.addr2,
        originObj.city = addr.city,
        originObj.sau = addr.state,
        originObj.postalCode = addr.postalCode,
        originObj.country = addr.country,
        originObj.entityId = addr.entityId
      }

      if(addr.orderAddressTypeCd == "20"){
        destObj.id = addr.id.orderAddressTypeCd;
        destObj.name = addr.addrName1,
        destObj.address = addr.addr1,
        destObj.address2 = addr.addr2,
        destObj.city = addr.city,
        destObj.sau = addr.state,
        destObj.postalCode = addr.postalCode,
        destObj.country = addr.country,
        destObj.entityId = addr.entityId
      }
    });

    if(codeListService.codeLists && codeListService.codeLists.CustomerConfigList && codeListService.codeLists.CustomerConfigList[0].OF_UI_CONSOLIDATION_STRATEGY_EQUAL_PROP_LIST){
      var reqCondition = [];

      reqCondition = codeListService.codeLists.CustomerConfigList[0].OF_UI_CONSOLIDATION_STRATEGY_EQUAL_PROP_LIST;

      var keyVal = "";

      angular.forEach(reqCondition, function(cond, index){
        var tempVal = jsonPath.eval(item, cond)[0];
        // var tempArray = cond.split(".");
          // if(tempArray.length <= 1) {
          //   tempVal =  item[code];
          // }

          // if(tempArray.length > 1) {
          //   angular.forEach(tempArray, function( prop, ind ) {
          //     //supplyOrder.addresses[addressesTypeId='20'].addr1
          //     //firstIndex = supplyOrder, lastIndex = addr1, tempArray[1] will have "addresses[addressesTypeId='20']"

              

          //     if(tempArray.length == 2){
          //       firstIndex = "";
          //       middleIndex = tempArray[0];
          //       lastIndex = tempArray[1];
          //     }
          //     else{
          //       firstIndex = tempArray[0];
          //        middleIndex = tempArray[1]
          //       lastIndex = tempArray[2];
          //     }

          //     if(tempArray.length == 2 && middleIndex == "supplyOrder"){
          //       tempVal =  item[middleIndex][lastIndex];
          //     }
          //     else{
          //       //Below code will split the values and get the array values as a object
          //       var matched = middleIndex.match( /\[(.*)\]/ );
          //       var propname = middleIndex.split( '[' )[ 0 ];
          //       var matchedArr = matched[ 1 ].replace( /'/g, '' )
          //             .split( "=" );
          //         var returnObj = {
          //             matched: true,
          //             propname: propname
          //         };
          //         returnObj.matchValues = {};
          //         returnObj.matchValues[ matchedArr[ 0 ] ] = matchedArr[ 1 ]
                  
          //         if(tempArray.length == 2){
          //           var addrObj = item[returnObj.propname];
          //         }
          //         else{
          //           var addrObj = item[firstIndex][returnObj.propname]; 
          //         }

          //         if(!addrObj){
          //           tempVal = "";
          //         }
          //         else{
          //           angular.forEach(addrObj, function(addr, i) {
          //             var val1 = addr[matchedArr[0]];
          //             var val2 = matchedArr[ 1 ];

          //             if(val1 == val2){
          //               tempVal = addr[lastIndex]; 
          //             }
          //           });
          //         }
          //       }
          //   });
          // }
          if(index == 0){
            keyVal = tempVal || "";
          }
          else{
            keyVal = keyVal+"_"+ (tempVal || "");
          }          
      });
      key = keyVal;
    }

    var destination = destObj.entityId;
    //find delivery with the same destination
    svc.deliveryOrders.filter(function(order,index) {
     if(codeListService.codeLists && codeListService.codeLists.CustomerConfigList && codeListService.codeLists.CustomerConfigList[0].OF_UI_CONSOLIDATION_STRATEGY == "EQUAL_DESTINATION"){
      if(order.destination.entityId === destination || order.destination.entityId.name === destination){
        dIndex=index;
        return;
      };
     }
     else if(codeListService.codeLists && codeListService.codeLists.CustomerConfigList && codeListService.codeLists.CustomerConfigList[0].OF_UI_CONSOLIDATION_STRATEGY_EQUAL_PROP_LIST){
      if(order.key === keyVal){
        dIndex=index;
        return;
      };
     }
     else{
      if(order.destination.entityId === destination){
        dIndex=index;
        return;
      };
     }
      
    });


  if (item.$$selected) {
    if( (dIndex <=0) && svc.deliveryOrders[dIndex] && svc.deliveryOrders[dIndex].visitedFlag){
      uiModalSvc.show('ok-cancel-modal.tpl',{
        title:"Remove from Order",
        close: 'true',
        icon: "fa fa-exclamation-triangle",
        text:"Any modifications done to this Shipment would be removed, are you sure you want to remove this order item from '"+svc.deliveryOrders[dIndex].name+"'?",
        callback:function(){

          angular.forEach(svc.selectedSearchOrders, function(order, index) {
             var orderComponentNum = item.orderNumber+"_"+item.lineItemNum;
            if(svc.selectedSearchOrders[index] == orderComponentNum){
              svc.selectedSearchOrders.splice(index, 1);
            }
          });
         
        for(var i=0; i< svc.deliveryOrders.length; i++){
          svc.deliveryOrders[i].references = item.references;
          for(var j=0; j<svc.deliveryOrders[i].handlingUnits.length; j++){
            for(var k=0; k<svc.deliveryOrders[i].handlingUnits[j].items.length; k++){
              var itemNumber = item.orderNumber+"_"+item.lineItemNum

              if(svc.deliveryOrders[i].shipmentObject && svc.deliveryOrders[i].shipmentObject.handlingUnits[j].items[k].lineItemBreakdownNum){
                if(svc.deliveryOrders[i].shipmentObject.handlingUnits[j].items[k].lineItemBreakdownNum == itemNumber){
                  svc.deliveryOrders[i].shipmentObject.handlingUnits[j].items.splice(k, 1);
                  svc.deliveryOrders[i].handlingUnits[j].items.splice(k, 1);
                  break;                  
                }
              }
              else{
                 if(svc.deliveryOrders[i].handlingUnits[j].items[k].orderNumber == item.orderNumber){
                  svc.deliveryOrders[i].handlingUnits[j].items.splice(k, 1);        
                  break;          
                }
              }       
              
            }
          }
        }
        
        if(dIndex > -1){
          if(svc.deliveryOrders[dIndex].handlingUnits.length == 1 && svc.deliveryOrders[dIndex].handlingUnits[0].items.length == 0){
            svc.deleteHandlingUnit(dIndex, 0);
            svc.deleteDeliveryOrder(dIndex);
           }
           else{
             angular.forEach(svc.deliveryOrders[dIndex].handlingUnits, function(hu, index) {
               if(hu.items.length <= 0){
                  svc.deleteHandlingUnit(dIndex, index);
                }
             });
           }


            item.$$selected=false;
            if(svc.deliveryOrders[dIndex]){
              svc.deliveryOrders[dIndex].visitedFlag = false;
            }
          }
        }
      });
    }
    else{
      angular.forEach(svc.selectedSearchOrders, function(order, index) {
         var orderComponentNum = item.orderNumber+"_"+item.lineItemNum;
        if(svc.selectedSearchOrders[index] == orderComponentNum){
          svc.selectedSearchOrders.splice(index, 1);
        }
      });
     
      for(var i=0; i< svc.deliveryOrders.length; i++){
        svc.deliveryOrders[i].references = item.references;
        for(var j=0; j<svc.deliveryOrders[i].handlingUnits.length; j++){
          for(var k=0; k<svc.deliveryOrders[i].handlingUnits[j].items.length; k++){
            var itemNumber = item.orderNumber+"_"+item.lineItemNum

            if(svc.deliveryOrders[i].shipmentObject && svc.deliveryOrders[i].shipmentObject.handlingUnits[j].items[k].lineItemBreakdownNum){
              if(svc.deliveryOrders[i].shipmentObject.handlingUnits[j].items[k].lineItemBreakdownNum == itemNumber){
                svc.deliveryOrders[i].shipmentObject.handlingUnits[j].items.splice(k, 1); 
                svc.deliveryOrders[i].handlingUnits[j].items.splice(k, 1); 
                break;                 
              }
            }
            else{
               if(svc.deliveryOrders[i].handlingUnits[j].items[k].orderNumber == item.orderNumber){
                svc.deliveryOrders[i].handlingUnits[j].items.splice(k, 1);   
                break;               
              }
            }       
          }
        }
      }

     if(svc.deliveryOrders[dIndex].handlingUnits.length == 1 && svc.deliveryOrders[dIndex].handlingUnits[0].items.length == 0){
      svc.deleteHandlingUnit(dIndex, 0);
      svc.deleteDeliveryOrder(dIndex);
     }
     else{
       angular.forEach(svc.deliveryOrders[dIndex].handlingUnits, function(hu, index) {
         if(hu.items.length <= 0){
            svc.deleteHandlingUnit(dIndex, index);
          }
       });
     }
     
      item.$$selected=false;
    }

    svc.selectAllFlag = false;
  }
  else{
    // var destination=item.lines[0].locations.destination.name;    

    //if doesn't exist, create one
    if (dIndex<0){

      svc.doInc++;

      svc.createDeliveryOrder(new svc.DeliveryOrder({
        name:"Shipment "+(svc.doInc),
        sector:"",
        visitedFlag: false,
        origin:originObj,
        destination:destObj,
        references: references,
        key: key
      }))
      dIndex=svc.deliveryOrders.length-1;

      //check if delivery has handling unit; if not, create one
      if (svc.deliveryOrders[dIndex].handlingUnits.length==0){
        svc.addHandlingUnit(dIndex,new svc.HandlingUnit({
          name:'Handling Unit ' + (svc.deliveryOrders[dIndex].handlingUnits.length+1)
        }))
      }
      
      //if only one handling unit exists, add to it
      if (svc.deliveryOrders[dIndex].handlingUnits.length >= 1){
        svc.addItem(dIndex,0,item);
      }
      else{//prompt which handling unit to add it to

      }

      svc.deliveryOrders[dIndex].visitedFlag = false;
      var orderComponentNum = item.orderNumber+"_"+item.lineItemNum;
      svc.selectedSearchOrders.push(orderComponentNum);
      item.$$selected=true;

    }
    else if(svc.deliveryOrders[dIndex] && svc.deliveryOrders[dIndex].visitedFlag){
      uiModalSvc.show('ok-cancel-modal.tpl',{
        title:"Add to Order",
        close: 'true',
        icon: "fa fa-exclamation-triangle",
        text:"Any modifications done to this Shipment would be removed, are you sure you want to add this order item to '"+svc.deliveryOrders[dIndex].name+"'?",
        callback:function(){

          //check if delivery has handling unit; if not, create one
          if (svc.deliveryOrders[dIndex].handlingUnits.length==0){
            svc.addHandlingUnit(dIndex,new svc.HandlingUnit({
              name:'Handling Unit ' + (svc.deliveryOrders[dIndex].handlingUnits.length+1)
            }))
          }
          
          //if only one handling unit exists, add to it
          if (svc.deliveryOrders[dIndex].handlingUnits.length >= 1){
            svc.addItem(dIndex,0,item);
          }
          else{//prompt which handling unit to add it to

          }

          svc.deliveryOrders[dIndex].visitedFlag = false;
          var orderComponentNum = item.orderNumber+"_"+item.lineItemNum;
          svc.selectedSearchOrders.push(orderComponentNum);
          item.$$selected=true;

        }
      });
    }
    else{
      //check if delivery has handling unit; if not, create one
      if (svc.deliveryOrders[dIndex].handlingUnits.length==0){
        svc.addHandlingUnit(dIndex,new svc.HandlingUnit({
          name:'Handling Unit ' + (svc.deliveryOrders[dIndex].handlingUnits.length+1)
        }))
      }
      
      //if only one handling unit exists, add to it
      if (svc.deliveryOrders[dIndex].handlingUnits.length >= 1){
        svc.addItem(dIndex,0,item);
      }
      else{//prompt which handling unit to add it to

      }

      svc.deliveryOrders[dIndex].visitedFlag = false;
       var orderComponentNum = item.orderNumber+"_"+item.lineItemNum;
      svc.selectedSearchOrders.push(orderComponentNum);
      item.$$selected=true;
    }
  }
}

  svc.selectedDelivery=function(){;
    return svc.deliveryOrders[svc.dIndex];
  }

  svc.selectedShipmentUpdated=function(){;
     var delivery = svc.selectedDelivery();
     if (!delivery) {
       return null;
     }
     delivery.shipmentObject.originName = delivery.origin.name.name;
     delivery.shipmentObject.originAddr = delivery.origin.address;
     delivery.shipmentObject.originAddr2 = delivery.origin.address2;
     delivery.shipmentObject.originCity = delivery.origin.city;
     delivery.shipmentObject.originState = delivery.origin.sau.name;
     delivery.shipmentObject.originPostalCode = delivery.origin.postalCode;
     delivery.shipmentObject.originCountry = delivery.origin.country.name;
     delivery.shipmentObject.originAddressId = delivery.origin.entityId.name;
     delivery.shipmentObject.destName = delivery.destination.name.name;
     delivery.shipmentObject.destAddr = delivery.destination.address;
     delivery.shipmentObject.destAddr2 = delivery.destination.address2;
     delivery.shipmentObject.destCity = delivery.destination.city;
     delivery.shipmentObject.destState = delivery.destination.sau.name;
     delivery.shipmentObject.destPostalCode = delivery.destination.postalCode;
     delivery.shipmentObject.destCountry = delivery.destination.country.name;
     delivery.shipmentObject.destAddressId = delivery.destination.entityId.name;
     return delivery.shipmentObject;
  }

  svc.moveItem=function(dIndex,hIndex,index,targetHandlingUnit){
    svc.deliveryOrders[dIndex].$$lastTargetHandlingUnit=targetHandlingUnit;
    targetHandlingUnit.items.push(svc.deliveryOrders[dIndex].shipmentObject.handlingUnits[hIndex].items[index]);
    svc.deliveryOrders[dIndex].shipmentObject.handlingUnits[hIndex].items.splice(index,1);

    // targetHandlingUnit.items.push(svc.deliveryOrders[dIndex].handlingUnits[hIndex].items[index]);
    svc.deliveryOrders[dIndex].handlingUnits[hIndex].items.splice(index,1);
  }


  svc.splitItem=function(dIndex,hIndex,index,targetHandlingUnit,targetQuantity){
    svc.deliveryOrders[dIndex].$$lastTargetHandlingUnit=targetHandlingUnit;
    var newItem=angular.copy(svc.deliveryOrders[dIndex].shipmentObject.handlingUnits[hIndex].items[index]);

    newItem.quantity=targetQuantity;
    targetHandlingUnit.items.push(newItem);
    targetHandlingUnit.items[0]["serArray"] = [];
    

    if(svc.deliveryOrders[dIndex].shipmentObject.handlingUnits[hIndex].items[index].serArray){
      //Pushing serialization to the new HU
      angular.forEach(svc.deliveryOrders[dIndex].shipmentObject.handlingUnits[hIndex].items[index].serArray, function(ser, s){
        if(s < targetQuantity){
          targetHandlingUnit.items[0]["serArray"].push(ser);
        }
      });
      //Removing serialization from the old HU
      for(var j=0;j<targetQuantity; j++){
       svc.deliveryOrders[dIndex].shipmentObject.handlingUnits[hIndex].items[index].serArray.splice(0, 1); 
      }
    }
    //Resetting the quantity after split
    svc.deliveryOrders[dIndex].shipmentObject.handlingUnits[hIndex].items[index].quantity = svc.deliveryOrders[dIndex].shipmentObject.handlingUnits[hIndex].items[index].quantity - targetQuantity;
    // if (!svc.deliveryOrders[dIndex].shipmentObject.handlingUnits[hIndex].items[index].quantity) {
    //  svc.deliveryOrders[dIndex].shipmentObject.handlingUnits[hIndex].items[index].quantity=100;
    // }
    // svc.deliveryOrders[dIndex].shipmentObject.handlingUnits[hIndex].items[index].quantity-=targetQuantity;

  }


/*  svc.HandlingUnit.prototype.addItem=function(item){
    this.items.push(item);
  }

  svc.HandlingUnit.prototype.deleteItem=function(index){
    this.items.splice(index,1)
  }*/

  return svc;
}]);



































