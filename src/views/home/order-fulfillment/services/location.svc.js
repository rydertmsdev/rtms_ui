App.service('LocationService',
  /**
   * Create Location Service
   */
  function LocationService($rootScope, $http, $filter, $q) {

    this.createLocation = function (module, data, type) {
      
      var deferred = $q.defer();

      if(type == "update"){
        $http.post("/ryderonline/rtms/service/"+module+"/update", data)
        .then( function(response) {
              deferred.resolve(response);
          }, function(errResp) {
               deferred.reject(errResp);
        });
      }
      else{
        $http.post("/ryderonline/rtms/service/"+module+"/create", data)
        .then( function(response) {
              deferred.resolve(response);
          }, function(errResp) {
               deferred.reject(errResp);
        });
      }

      
      return deferred.promise;
    }

    this.deleteLocation = function (module, data) {
      
      var deferred = $q.defer();

      $http.post("/ryderonline/rtms/service/"+module+"/delete", data)
      .then( function(response) {
            deferred.resolve(response);
        }, function(errResp) {
             deferred.reject(errResp);
      });
      
      return deferred.promise;
    }

        /* Get the search result  */

    this.getResults = function (module, data, pageNum, pageSize) {
      
      var deferred = $q.defer();

      $http.post("/ryderonline/rtms/service/"+module+"/search?pageSize="+pageSize+"&pageNum="+pageNum, data)
      .then( function(response) {
               deferred.resolve(response);
          }, function(errResp) {
               deferred.reject(errResp);
          });
      return deferred.promise;
    }

});

App.service('locationSvc', [
  '$rootScope',
  '$window', 
  '$q', 
  '$timeout', 
  '$filter', 
  '$state',
  function ($rootScope, $window, $q, $timeout, $filter, $state) {
    var svc=this;

    svc.module = "location";
    svc.locationUpdateDetails = {};
    svc.contactsIndex = [0];
    svc.locationContacts = [];
    svc.searchResults = [];
    svc.index = 1;
    svc.pageTotal = "";
    svc.recordsCount = "";
    svc.pageSize = 50;
    svc.rowsPerbatch = 50;

  return svc;
}]);