App.service('testData', ['$q','$timeout',function ($q,$timeout) {
    var svc=this;



    svc.states=[
    "AL - Alabama",
    "AK - Alaska",
    "AS - American Samoa",
    "AZ - Arizona",
    "AR - Arkansas",
    "CA - California",
    "CO - Colorado",
    "CT - Connecticut",
    "DE - Delaware",
    "DC - District Of Columbia",
    "FM - Federated States Of Micronesia",
    "FL - Florida",
    "GA - Georgia",
    "GU - Guam",
    "HI - Hawaii",
    "ID - Idaho",
    "IL - Illinois",
    "IN - Indiana",
    "IA - Iowa",
    "KS - Kansas",
    "KY - Kentucky",
    "LA - Louisiana",
    "ME - Maine",
    "MH - Marshall Islands",
    "MD - Maryland",
    "MA - Massachusetts",
    "MI - Michigan",
    "MN - Minnesota",
    "MS - Mississippi",
    "MO - Missouri",
    "MT - Montana",
    "NE - Nebraska",
    "NV - Nevada",
    "NH - New Hampshire",
    "NJ - New Jersey",
    "NM - New Mexico",
    "NY - New York",
    "NC - North Carolina",
    "ND - North Dakota",
    "MP - Northern Mariana Islands",
    "OH - Ohio",
    "OK - Oklahoma",
    "OR - Oregon",
    "PW - Palau",
    "PA - Pennsylvania",
    "PR - Puerto Rico",
    "RI - Rhode Island",
    "SC - South Carolina",
    "SD - South Dakota",
    "TN - Tennessee",
    "TX - Texas",
    "UT - Utah",
    "VT - Vermont",
    "VI - Virgin Islands",
    "VA - Virginia",
    "WA - Washington",
    "WV - West Virginia",
    "WI - Wisconsin",
    "WY - Wyoming"
    ]

    svc.purchaseOrders=[
{
    orderNumber: "2891313", 
    orderDate: "42690", 
    orderType: "STND", 
    quantity: 298, sector:"TS - Technology Services", 
    psa:true, 
    incoTerms: "FOB Destination", 
    supplierId: "049437", 
    supplierName: "CENTURY PRECISION ENGINEERING INC", 
    lines: [
        {
            lineNumber:"1", 
            itemNumber:"2CHH51216-0005", 
            itemDescription:"SHIM UPLOCK RLR  DC", 
            fulfillmentMethod:"EA", 
            fulfillmentTolerance:"0", 
            status:"FALSE", 
            deliveryScheduleNumber:"1", 
            executeFrom:"07/10/2017", 
            shipQuantity:"18", 
            locations: {
                origin: {
                    id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"
                }, 
                destination: {
                    id:"30SEG", name:"ST F35 DOCK", address:"4020 REDONDO BEACH AVE", city:"REDONDO BEACH", sau:"CA", postalCode:"90278", country:"US"
                }
            }, 
            references: [
                {qualifier: "P4", value: "JSFXX"}, 
                {qualifier: "87", value: "AS"}, 
                {qualifier: "SLI", value: "30"}, 
                {qualifier: "PE", value: "SEG"}, 
                {qualifier: "12", value: "1"}, 
                {qualifier: "KD", value: "N"}, 
                {qualifier: "PKG", value: "N"}, 
                {qualifier: "KL", value: "D"}, 
                {qualifier: "CT", value: "SEE CHARGE TEXT"}, 
                {qualifier: "CCNM", value: ""}, 
                {qualifier: "72", value: "2C"}, 
                {qualifier: "IL", value: "HH51216-0005"}, 
                {qualifier: "RSFD", value: "42956"}, 
                {qualifier: "QTY", value: "18"}
            ]
        }
    ]
},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 438, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"1", itemNumber:"2CHH51216-0005", itemDescription:"SHIM UPLOCK RLR  DC", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"2", executeFrom:"04/26/2017", shipQuantity:"14", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30SEG", name:"ST F35 DOCK", address:"4020 REDONDO BEACH AVE", city:"REDONDO BEACH", sau:"CA", postalCode:"90278", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "SEE CHARGE TEXT"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH51216-0005"}, {qualifier: "RSFD", value: "42881"}, {qualifier: "QTY", value: "14"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 97, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"1", itemNumber:"2CHH51216-0005", itemDescription:"SHIM UPLOCK RLR  DC", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"3", executeFrom:"02/06/2017", shipQuantity:"16", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30SEG", name:"ST F35 DOCK", address:"4020 REDONDO BEACH AVE", city:"REDONDO BEACH", sau:"CA", postalCode:"90278", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "SEE CHARGE TEXT"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH51216-0005"}, {qualifier: "RSFD", value: "42802"}, {qualifier: "QTY", value: "16"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 321, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"1", itemNumber:"2CHH51216-0005", itemDescription:"SHIM UPLOCK RLR  DC", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"4", executeFrom:"12/04/2016", shipQuantity:"20", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30SEG", name:"ST F35 DOCK", address:"4020 REDONDO BEACH AVE", city:"REDONDO BEACH", sau:"CA", postalCode:"90278", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "SEE CHARGE TEXT"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH51216-0005"}, {qualifier: "RSFD", value: "42738"}, {qualifier: "QTY", value: "20"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 364, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"1", itemNumber:"2CHH51216-0005", itemDescription:"SHIM UPLOCK RLR  DC", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"5", executeFrom:"01/05/2016", shipQuantity:"0", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30SEG", name:"ST F35 DOCK", address:"4020 REDONDO BEACH AVE", city:"REDONDO BEACH", sau:"CA", postalCode:"90278", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "SEE CHARGE TEXT"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH51216-0005"}, {qualifier: "RSFD", value: "42404"}, {qualifier: "QTY", value: "0"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 274, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"11", itemNumber:"2CHH55308-0003", itemDescription:"BRACKET, MECHANISM - GPD", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"6", executeFrom:"05/23/2017", shipQuantity:"2", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55308-0003"}, {qualifier: "RSFD", value: "42908"}, {qualifier: "QTY", value: "2"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 199, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"11", itemNumber:"2CHH55308-0003", itemDescription:"BRACKET, MECHANISM - GPD", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"7", executeFrom:"04/22/2017", shipQuantity:"5", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55308-0003"}, {qualifier: "RSFD", value: "42877"}, {qualifier: "QTY", value: "5"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 388, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"11", itemNumber:"2CHH55308-0003", itemDescription:"BRACKET, MECHANISM - GPD", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"8", executeFrom:"04/05/2017", shipQuantity:"3", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55308-0003"}, {qualifier: "RSFD", value: "42860"}, {qualifier: "QTY", value: "3"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 103, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"11", itemNumber:"2CHH55308-0003", itemDescription:"BRACKET, MECHANISM - GPD", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"9", executeFrom:"02/07/2017", shipQuantity:"4", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55308-0003"}, {qualifier: "RSFD", value: "42803"}, {qualifier: "QTY", value: "4"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 38, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"11", itemNumber:"2CHH55308-0003", itemDescription:"BRACKET, MECHANISM - GPD", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"10", executeFrom:"01/22/2017", shipQuantity:"4", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55308-0003"}, {qualifier: "RSFD", value: "42787"}, {qualifier: "QTY", value: "4"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 348, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"11", itemNumber:"2CHH55308-0003", itemDescription:"BRACKET, MECHANISM - GPD", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"11", executeFrom:"01/10/2017", shipQuantity:"5", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55308-0003"}, {qualifier: "RSFD", value: "42775"}, {qualifier: "QTY", value: "5"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 430, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"11", itemNumber:"2CHH55308-0003", itemDescription:"BRACKET, MECHANISM - GPD", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"12", executeFrom:"12/27/2016", shipQuantity:"3", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55308-0003"}, {qualifier: "RSFD", value: "42761"}, {qualifier: "QTY", value: "3"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 77, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"11", itemNumber:"2CHH55308-0003", itemDescription:"BRACKET, MECHANISM - GPD", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"13", executeFrom:"11/01/2016", shipQuantity:"0", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55308-0003"}, {qualifier: "RSFD", value: "42705"}, {qualifier: "QTY", value: "0"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 287, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"11", itemNumber:"2CHH55308-0003", itemDescription:"BRACKET, MECHANISM - GPD", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"14", executeFrom:"10/23/2016", shipQuantity:"0", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55308-0003"}, {qualifier: "RSFD", value: "42696"}, {qualifier: "QTY", value: "0"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 300, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"11", itemNumber:"2CHH55308-0003", itemDescription:"BRACKET, MECHANISM - GPD", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"15", executeFrom:"09/09/2016", shipQuantity:"0", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55308-0003"}, {qualifier: "RSFD", value: "42652"}, {qualifier: "QTY", value: "0"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 51, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"11", itemNumber:"2CHH55308-0003", itemDescription:"BRACKET, MECHANISM - GPD", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"16", executeFrom:"08/29/2016", shipQuantity:"0", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55308-0003"}, {qualifier: "RSFD", value: "42641"}, {qualifier: "QTY", value: "0"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 437, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"11", itemNumber:"2CHH55308-0003", itemDescription:"BRACKET, MECHANISM - GPD", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"17", executeFrom:"08/07/2016", shipQuantity:"0", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55308-0003"}, {qualifier: "RSFD", value: "42619"}, {qualifier: "QTY", value: "0"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 422, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"11", itemNumber:"2CHH55308-0003", itemDescription:"BRACKET, MECHANISM - GPD", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"18", executeFrom:"07/07/2016", shipQuantity:"0", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55308-0003"}, {qualifier: "RSFD", value: "42588"}, {qualifier: "QTY", value: "0"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 176, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"11", itemNumber:"2CHH55308-0003", itemDescription:"BRACKET, MECHANISM - GPD", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"19", executeFrom:"07/07/2016", shipQuantity:"0", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55308-0003"}, {qualifier: "RSFD", value: "42588"}, {qualifier: "QTY", value: "0"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 489, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"11", itemNumber:"2CHH55308-0003", itemDescription:"BRACKET, MECHANISM - GPD", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"20", executeFrom:"06/11/2016", shipQuantity:"0", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55308-0003"}, {qualifier: "RSFD", value: "42562"}, {qualifier: "QTY", value: "0"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 176, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"11", itemNumber:"2CHH55308-0003", itemDescription:"BRACKET, MECHANISM - GPD", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"21", executeFrom:"06/07/2016", shipQuantity:"0", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55308-0003"}, {qualifier: "RSFD", value: "42558"}, {qualifier: "QTY", value: "0"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 381, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"11", itemNumber:"2CHH55308-0003", itemDescription:"BRACKET, MECHANISM - GPD", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"22", executeFrom:"11/17/2015", shipQuantity:"0", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55308-0003"}, {qualifier: "RSFD", value: "42355"}, {qualifier: "QTY", value: "0"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 199, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"11", itemNumber:"2CHH55308-0003", itemDescription:"BRACKET, MECHANISM - GPD", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"23", executeFrom:"08/11/2015", shipQuantity:"0", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55308-0003"}, {qualifier: "RSFD", value: "42257"}, {qualifier: "QTY", value: "0"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 443, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"13", itemNumber:"2CSH00875-0011", itemDescription:"SPLICE, FS 331,  FCNT", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"24", executeFrom:"01/09/2017", shipQuantity:"2", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "6533583810"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "SH00875-0011"}, {qualifier: "RSFD", value: "42774"}, {qualifier: "QTY", value: "2"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 317, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"13", itemNumber:"2CSH00875-0011", itemDescription:"SPLICE, FS 331,  FCNT", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"25", executeFrom:"07/29/2015", shipQuantity:"0", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "6533583810"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "SH00875-0011"}, {qualifier: "RSFD", value: "42244"}, {qualifier: "QTY", value: "0"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 271, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"15", itemNumber:"2CSH00875-0012", itemDescription:"SPLICE, FS 331, LW  FCNT", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"26", executeFrom:"01/09/2017", shipQuantity:"2", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "6533583810"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "SH00875-0012"}, {qualifier: "RSFD", value: "42774"}, {qualifier: "QTY", value: "2"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 361, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"15", itemNumber:"2CSH00875-0012", itemDescription:"SPLICE, FS 331, LW  FCNT", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"27", executeFrom:"07/29/2015", shipQuantity:"0", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "6533583810"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "SH00875-0012"}, {qualifier: "RSFD", value: "42244"}, {qualifier: "QTY", value: "0"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 452, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"17", itemNumber:"2CSH00875-0013", itemDescription:"SPLICE, FS 331, UP  FCNT", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"28", executeFrom:"01/09/2017", shipQuantity:"2", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "SH00875-0013"}, {qualifier: "RSFD", value: "42774"}, {qualifier: "QTY", value: "2"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 58, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"17", itemNumber:"2CSH00875-0013", itemDescription:"SPLICE, FS 331, UP  FCNT", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"29", executeFrom:"07/29/2015", shipQuantity:"0", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "SH00875-0013"}, {qualifier: "RSFD", value: "42244"}, {qualifier: "QTY", value: "0"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 127, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"19", itemNumber:"2CSH00875-0014", itemDescription:"SPLICE, FS 331, UP  FCNT", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"30", executeFrom:"01/09/2017", shipQuantity:"2", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "SH00875-0014"}, {qualifier: "RSFD", value: "42774"}, {qualifier: "QTY", value: "2"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 406, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"19", itemNumber:"2CSH00875-0014", itemDescription:"SPLICE, FS 331, UP  FCNT", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"31", executeFrom:"07/29/2015", shipQuantity:"0", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "SH00875-0014"}, {qualifier: "RSFD", value: "42244"}, {qualifier: "QTY", value: "0"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 351, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"21", itemNumber:"2CHH51216-0005", itemDescription:"SHIM UPLOCK RLR  DC", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"32", executeFrom:"12/04/2016", shipQuantity:"1", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30SEG", name:"ST F35 DOCK", address:"4020 REDONDO BEACH AVE", city:"REDONDO BEACH", sau:"CA", postalCode:"90278", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "6500002312"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH51216-0005"}, {qualifier: "RSFD", value: "42738"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 240, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"21", itemNumber:"2CHH51216-0005", itemDescription:"SHIM UPLOCK RLR  DC", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"33", executeFrom:"12/04/2016", shipQuantity:"1", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30SEG", name:"ST F35 DOCK", address:"4020 REDONDO BEACH AVE", city:"REDONDO BEACH", sau:"CA", postalCode:"90278", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "6500002312"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH51216-0005"}, {qualifier: "RSFD", value: "42738"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 82, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"22", itemNumber:"2CHH51216-0007", itemDescription:"SHIM, UPLOCK RLR  DC", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"34", executeFrom:"12/04/2016", shipQuantity:"1", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30SEG", name:"ST F35 DOCK", address:"4020 REDONDO BEACH AVE", city:"REDONDO BEACH", sau:"CA", postalCode:"90278", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "6500002312"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH51216-0007"}, {qualifier: "RSFD", value: "42738"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 439, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"23", itemNumber:"2CHH51216-0007", itemDescription:"SHIM, UPLOCK RLR  DC", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"35", executeFrom:"12/04/2016", shipQuantity:"1", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30SEG", name:"ST F35 DOCK", address:"4020 REDONDO BEACH AVE", city:"REDONDO BEACH", sau:"CA", postalCode:"90278", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "6500002312"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH51216-0007"}, {qualifier: "RSFD", value: "42738"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 281, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"24", itemNumber:"2CHH55308-0003", itemDescription:"BRACKET, MECHANISM - GPD", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"36", executeFrom:"10/09/2018", shipQuantity:"3", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55308-0003"}, {qualifier: "RSFD", value: "43412"}, {qualifier: "QTY", value: "3"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 165, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"24", itemNumber:"2CHH55308-0003", itemDescription:"BRACKET, MECHANISM - GPD", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"37", executeFrom:"09/05/2018", shipQuantity:"8", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55308-0003"}, {qualifier: "RSFD", value: "43378"}, {qualifier: "QTY", value: "8"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 133, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"24", itemNumber:"2CHH55308-0003", itemDescription:"BRACKET, MECHANISM - GPD", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"38", executeFrom:"08/05/2018", shipQuantity:"6", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55308-0003"}, {qualifier: "RSFD", value: "43347"}, {qualifier: "QTY", value: "6"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 386, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"24", itemNumber:"2CHH55308-0003", itemDescription:"BRACKET, MECHANISM - GPD", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"39", executeFrom:"07/09/2018", shipQuantity:"6", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55308-0003"}, {qualifier: "RSFD", value: "43320"}, {qualifier: "QTY", value: "6"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 231, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"24", itemNumber:"2CHH55308-0003", itemDescription:"BRACKET, MECHANISM - GPD", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"40", executeFrom:"06/06/2018", shipQuantity:"7", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55308-0003"}, {qualifier: "RSFD", value: "43287"}, {qualifier: "QTY", value: "7"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 479, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"24", itemNumber:"2CHH55308-0003", itemDescription:"BRACKET, MECHANISM - GPD", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"41", executeFrom:"05/09/2018", shipQuantity:"6", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55308-0003"}, {qualifier: "RSFD", value: "43259"}, {qualifier: "QTY", value: "6"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 405, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"24", itemNumber:"2CHH55308-0003", itemDescription:"BRACKET, MECHANISM - GPD", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"42", executeFrom:"04/04/2018", shipQuantity:"7", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55308-0003"}, {qualifier: "RSFD", value: "43224"}, {qualifier: "QTY", value: "7"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 210, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"24", itemNumber:"2CHH55308-0003", itemDescription:"BRACKET, MECHANISM - GPD", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"43", executeFrom:"03/04/2018", shipQuantity:"7", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55308-0003"}, {qualifier: "RSFD", value: "43193"}, {qualifier: "QTY", value: "7"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 2, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"24", itemNumber:"2CHH55308-0003", itemDescription:"BRACKET, MECHANISM - GPD", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"44", executeFrom:"02/03/2018", shipQuantity:"7", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55308-0003"}, {qualifier: "RSFD", value: "43164"}, {qualifier: "QTY", value: "7"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 405, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"24", itemNumber:"2CHH55308-0003", itemDescription:"BRACKET, MECHANISM - GPD", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"45", executeFrom:"01/02/2018", shipQuantity:"5", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55308-0003"}, {qualifier: "RSFD", value: "43132"}, {qualifier: "QTY", value: "5"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 197, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"24", itemNumber:"2CHH55308-0003", itemDescription:"BRACKET, MECHANISM - GPD", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"46", executeFrom:"12/05/2017", shipQuantity:"4", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55308-0003"}, {qualifier: "RSFD", value: "43104"}, {qualifier: "QTY", value: "4"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 311, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"24", itemNumber:"2CHH55308-0003", itemDescription:"BRACKET, MECHANISM - GPD", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"47", executeFrom:"10/30/2017", shipQuantity:"6", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55308-0003"}, {qualifier: "RSFD", value: "43068"}, {qualifier: "QTY", value: "6"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 356, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"24", itemNumber:"2CHH55308-0003", itemDescription:"BRACKET, MECHANISM - GPD", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"48", executeFrom:"10/03/2017", shipQuantity:"4", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55308-0003"}, {qualifier: "RSFD", value: "43041"}, {qualifier: "QTY", value: "4"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 76, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"24", itemNumber:"2CHH55308-0003", itemDescription:"BRACKET, MECHANISM - GPD", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"49", executeFrom:"09/06/2017", shipQuantity:"5", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55308-0003"}, {qualifier: "RSFD", value: "43014"}, {qualifier: "QTY", value: "5"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 303, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"24", itemNumber:"2CHH55308-0003", itemDescription:"BRACKET, MECHANISM - GPD", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"50", executeFrom:"07/05/2017", shipQuantity:"7", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55308-0003"}, {qualifier: "RSFD", value: "42951"}, {qualifier: "QTY", value: "7"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 467, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"24", itemNumber:"2CHH55308-0003", itemDescription:"BRACKET, MECHANISM - GPD", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"51", executeFrom:"06/18/2017", shipQuantity:"3", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55308-0003"}, {qualifier: "RSFD", value: "42934"}, {qualifier: "QTY", value: "3"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 453, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"24", itemNumber:"2CHH55308-0003", itemDescription:"BRACKET, MECHANISM - GPD", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"52", executeFrom:"05/24/2017", shipQuantity:"6", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55308-0003"}, {qualifier: "RSFD", value: "42909"}, {qualifier: "QTY", value: "6"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 158, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"25", itemNumber:"2CHH55302-3001", itemDescription:"BELLCRANK ASSY", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"53", executeFrom:"09/01/2018", shipQuantity:"6", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55302-3001"}, {qualifier: "RSFD", value: "43374"}, {qualifier: "QTY", value: "6"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 469, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"25", itemNumber:"2CHH55302-3001", itemDescription:"BELLCRANK ASSY", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"54", executeFrom:"08/28/2018", shipQuantity:"6", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55302-3001"}, {qualifier: "RSFD", value: "43370"}, {qualifier: "QTY", value: "6"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 36, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"25", itemNumber:"2CHH55302-3001", itemDescription:"BELLCRANK ASSY", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"55", executeFrom:"08/06/2018", shipQuantity:"5", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55302-3001"}, {qualifier: "RSFD", value: "43348"}, {qualifier: "QTY", value: "5"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 220, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"25", itemNumber:"2CHH55302-3001", itemDescription:"BELLCRANK ASSY", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"56", executeFrom:"06/13/2018", shipQuantity:"8", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55302-3001"}, {qualifier: "RSFD", value: "43294"}, {qualifier: "QTY", value: "8"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 491, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"25", itemNumber:"2CHH55302-3001", itemDescription:"BELLCRANK ASSY", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"57", executeFrom:"05/28/2018", shipQuantity:"5", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55302-3001"}, {qualifier: "RSFD", value: "43278"}, {qualifier: "QTY", value: "5"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 184, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"25", itemNumber:"2CHH55302-3001", itemDescription:"BELLCRANK ASSY", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"58", executeFrom:"05/02/2018", shipQuantity:"7", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55302-3001"}, {qualifier: "RSFD", value: "43252"}, {qualifier: "QTY", value: "7"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 462, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"25", itemNumber:"2CHH55302-3001", itemDescription:"BELLCRANK ASSY", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"59", executeFrom:"04/11/2018", shipQuantity:"6", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55302-3001"}, {qualifier: "RSFD", value: "43231"}, {qualifier: "QTY", value: "6"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 410, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"25", itemNumber:"2CHH55302-3001", itemDescription:"BELLCRANK ASSY", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"60", executeFrom:"03/07/2018", shipQuantity:"7", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55302-3001"}, {qualifier: "RSFD", value: "43196"}, {qualifier: "QTY", value: "7"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 442, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"25", itemNumber:"2CHH55302-3001", itemDescription:"BELLCRANK ASSY", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"61", executeFrom:"01/17/2018", shipQuantity:"7", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55302-3001"}, {qualifier: "RSFD", value: "43147"}, {qualifier: "QTY", value: "7"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 393, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"25", itemNumber:"2CHH55302-3001", itemDescription:"BELLCRANK ASSY", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"62", executeFrom:"12/06/2017", shipQuantity:"6", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55302-3001"}, {qualifier: "RSFD", value: "43105"}, {qualifier: "QTY", value: "6"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 273, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"25", itemNumber:"2CHH55302-3001", itemDescription:"BELLCRANK ASSY", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"63", executeFrom:"11/22/2017", shipQuantity:"6", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55302-3001"}, {qualifier: "RSFD", value: "43091"}, {qualifier: "QTY", value: "6"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 467, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"25", itemNumber:"2CHH55302-3001", itemDescription:"BELLCRANK ASSY", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"64", executeFrom:"10/23/2017", shipQuantity:"6", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55302-3001"}, {qualifier: "RSFD", value: "43061"}, {qualifier: "QTY", value: "6"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 156, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"25", itemNumber:"2CHH55302-3001", itemDescription:"BELLCRANK ASSY", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"65", executeFrom:"08/02/2017", shipQuantity:"5", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55302-3001"}, {qualifier: "RSFD", value: "42979"}, {qualifier: "QTY", value: "5"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 54, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"25", itemNumber:"2CHH55302-3001", itemDescription:"BELLCRANK ASSY", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"66", executeFrom:"07/05/2017", shipQuantity:"5", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55302-3001"}, {qualifier: "RSFD", value: "42951"}, {qualifier: "QTY", value: "5"}]}]},
{orderNumber: "2891313", orderDate: "42690", orderType: "STND", quantity: 199, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "049437", supplierName: "CENTURY PRECISION ENGINEERING INC", lines: [{lineNumber:"25", itemNumber:"2CHH55302-3001", itemDescription:"BELLCRANK ASSY", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"67", executeFrom:"06/06/2017", shipQuantity:"12", locations: {origin: {id:"049437", name:"CENTURY PRECISION ENGINEERING INC", address:"2141 W 139TH ST", city:"GARDENA", sau:"CA", postalCode:"90249-2451", country:"US"}, destination: {id:"30AVM", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 EAST AVENUE  M  - SITE 4, GATE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-15-C-0003"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "2C"}, {qualifier: "IL", value: "HH55302-3001"}, {qualifier: "RSFD", value: "42922"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2897578", orderDate: "42690", orderType: "STND", quantity: 356, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "040416", supplierName: "SARGENT CONTROLS AND AEROSPACE", lines: [{lineNumber:"1", itemNumber:"KMDB6-18", itemDescription:"BEARING,PLAIN,SELF-ALIGN", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"68", executeFrom:"12/13/2015", shipQuantity:"0", locations: {origin: {id:"040416", name:"SARGENT CONTROLS AND AEROSPACE", address:"5675 W BURLINGAME RD", city:"TUCSON", sau:"AZ", postalCode:"85743-9453", country:"US"}, destination: {id:"30STA", name:"NORTHROP GRUMMAN CORPORATION", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "T38F5"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "STA"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "SEE CHARGE TEXT"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "KM"}, {qualifier: "IL", value: "DB6-18"}, {qualifier: "RSFD", value: "42381"}, {qualifier: "QTY", value: "0"}]}]},
{orderNumber: "2897578", orderDate: "42690", orderType: "STND", quantity: 441, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "040416", supplierName: "SARGENT CONTROLS AND AEROSPACE", lines: [{lineNumber:"2", itemNumber:"KMDB6-18", itemDescription:"BEARING,PLAIN,SELF-ALIGN", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"69", executeFrom:"12/13/2015", shipQuantity:"20", locations: {origin: {id:"040416", name:"SARGENT CONTROLS AND AEROSPACE", address:"5675 W BURLINGAME RD", city:"TUCSON", sau:"AZ", postalCode:"85743-9453", country:"US"}, destination: {id:"30STA", name:"NORTHROP GRUMMAN CORPORATION", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "T38F5"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "STA"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N0001914D0022"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "KM"}, {qualifier: "IL", value: "DB6-18"}, {qualifier: "RSFD", value: "42381"}, {qualifier: "QTY", value: "20"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 319, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"1", itemNumber:"123B10268-7013", itemDescription:"STRAP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"70", executeFrom:"01/29/2017", shipQuantity:"72", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10268-7013"}, {qualifier: "RSFD", value: "42794"}, {qualifier: "QTY", value: "72"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 17, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"2", itemNumber:"123B10268-7015", itemDescription:"STRAP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"71", executeFrom:"01/29/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10268-7015"}, {qualifier: "RSFD", value: "42794"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 344, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"10", itemNumber:"123B10279-729", itemDescription:"TEE", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"72", executeFrom:"01/29/2017", shipQuantity:"36", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10279-729"}, {qualifier: "RSFD", value: "42794"}, {qualifier: "QTY", value: "36"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 479, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"11", itemNumber:"123B10282-333", itemDescription:"BRACKET ASSY", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"73", executeFrom:"02/06/2017", shipQuantity:"13", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10282-333"}, {qualifier: "RSFD", value: "42802"}, {qualifier: "QTY", value: "13"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 402, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"12", itemNumber:"123B10282-63", itemDescription:"TEE", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"74", executeFrom:"02/06/2017", shipQuantity:"72", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10282-63"}, {qualifier: "RSFD", value: "42802"}, {qualifier: "QTY", value: "72"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 423, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"13", itemNumber:"123B10282-65", itemDescription:"TEE", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"75", executeFrom:"02/06/2017", shipQuantity:"36", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10282-65"}, {qualifier: "RSFD", value: "42802"}, {qualifier: "QTY", value: "36"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 404, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"14", itemNumber:"123B10282-67", itemDescription:"TEE", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"76", executeFrom:"02/06/2017", shipQuantity:"13", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10282-67"}, {qualifier: "RSFD", value: "42802"}, {qualifier: "QTY", value: "13"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 423, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"16", itemNumber:"123B10283-17", itemDescription:"CHANNEL", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"77", executeFrom:"01/29/2017", shipQuantity:"36", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10283-17"}, {qualifier: "RSFD", value: "42794"}, {qualifier: "QTY", value: "36"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 389, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"17", itemNumber:"123B10283-21", itemDescription:"ANGLE", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"78", executeFrom:"01/29/2017", shipQuantity:"36", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10283-21"}, {qualifier: "RSFD", value: "42794"}, {qualifier: "QTY", value: "36"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 197, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"18", itemNumber:"123B10283-22", itemDescription:"ANGLE", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"79", executeFrom:"01/29/2017", shipQuantity:"36", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10283-22"}, {qualifier: "RSFD", value: "42794"}, {qualifier: "QTY", value: "36"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 280, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"21", itemNumber:"123B10289-19", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"80", executeFrom:"01/29/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10289-19"}, {qualifier: "RSFD", value: "42794"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 251, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"22", itemNumber:"123B10289-511", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"81", executeFrom:"01/29/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10289-511"}, {qualifier: "RSFD", value: "42794"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 332, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"23", itemNumber:"123B10293-33", itemDescription:"INTERCOSTAL", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"82", executeFrom:"02/06/2017", shipQuantity:"23", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10293-33"}, {qualifier: "RSFD", value: "42802"}, {qualifier: "QTY", value: "23"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 244, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"24", itemNumber:"123B10598-11", itemDescription:"TEE", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"83", executeFrom:"01/29/2017", shipQuantity:"33", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10598-11"}, {qualifier: "RSFD", value: "42794"}, {qualifier: "QTY", value: "33"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 376, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"100", itemNumber:"123B50675-11", itemDescription:"BRACKET", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"84", executeFrom:"02/14/2017", shipQuantity:"15", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B50675-11"}, {qualifier: "RSFD", value: "42810"}, {qualifier: "QTY", value: "15"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 314, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"102", itemNumber:"123B10293-321", itemDescription:"INTERCOSTAL", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"85", executeFrom:"03/11/2017", shipQuantity:"13", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10293-321"}, {qualifier: "RSFD", value: "42835"}, {qualifier: "QTY", value: "13"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 218, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"103", itemNumber:"123B10846-511", itemDescription:"CHANNEL", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"86", executeFrom:"03/11/2017", shipQuantity:"15", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10846-511"}, {qualifier: "RSFD", value: "42835"}, {qualifier: "QTY", value: "15"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 354, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"104", itemNumber:"123ELP50127-313", itemDescription:"WIRE GUARD", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"87", executeFrom:"03/11/2017", shipQuantity:"14", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3ELP50127-313"}, {qualifier: "RSFD", value: "42835"}, {qualifier: "QTY", value: "14"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 386, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"105", itemNumber:"123ABM91111-11", itemDescription:"BRACKET", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"88", executeFrom:"03/13/2017", shipQuantity:"13", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3ABM91111-11"}, {qualifier: "RSFD", value: "42837"}, {qualifier: "QTY", value: "13"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 12, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"106", itemNumber:"123ABM91111-12", itemDescription:"BRACKET", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"89", executeFrom:"03/13/2017", shipQuantity:"13", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3ABM91111-12"}, {qualifier: "RSFD", value: "42837"}, {qualifier: "QTY", value: "13"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 52, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"107", itemNumber:"123ABM91111-13", itemDescription:"BRACKET", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"90", executeFrom:"03/13/2017", shipQuantity:"24", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3ABM91111-13"}, {qualifier: "RSFD", value: "42837"}, {qualifier: "QTY", value: "24"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 1, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"108", itemNumber:"123ABM91111-14", itemDescription:"BRACKET", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"91", executeFrom:"03/13/2017", shipQuantity:"24", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3ABM91111-14"}, {qualifier: "RSFD", value: "42837"}, {qualifier: "QTY", value: "24"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 202, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"109", itemNumber:"123B51267-15", itemDescription:"BRACKET", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"92", executeFrom:"03/13/2017", shipQuantity:"27", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B51267-15"}, {qualifier: "RSFD", value: "42837"}, {qualifier: "QTY", value: "27"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 258, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"110", itemNumber:"123B51267-17", itemDescription:"BRACKET", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"93", executeFrom:"03/13/2017", shipQuantity:"27", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B51267-17"}, {qualifier: "RSFD", value: "42837"}, {qualifier: "QTY", value: "27"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 214, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"111", itemNumber:"123B51267-20", itemDescription:"BRACKET", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"94", executeFrom:"03/13/2017", shipQuantity:"5", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B51267-20"}, {qualifier: "RSFD", value: "42837"}, {qualifier: "QTY", value: "5"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 263, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"112", itemNumber:"123B51275-14", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"95", executeFrom:"12/03/2016", shipQuantity:"10", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B51275-14"}, {qualifier: "RSFD", value: "42737"}, {qualifier: "QTY", value: "10"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 127, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"113", itemNumber:"123AB93463-31", itemDescription:"BRACKET", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"96", executeFrom:"03/01/2017", shipQuantity:"24", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3AB93463-31"}, {qualifier: "RSFD", value: "42825"}, {qualifier: "QTY", value: "24"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 293, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"115", itemNumber:"123AB93611-719", itemDescription:"ANGLE", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"97", executeFrom:"03/01/2017", shipQuantity:"22", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3AB93611-719"}, {qualifier: "RSFD", value: "42825"}, {qualifier: "QTY", value: "22"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 365, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"116", itemNumber:"123B10268-7515", itemDescription:"STRAP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"98", executeFrom:"03/01/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10268-7515"}, {qualifier: "RSFD", value: "42825"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 33, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"117", itemNumber:"123B10268-7516", itemDescription:"STRAP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"99", executeFrom:"03/01/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10268-7516"}, {qualifier: "RSFD", value: "42825"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 479, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"118", itemNumber:"123B10268-7522", itemDescription:"STRAP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"100", executeFrom:"03/01/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10268-7522"}, {qualifier: "RSFD", value: "42825"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 201, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"119", itemNumber:"123B10281-715", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"101", executeFrom:"03/01/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10281-715"}, {qualifier: "RSFD", value: "42825"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 39, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"121", itemNumber:"123B10281-717", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"102", executeFrom:"03/01/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10281-717"}, {qualifier: "RSFD", value: "42825"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 271, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"122", itemNumber:"123B10281-718", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"103", executeFrom:"03/01/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10281-718"}, {qualifier: "RSFD", value: "42825"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 331, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"123", itemNumber:"123B10285-514", itemDescription:"FRAME", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"104", executeFrom:"03/01/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10285-514"}, {qualifier: "RSFD", value: "42825"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 98, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"124", itemNumber:"123B10289-715", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"105", executeFrom:"03/01/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10289-715"}, {qualifier: "RSFD", value: "42825"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 269, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"125", itemNumber:"123B10289-716", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"106", executeFrom:"03/01/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10289-716"}, {qualifier: "RSFD", value: "42825"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 148, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"126", itemNumber:"123B10311-718", itemDescription:"STRAP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"107", executeFrom:"03/01/2017", shipQuantity:"24", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10311-718"}, {qualifier: "RSFD", value: "42825"}, {qualifier: "QTY", value: "24"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 271, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"127", itemNumber:"123B10311-719", itemDescription:"STRAP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"108", executeFrom:"03/01/2017", shipQuantity:"24", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10311-719"}, {qualifier: "RSFD", value: "42825"}, {qualifier: "QTY", value: "24"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 197, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"128", itemNumber:"123B10311-720", itemDescription:"STRAP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"109", executeFrom:"03/01/2017", shipQuantity:"24", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10311-720"}, {qualifier: "RSFD", value: "42825"}, {qualifier: "QTY", value: "24"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 214, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"129", itemNumber:"123B10311-7327", itemDescription:"STRAP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"110", executeFrom:"12/18/2016", shipQuantity:"24", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10311-7327"}, {qualifier: "RSFD", value: "42752"}, {qualifier: "QTY", value: "24"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 99, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"130", itemNumber:"123B10488-35", itemDescription:"ANGLE", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"111", executeFrom:"01/11/2017", shipQuantity:"11", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10488-35"}, {qualifier: "RSFD", value: "42776"}, {qualifier: "QTY", value: "11"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 111, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"131", itemNumber:"123B10488-36", itemDescription:"ANGLE", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"112", executeFrom:"03/01/2017", shipQuantity:"11", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10488-36"}, {qualifier: "RSFD", value: "42825"}, {qualifier: "QTY", value: "11"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 146, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"132", itemNumber:"123B10488-37", itemDescription:"ANGLE", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"113", executeFrom:"03/01/2017", shipQuantity:"11", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10488-37"}, {qualifier: "RSFD", value: "42825"}, {qualifier: "QTY", value: "11"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 88, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"133", itemNumber:"123B10488-38", itemDescription:"ANGLE", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"114", executeFrom:"03/01/2017", shipQuantity:"11", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10488-38"}, {qualifier: "RSFD", value: "42825"}, {qualifier: "QTY", value: "11"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 134, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"134", itemNumber:"123B10488-517", itemDescription:"ANGLE", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"115", executeFrom:"03/01/2017", shipQuantity:"46", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10488-517"}, {qualifier: "RSFD", value: "42825"}, {qualifier: "QTY", value: "46"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 252, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"135", itemNumber:"123B10488-519", itemDescription:"CHANNEL", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"116", executeFrom:"03/01/2017", shipQuantity:"22", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10488-519"}, {qualifier: "RSFD", value: "42825"}, {qualifier: "QTY", value: "22"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 27, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"136", itemNumber:"123B10488-61", itemDescription:"PLATE", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"117", executeFrom:"03/01/2017", shipQuantity:"22", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10488-61"}, {qualifier: "RSFD", value: "42825"}, {qualifier: "QTY", value: "22"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 406, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"137", itemNumber:"123B10516-719", itemDescription:"ANGLE", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"118", executeFrom:"03/15/2017", shipQuantity:"25", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10516-719"}, {qualifier: "RSFD", value: "42839"}, {qualifier: "QTY", value: "25"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 72, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"138", itemNumber:"123B10516-721", itemDescription:"ANGLE", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"119", executeFrom:"03/15/2017", shipQuantity:"26", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10516-721"}, {qualifier: "RSFD", value: "42839"}, {qualifier: "QTY", value: "26"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 113, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"139", itemNumber:"123B10844-13", itemDescription:"ANGLE", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"120", executeFrom:"03/01/2017", shipQuantity:"22", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10844-13"}, {qualifier: "RSFD", value: "42825"}, {qualifier: "QTY", value: "22"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 133, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"140", itemNumber:"123B10844-14", itemDescription:"ANGLE", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"121", executeFrom:"03/01/2017", shipQuantity:"22", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10844-14"}, {qualifier: "RSFD", value: "42825"}, {qualifier: "QTY", value: "22"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 265, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"141", itemNumber:"123B10844-19", itemDescription:"ANGLE", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"122", executeFrom:"03/01/2017", shipQuantity:"22", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10844-19"}, {qualifier: "RSFD", value: "42825"}, {qualifier: "QTY", value: "22"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 474, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"142", itemNumber:"123B10844-20", itemDescription:"ANGLE", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"123", executeFrom:"03/01/2017", shipQuantity:"22", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10844-20"}, {qualifier: "RSFD", value: "42825"}, {qualifier: "QTY", value: "22"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 174, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"143", itemNumber:"123B10963-17", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"124", executeFrom:"03/01/2017", shipQuantity:"24", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10963-17"}, {qualifier: "RSFD", value: "42825"}, {qualifier: "QTY", value: "24"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 248, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"144", itemNumber:"123B10963-18", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"125", executeFrom:"03/01/2017", shipQuantity:"24", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10963-18"}, {qualifier: "RSFD", value: "42825"}, {qualifier: "QTY", value: "24"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 220, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"145", itemNumber:"123B10964-520", itemDescription:"CHANNEL", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"126", executeFrom:"03/01/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10964-520"}, {qualifier: "RSFD", value: "42825"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 33, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"146", itemNumber:"123B10964-521", itemDescription:"CHANNEL", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"127", executeFrom:"03/01/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10964-521"}, {qualifier: "RSFD", value: "42825"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 187, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"147", itemNumber:"123B10964-522", itemDescription:"CHANNEL", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"128", executeFrom:"01/03/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10964-522"}, {qualifier: "RSFD", value: "42768"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 450, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"148", itemNumber:"123B10965-515", itemDescription:"CHANNEL", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"129", executeFrom:"03/01/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10965-515"}, {qualifier: "RSFD", value: "42825"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 450, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"149", itemNumber:"123B10965-516", itemDescription:"CHANNEL", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"130", executeFrom:"03/01/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10965-516"}, {qualifier: "RSFD", value: "42825"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 297, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"150", itemNumber:"123B10965-517", itemDescription:"CHANNEL", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"131", executeFrom:"03/01/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10965-517"}, {qualifier: "RSFD", value: "42825"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 271, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"151", itemNumber:"123B11057-317", itemDescription:"ANGLE", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"132", executeFrom:"03/01/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B11057-317"}, {qualifier: "RSFD", value: "42825"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 467, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"152", itemNumber:"123B11057-318", itemDescription:"ANGLE", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"133", executeFrom:"03/01/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B11057-318"}, {qualifier: "RSFD", value: "42825"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 382, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"153", itemNumber:"123EC92251-13", itemDescription:"BRACKET", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"134", executeFrom:"03/01/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3EC92251-13"}, {qualifier: "RSFD", value: "42825"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 456, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"154", itemNumber:"123B10473-511", itemDescription:"COVER", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"135", executeFrom:"03/18/2017", shipQuantity:"13", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10473-511"}, {qualifier: "RSFD", value: "42842"}, {qualifier: "QTY", value: "13"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 116, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"155", itemNumber:"123B10473-515", itemDescription:"BRACKET", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"136", executeFrom:"03/18/2017", shipQuantity:"13", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10473-515"}, {qualifier: "RSFD", value: "42842"}, {qualifier: "QTY", value: "13"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 398, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"156", itemNumber:"123B10474-517", itemDescription:"DOOR", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"137", executeFrom:"03/18/2017", shipQuantity:"13", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10474-517"}, {qualifier: "RSFD", value: "42842"}, {qualifier: "QTY", value: "13"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 318, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"157", itemNumber:"123B10825-13", itemDescription:"INTERCOSTAL", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"138", executeFrom:"03/18/2017", shipQuantity:"13", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10825-13"}, {qualifier: "RSFD", value: "42842"}, {qualifier: "QTY", value: "13"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 222, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"158", itemNumber:"123B10825-15", itemDescription:"INTERCOSTAL", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"139", executeFrom:"03/18/2017", shipQuantity:"13", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10825-15"}, {qualifier: "RSFD", value: "42842"}, {qualifier: "QTY", value: "13"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 290, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"159", itemNumber:"123B10825-17", itemDescription:"INTERCOSTAL", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"140", executeFrom:"03/18/2017", shipQuantity:"13", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10825-17"}, {qualifier: "RSFD", value: "42842"}, {qualifier: "QTY", value: "13"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 209, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"160", itemNumber:"123B10825-19", itemDescription:"INTERCOSTAL", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"141", executeFrom:"03/18/2017", shipQuantity:"13", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10825-19"}, {qualifier: "RSFD", value: "42842"}, {qualifier: "QTY", value: "13"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 412, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"161", itemNumber:"123B10825-21", itemDescription:"INTERCOSTAL", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"142", executeFrom:"03/18/2017", shipQuantity:"13", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10825-21"}, {qualifier: "RSFD", value: "42842"}, {qualifier: "QTY", value: "13"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 182, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"162", itemNumber:"123B10827-13", itemDescription:"DOUBLER", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"143", executeFrom:"03/18/2017", shipQuantity:"13", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10827-13"}, {qualifier: "RSFD", value: "42842"}, {qualifier: "QTY", value: "13"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 406, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"163", itemNumber:"123B10886-11", itemDescription:"SHIM", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"144", executeFrom:"03/18/2017", shipQuantity:"26", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10886-11"}, {qualifier: "RSFD", value: "42842"}, {qualifier: "QTY", value: "26"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 236, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"164", itemNumber:"123B10886-13", itemDescription:"SHIM", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"145", executeFrom:"03/18/2017", shipQuantity:"26", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10886-13"}, {qualifier: "RSFD", value: "42842"}, {qualifier: "QTY", value: "26"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 333, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"165", itemNumber:"123B10886-17", itemDescription:"SHIM", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"146", executeFrom:"03/18/2017", shipQuantity:"52", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10886-17"}, {qualifier: "RSFD", value: "42842"}, {qualifier: "QTY", value: "52"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 22, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"166", itemNumber:"123B10886-21", itemDescription:"SHIM", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"147", executeFrom:"03/18/2017", shipQuantity:"52", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10886-21"}, {qualifier: "RSFD", value: "42842"}, {qualifier: "QTY", value: "52"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 405, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"167", itemNumber:"123W60103-100", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"148", executeFrom:"03/28/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60103-100"}, {qualifier: "RSFD", value: "42852"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 300, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"168", itemNumber:"123W60103-101", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"149", executeFrom:"03/28/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60103-101"}, {qualifier: "RSFD", value: "42852"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 36, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"169", itemNumber:"123W60103-102", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"150", executeFrom:"03/28/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60103-102"}, {qualifier: "RSFD", value: "42852"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 466, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"170", itemNumber:"123W60103-105", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"151", executeFrom:"03/28/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60103-105"}, {qualifier: "RSFD", value: "42852"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 374, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"171", itemNumber:"123W60103-106", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"152", executeFrom:"03/28/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60103-106"}, {qualifier: "RSFD", value: "42852"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 495, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"172", itemNumber:"123W60103-131", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"153", executeFrom:"03/28/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60103-131"}, {qualifier: "RSFD", value: "42852"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 118, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"173", itemNumber:"123W60103-132", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"154", executeFrom:"03/28/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60103-132"}, {qualifier: "RSFD", value: "42852"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 155, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"174", itemNumber:"123W60103-53", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"155", executeFrom:"03/28/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60103-53"}, {qualifier: "RSFD", value: "42852"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 349, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"175", itemNumber:"123W60103-73", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"156", executeFrom:"03/28/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60103-73"}, {qualifier: "RSFD", value: "42852"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 72, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"176", itemNumber:"123W60103-74", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"157", executeFrom:"03/28/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60103-74"}, {qualifier: "RSFD", value: "42852"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 223, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"177", itemNumber:"123W60103-75", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"158", executeFrom:"03/28/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60103-75"}, {qualifier: "RSFD", value: "42852"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 6, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"178", itemNumber:"123W60103-76", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"159", executeFrom:"03/28/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60103-76"}, {qualifier: "RSFD", value: "42852"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 179, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"179", itemNumber:"123W60103-77", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"160", executeFrom:"03/28/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60103-77"}, {qualifier: "RSFD", value: "42852"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 271, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"180", itemNumber:"123W60103-78", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"161", executeFrom:"03/28/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60103-78"}, {qualifier: "RSFD", value: "42852"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 477, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"181", itemNumber:"123W60103-79", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"162", executeFrom:"03/28/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60103-79"}, {qualifier: "RSFD", value: "42852"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 335, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"182", itemNumber:"123W60103-80", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"163", executeFrom:"03/28/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60103-80"}, {qualifier: "RSFD", value: "42852"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 57, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"183", itemNumber:"123W60103-81", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"164", executeFrom:"03/28/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60103-81"}, {qualifier: "RSFD", value: "42852"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 241, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"184", itemNumber:"123W60103-82", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"165", executeFrom:"03/28/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60103-82"}, {qualifier: "RSFD", value: "42852"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 265, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"185", itemNumber:"123W60103-83", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"166", executeFrom:"03/28/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60103-83"}, {qualifier: "RSFD", value: "42852"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 32, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"186", itemNumber:"123W60103-84", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"167", executeFrom:"03/28/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60103-84"}, {qualifier: "RSFD", value: "42852"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 434, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"187", itemNumber:"123W60103-85", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"168", executeFrom:"03/28/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60103-85"}, {qualifier: "RSFD", value: "42852"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 366, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"188", itemNumber:"123W60103-86", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"169", executeFrom:"03/28/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60103-86"}, {qualifier: "RSFD", value: "42852"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 474, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"189", itemNumber:"123W60103-87", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"170", executeFrom:"03/28/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60103-87"}, {qualifier: "RSFD", value: "42852"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 155, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"190", itemNumber:"123W60103-88", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"171", executeFrom:"03/28/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60103-88"}, {qualifier: "RSFD", value: "42852"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 211, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"191", itemNumber:"123W60103-89", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"172", executeFrom:"03/28/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60103-89"}, {qualifier: "RSFD", value: "42852"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 500, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"192", itemNumber:"123W60103-90", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"173", executeFrom:"03/28/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60103-90"}, {qualifier: "RSFD", value: "42852"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 330, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"193", itemNumber:"123W60103-91", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"174", executeFrom:"03/28/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60103-91"}, {qualifier: "RSFD", value: "42852"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 472, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"194", itemNumber:"123W60103-92", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"175", executeFrom:"03/28/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60103-92"}, {qualifier: "RSFD", value: "42852"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 238, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"195", itemNumber:"123W60103-93", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"176", executeFrom:"03/28/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60103-93"}, {qualifier: "RSFD", value: "42852"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 241, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"196", itemNumber:"123W60103-94", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"177", executeFrom:"03/28/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60103-94"}, {qualifier: "RSFD", value: "42852"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 344, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"197", itemNumber:"123W60103-95", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"178", executeFrom:"03/28/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60103-95"}, {qualifier: "RSFD", value: "42852"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 391, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"198", itemNumber:"123W60103-96", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"179", executeFrom:"03/28/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60103-96"}, {qualifier: "RSFD", value: "42852"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 254, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"199", itemNumber:"123W60103-97", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"180", executeFrom:"03/28/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60103-97"}, {qualifier: "RSFD", value: "42852"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 102, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"200", itemNumber:"123W60103-98", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"181", executeFrom:"03/28/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60103-98"}, {qualifier: "RSFD", value: "42852"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 129, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"201", itemNumber:"123W60103-99", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"182", executeFrom:"03/28/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60103-99"}, {qualifier: "RSFD", value: "42852"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 484, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"202", itemNumber:"123W60108-17", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"183", executeFrom:"03/28/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60108-17"}, {qualifier: "RSFD", value: "42852"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 204, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"203", itemNumber:"123W60108-18", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"184", executeFrom:"03/28/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60108-18"}, {qualifier: "RSFD", value: "42852"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 8, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"204", itemNumber:"123W60108-19", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"185", executeFrom:"03/28/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60108-19"}, {qualifier: "RSFD", value: "42852"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 473, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"205", itemNumber:"123W60108-20", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"186", executeFrom:"03/28/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60108-20"}, {qualifier: "RSFD", value: "42852"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 494, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"206", itemNumber:"123W60108-21", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"187", executeFrom:"03/28/2017", shipQuantity:"72", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60108-21"}, {qualifier: "RSFD", value: "42852"}, {qualifier: "QTY", value: "72"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 360, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"207", itemNumber:"123W60108-22", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"188", executeFrom:"03/28/2017", shipQuantity:"72", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60108-22"}, {qualifier: "RSFD", value: "42852"}, {qualifier: "QTY", value: "72"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 347, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"208", itemNumber:"123W60108-23", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"189", executeFrom:"03/28/2017", shipQuantity:"24", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60108-23"}, {qualifier: "RSFD", value: "42852"}, {qualifier: "QTY", value: "24"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 406, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"209", itemNumber:"123W60108-24", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"190", executeFrom:"03/28/2017", shipQuantity:"24", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60108-24"}, {qualifier: "RSFD", value: "42852"}, {qualifier: "QTY", value: "24"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 466, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"210", itemNumber:"123W60108-25", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"191", executeFrom:"03/28/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60108-25"}, {qualifier: "RSFD", value: "42852"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 65, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"211", itemNumber:"123W60108-26", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"192", executeFrom:"03/28/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60108-26"}, {qualifier: "RSFD", value: "42852"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 5, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"212", itemNumber:"123W60108-27", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"193", executeFrom:"03/28/2017", shipQuantity:"70", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60108-27"}, {qualifier: "RSFD", value: "42852"}, {qualifier: "QTY", value: "70"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 222, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"213", itemNumber:"123W60108-29", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"194", executeFrom:"03/28/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60108-29"}, {qualifier: "RSFD", value: "42852"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 189, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"214", itemNumber:"123W60108-30", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"195", executeFrom:"03/28/2017", shipQuantity:"12", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60108-30"}, {qualifier: "RSFD", value: "42852"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 177, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"215", itemNumber:"123W10779-513", itemDescription:"STIFFENER", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"196", executeFrom:"03/29/2017", shipQuantity:"27", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W10779-513"}, {qualifier: "RSFD", value: "42853"}, {qualifier: "QTY", value: "27"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 33, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"216", itemNumber:"123W10804-15", itemDescription:"BRACKET", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"197", executeFrom:"03/29/2017", shipQuantity:"14", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W10804-15"}, {qualifier: "RSFD", value: "42853"}, {qualifier: "QTY", value: "14"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 500, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"217", itemNumber:"123W10805-7333", itemDescription:"DOUBLER", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"198", executeFrom:"03/29/2017", shipQuantity:"14", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W10805-7333"}, {qualifier: "RSFD", value: "42853"}, {qualifier: "QTY", value: "14"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 39, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"218", itemNumber:"123W10669-17", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"199", executeFrom:"01/07/2017", shipQuantity:"13", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W10669-17"}, {qualifier: "RSFD", value: "42772"}, {qualifier: "QTY", value: "13"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 103, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"219", itemNumber:"123W10678-17", itemDescription:"TEE", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"200", executeFrom:"04/02/2017", shipQuantity:"13", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W10678-17"}, {qualifier: "RSFD", value: "42857"}, {qualifier: "QTY", value: "13"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 264, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"220", itemNumber:"123W10678-18", itemDescription:"TEE", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"201", executeFrom:"04/02/2017", shipQuantity:"13", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W10678-18"}, {qualifier: "RSFD", value: "42857"}, {qualifier: "QTY", value: "13"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 120, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"221", itemNumber:"123W10805-732", itemDescription:"ANGLE", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"202", executeFrom:"04/02/2017", shipQuantity:"15", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W10805-732"}, {qualifier: "RSFD", value: "42857"}, {qualifier: "QTY", value: "15"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 104, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"222", itemNumber:"123W10917-523", itemDescription:"ANGLE", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"203", executeFrom:"04/02/2017", shipQuantity:"26", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W10917-523"}, {qualifier: "RSFD", value: "42857"}, {qualifier: "QTY", value: "26"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 179, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"223", itemNumber:"123W10917-525", itemDescription:"ANGLE", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"204", executeFrom:"04/02/2017", shipQuantity:"18", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W10917-525"}, {qualifier: "RSFD", value: "42857"}, {qualifier: "QTY", value: "18"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 375, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"224", itemNumber:"123W10932-11", itemDescription:"CHANNEL", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"205", executeFrom:"04/02/2017", shipQuantity:"26", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W10932-11"}, {qualifier: "RSFD", value: "42857"}, {qualifier: "QTY", value: "26"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 135, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"225", itemNumber:"123W10932-511", itemDescription:"TEE", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"206", executeFrom:"04/02/2017", shipQuantity:"26", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W10932-511"}, {qualifier: "RSFD", value: "42857"}, {qualifier: "QTY", value: "26"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 473, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"226", itemNumber:"123W10932-512", itemDescription:"TEE", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"207", executeFrom:"04/02/2017", shipQuantity:"26", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W10932-512"}, {qualifier: "RSFD", value: "42857"}, {qualifier: "QTY", value: "26"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 229, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"227", itemNumber:"123W40078-511", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"208", executeFrom:"04/02/2017", shipQuantity:"13", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W40078-511"}, {qualifier: "RSFD", value: "42857"}, {qualifier: "QTY", value: "13"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 90, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"228", itemNumber:"123W40078-512", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"209", executeFrom:"04/02/2017", shipQuantity:"13", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W40078-512"}, {qualifier: "RSFD", value: "42857"}, {qualifier: "QTY", value: "13"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 103, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"229", itemNumber:"123W60041-33", itemDescription:"SHIM", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"210", executeFrom:"04/02/2017", shipQuantity:"26", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60041-33"}, {qualifier: "RSFD", value: "42857"}, {qualifier: "QTY", value: "26"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 191, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"230", itemNumber:"123W60041-35", itemDescription:"SHIM", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"211", executeFrom:"04/02/2017", shipQuantity:"26", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60041-35"}, {qualifier: "RSFD", value: "42857"}, {qualifier: "QTY", value: "26"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 347, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"231", itemNumber:"123W60103-107", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"212", executeFrom:"04/02/2017", shipQuantity:"13", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60103-107"}, {qualifier: "RSFD", value: "42857"}, {qualifier: "QTY", value: "13"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 363, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"232", itemNumber:"123W60103-123", itemDescription:"DOUBLER", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"213", executeFrom:"04/02/2017", shipQuantity:"26", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60103-123"}, {qualifier: "RSFD", value: "42857"}, {qualifier: "QTY", value: "26"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 486, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"233", itemNumber:"123W60103-127", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"214", executeFrom:"04/02/2017", shipQuantity:"13", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60103-127"}, {qualifier: "RSFD", value: "42857"}, {qualifier: "QTY", value: "13"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 158, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"234", itemNumber:"123W60103-128", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"215", executeFrom:"04/02/2017", shipQuantity:"13", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60103-128"}, {qualifier: "RSFD", value: "42857"}, {qualifier: "QTY", value: "13"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 334, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"235", itemNumber:"123W60103-133", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"216", executeFrom:"04/02/2017", shipQuantity:"13", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60103-133"}, {qualifier: "RSFD", value: "42857"}, {qualifier: "QTY", value: "13"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 376, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"236", itemNumber:"123W60103-134", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"217", executeFrom:"04/02/2017", shipQuantity:"13", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3W60103-134"}, {qualifier: "RSFD", value: "42857"}, {qualifier: "QTY", value: "13"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 23, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"237", itemNumber:"123B10491-311", itemDescription:"PLATE", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"218", executeFrom:"03/15/2017", shipQuantity:"48", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10491-311"}, {qualifier: "RSFD", value: "42839"}, {qualifier: "QTY", value: "48"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 198, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"238", itemNumber:"123B10653-7311", itemDescription:"SUPPORT", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"219", executeFrom:"04/02/2017", shipQuantity:"1", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10653-7311"}, {qualifier: "RSFD", value: "42857"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 485, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"238", itemNumber:"123B10653-7311", itemDescription:"SUPPORT", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"220", executeFrom:"04/02/2017", shipQuantity:"1", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10653-7311"}, {qualifier: "RSFD", value: "42857"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 132, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"239", itemNumber:"123B10953-524", itemDescription:"STIFFENER", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"221", executeFrom:"04/12/2017", shipQuantity:"14", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B10953-524"}, {qualifier: "RSFD", value: "42867"}, {qualifier: "QTY", value: "14"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 160, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"240", itemNumber:"123B50808-17", itemDescription:"SUPPORT", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"222", executeFrom:"04/12/2017", shipQuantity:"13", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B50808-17"}, {qualifier: "RSFD", value: "42867"}, {qualifier: "QTY", value: "13"}]}]},
{orderNumber: "2913937", orderDate: "42690", orderType: "STND", quantity: 206, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "051306", supplierName: "L-3 COMMUNICATIONS INTEGRATED", lines: [{lineNumber:"241", itemNumber:"123B50808-18", itemDescription:"SUPPORT", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"223", executeFrom:"04/12/2017", shipQuantity:"13", locations: {origin: {id:"051306", name:"L-3 COMMUNICATIONS INTEGRATED", address:"5486 FAIRCHILD RD", city:"CRESTVIEW", sau:"FL", postalCode:"32539-8155", country:"US"}, destination: {id:"30BET", name:"NORTHROP GRUMMAN CORPORATION.", address:"5000 U.S. 1 NORTH", city:"SAINT AUGUSTINE", sau:"FL", postalCode:"32095", country:"US"}}, references: [{qualifier: "P4", value: "E2CXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "BET"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "Y"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-13-C-9999"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "12"}, {qualifier: "IL", value: "3B50808-18"}, {qualifier: "RSFD", value: "42867"}, {qualifier: "QTY", value: "13"}]}]},
{orderNumber: "2914053", orderDate: "42690", orderType: "PULL", quantity: 154, sector:"AS - Aerospace Systems", psa:false, incoTerms: "FOB Destination", supplierId: "049033", supplierName: "BARNES GROUP INC", lines: [{lineNumber:"2", itemNumber:"74A343922-2001", itemDescription:"VENT,", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"224", executeFrom:"07/02/2017", shipQuantity:"2", locations: {origin: {id:"049033", name:"BARNES GROUP INC", address:"5300 AURELIUS RD", city:"LANSING", sau:"MI", postalCode:"48911-4116", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18EF"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "SEE CHARGE TEXT"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A343922-2001"}, {qualifier: "RSFD", value: "42948"}, {qualifier: "QTY", value: "2"}]}]},
{orderNumber: "2914053", orderDate: "42690", orderType: "PULL", quantity: 317, sector:"AS - Aerospace Systems", psa:false, incoTerms: "FOB Destination", supplierId: "049033", supplierName: "BARNES GROUP INC", lines: [{lineNumber:"4", itemNumber:"74A231734-2005", itemDescription:"RIB, CLOSEOUT", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"225", executeFrom:"12/01/2016", shipQuantity:"7", locations: {origin: {id:"049033", name:"BARNES GROUP INC", address:"5300 AURELIUS RD", city:"LANSING", sau:"MI", postalCode:"48911-4116", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18EF"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "SEE CHARGE TEXT"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A231734-2005"}, {qualifier: "RSFD", value: "42735"}, {qualifier: "QTY", value: "7"}]}]},
{orderNumber: "2914053", orderDate: "42690", orderType: "PULL", quantity: 283, sector:"AS - Aerospace Systems", psa:false, incoTerms: "FOB Destination", supplierId: "049033", supplierName: "BARNES GROUP INC", lines: [{lineNumber:"10", itemNumber:"74A340830-2010-03", itemDescription:"HEAT SHIELD", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"226", executeFrom:"11/08/2016", shipQuantity:"11", locations: {origin: {id:"049033", name:"BARNES GROUP INC", address:"5300 AURELIUS RD", city:"LANSING", sau:"MI", postalCode:"48911-4116", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18EF"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "SEE CHARGE TEXT"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A340830-2010-03"}, {qualifier: "RSFD", value: "42712"}, {qualifier: "QTY", value: "11"}]}]},
{orderNumber: "2914053", orderDate: "42690", orderType: "PULL", quantity: 329, sector:"AS - Aerospace Systems", psa:false, incoTerms: "FOB Destination", supplierId: "049033", supplierName: "BARNES GROUP INC", lines: [{lineNumber:"11", itemNumber:"74A343922-2005", itemDescription:"VENT               T171", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"227", executeFrom:"02/11/2017", shipQuantity:"13", locations: {origin: {id:"049033", name:"BARNES GROUP INC", address:"5300 AURELIUS RD", city:"LANSING", sau:"MI", postalCode:"48911-4116", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18EF"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "SPM4A109G0004"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A343922-2005"}, {qualifier: "RSFD", value: "42807"}, {qualifier: "QTY", value: "13"}]}]},
{orderNumber: "2914053", orderDate: "42690", orderType: "PULL", quantity: 481, sector:"AS - Aerospace Systems", psa:false, incoTerms: "FOB Destination", supplierId: "049033", supplierName: "BARNES GROUP INC", lines: [{lineNumber:"12", itemNumber:"74A231734-2005", itemDescription:"RIB, CLOSEOUT", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"228", executeFrom:"01/04/2017", shipQuantity:"12", locations: {origin: {id:"049033", name:"BARNES GROUP INC", address:"5300 AURELIUS RD", city:"LANSING", sau:"MI", postalCode:"48911-4116", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18EF"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "SEE CHARGE TEXT"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A231734-2005"}, {qualifier: "RSFD", value: "42769"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2914053", orderDate: "42690", orderType: "PULL", quantity: 52, sector:"AS - Aerospace Systems", psa:false, incoTerms: "FOB Destination", supplierId: "049033", supplierName: "BARNES GROUP INC", lines: [{lineNumber:"13", itemNumber:"74A343922-2002", itemDescription:"VENT,", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"229", executeFrom:"03/15/2017", shipQuantity:"3", locations: {origin: {id:"049033", name:"BARNES GROUP INC", address:"5300 AURELIUS RD", city:"LANSING", sau:"MI", postalCode:"48911-4116", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18SP"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: ""}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A343922-2002"}, {qualifier: "RSFD", value: "42839"}, {qualifier: "QTY", value: "3"}]}]},
{orderNumber: "2914053", orderDate: "42690", orderType: "PULL", quantity: 343, sector:"AS - Aerospace Systems", psa:false, incoTerms: "FOB Destination", supplierId: "049033", supplierName: "BARNES GROUP INC", lines: [{lineNumber:"14", itemNumber:"74A231734-2005", itemDescription:"RIB, CLOSEOUT", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"230", executeFrom:"04/08/2017", shipQuantity:"2", locations: {origin: {id:"049033", name:"BARNES GROUP INC", address:"5300 AURELIUS RD", city:"LANSING", sau:"MI", postalCode:"48911-4116", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18EF"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: ""}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A231734-2005"}, {qualifier: "RSFD", value: "42863"}, {qualifier: "QTY", value: "2"}]}]},
{orderNumber: "2914113", orderDate: "42690", orderType: "STND", quantity: 485, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "038707", supplierName: "DANVO MACHINING CO", lines: [{lineNumber:"10", itemNumber:"74A588100-2039", itemDescription:"CONNECTOR", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"231", executeFrom:"02/08/2017", shipQuantity:"2", locations: {origin: {id:"038707", name:"DANVO MACHINING CO", address:"2107 S HATHAWAY ST", city:"SANTA ANA", sau:"CA", postalCode:"92705-5238", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18EF"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "1889481"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A588100-2039"}, {qualifier: "RSFD", value: "42804"}, {qualifier: "QTY", value: "2"}]}]},
{orderNumber: "2914113", orderDate: "42690", orderType: "STND", quantity: 211, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "038707", supplierName: "DANVO MACHINING CO", lines: [{lineNumber:"10", itemNumber:"74A588100-2039", itemDescription:"CONNECTOR", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"232", executeFrom:"12/12/2016", shipQuantity:"4", locations: {origin: {id:"038707", name:"DANVO MACHINING CO", address:"2107 S HATHAWAY ST", city:"SANTA ANA", sau:"CA", postalCode:"92705-5238", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18EF"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "1889481"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A588100-2039"}, {qualifier: "RSFD", value: "42746"}, {qualifier: "QTY", value: "4"}]}]},
{orderNumber: "2914113", orderDate: "42690", orderType: "STND", quantity: 206, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "038707", supplierName: "DANVO MACHINING CO", lines: [{lineNumber:"10", itemNumber:"74A588100-2039", itemDescription:"CONNECTOR", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"233", executeFrom:"10/12/2016", shipQuantity:"0", locations: {origin: {id:"038707", name:"DANVO MACHINING CO", address:"2107 S HATHAWAY ST", city:"SANTA ANA", sau:"CA", postalCode:"92705-5238", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18EF"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "1889481"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A588100-2039"}, {qualifier: "RSFD", value: "42685"}, {qualifier: "QTY", value: "0"}]}]},
{orderNumber: "2914113", orderDate: "42690", orderType: "STND", quantity: 37, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "038707", supplierName: "DANVO MACHINING CO", lines: [{lineNumber:"10", itemNumber:"74A588100-2039", itemDescription:"CONNECTOR", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"234", executeFrom:"08/29/2016", shipQuantity:"0", locations: {origin: {id:"038707", name:"DANVO MACHINING CO", address:"2107 S HATHAWAY ST", city:"SANTA ANA", sau:"CA", postalCode:"92705-5238", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18EF"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "1889481"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A588100-2039"}, {qualifier: "RSFD", value: "42641"}, {qualifier: "QTY", value: "0"}]}]},
{orderNumber: "2914113", orderDate: "42690", orderType: "STND", quantity: 17, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "038707", supplierName: "DANVO MACHINING CO", lines: [{lineNumber:"11", itemNumber:"74A585736-2003", itemDescription:"RETAINER, FUEL CELL SMI", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"235", executeFrom:"01/11/2017", shipQuantity:"3", locations: {origin: {id:"038707", name:"DANVO MACHINING CO", address:"2107 S HATHAWAY ST", city:"SANTA ANA", sau:"CA", postalCode:"92705-5238", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18EF"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "SPM4A109G0004"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A585736-2003"}, {qualifier: "RSFD", value: "42776"}, {qualifier: "QTY", value: "3"}]}]},
{orderNumber: "2914113", orderDate: "42690", orderType: "STND", quantity: 277, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "038707", supplierName: "DANVO MACHINING CO", lines: [{lineNumber:"11", itemNumber:"74A585736-2003", itemDescription:"RETAINER, FUEL CELL SMI", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"236", executeFrom:"10/15/2016", shipQuantity:"0", locations: {origin: {id:"038707", name:"DANVO MACHINING CO", address:"2107 S HATHAWAY ST", city:"SANTA ANA", sau:"CA", postalCode:"92705-5238", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18EF"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "SPM4A109G0004"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A585736-2003"}, {qualifier: "RSFD", value: "42688"}, {qualifier: "QTY", value: "0"}]}]},
{orderNumber: "2914113", orderDate: "42690", orderType: "STND", quantity: 168, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "038707", supplierName: "DANVO MACHINING CO", lines: [{lineNumber:"11", itemNumber:"74A585736-2003", itemDescription:"RETAINER, FUEL CELL SMI", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"237", executeFrom:"07/11/2016", shipQuantity:"0", locations: {origin: {id:"038707", name:"DANVO MACHINING CO", address:"2107 S HATHAWAY ST", city:"SANTA ANA", sau:"CA", postalCode:"92705-5238", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18EF"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "SPM4A109G0004"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A585736-2003"}, {qualifier: "RSFD", value: "42592"}, {qualifier: "QTY", value: "0"}]}]},
{orderNumber: "2914113", orderDate: "42690", orderType: "STND", quantity: 344, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "038707", supplierName: "DANVO MACHINING CO", lines: [{lineNumber:"13", itemNumber:"74A425056-2005", itemDescription:"SHAFT,", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"238", executeFrom:"11/08/2016", shipQuantity:"20", locations: {origin: {id:"038707", name:"DANVO MACHINING CO", address:"2107 S HATHAWAY ST", city:"SANTA ANA", sau:"CA", postalCode:"92705-5238", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18SP"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "SPRPA115G001Z"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A425056-2005"}, {qualifier: "RSFD", value: "42712"}, {qualifier: "QTY", value: "20"}]}]},
{orderNumber: "2914113", orderDate: "42690", orderType: "STND", quantity: 382, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "038707", supplierName: "DANVO MACHINING CO", lines: [{lineNumber:"13", itemNumber:"74A425056-2005", itemDescription:"SHAFT,", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"239", executeFrom:"10/08/2016", shipQuantity:"7", locations: {origin: {id:"038707", name:"DANVO MACHINING CO", address:"2107 S HATHAWAY ST", city:"SANTA ANA", sau:"CA", postalCode:"92705-5238", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18SP"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "SPRPA115G001Z"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A425056-2005"}, {qualifier: "RSFD", value: "42681"}, {qualifier: "QTY", value: "7"}]}]},
{orderNumber: "2914113", orderDate: "42690", orderType: "STND", quantity: 385, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "038707", supplierName: "DANVO MACHINING CO", lines: [{lineNumber:"15", itemNumber:"74A328966-2002-01", itemDescription:"BUSHING", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"240", executeFrom:"05/09/2017", shipQuantity:"4", locations: {origin: {id:"038707", name:"DANVO MACHINING CO", address:"2107 S HATHAWAY ST", city:"SANTA ANA", sau:"CA", postalCode:"92705-5238", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18EF"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "SPM4A109G0004"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A328966-2002-01"}, {qualifier: "RSFD", value: "42894"}, {qualifier: "QTY", value: "4"}]}]},
{orderNumber: "2914113", orderDate: "42690", orderType: "STND", quantity: 219, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "038707", supplierName: "DANVO MACHINING CO", lines: [{lineNumber:"15", itemNumber:"74A328966-2002-01", itemDescription:"BUSHING", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"241", executeFrom:"09/28/2016", shipQuantity:"21", locations: {origin: {id:"038707", name:"DANVO MACHINING CO", address:"2107 S HATHAWAY ST", city:"SANTA ANA", sau:"CA", postalCode:"92705-5238", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18EF"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "SPM4A109G0004"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A328966-2002-01"}, {qualifier: "RSFD", value: "42671"}, {qualifier: "QTY", value: "21"}]}]},
{orderNumber: "2914113", orderDate: "42690", orderType: "STND", quantity: 440, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "038707", supplierName: "DANVO MACHINING CO", lines: [{lineNumber:"16", itemNumber:"74A586376-2005-01", itemDescription:"ADAPTER DRAIN", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"242", executeFrom:"10/31/2016", shipQuantity:"6", locations: {origin: {id:"038707", name:"DANVO MACHINING CO", address:"2107 S HATHAWAY ST", city:"SANTA ANA", sau:"CA", postalCode:"92705-5238", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18EF"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "1889481"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A586376-2005-01"}, {qualifier: "RSFD", value: "42704"}, {qualifier: "QTY", value: "6"}]}]},
{orderNumber: "2914113", orderDate: "42690", orderType: "STND", quantity: 227, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "038707", supplierName: "DANVO MACHINING CO", lines: [{lineNumber:"17", itemNumber:"74A587012-2001", itemDescription:"FERRULE,", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"243", executeFrom:"11/07/2016", shipQuantity:"12", locations: {origin: {id:"038707", name:"DANVO MACHINING CO", address:"2107 S HATHAWAY ST", city:"SANTA ANA", sau:"CA", postalCode:"92705-5238", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18SP"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: ""}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A587012-2001"}, {qualifier: "RSFD", value: "42711"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2914113", orderDate: "42690", orderType: "STND", quantity: 150, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "038707", supplierName: "DANVO MACHINING CO", lines: [{lineNumber:"18", itemNumber:"74A425043-2001", itemDescription:"PIN, STRAIGHT, HEADED", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"244", executeFrom:"01/17/2017", shipQuantity:"81", locations: {origin: {id:"038707", name:"DANVO MACHINING CO", address:"2107 S HATHAWAY ST", city:"SANTA ANA", sau:"CA", postalCode:"92705-5238", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18EF"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "SPM4A109G0004"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A425043-2001"}, {qualifier: "RSFD", value: "42782"}, {qualifier: "QTY", value: "81"}]}]},
{orderNumber: "2914113", orderDate: "42690", orderType: "STND", quantity: 124, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "038707", supplierName: "DANVO MACHINING CO", lines: [{lineNumber:"19", itemNumber:"74A588100-2027", itemDescription:"CONNECTOR,", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"245", executeFrom:"09/21/2016", shipQuantity:"1", locations: {origin: {id:"038707", name:"DANVO MACHINING CO", address:"2107 S HATHAWAY ST", city:"SANTA ANA", sau:"CA", postalCode:"92705-5238", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18EF"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "1889481"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A588100-2027"}, {qualifier: "RSFD", value: "42664"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "2914113", orderDate: "42690", orderType: "STND", quantity: 371, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "038707", supplierName: "DANVO MACHINING CO", lines: [{lineNumber:"20", itemNumber:"74A328966-2013-01", itemDescription:"BUSHING", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"246", executeFrom:"11/22/2016", shipQuantity:"9", locations: {origin: {id:"038707", name:"DANVO MACHINING CO", address:"2107 S HATHAWAY ST", city:"SANTA ANA", sau:"CA", postalCode:"92705-5238", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18EF"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "1889481"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A328966-2013-01"}, {qualifier: "RSFD", value: "42726"}, {qualifier: "QTY", value: "9"}]}]},
{orderNumber: "2914113", orderDate: "42690", orderType: "STND", quantity: 290, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "038707", supplierName: "DANVO MACHINING CO", lines: [{lineNumber:"20", itemNumber:"74A328966-2013-01", itemDescription:"BUSHING", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"247", executeFrom:"11/22/2016", shipQuantity:"0", locations: {origin: {id:"038707", name:"DANVO MACHINING CO", address:"2107 S HATHAWAY ST", city:"SANTA ANA", sau:"CA", postalCode:"92705-5238", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18EF"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "1889481"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A328966-2013-01"}, {qualifier: "RSFD", value: "42726"}, {qualifier: "QTY", value: "0"}]}]},
{orderNumber: "2914113", orderDate: "42690", orderType: "STND", quantity: 334, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "038707", supplierName: "DANVO MACHINING CO", lines: [{lineNumber:"21", itemNumber:"74A425042-2001", itemDescription:"ROLLER,", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"248", executeFrom:"01/11/2017", shipQuantity:"24", locations: {origin: {id:"038707", name:"DANVO MACHINING CO", address:"2107 S HATHAWAY ST", city:"SANTA ANA", sau:"CA", postalCode:"92705-5238", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18EF"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "1889481"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A425042-2001"}, {qualifier: "RSFD", value: "42776"}, {qualifier: "QTY", value: "24"}]}]},
{orderNumber: "2914113", orderDate: "42690", orderType: "STND", quantity: 40, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "038707", supplierName: "DANVO MACHINING CO", lines: [{lineNumber:"22", itemNumber:"74A838239-1013", itemDescription:"BOSS DETAILS", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"249", executeFrom:"08/02/2017", shipQuantity:"1", locations: {origin: {id:"038707", name:"DANVO MACHINING CO", address:"2107 S HATHAWAY ST", city:"SANTA ANA", sau:"CA", postalCode:"92705-5238", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18EF"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "SPM4A109G0004"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A838239-1013"}, {qualifier: "RSFD", value: "42979"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "2914113", orderDate: "42690", orderType: "STND", quantity: 442, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "038707", supplierName: "DANVO MACHINING CO", lines: [{lineNumber:"22", itemNumber:"74A838239-1013", itemDescription:"BOSS DETAILS", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"250", executeFrom:"06/28/2017", shipQuantity:"4", locations: {origin: {id:"038707", name:"DANVO MACHINING CO", address:"2107 S HATHAWAY ST", city:"SANTA ANA", sau:"CA", postalCode:"92705-5238", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18EF"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "SPM4A109G0004"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A838239-1013"}, {qualifier: "RSFD", value: "42944"}, {qualifier: "QTY", value: "4"}]}]},
{orderNumber: "2914113", orderDate: "42690", orderType: "STND", quantity: 485, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "038707", supplierName: "DANVO MACHINING CO", lines: [{lineNumber:"22", itemNumber:"74A838239-1013", itemDescription:"BOSS DETAILS", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"251", executeFrom:"04/05/2017", shipQuantity:"4", locations: {origin: {id:"038707", name:"DANVO MACHINING CO", address:"2107 S HATHAWAY ST", city:"SANTA ANA", sau:"CA", postalCode:"92705-5238", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18EF"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "SPM4A109G0004"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A838239-1013"}, {qualifier: "RSFD", value: "42860"}, {qualifier: "QTY", value: "4"}]}]},
{orderNumber: "2914113", orderDate: "42690", orderType: "STND", quantity: 247, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "038707", supplierName: "DANVO MACHINING CO", lines: [{lineNumber:"22", itemNumber:"74A838239-1013", itemDescription:"BOSS DETAILS", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"252", executeFrom:"02/08/2017", shipQuantity:"3", locations: {origin: {id:"038707", name:"DANVO MACHINING CO", address:"2107 S HATHAWAY ST", city:"SANTA ANA", sau:"CA", postalCode:"92705-5238", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18EF"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "SPM4A109G0004"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A838239-1013"}, {qualifier: "RSFD", value: "42804"}, {qualifier: "QTY", value: "3"}]}]},
{orderNumber: "2914113", orderDate: "42690", orderType: "STND", quantity: 352, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "038707", supplierName: "DANVO MACHINING CO", lines: [{lineNumber:"23", itemNumber:"74A838239-2049", itemDescription:"TRUNNION", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"253", executeFrom:"09/11/2017", shipQuantity:"1", locations: {origin: {id:"038707", name:"DANVO MACHINING CO", address:"2107 S HATHAWAY ST", city:"SANTA ANA", sau:"CA", postalCode:"92705-5238", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18EF"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "SPM4A109G0004"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A838239-2049"}, {qualifier: "RSFD", value: "43019"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "2914113", orderDate: "42690", orderType: "STND", quantity: 134, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "038707", supplierName: "DANVO MACHINING CO", lines: [{lineNumber:"23", itemNumber:"74A838239-2049", itemDescription:"TRUNNION", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"254", executeFrom:"07/16/2017", shipQuantity:"4", locations: {origin: {id:"038707", name:"DANVO MACHINING CO", address:"2107 S HATHAWAY ST", city:"SANTA ANA", sau:"CA", postalCode:"92705-5238", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18EF"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "SPM4A109G0004"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A838239-2049"}, {qualifier: "RSFD", value: "42962"}, {qualifier: "QTY", value: "4"}]}]},
{orderNumber: "2914113", orderDate: "42690", orderType: "STND", quantity: 118, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "038707", supplierName: "DANVO MACHINING CO", lines: [{lineNumber:"23", itemNumber:"74A838239-2049", itemDescription:"TRUNNION", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"255", executeFrom:"05/16/2017", shipQuantity:"4", locations: {origin: {id:"038707", name:"DANVO MACHINING CO", address:"2107 S HATHAWAY ST", city:"SANTA ANA", sau:"CA", postalCode:"92705-5238", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18EF"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "SPM4A109G0004"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A838239-2049"}, {qualifier: "RSFD", value: "42901"}, {qualifier: "QTY", value: "4"}]}]},
{orderNumber: "2914113", orderDate: "42690", orderType: "STND", quantity: 475, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "038707", supplierName: "DANVO MACHINING CO", lines: [{lineNumber:"23", itemNumber:"74A838239-2049", itemDescription:"TRUNNION", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"256", executeFrom:"01/11/2017", shipQuantity:"3", locations: {origin: {id:"038707", name:"DANVO MACHINING CO", address:"2107 S HATHAWAY ST", city:"SANTA ANA", sau:"CA", postalCode:"92705-5238", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18EF"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "SPM4A109G0004"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A838239-2049"}, {qualifier: "RSFD", value: "42776"}, {qualifier: "QTY", value: "3"}]}]},
{orderNumber: "2914113", orderDate: "42690", orderType: "STND", quantity: 113, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "038707", supplierName: "DANVO MACHINING CO", lines: [{lineNumber:"24", itemNumber:"74A588245-2001", itemDescription:"FLANGE,", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"257", executeFrom:"12/07/2016", shipQuantity:"8", locations: {origin: {id:"038707", name:"DANVO MACHINING CO", address:"2107 S HATHAWAY ST", city:"SANTA ANA", sau:"CA", postalCode:"92705-5238", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18SP"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "SPRPA1-11-G-002Z"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A588245-2001"}, {qualifier: "RSFD", value: "42741"}, {qualifier: "QTY", value: "8"}]}]},
{orderNumber: "2914113", orderDate: "42690", orderType: "STND", quantity: 166, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "038707", supplierName: "DANVO MACHINING CO", lines: [{lineNumber:"34", itemNumber:"74A341654-2023", itemDescription:"BUSHING,", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"258", executeFrom:"11/01/2017", shipQuantity:"81", locations: {origin: {id:"038707", name:"DANVO MACHINING CO", address:"2107 S HATHAWAY ST", city:"SANTA ANA", sau:"CA", postalCode:"92705-5238", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18EF"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "1224892"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A341654-2023"}, {qualifier: "RSFD", value: "43070"}, {qualifier: "QTY", value: "81"}]}]},
{orderNumber: "2914113", orderDate: "42690", orderType: "STND", quantity: 158, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "038707", supplierName: "DANVO MACHINING CO", lines: [{lineNumber:"34", itemNumber:"74A341654-2023", itemDescription:"BUSHING,", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"259", executeFrom:"05/21/2017", shipQuantity:"297", locations: {origin: {id:"038707", name:"DANVO MACHINING CO", address:"2107 S HATHAWAY ST", city:"SANTA ANA", sau:"CA", postalCode:"92705-5238", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18EF"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "1224892"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A341654-2023"}, {qualifier: "RSFD", value: "42906"}, {qualifier: "QTY", value: "297"}]}]},
{orderNumber: "2914113", orderDate: "42690", orderType: "STND", quantity: 139, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "038707", supplierName: "DANVO MACHINING CO", lines: [{lineNumber:"35", itemNumber:"74A425039-2005", itemDescription:"RETAINER", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"260", executeFrom:"02/28/2017", shipQuantity:"36", locations: {origin: {id:"038707", name:"DANVO MACHINING CO", address:"2107 S HATHAWAY ST", city:"SANTA ANA", sau:"CA", postalCode:"92705-5238", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18EF"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "1189481"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A425039-2005"}, {qualifier: "RSFD", value: "42824"}, {qualifier: "QTY", value: "36"}]}]},
{orderNumber: "2914113", orderDate: "42690", orderType: "STND", quantity: 468, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "038707", supplierName: "DANVO MACHINING CO", lines: [{lineNumber:"36", itemNumber:"74A585706-2003", itemDescription:"CONNECTOR,", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"261", executeFrom:"04/18/2017", shipQuantity:"8", locations: {origin: {id:"038707", name:"DANVO MACHINING CO", address:"2107 S HATHAWAY ST", city:"SANTA ANA", sau:"CA", postalCode:"92705-5238", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18EF"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "1224892"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A585706-2003"}, {qualifier: "RSFD", value: "42873"}, {qualifier: "QTY", value: "8"}]}]},
{orderNumber: "2914113", orderDate: "42690", orderType: "STND", quantity: 78, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "038707", supplierName: "DANVO MACHINING CO", lines: [{lineNumber:"36", itemNumber:"74A585706-2003", itemDescription:"CONNECTOR,", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"262", executeFrom:"02/28/2017", shipQuantity:"6", locations: {origin: {id:"038707", name:"DANVO MACHINING CO", address:"2107 S HATHAWAY ST", city:"SANTA ANA", sau:"CA", postalCode:"92705-5238", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18EF"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "1224892"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A585706-2003"}, {qualifier: "RSFD", value: "42824"}, {qualifier: "QTY", value: "6"}]}]},
{orderNumber: "2914113", orderDate: "42690", orderType: "STND", quantity: 72, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "038707", supplierName: "DANVO MACHINING CO", lines: [{lineNumber:"37", itemNumber:"74A585735-2001", itemDescription:"RETAINER,", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"263", executeFrom:"04/04/2017", shipQuantity:"12", locations: {origin: {id:"038707", name:"DANVO MACHINING CO", address:"2107 S HATHAWAY ST", city:"SANTA ANA", sau:"CA", postalCode:"92705-5238", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18EF"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "1189481"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A585735-2001"}, {qualifier: "RSFD", value: "42859"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2914113", orderDate: "42690", orderType: "STND", quantity: 131, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "038707", supplierName: "DANVO MACHINING CO", lines: [{lineNumber:"37", itemNumber:"74A585735-2001", itemDescription:"RETAINER,", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"264", executeFrom:"02/28/2017", shipQuantity:"12", locations: {origin: {id:"038707", name:"DANVO MACHINING CO", address:"2107 S HATHAWAY ST", city:"SANTA ANA", sau:"CA", postalCode:"92705-5238", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18EF"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "1189481"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A585735-2001"}, {qualifier: "RSFD", value: "42824"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2914113", orderDate: "42690", orderType: "STND", quantity: 3, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "038707", supplierName: "DANVO MACHINING CO", lines: [{lineNumber:"38", itemNumber:"74A586326-2025", itemDescription:"ADAPTER,", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"265", executeFrom:"03/07/2017", shipQuantity:"3", locations: {origin: {id:"038707", name:"DANVO MACHINING CO", address:"2107 S HATHAWAY ST", city:"SANTA ANA", sau:"CA", postalCode:"92705-5238", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18SP"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "SPRPA1-11-G-002Z"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A586326-2025"}, {qualifier: "RSFD", value: "42831"}, {qualifier: "QTY", value: "3"}]}]},
{orderNumber: "2914113", orderDate: "42690", orderType: "STND", quantity: 403, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "038707", supplierName: "DANVO MACHINING CO", lines: [{lineNumber:"39", itemNumber:"74A586482-2041", itemDescription:"FITTING,", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"266", executeFrom:"03/07/2017", shipQuantity:"12", locations: {origin: {id:"038707", name:"DANVO MACHINING CO", address:"2107 S HATHAWAY ST", city:"SANTA ANA", sau:"CA", postalCode:"92705-5238", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18SP"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "SPRPA115G001Z"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A586482-2041"}, {qualifier: "RSFD", value: "42831"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2914113", orderDate: "42690", orderType: "STND", quantity: 76, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "038707", supplierName: "DANVO MACHINING CO", lines: [{lineNumber:"40", itemNumber:"74A586482-2043", itemDescription:"BOSS,", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"267", executeFrom:"03/07/2017", shipQuantity:"12", locations: {origin: {id:"038707", name:"DANVO MACHINING CO", address:"2107 S HATHAWAY ST", city:"SANTA ANA", sau:"CA", postalCode:"92705-5238", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18SP"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "SPRPA115G001Z"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A586482-2043"}, {qualifier: "RSFD", value: "42831"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2914113", orderDate: "42690", orderType: "STND", quantity: 188, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "038707", supplierName: "DANVO MACHINING CO", lines: [{lineNumber:"41", itemNumber:"74A586723-2001", itemDescription:"CONNECTOR,", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"268", executeFrom:"12/19/2017", shipQuantity:"24", locations: {origin: {id:"038707", name:"DANVO MACHINING CO", address:"2107 S HATHAWAY ST", city:"SANTA ANA", sau:"CA", postalCode:"92705-5238", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18EF"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "1224892"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A586723-2001"}, {qualifier: "RSFD", value: "43118"}, {qualifier: "QTY", value: "24"}]}]},
{orderNumber: "2914113", orderDate: "42690", orderType: "STND", quantity: 448, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "038707", supplierName: "DANVO MACHINING CO", lines: [{lineNumber:"41", itemNumber:"74A586723-2001", itemDescription:"CONNECTOR,", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"269", executeFrom:"02/28/2017", shipQuantity:"12", locations: {origin: {id:"038707", name:"DANVO MACHINING CO", address:"2107 S HATHAWAY ST", city:"SANTA ANA", sau:"CA", postalCode:"92705-5238", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18EF"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "1224892"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A586723-2001"}, {qualifier: "RSFD", value: "42824"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2914113", orderDate: "42690", orderType: "STND", quantity: 447, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "038707", supplierName: "DANVO MACHINING CO", lines: [{lineNumber:"42", itemNumber:"74A586944-2001", itemDescription:"ELBOW,", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"270", executeFrom:"02/01/2017", shipQuantity:"15", locations: {origin: {id:"038707", name:"DANVO MACHINING CO", address:"2107 S HATHAWAY ST", city:"SANTA ANA", sau:"CA", postalCode:"92705-5238", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18SP"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "SPRPA115G001Z"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A586944-2001"}, {qualifier: "RSFD", value: "42797"}, {qualifier: "QTY", value: "15"}]}]},
{orderNumber: "2914113", orderDate: "42690", orderType: "STND", quantity: 465, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "038707", supplierName: "DANVO MACHINING CO", lines: [{lineNumber:"43", itemNumber:"74A587012-2001", itemDescription:"FERRULE,", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"271", executeFrom:"03/04/2017", shipQuantity:"45", locations: {origin: {id:"038707", name:"DANVO MACHINING CO", address:"2107 S HATHAWAY ST", city:"SANTA ANA", sau:"CA", postalCode:"92705-5238", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18SP"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "SPRPA115G001Z"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A587012-2001"}, {qualifier: "RSFD", value: "42828"}, {qualifier: "QTY", value: "45"}]}]},
{orderNumber: "2914113", orderDate: "42690", orderType: "STND", quantity: 368, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "038707", supplierName: "DANVO MACHINING CO", lines: [{lineNumber:"44", itemNumber:"74A686471-2001", itemDescription:"PIN,", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"272", executeFrom:"08/02/2017", shipQuantity:"8", locations: {origin: {id:"038707", name:"DANVO MACHINING CO", address:"2107 S HATHAWAY ST", city:"SANTA ANA", sau:"CA", postalCode:"92705-5238", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18EF"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "1224892"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A686471-2001"}, {qualifier: "RSFD", value: "42979"}, {qualifier: "QTY", value: "8"}]}]},
{orderNumber: "2914113", orderDate: "42690", orderType: "STND", quantity: 220, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "038707", supplierName: "DANVO MACHINING CO", lines: [{lineNumber:"44", itemNumber:"74A686471-2001", itemDescription:"PIN,", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"273", executeFrom:"05/07/2017", shipQuantity:"6", locations: {origin: {id:"038707", name:"DANVO MACHINING CO", address:"2107 S HATHAWAY ST", city:"SANTA ANA", sau:"CA", postalCode:"92705-5238", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18EF"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "1224892"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A686471-2001"}, {qualifier: "RSFD", value: "42892"}, {qualifier: "QTY", value: "6"}]}]},
{orderNumber: "2922222", orderDate: "42690", orderType: "SUBK", quantity: 18, sector:"ES - Electronic Systems", psa:false, incoTerms: "FOB Destination", supplierId: "60435", supplierName: "XACTIV INC", lines: [{lineNumber:"9", itemNumber:"", itemDescription:"TASK 5  8.1.5", fulfillmentMethod:"LO", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"274", executeFrom:"08/16/2016", shipQuantity:"1", locations: {origin: {id:"60435", name:"XACTIV INC", address:"71 PERINTON PKWY", city:"FAIRPORT", sau:"NY", postalCode:"14450-9104", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"3520 E AVE M - SITE 3", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "NCTAX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "4"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "SEE CHARGE TEXT"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42628"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "2929652", orderDate: "42690", orderType: "STND", quantity: 407, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "38707", supplierName: "DANVO MACHINING CO", lines: [{lineNumber:"1", itemNumber:"74A541136-2007", itemDescription:"MOUNT,", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"275", executeFrom:"08/31/2016", shipQuantity:"0", locations: {origin: {id:"38707", name:"DANVO MACHINING CO", address:"2107 S HATHAWAY ST", city:"SANTA ANA", sau:"CA", postalCode:"92705-5238", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18SP"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "SPRPA115G001Z"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A541136-2007"}, {qualifier: "RSFD", value: "42643"}, {qualifier: "QTY", value: "0"}]}]},
{orderNumber: "2933342", orderDate: "42690", orderType: "STND", quantity: 292, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "37130", supplierName: "COAST PRECISION ENTERPRISES INC", lines: [{lineNumber:"4", itemNumber:"3671213U01247-13", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"276", executeFrom:"03/18/2017", shipQuantity:"1", locations: {origin: {id:"37130", name:"COAST PRECISION ENTERPRISES INC", address:"8636 COMMERCE AVE", city:"SAN DIEGO", sau:"CA", postalCode:"92121-2613", country:"US"}, destination: {id:"30MPM", name:"NORTHROP GRUMMAN CORP.", address:"8319 AVTECH PARKWAY", city:"MOSS POINT", sau:"MS", postalCode:"39563", country:"US"}}, references: [{qualifier: "P4", value: "GHAWK"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "MPM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "FA862015C3001"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "36"}, {qualifier: "IL", value: "71213U01247-13"}, {qualifier: "RSFD", value: "42842"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "2933342", orderDate: "42690", orderType: "STND", quantity: 12, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "37130", supplierName: "COAST PRECISION ENTERPRISES INC", lines: [{lineNumber:"4", itemNumber:"3671213U01247-13", itemDescription:"CLIP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"277", executeFrom:"03/18/2017", shipQuantity:"2", locations: {origin: {id:"37130", name:"COAST PRECISION ENTERPRISES INC", address:"8636 COMMERCE AVE", city:"SAN DIEGO", sau:"CA", postalCode:"92121-2613", country:"US"}, destination: {id:"30MPM", name:"NORTHROP GRUMMAN CORP.", address:"8319 AVTECH PARKWAY", city:"MOSS POINT", sau:"MS", postalCode:"39563", country:"US"}}, references: [{qualifier: "P4", value: "GHAWK"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "MPM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "FA862015C3001"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "36"}, {qualifier: "IL", value: "71213U01247-13"}, {qualifier: "RSFD", value: "42842"}, {qualifier: "QTY", value: "2"}]}]},
{orderNumber: "2938415", orderDate: "42690", orderType: "SUBK", quantity: 171, sector:"ES - Electronic Systems", psa:false, incoTerms: "FOB Destination", supplierId: "55765", supplierName: "CURTISS WRIGHT CONTROLS ELECTRONIC", lines: [{lineNumber:"1", itemNumber:"", itemDescription:"PME REPAIR MGT POP 01 NO", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"278", executeFrom:"10/31/2016", shipQuantity:"1", locations: {origin: {id:"55765", name:"CURTISS WRIGHT CONTROLS ELECTRONIC", address:"28965 AVENUE PENN", city:"SANTA CLARITA", sau:"CA", postalCode:"91355-4185", country:"US"}, destination: {id:"30WAR", name:"GARY HOKANSON", address:"440 JOE TAMPLIN INDUSTRIAL BLVD, BLDG 5", city:"MACON", sau:"GA", postalCode:"31217", country:"US"}}, references: [{qualifier: "P4", value: "JSTAR"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "WAR"}, {qualifier: "12", value: "4"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "F0960300D0210"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42704"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "2938415", orderDate: "42690", orderType: "SUBK", quantity: 49, sector:"ES - Electronic Systems", psa:false, incoTerms: "FOB Destination", supplierId: "55765", supplierName: "CURTISS WRIGHT CONTROLS ELECTRONIC", lines: [{lineNumber:"2", itemNumber:"", itemDescription:"PME REPAIR MGT POP 01 DE", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"279", executeFrom:"12/01/2016", shipQuantity:"1", locations: {origin: {id:"55765", name:"CURTISS WRIGHT CONTROLS ELECTRONIC", address:"28965 AVENUE PENN", city:"SANTA CLARITA", sau:"CA", postalCode:"91355-4185", country:"US"}, destination: {id:"30WAR", name:"GARY HOKANSON", address:"440 JOE TAMPLIN INDUSTRIAL BLVD, BLDG 5", city:"MACON", sau:"GA", postalCode:"31217", country:"US"}}, references: [{qualifier: "P4", value: "JSTAR"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "WAR"}, {qualifier: "12", value: "4"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "F0960300D0210"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42735"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "2938415", orderDate: "42690", orderType: "SUBK", quantity: 1, sector:"ES - Electronic Systems", psa:false, incoTerms: "FOB Destination", supplierId: "55765", supplierName: "CURTISS WRIGHT CONTROLS ELECTRONIC", lines: [{lineNumber:"3", itemNumber:"", itemDescription:"PME REPAIR MGT POP 01 JA", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"280", executeFrom:"01/01/2017", shipQuantity:"1", locations: {origin: {id:"55765", name:"CURTISS WRIGHT CONTROLS ELECTRONIC", address:"28965 AVENUE PENN", city:"SANTA CLARITA", sau:"CA", postalCode:"91355-4185", country:"US"}, destination: {id:"30WAR", name:"GARY HOKANSON", address:"440 JOE TAMPLIN INDUSTRIAL BLVD, BLDG 5", city:"MACON", sau:"GA", postalCode:"31217", country:"US"}}, references: [{qualifier: "P4", value: "JSTAR"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "WAR"}, {qualifier: "12", value: "4"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "F0960300D0210"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42766"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "2938415", orderDate: "42690", orderType: "SUBK", quantity: 29, sector:"ES - Electronic Systems", psa:false, incoTerms: "FOB Destination", supplierId: "55765", supplierName: "CURTISS WRIGHT CONTROLS ELECTRONIC", lines: [{lineNumber:"4", itemNumber:"", itemDescription:"PME REPAIR MGT POP 01 FE", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"281", executeFrom:"01/29/2017", shipQuantity:"1", locations: {origin: {id:"55765", name:"CURTISS WRIGHT CONTROLS ELECTRONIC", address:"28965 AVENUE PENN", city:"SANTA CLARITA", sau:"CA", postalCode:"91355-4185", country:"US"}, destination: {id:"30WAR", name:"GARY HOKANSON", address:"440 JOE TAMPLIN INDUSTRIAL BLVD, BLDG 5", city:"MACON", sau:"GA", postalCode:"31217", country:"US"}}, references: [{qualifier: "P4", value: "JSTAR"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "WAR"}, {qualifier: "12", value: "4"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "F0960300D0210"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42794"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "2938415", orderDate: "42690", orderType: "SUBK", quantity: 256, sector:"ES - Electronic Systems", psa:false, incoTerms: "FOB Destination", supplierId: "55765", supplierName: "CURTISS WRIGHT CONTROLS ELECTRONIC", lines: [{lineNumber:"5", itemNumber:"", itemDescription:"PME REPAIR MGT POP 01 MA", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"282", executeFrom:"03/01/2017", shipQuantity:"1", locations: {origin: {id:"55765", name:"CURTISS WRIGHT CONTROLS ELECTRONIC", address:"28965 AVENUE PENN", city:"SANTA CLARITA", sau:"CA", postalCode:"91355-4185", country:"US"}, destination: {id:"30WAR", name:"GARY HOKANSON", address:"440 JOE TAMPLIN INDUSTRIAL BLVD, BLDG 5", city:"MACON", sau:"GA", postalCode:"31217", country:"US"}}, references: [{qualifier: "P4", value: "JSTAR"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "WAR"}, {qualifier: "12", value: "4"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "F0960300D0210"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42825"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "2938415", orderDate: "42690", orderType: "SUBK", quantity: 201, sector:"ES - Electronic Systems", psa:false, incoTerms: "FOB Destination", supplierId: "55765", supplierName: "CURTISS WRIGHT CONTROLS ELECTRONIC", lines: [{lineNumber:"6", itemNumber:"", itemDescription:"PME REPAIR MGT POP 01 AP", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"283", executeFrom:"03/31/2017", shipQuantity:"1", locations: {origin: {id:"55765", name:"CURTISS WRIGHT CONTROLS ELECTRONIC", address:"28965 AVENUE PENN", city:"SANTA CLARITA", sau:"CA", postalCode:"91355-4185", country:"US"}, destination: {id:"30WAR", name:"GARY HOKANSON", address:"440 JOE TAMPLIN INDUSTRIAL BLVD, BLDG 5", city:"MACON", sau:"GA", postalCode:"31217", country:"US"}}, references: [{qualifier: "P4", value: "JSTAR"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "WAR"}, {qualifier: "12", value: "4"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "F0960300D0210"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42855"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "2938415", orderDate: "42690", orderType: "SUBK", quantity: 76, sector:"ES - Electronic Systems", psa:false, incoTerms: "FOB Destination", supplierId: "55765", supplierName: "CURTISS WRIGHT CONTROLS ELECTRONIC", lines: [{lineNumber:"7", itemNumber:"", itemDescription:"PME REPAIR MGT POP 01 MA", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"284", executeFrom:"05/01/2017", shipQuantity:"1", locations: {origin: {id:"55765", name:"CURTISS WRIGHT CONTROLS ELECTRONIC", address:"28965 AVENUE PENN", city:"SANTA CLARITA", sau:"CA", postalCode:"91355-4185", country:"US"}, destination: {id:"30WAR", name:"GARY HOKANSON", address:"440 JOE TAMPLIN INDUSTRIAL BLVD, BLDG 5", city:"MACON", sau:"GA", postalCode:"31217", country:"US"}}, references: [{qualifier: "P4", value: "JSTAR"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "WAR"}, {qualifier: "12", value: "4"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "F0960300D0210"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42886"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "2938415", orderDate: "42690", orderType: "SUBK", quantity: 178, sector:"ES - Electronic Systems", psa:false, incoTerms: "FOB Destination", supplierId: "55765", supplierName: "CURTISS WRIGHT CONTROLS ELECTRONIC", lines: [{lineNumber:"8", itemNumber:"", itemDescription:"PME REPAIR MGT POP 01 JU", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"285", executeFrom:"05/31/2017", shipQuantity:"1", locations: {origin: {id:"55765", name:"CURTISS WRIGHT CONTROLS ELECTRONIC", address:"28965 AVENUE PENN", city:"SANTA CLARITA", sau:"CA", postalCode:"91355-4185", country:"US"}, destination: {id:"30WAR", name:"GARY HOKANSON", address:"440 JOE TAMPLIN INDUSTRIAL BLVD, BLDG 5", city:"MACON", sau:"GA", postalCode:"31217", country:"US"}}, references: [{qualifier: "P4", value: "JSTAR"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "WAR"}, {qualifier: "12", value: "4"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "F0960300D0210"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42916"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "2938415", orderDate: "42690", orderType: "SUBK", quantity: 317, sector:"ES - Electronic Systems", psa:false, incoTerms: "FOB Destination", supplierId: "55765", supplierName: "CURTISS WRIGHT CONTROLS ELECTRONIC", lines: [{lineNumber:"9", itemNumber:"", itemDescription:"PME REPAIR MGT POP 01 JU", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"286", executeFrom:"07/01/2017", shipQuantity:"1", locations: {origin: {id:"55765", name:"CURTISS WRIGHT CONTROLS ELECTRONIC", address:"28965 AVENUE PENN", city:"SANTA CLARITA", sau:"CA", postalCode:"91355-4185", country:"US"}, destination: {id:"30WAR", name:"GARY HOKANSON", address:"440 JOE TAMPLIN INDUSTRIAL BLVD, BLDG 5", city:"MACON", sau:"GA", postalCode:"31217", country:"US"}}, references: [{qualifier: "P4", value: "JSTAR"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "WAR"}, {qualifier: "12", value: "4"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "F0960300D0210"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42947"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "2938415", orderDate: "42690", orderType: "SUBK", quantity: 295, sector:"ES - Electronic Systems", psa:false, incoTerms: "FOB Destination", supplierId: "55765", supplierName: "CURTISS WRIGHT CONTROLS ELECTRONIC", lines: [{lineNumber:"10", itemNumber:"", itemDescription:"PME REPAIR MGT POP 01 AU", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"287", executeFrom:"08/01/2017", shipQuantity:"1", locations: {origin: {id:"55765", name:"CURTISS WRIGHT CONTROLS ELECTRONIC", address:"28965 AVENUE PENN", city:"SANTA CLARITA", sau:"CA", postalCode:"91355-4185", country:"US"}, destination: {id:"30WAR", name:"GARY HOKANSON", address:"440 JOE TAMPLIN INDUSTRIAL BLVD, BLDG 5", city:"MACON", sau:"GA", postalCode:"31217", country:"US"}}, references: [{qualifier: "P4", value: "JSTAR"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "WAR"}, {qualifier: "12", value: "4"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "F0960300D0210"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42978"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "2938415", orderDate: "42690", orderType: "SUBK", quantity: 226, sector:"ES - Electronic Systems", psa:false, incoTerms: "FOB Destination", supplierId: "55765", supplierName: "CURTISS WRIGHT CONTROLS ELECTRONIC", lines: [{lineNumber:"11", itemNumber:"", itemDescription:"PME REPAIR MGT POP 01 SE", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"288", executeFrom:"08/31/2017", shipQuantity:"1", locations: {origin: {id:"55765", name:"CURTISS WRIGHT CONTROLS ELECTRONIC", address:"28965 AVENUE PENN", city:"SANTA CLARITA", sau:"CA", postalCode:"91355-4185", country:"US"}, destination: {id:"30WAR", name:"GARY HOKANSON", address:"440 JOE TAMPLIN INDUSTRIAL BLVD, BLDG 5", city:"MACON", sau:"GA", postalCode:"31217", country:"US"}}, references: [{qualifier: "P4", value: "JSTAR"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "WAR"}, {qualifier: "12", value: "4"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "F0960300D0210"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "43008"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "2938415", orderDate: "42690", orderType: "SUBK", quantity: 277, sector:"ES - Electronic Systems", psa:false, incoTerms: "FOB Destination", supplierId: "55765", supplierName: "CURTISS WRIGHT CONTROLS ELECTRONIC", lines: [{lineNumber:"12", itemNumber:"", itemDescription:"PME REPAIR MGT POP 01 OC", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"289", executeFrom:"10/01/2017", shipQuantity:"1", locations: {origin: {id:"55765", name:"CURTISS WRIGHT CONTROLS ELECTRONIC", address:"28965 AVENUE PENN", city:"SANTA CLARITA", sau:"CA", postalCode:"91355-4185", country:"US"}, destination: {id:"30WAR", name:"GARY HOKANSON", address:"440 JOE TAMPLIN INDUSTRIAL BLVD, BLDG 5", city:"MACON", sau:"GA", postalCode:"31217", country:"US"}}, references: [{qualifier: "P4", value: "JSTAR"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "WAR"}, {qualifier: "12", value: "4"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "F0960300D0210"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "43039"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "2939392", orderDate: "42690", orderType: "STND", quantity: 385, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "24458", supplierName: "ANILLO INDUSTRIES INC", lines: [{lineNumber:"1", itemNumber:"NAS1149D2290J", itemDescription:"WASHER, FLAT,", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"290", executeFrom:"11/16/2016", shipQuantity:"100", locations: {origin: {id:"24458", name:"ANILLO INDUSTRIES INC", address:"2090 N GLASSELL ST", city:"ORANGE", sau:"CA", postalCode:"92865-3306", country:"US"}, destination: {id:"30MEL", name:"NORTHROP GRUMMAN CORPORATION", address:"2000 W NASA BLVD", city:"MELBOURNE", sau:"FL", postalCode:"32904", country:"US"}}, references: [{qualifier: "P4", value: "MISCX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "MEL"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N/A"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "NA"}, {qualifier: "IL", value: "S1149D2290J"}, {qualifier: "RSFD", value: "42720"}, {qualifier: "QTY", value: "100"}]}]},
{orderNumber: "2940738", orderDate: "42690", orderType: "STND", quantity: 346, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "51610", supplierName: "MAY TECHNOLOGY & MFG INC", lines: [{lineNumber:"1", itemNumber:"E6KSPBMO", itemDescription:"GAUGE, 0-6000 PSI", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"291", executeFrom:"03/13/2017", shipQuantity:"10", locations: {origin: {id:"51610", name:"MAY TECHNOLOGY & MFG INC", address:"2922 WHEELING AVE", city:"KANSAS CITY", sau:"MO", postalCode:"64129-1165", country:"US"}, destination: {id:"30AVM", name:"Q95433", address:"5300 ALLIANCE GATEWAY FREEWAY, SUITE 100", city:"FORT WORTH", sau:"TX", postalCode:"76177", country:"US"}}, references: [{qualifier: "P4", value: "JSFXX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "AVM"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "6533REPAIRREPLENS"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "E6"}, {qualifier: "IL", value: "KSPBMO"}, {qualifier: "RSFD", value: "42837"}, {qualifier: "QTY", value: "10"}]}]},
{orderNumber: "2941055", orderDate: "42690", orderType: "STND", quantity: 195, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "52313", supplierName: "PRC DESOTO INTERNATIONAL INC", lines: [{lineNumber:"1", itemNumber:"GC130AS2B", itemDescription:"COATING", fulfillmentMethod:"EA", fulfillmentTolerance:"10", status:"FALSE", deliveryScheduleNumber:"292", executeFrom:"11/12/2016", shipQuantity:"38", locations: {origin: {id:"52313", name:"PRC DESOTO INTERNATIONAL INC", address:"3330 W TOWN POINT DR STE F", city:"KENNESAW", sau:"GA", postalCode:"30144-7033", country:"US"}, destination: {id:"30MEL", name:"NORTHROP GRUMMAN CORPORATION", address:"2000 W NASA BLVD", city:"MELBOURNE", sau:"FL", postalCode:"32904", country:"US"}}, references: [{qualifier: "P4", value: "MISCX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "MEL"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N/A"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "GC"}, {qualifier: "IL", value: "130AS2B"}, {qualifier: "RSFD", value: "42716"}, {qualifier: "QTY", value: "38"}]}]},
{orderNumber: "2941081", orderDate: "42690", orderType: "STND", quantity: 492, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "64387", supplierName: "KLX INC", lines: [{lineNumber:"1", itemNumber:"MS3348-8-10", itemDescription:"BUSHING,CONTACT", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"293", executeFrom:"10/23/2016", shipQuantity:"18", locations: {origin: {id:"64387", name:"KLX INC", address:"10000 NW 15TH TER", city:"DORAL", sau:"FL", postalCode:"33172-2754", country:"US"}, destination: {id:"30MEL", name:"NORTHROP GRUMMAN CORPORATION", address:"2000 W NASA BLVD", city:"MELBOURNE", sau:"FL", postalCode:"32904", country:"US"}}, references: [{qualifier: "P4", value: "MISCX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "MEL"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N/A"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "MS"}, {qualifier: "IL", value: "529095"}, {qualifier: "RSFD", value: "42696"}, {qualifier: "QTY", value: "18"}]}]},
{orderNumber: "2941108", orderDate: "42690", orderType: "STND", quantity: 404, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "48574", supplierName: "AIRMO INC", lines: [{lineNumber:"1", itemNumber:"", itemDescription:"64-0000-0375", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"294", executeFrom:"11/05/2016", shipQuantity:"1", locations: {origin: {id:"48574", name:"AIRMO INC", address:"9445  EVERGREEN BLVD NW", city:"COON RAPIDS", sau:"MN", postalCode:"55433-5840", country:"US"}, destination: {id:"30PAL", name:"VILLA, CYNTHIA", address:"3520 EAST AVENUE M BLDG 401", city:"PALMDALE", sau:"CA", postalCode:"93550", country:"US"}}, references: [{qualifier: "P4", value: "INDIR"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "PAL"}, {qualifier: "12", value: "4"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "SEE CHARGE TEXT"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42709"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "2941214", orderDate: "42690", orderType: "STND", quantity: 57, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "52495", supplierName: "RALC DBA CNC MANUFACTURING", lines: [{lineNumber:"1", itemNumber:"74A426064-2009", itemDescription:"FORMER,", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"295", executeFrom:"02/25/2017", shipQuantity:"2", locations: {origin: {id:"52495", name:"RALC DBA CNC MANUFACTURING", address:"42158 SARAH WAY", city:"TEMECULA", sau:"CA", postalCode:"92590-3401", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18SP"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "SPRPA111G002Z"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A426064-2009"}, {qualifier: "RSFD", value: "42821"}, {qualifier: "QTY", value: "2"}]}]},
{orderNumber: "2941214", orderDate: "42690", orderType: "STND", quantity: 430, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "52495", supplierName: "RALC DBA CNC MANUFACTURING", lines: [{lineNumber:"2", itemNumber:"74A426064-2009", itemDescription:"FORMER,", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"296", executeFrom:"02/25/2017", shipQuantity:"1", locations: {origin: {id:"52495", name:"RALC DBA CNC MANUFACTURING", address:"42158 SARAH WAY", city:"TEMECULA", sau:"CA", postalCode:"92590-3401", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18SP"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "SPRPA111G002Z"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A426064-2009"}, {qualifier: "RSFD", value: "42821"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "2941214", orderDate: "42690", orderType: "STND", quantity: 174, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "52495", supplierName: "RALC DBA CNC MANUFACTURING", lines: [{lineNumber:"3", itemNumber:"74A426064-2009", itemDescription:"FORMER,", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"297", executeFrom:"02/25/2017", shipQuantity:"1", locations: {origin: {id:"52495", name:"RALC DBA CNC MANUFACTURING", address:"42158 SARAH WAY", city:"TEMECULA", sau:"CA", postalCode:"92590-3401", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18SP"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "SPRPA111G002Z"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A426064-2009"}, {qualifier: "RSFD", value: "42821"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "2941214", orderDate: "42690", orderType: "STND", quantity: 246, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "52495", supplierName: "RALC DBA CNC MANUFACTURING", lines: [{lineNumber:"4", itemNumber:"74A426064-2009", itemDescription:"FORMER,", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"298", executeFrom:"02/25/2017", shipQuantity:"1", locations: {origin: {id:"52495", name:"RALC DBA CNC MANUFACTURING", address:"42158 SARAH WAY", city:"TEMECULA", sau:"CA", postalCode:"92590-3401", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18SP"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "SPRPA111G002Z"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A426064-2009"}, {qualifier: "RSFD", value: "42821"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "2941214", orderDate: "42690", orderType: "STND", quantity: 28, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "52495", supplierName: "RALC DBA CNC MANUFACTURING", lines: [{lineNumber:"5", itemNumber:"74A426064-2009", itemDescription:"FORMER,", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"299", executeFrom:"02/25/2017", shipQuantity:"1", locations: {origin: {id:"52495", name:"RALC DBA CNC MANUFACTURING", address:"42158 SARAH WAY", city:"TEMECULA", sau:"CA", postalCode:"92590-3401", country:"US"}, destination: {id:"30SEG", name:"NORTHROP GRUMMAN CORPORATION", address:"700 N. DOUGLAS ST. - ENTRANCE 2", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "F18SP"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "SPRPA111G002Z"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "74"}, {qualifier: "IL", value: "A426064-2009"}, {qualifier: "RSFD", value: "42821"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "2941215", orderDate: "42690", orderType: "NGBK", quantity: 28, sector:"TS - Technology Services", incoTerms: "FOB Destination", supplierId: "33249", supplierName: "HURLEN CORPORATION", lines: [{lineNumber:"1", itemNumber:"6A0200-0502B090", itemDescription:"AL SHT, 6061, T4, 0.090I", fulfillmentMethod:"SI", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"300", executeFrom:"10/18/2016", shipQuantity:"3456", locations: {origin: {id:"33249", name:"HURLEN CORPORATION", address:"9841 BELL RANCH DR", city:"SANTA FE SPRINGS", sau:"CA", postalCode:"90670-2953", country:"US"}, destination: {id:"30STS", name:"NORTHROP GRUMMAN CORPORATION", address:"4020 REDONDO BEACH AVE.", city:"REDONDO BEACH", sau:"CA", postalCode:"90278", country:"US"}}, references: [{qualifier: "P4", value: "VELPS"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "STS"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "NONE"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "6A"}, {qualifier: "IL", value: "0200-0502B090"}, {qualifier: "RSFD", value: "42691"}, {qualifier: "QTY", value: "3456"}]}]},
{orderNumber: "2941217", orderDate: "42690", orderType: "STND", quantity: 444, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "65385", supplierName: "SOUTHLAND TECHNOLOGY INC", lines: [{lineNumber:"1", itemNumber:"", itemDescription:"CAT6 UTP CMP SOLID PLENU", fulfillmentMethod:"RL", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"301", executeFrom:"10/31/2016", shipQuantity:"2", locations: {origin: {id:"65385", name:"SOUTHLAND TECHNOLOGY INC", address:"8053 VICKERS ST", city:"SAN DIEGO", sau:"CA", postalCode:"92111-1917", country:"US"}, destination: {id:"30MEL", name:"NORTHROP GRUMMAN SYSTEMS CORP.", address:"2000 W. NASA BLVD", city:"MELBOURNE", sau:"FL", postalCode:"32901", country:"US"}}, references: [{qualifier: "P4", value: "MISCX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "MEL"}, {qualifier: "12", value: "4"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "12C0534"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42704"}, {qualifier: "QTY", value: "2"}]}]},
{orderNumber: "2941217", orderDate: "42690", orderType: "STND", quantity: 205, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "65385", supplierName: "SOUTHLAND TECHNOLOGY INC", lines: [{lineNumber:"2", itemNumber:"", itemDescription:"RJ45 CAT6 CONNECTORS  BO", fulfillmentMethod:"BX", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"302", executeFrom:"10/31/2016", shipQuantity:"1", locations: {origin: {id:"65385", name:"SOUTHLAND TECHNOLOGY INC", address:"8053 VICKERS ST", city:"SAN DIEGO", sau:"CA", postalCode:"92111-1917", country:"US"}, destination: {id:"30MEL", name:"NORTHROP GRUMMAN SYSTEMS CORP.", address:"2000 W. NASA BLVD", city:"MELBOURNE", sau:"FL", postalCode:"32901", country:"US"}}, references: [{qualifier: "P4", value: "MISCX"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "MEL"}, {qualifier: "12", value: "4"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "12C0534"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42704"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "2941218", orderDate: "42690", orderType: "STND", quantity: 226, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "54346", supplierName: "FALCON AEROSPACE", lines: [{lineNumber:"1", itemNumber:"", itemDescription:"- ASTM F-15 ALLOY SHEET", fulfillmentMethod:"EA", fulfillmentTolerance:"10", status:"FALSE", deliveryScheduleNumber:"303", executeFrom:"10/31/2016", shipQuantity:"12", locations: {origin: {id:"54346", name:"FALCON AEROSPACE", address:"3350 ENTERPRISE AVE STE 180", city:"WESTON", sau:"FL", postalCode:"33331-3518", country:"US"}, destination: {id:"30SEG", name:"TUCKER, JOHN F", address:"700 NORTH DOUGLAS STREET BLDG 902  ATTN", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: "APTBU"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "4"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "RESTRICTED"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42704"}, {qualifier: "QTY", value: "12"}]}]},
{orderNumber: "2941221", orderDate: "42690", orderType: "STND", quantity: 326, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "58007", supplierName: "EAST COAST ELECTRONICS AND DATA", lines: [{lineNumber:"1", itemNumber:"F6-20-S1A50WR", itemDescription:"HINGE,RELEASE", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"304", executeFrom:"10/24/2016", shipQuantity:"4", locations: {origin: {id:"58007", name:"EAST COAST ELECTRONICS AND DATA", address:"14 BEACH ST", city:"ROCKAWAY", sau:"NJ", postalCode:"07866-3502", country:"US"}, destination: {id:"30MEL", name:"NORTHROP GRUMMAN CORPORATION", address:"2000 W NASA BLVD", city:"MELBOURNE", sau:"FL", postalCode:"32904", country:"US"}}, references: [{qualifier: "P4", value: "JSTAR"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "MEL"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "FA873014D0002"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "F6"}, {qualifier: "IL", value: "020-S1A50WR"}, {qualifier: "RSFD", value: "42697"}, {qualifier: "QTY", value: "4"}]}]},
{orderNumber: "2941221", orderDate: "42690", orderType: "STND", quantity: 226, sector:"TS - Technology Services", psa:true, incoTerms: "FOB Destination", supplierId: "58007", supplierName: "EAST COAST ELECTRONICS AND DATA", lines: [{lineNumber:"1", itemNumber:"F6-20-S1A50WR", itemDescription:"HINGE,RELEASE", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"305", executeFrom:"10/24/2016", shipQuantity:"1", locations: {origin: {id:"58007", name:"EAST COAST ELECTRONICS AND DATA", address:"14 BEACH ST", city:"ROCKAWAY", sau:"NJ", postalCode:"07866-3502", country:"US"}, destination: {id:"30MEL", name:"NORTHROP GRUMMAN CORPORATION", address:"2000 W NASA BLVD", city:"MELBOURNE", sau:"FL", postalCode:"32904", country:"US"}}, references: [{qualifier: "P4", value: "JSTAR"}, {qualifier: "87", value: "AS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "MEL"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "FA873014D0002"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "F6"}, {qualifier: "IL", value: "020-S1A50WR"}, {qualifier: "RSFD", value: "42697"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "4800030397", orderDate: "42690", orderType: "ZEFO", quantity: 226, sector:"AS - Aerospace Systems", psa:true, incoTerms: "FOB Destination", supplierId: "35970", supplierName: "KOLAKOWSKI LLC", lines: [{lineNumber:"1", itemNumber:"", itemDescription:"MONTHLY CAMPAIGN - MARC", fulfillmentMethod:"LO", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"306", executeFrom:"02/25/2015", shipQuantity:"1", locations: {origin: {id:"35970", name:"KOLAKOWSKI LLC", address:"250 N HARBOR DR STE 321", city:"REDONDO BEACH", sau:"CA", postalCode:"90277-2585", country:"US"}, destination: {id:"30A44", name:"BETH GREEN", address:"101 CONTINENTAL AVE", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "ESS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "A44"}, {qualifier: "12", value: "5"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: ""}, {qualifier: "CCNM", value: "K08000"}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42090"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "4800030397", orderDate: "42690", orderType: "ZEFO", quantity: 412, sector:"AS - Aerospace Systems", psa:true, incoTerms: "FOB Destination", supplierId: "35970", supplierName: "KOLAKOWSKI LLC", lines: [{lineNumber:"2", itemNumber:"", itemDescription:"MONTHLY CAMPAIGN - APRI", fulfillmentMethod:"LO", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"307", executeFrom:"02/25/2015", shipQuantity:"1", locations: {origin: {id:"35970", name:"KOLAKOWSKI LLC", address:"250 N HARBOR DR STE 321", city:"REDONDO BEACH", sau:"CA", postalCode:"90277-2585", country:"US"}, destination: {id:"30A44", name:"BETH GREEN", address:"101 CONTINENTAL AVE", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "ESS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "A44"}, {qualifier: "12", value: "5"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: ""}, {qualifier: "CCNM", value: "K08000"}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42090"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "4800030397", orderDate: "42690", orderType: "ZEFO", quantity: 401, sector:"AS - Aerospace Systems", psa:true, incoTerms: "FOB Destination", supplierId: "35970", supplierName: "KOLAKOWSKI LLC", lines: [{lineNumber:"3", itemNumber:"", itemDescription:"MONTHLY CAMPAIGN - MAY 2", fulfillmentMethod:"LO", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"308", executeFrom:"02/25/2015", shipQuantity:"1", locations: {origin: {id:"35970", name:"KOLAKOWSKI LLC", address:"250 N HARBOR DR STE 321", city:"REDONDO BEACH", sau:"CA", postalCode:"90277-2585", country:"US"}, destination: {id:"30A44", name:"BETH GREEN", address:"101 CONTINENTAL AVE", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "ESS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "A44"}, {qualifier: "12", value: "5"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: ""}, {qualifier: "CCNM", value: "K08000"}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42090"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "4800030397", orderDate: "42690", orderType: "ZEFO", quantity: 275, sector:"AS - Aerospace Systems", psa:true, incoTerms: "FOB Destination", supplierId: "35970", supplierName: "KOLAKOWSKI LLC", lines: [{lineNumber:"4", itemNumber:"", itemDescription:"MONTHLY CAMPAIGN - JUNE", fulfillmentMethod:"LO", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"309", executeFrom:"02/25/2015", shipQuantity:"1", locations: {origin: {id:"35970", name:"KOLAKOWSKI LLC", address:"250 N HARBOR DR STE 321", city:"REDONDO BEACH", sau:"CA", postalCode:"90277-2585", country:"US"}, destination: {id:"30A44", name:"BETH GREEN", address:"101 CONTINENTAL AVE", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "ESS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "A44"}, {qualifier: "12", value: "5"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: ""}, {qualifier: "CCNM", value: "K08000"}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42090"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "4800030397", orderDate: "42690", orderType: "ZEFO", quantity: 195, sector:"AS - Aerospace Systems", psa:true, incoTerms: "FOB Destination", supplierId: "35970", supplierName: "KOLAKOWSKI LLC", lines: [{lineNumber:"5", itemNumber:"", itemDescription:"MONTHLY CAMPAIGN - JULY", fulfillmentMethod:"LO", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"310", executeFrom:"02/25/2015", shipQuantity:"1", locations: {origin: {id:"35970", name:"KOLAKOWSKI LLC", address:"250 N HARBOR DR STE 321", city:"REDONDO BEACH", sau:"CA", postalCode:"90277-2585", country:"US"}, destination: {id:"30A44", name:"BETH GREEN", address:"101 CONTINENTAL AVE", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "ESS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "A44"}, {qualifier: "12", value: "5"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: ""}, {qualifier: "CCNM", value: "K08000"}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42090"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "4800030397", orderDate: "42690", orderType: "ZEFO", quantity: 265, sector:"AS - Aerospace Systems", psa:true, incoTerms: "FOB Destination", supplierId: "35970", supplierName: "KOLAKOWSKI LLC", lines: [{lineNumber:"6", itemNumber:"", itemDescription:"MONTHLY CAMPAIGN - AUGUS", fulfillmentMethod:"LO", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"311", executeFrom:"02/25/2015", shipQuantity:"1", locations: {origin: {id:"35970", name:"KOLAKOWSKI LLC", address:"250 N HARBOR DR STE 321", city:"REDONDO BEACH", sau:"CA", postalCode:"90277-2585", country:"US"}, destination: {id:"30A44", name:"BETH GREEN", address:"101 CONTINENTAL AVE", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "ESS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "A44"}, {qualifier: "12", value: "5"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: ""}, {qualifier: "CCNM", value: "K08000"}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42090"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "4800030397", orderDate: "42690", orderType: "ZEFO", quantity: 429, sector:"AS - Aerospace Systems", psa:true, incoTerms: "FOB Destination", supplierId: "35970", supplierName: "KOLAKOWSKI LLC", lines: [{lineNumber:"7", itemNumber:"", itemDescription:"MONTHLY CAMPAIGN - SEPTE", fulfillmentMethod:"LO", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"312", executeFrom:"02/25/2015", shipQuantity:"1", locations: {origin: {id:"35970", name:"KOLAKOWSKI LLC", address:"250 N HARBOR DR STE 321", city:"REDONDO BEACH", sau:"CA", postalCode:"90277-2585", country:"US"}, destination: {id:"30A44", name:"BETH GREEN", address:"101 CONTINENTAL AVE", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "ESS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "A44"}, {qualifier: "12", value: "5"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: ""}, {qualifier: "CCNM", value: "K08000"}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42090"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "4800030397", orderDate: "42690", orderType: "ZEFO", quantity: 489, sector:"AS - Aerospace Systems", psa:true, incoTerms: "FOB Destination", supplierId: "35970", supplierName: "KOLAKOWSKI LLC", lines: [{lineNumber:"8", itemNumber:"", itemDescription:"MONTHLY CAMPAIGN - OCTOB", fulfillmentMethod:"LO", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"313", executeFrom:"02/25/2015", shipQuantity:"1", locations: {origin: {id:"35970", name:"KOLAKOWSKI LLC", address:"250 N HARBOR DR STE 321", city:"REDONDO BEACH", sau:"CA", postalCode:"90277-2585", country:"US"}, destination: {id:"30A44", name:"BETH GREEN", address:"101 CONTINENTAL AVE", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "ESS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "A44"}, {qualifier: "12", value: "5"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: ""}, {qualifier: "CCNM", value: "K08000"}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42090"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "4800030397", orderDate: "42690", orderType: "ZEFO", quantity: 221, sector:"AS - Aerospace Systems", psa:true, incoTerms: "FOB Destination", supplierId: "35970", supplierName: "KOLAKOWSKI LLC", lines: [{lineNumber:"9", itemNumber:"", itemDescription:"MONTHLY CAMPAIGN - NOVEM", fulfillmentMethod:"LO", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"314", executeFrom:"02/25/2015", shipQuantity:"1", locations: {origin: {id:"35970", name:"KOLAKOWSKI LLC", address:"250 N HARBOR DR STE 321", city:"REDONDO BEACH", sau:"CA", postalCode:"90277-2585", country:"US"}, destination: {id:"30A44", name:"BETH GREEN", address:"101 CONTINENTAL AVE", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "ESS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "A44"}, {qualifier: "12", value: "5"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: ""}, {qualifier: "CCNM", value: "K08000"}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42090"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "4800030397", orderDate: "42690", orderType: "ZEFO", quantity: 77, sector:"AS - Aerospace Systems", psa:true, incoTerms: "FOB Destination", supplierId: "35970", supplierName: "KOLAKOWSKI LLC", lines: [{lineNumber:"10", itemNumber:"", itemDescription:"MONTHLY CAMPAIGN - DECEM", fulfillmentMethod:"LO", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"315", executeFrom:"02/25/2015", shipQuantity:"1", locations: {origin: {id:"35970", name:"KOLAKOWSKI LLC", address:"250 N HARBOR DR STE 321", city:"REDONDO BEACH", sau:"CA", postalCode:"90277-2585", country:"US"}, destination: {id:"30A44", name:"BETH GREEN", address:"101 CONTINENTAL AVE", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "ESS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "A44"}, {qualifier: "12", value: "5"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: ""}, {qualifier: "CCNM", value: "K08000"}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42090"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "4800030397", orderDate: "42690", orderType: "ZEFO", quantity: 367, sector:"AS - Aerospace Systems", psa:true, incoTerms: "FOB Destination", supplierId: "35970", supplierName: "KOLAKOWSKI LLC", lines: [{lineNumber:"11", itemNumber:"", itemDescription:"MONTHLY CAMPAIGN - JANUA", fulfillmentMethod:"LO", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"316", executeFrom:"02/25/2015", shipQuantity:"1", locations: {origin: {id:"35970", name:"KOLAKOWSKI LLC", address:"250 N HARBOR DR STE 321", city:"REDONDO BEACH", sau:"CA", postalCode:"90277-2585", country:"US"}, destination: {id:"30A44", name:"BETH GREEN", address:"101 CONTINENTAL AVE", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "ESS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "A44"}, {qualifier: "12", value: "5"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: ""}, {qualifier: "CCNM", value: "K08000"}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42090"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "4800030397", orderDate: "42690", orderType: "ZEFO", quantity: 155, sector:"AS - Aerospace Systems", psa:true, incoTerms: "FOB Destination", supplierId: "35970", supplierName: "KOLAKOWSKI LLC", lines: [{lineNumber:"12", itemNumber:"", itemDescription:"GSC OASIS SUPT - MARCH 2", fulfillmentMethod:"LO", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"317", executeFrom:"02/25/2015", shipQuantity:"1", locations: {origin: {id:"35970", name:"KOLAKOWSKI LLC", address:"250 N HARBOR DR STE 321", city:"REDONDO BEACH", sau:"CA", postalCode:"90277-2585", country:"US"}, destination: {id:"30A44", name:"BETH GREEN", address:"101 CONTINENTAL AVE", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "ESS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "A44"}, {qualifier: "12", value: "5"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: ""}, {qualifier: "CCNM", value: "K08000"}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42090"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "4800030397", orderDate: "42690", orderType: "ZEFO", quantity: 217, sector:"AS - Aerospace Systems", psa:true, incoTerms: "FOB Destination", supplierId: "35970", supplierName: "KOLAKOWSKI LLC", lines: [{lineNumber:"13", itemNumber:"", itemDescription:"GSC OASIS SUPT - APRIL 2", fulfillmentMethod:"LO", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"318", executeFrom:"02/25/2015", shipQuantity:"1", locations: {origin: {id:"35970", name:"KOLAKOWSKI LLC", address:"250 N HARBOR DR STE 321", city:"REDONDO BEACH", sau:"CA", postalCode:"90277-2585", country:"US"}, destination: {id:"30A44", name:"BETH GREEN", address:"101 CONTINENTAL AVE", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "ESS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "A44"}, {qualifier: "12", value: "5"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: ""}, {qualifier: "CCNM", value: "K08000"}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42090"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "4800030397", orderDate: "42690", orderType: "ZEFO", quantity: 111, sector:"AS - Aerospace Systems", psa:true, incoTerms: "FOB Destination", supplierId: "35970", supplierName: "KOLAKOWSKI LLC", lines: [{lineNumber:"14", itemNumber:"", itemDescription:"GSC OASIS SUPT - MAY 201", fulfillmentMethod:"LO", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"319", executeFrom:"02/25/2015", shipQuantity:"1", locations: {origin: {id:"35970", name:"KOLAKOWSKI LLC", address:"250 N HARBOR DR STE 321", city:"REDONDO BEACH", sau:"CA", postalCode:"90277-2585", country:"US"}, destination: {id:"30A44", name:"BETH GREEN", address:"101 CONTINENTAL AVE", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "ESS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "A44"}, {qualifier: "12", value: "5"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: ""}, {qualifier: "CCNM", value: "K08000"}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42090"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "4800030397", orderDate: "42690", orderType: "ZEFO", quantity: 311, sector:"AS - Aerospace Systems", psa:true, incoTerms: "FOB Destination", supplierId: "35970", supplierName: "KOLAKOWSKI LLC", lines: [{lineNumber:"15", itemNumber:"", itemDescription:"GSC OASIS SUPT - JUNE 20", fulfillmentMethod:"LO", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"320", executeFrom:"02/25/2015", shipQuantity:"1", locations: {origin: {id:"35970", name:"KOLAKOWSKI LLC", address:"250 N HARBOR DR STE 321", city:"REDONDO BEACH", sau:"CA", postalCode:"90277-2585", country:"US"}, destination: {id:"30A44", name:"BETH GREEN", address:"101 CONTINENTAL AVE", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "ESS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "A44"}, {qualifier: "12", value: "5"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: ""}, {qualifier: "CCNM", value: "K08000"}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42090"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "4800030397", orderDate: "42690", orderType: "ZEFO", quantity: 279, sector:"AS - Aerospace Systems", psa:true, incoTerms: "FOB Destination", supplierId: "35970", supplierName: "KOLAKOWSKI LLC", lines: [{lineNumber:"16", itemNumber:"", itemDescription:"GSC OASIS SUPT - JULY 20", fulfillmentMethod:"LO", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"321", executeFrom:"02/25/2015", shipQuantity:"1", locations: {origin: {id:"35970", name:"KOLAKOWSKI LLC", address:"250 N HARBOR DR STE 321", city:"REDONDO BEACH", sau:"CA", postalCode:"90277-2585", country:"US"}, destination: {id:"30A44", name:"BETH GREEN", address:"101 CONTINENTAL AVE", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "ESS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "A44"}, {qualifier: "12", value: "5"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: ""}, {qualifier: "CCNM", value: "K08000"}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42090"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "4800030397", orderDate: "42690", orderType: "ZEFO", quantity: 222, sector:"AS - Aerospace Systems", psa:true, incoTerms: "FOB Destination", supplierId: "35970", supplierName: "KOLAKOWSKI LLC", lines: [{lineNumber:"17", itemNumber:"", itemDescription:"GSC OASIS SUPT -AUGUST 2", fulfillmentMethod:"LO", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"322", executeFrom:"02/25/2015", shipQuantity:"1", locations: {origin: {id:"35970", name:"KOLAKOWSKI LLC", address:"250 N HARBOR DR STE 321", city:"REDONDO BEACH", sau:"CA", postalCode:"90277-2585", country:"US"}, destination: {id:"30A44", name:"BETH GREEN", address:"101 CONTINENTAL AVE", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "ESS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "A44"}, {qualifier: "12", value: "5"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: ""}, {qualifier: "CCNM", value: "K08000"}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42090"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "4800030397", orderDate: "42690", orderType: "ZEFO", quantity: 99, sector:"AS - Aerospace Systems", psa:true, incoTerms: "FOB Destination", supplierId: "35970", supplierName: "KOLAKOWSKI LLC", lines: [{lineNumber:"18", itemNumber:"", itemDescription:"GSC OASIS SUPT - SEPTEMB", fulfillmentMethod:"LO", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"323", executeFrom:"02/25/2015", shipQuantity:"1", locations: {origin: {id:"35970", name:"KOLAKOWSKI LLC", address:"250 N HARBOR DR STE 321", city:"REDONDO BEACH", sau:"CA", postalCode:"90277-2585", country:"US"}, destination: {id:"30A44", name:"BETH GREEN", address:"101 CONTINENTAL AVE", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "ESS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "A44"}, {qualifier: "12", value: "5"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: ""}, {qualifier: "CCNM", value: "K08000"}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42090"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "4800030397", orderDate: "42690", orderType: "ZEFO", quantity: 335, sector:"AS - Aerospace Systems", psa:true, incoTerms: "FOB Destination", supplierId: "35970", supplierName: "KOLAKOWSKI LLC", lines: [{lineNumber:"19", itemNumber:"", itemDescription:"GSC OASIS SUPT - OCTOBER", fulfillmentMethod:"LO", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"324", executeFrom:"02/25/2015", shipQuantity:"1", locations: {origin: {id:"35970", name:"KOLAKOWSKI LLC", address:"250 N HARBOR DR STE 321", city:"REDONDO BEACH", sau:"CA", postalCode:"90277-2585", country:"US"}, destination: {id:"30A44", name:"BETH GREEN", address:"101 CONTINENTAL AVE", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "ESS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "A44"}, {qualifier: "12", value: "5"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: ""}, {qualifier: "CCNM", value: "K08000"}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42090"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "4800030397", orderDate: "42690", orderType: "ZEFO", quantity: 33, sector:"AS - Aerospace Systems", psa:true, incoTerms: "FOB Destination", supplierId: "35970", supplierName: "KOLAKOWSKI LLC", lines: [{lineNumber:"20", itemNumber:"", itemDescription:"GSC OASIS SUPT - NOVEMBE", fulfillmentMethod:"LO", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"325", executeFrom:"02/25/2015", shipQuantity:"1", locations: {origin: {id:"35970", name:"KOLAKOWSKI LLC", address:"250 N HARBOR DR STE 321", city:"REDONDO BEACH", sau:"CA", postalCode:"90277-2585", country:"US"}, destination: {id:"30A44", name:"BETH GREEN", address:"101 CONTINENTAL AVE", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "ESS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "A44"}, {qualifier: "12", value: "5"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: ""}, {qualifier: "CCNM", value: "K08000"}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42090"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "4800030397", orderDate: "42690", orderType: "ZEFO", quantity: 162, sector:"AS - Aerospace Systems", psa:true, incoTerms: "FOB Destination", supplierId: "35970", supplierName: "KOLAKOWSKI LLC", lines: [{lineNumber:"21", itemNumber:"", itemDescription:"GSC OASIS SUPT - DECEMBE", fulfillmentMethod:"LO", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"326", executeFrom:"02/25/2015", shipQuantity:"1", locations: {origin: {id:"35970", name:"KOLAKOWSKI LLC", address:"250 N HARBOR DR STE 321", city:"REDONDO BEACH", sau:"CA", postalCode:"90277-2585", country:"US"}, destination: {id:"30A44", name:"BETH GREEN", address:"101 CONTINENTAL AVE", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "ESS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "A44"}, {qualifier: "12", value: "5"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: ""}, {qualifier: "CCNM", value: "K08000"}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42090"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "4800030397", orderDate: "42690", orderType: "ZEFO", quantity: 343, sector:"AS - Aerospace Systems", psa:true, incoTerms: "FOB Destination", supplierId: "35970", supplierName: "KOLAKOWSKI LLC", lines: [{lineNumber:"22", itemNumber:"", itemDescription:"GSC OASIS SUPT - JANUARY", fulfillmentMethod:"LO", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"327", executeFrom:"02/25/2015", shipQuantity:"1", locations: {origin: {id:"35970", name:"KOLAKOWSKI LLC", address:"250 N HARBOR DR STE 321", city:"REDONDO BEACH", sau:"CA", postalCode:"90277-2585", country:"US"}, destination: {id:"30A44", name:"BETH GREEN", address:"101 CONTINENTAL AVE", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "ESS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "A44"}, {qualifier: "12", value: "5"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: ""}, {qualifier: "CCNM", value: "K08000"}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42090"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "4800030397", orderDate: "42690", orderType: "ZEFO", quantity: 49, sector:"AS - Aerospace Systems", psa:true, incoTerms: "FOB Destination", supplierId: "35970", supplierName: "KOLAKOWSKI LLC", lines: [{lineNumber:"23", itemNumber:"", itemDescription:"APRIL 2016 WEB TOOLS AND", fulfillmentMethod:"LO", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"328", executeFrom:"12/29/2015", shipQuantity:"1", locations: {origin: {id:"35970", name:"KOLAKOWSKI LLC", address:"250 N HARBOR DR STE 321", city:"REDONDO BEACH", sau:"CA", postalCode:"90277-2585", country:"US"}, destination: {id:"30SEG", name:"ALEXANDER NIAKOULOV", address:"101 CONTINENTAL AVE", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "ESS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "5"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: ""}, {qualifier: "CCNM", value: "K02200"}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42397"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "4800030397", orderDate: "42690", orderType: "ZEFO", quantity: 493, sector:"AS - Aerospace Systems", psa:true, incoTerms: "FOB Destination", supplierId: "35970", supplierName: "KOLAKOWSKI LLC", lines: [{lineNumber:"24", itemNumber:"", itemDescription:"FEBRUARY 2016 WEB TOOLS", fulfillmentMethod:"LO", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"329", executeFrom:"12/29/2015", shipQuantity:"1", locations: {origin: {id:"35970", name:"KOLAKOWSKI LLC", address:"250 N HARBOR DR STE 321", city:"REDONDO BEACH", sau:"CA", postalCode:"90277-2585", country:"US"}, destination: {id:"30SEG", name:"ALEXANDER NIAKOULOV", address:"101 CONTINENTAL AVE", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "ESS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "5"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: ""}, {qualifier: "CCNM", value: "K02200"}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42397"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "4800030397", orderDate: "42690", orderType: "ZEFO", quantity: 114, sector:"AS - Aerospace Systems", psa:true, incoTerms: "FOB Destination", supplierId: "35970", supplierName: "KOLAKOWSKI LLC", lines: [{lineNumber:"25", itemNumber:"", itemDescription:"MARCH 2016 WEB TOOLS AND", fulfillmentMethod:"LO", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"330", executeFrom:"12/29/2015", shipQuantity:"1", locations: {origin: {id:"35970", name:"KOLAKOWSKI LLC", address:"250 N HARBOR DR STE 321", city:"REDONDO BEACH", sau:"CA", postalCode:"90277-2585", country:"US"}, destination: {id:"30SEG", name:"ALEXANDER NIAKOULOV", address:"101 CONTINENTAL AVE", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "ESS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "5"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: ""}, {qualifier: "CCNM", value: "K02200"}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42397"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "4800030397", orderDate: "42690", orderType: "ZEFO", quantity: 386, sector:"AS - Aerospace Systems", psa:true, incoTerms: "FOB Destination", supplierId: "35970", supplierName: "KOLAKOWSKI LLC", lines: [{lineNumber:"26", itemNumber:"", itemDescription:"GO WEB TOOLS & PROJECTS", fulfillmentMethod:"LO", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"331", executeFrom:"04/11/2016", shipQuantity:"1", locations: {origin: {id:"35970", name:"KOLAKOWSKI LLC", address:"250 N HARBOR DR STE 321", city:"REDONDO BEACH", sau:"CA", postalCode:"90277-2585", country:"US"}, destination: {id:"30SEG", name:"NGAS SECTOR", address:"101 N. CONTINENTAL BLVD", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "ESS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "5"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: ""}, {qualifier: "CCNM", value: "K02200"}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42501"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "4800030397", orderDate: "42690", orderType: "ZEFO", quantity: 284, sector:"AS - Aerospace Systems", psa:true, incoTerms: "FOB Destination", supplierId: "35970", supplierName: "KOLAKOWSKI LLC", lines: [{lineNumber:"27", itemNumber:"", itemDescription:"GO WEB TOOLS & PROJECTS", fulfillmentMethod:"LO", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"332", executeFrom:"05/04/2016", shipQuantity:"1", locations: {origin: {id:"35970", name:"KOLAKOWSKI LLC", address:"250 N HARBOR DR STE 321", city:"REDONDO BEACH", sau:"CA", postalCode:"90277-2585", country:"US"}, destination: {id:"30SEG", name:"NGAS SECTOR", address:"101 N. CONTINENTAL BLVD", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "ESS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "5"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: ""}, {qualifier: "CCNM", value: "K02200"}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42524"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "4800030397", orderDate: "42690", orderType: "ZEFO", quantity: 270, sector:"AS - Aerospace Systems", psa:true, incoTerms: "FOB Destination", supplierId: "35970", supplierName: "KOLAKOWSKI LLC", lines: [{lineNumber:"28", itemNumber:"", itemDescription:"6SC ARF ENHANCEMENTS FOR", fulfillmentMethod:"LO", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"333", executeFrom:"07/01/2016", shipQuantity:"1", locations: {origin: {id:"35970", name:"KOLAKOWSKI LLC", address:"250 N HARBOR DR STE 321", city:"REDONDO BEACH", sau:"CA", postalCode:"90277-2585", country:"US"}, destination: {id:"30SEG", name:"NGAS SECTOR", address:"101 N. CONTINENTAL BLVD", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "ESS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "4"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "SEE CHARGE TEXT"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42582"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "4800030397", orderDate: "42690", orderType: "ZEFO", quantity: 462, sector:"AS - Aerospace Systems", psa:true, incoTerms: "FOB Destination", supplierId: "35970", supplierName: "KOLAKOWSKI LLC", lines: [{lineNumber:"29", itemNumber:"", itemDescription:"GO WEB TOOLS & PROJECTS", fulfillmentMethod:"LO", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"334", executeFrom:"06/07/2016", shipQuantity:"1", locations: {origin: {id:"35970", name:"KOLAKOWSKI LLC", address:"250 N HARBOR DR STE 321", city:"REDONDO BEACH", sau:"CA", postalCode:"90277-2585", country:"US"}, destination: {id:"30SEG", name:"NGAS SECTOR", address:"101 N. CONTINENTAL BLVD", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "ESS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "5"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: ""}, {qualifier: "CCNM", value: "K02200"}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42558"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "4800030397", orderDate: "42690", orderType: "ZEFO", quantity: 285, sector:"AS - Aerospace Systems", psa:true, incoTerms: "FOB Destination", supplierId: "35970", supplierName: "KOLAKOWSKI LLC", lines: [{lineNumber:"30", itemNumber:"", itemDescription:"GO WEB TOOLS & PROJECTS", fulfillmentMethod:"LO", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"335", executeFrom:"07/02/2016", shipQuantity:"1", locations: {origin: {id:"35970", name:"KOLAKOWSKI LLC", address:"250 N HARBOR DR STE 321", city:"REDONDO BEACH", sau:"CA", postalCode:"90277-2585", country:"US"}, destination: {id:"30SEG", name:"NGAS SECTOR", address:"101 N. CONTINENTAL BLVD", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "ESS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "5"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: ""}, {qualifier: "CCNM", value: "K02200"}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42583"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "4800030397", orderDate: "42690", orderType: "ZEFO", quantity: 223, sector:"AS - Aerospace Systems", psa:true, incoTerms: "FOB Destination", supplierId: "35970", supplierName: "KOLAKOWSKI LLC", lines: [{lineNumber:"31", itemNumber:"", itemDescription:"GO WEB TOOLS & PROJECTS", fulfillmentMethod:"LO", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"336", executeFrom:"08/31/2016", shipQuantity:"1", locations: {origin: {id:"35970", name:"KOLAKOWSKI LLC", address:"250 N HARBOR DR STE 321", city:"REDONDO BEACH", sau:"CA", postalCode:"90277-2585", country:"US"}, destination: {id:"30SEG", name:"NGAS SECTOR", address:"101 N. CONTINENTAL BLVD", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "ESS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "5"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: ""}, {qualifier: "CCNM", value: "K02200"}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42643"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "4800030397", orderDate: "42690", orderType: "ZEFO", quantity: 238, sector:"AS - Aerospace Systems", psa:true, incoTerms: "FOB Destination", supplierId: "35970", supplierName: "KOLAKOWSKI LLC", lines: [{lineNumber:"32", itemNumber:"", itemDescription:"GO WEB TOOLS & PROJECTS", fulfillmentMethod:"LO", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"337", executeFrom:"09/01/2016", shipQuantity:"1", locations: {origin: {id:"35970", name:"KOLAKOWSKI LLC", address:"250 N HARBOR DR STE 321", city:"REDONDO BEACH", sau:"CA", postalCode:"90277-2585", country:"US"}, destination: {id:"30SEG", name:"NGAS SECTOR", address:"101 N. CONTINENTAL BLVD", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "ESS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "5"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: ""}, {qualifier: "CCNM", value: "K02200"}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42644"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "4800030397", orderDate: "42690", orderType: "ZEFO", quantity: 21, sector:"AS - Aerospace Systems", psa:true, incoTerms: "FOB Destination", supplierId: "35970", supplierName: "KOLAKOWSKI LLC", lines: [{lineNumber:"33", itemNumber:"", itemDescription:"GO WEB TOOLS & PROJECTS", fulfillmentMethod:"LO", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"338", executeFrom:"10/02/2016", shipQuantity:"1", locations: {origin: {id:"35970", name:"KOLAKOWSKI LLC", address:"250 N HARBOR DR STE 321", city:"REDONDO BEACH", sau:"CA", postalCode:"90277-2585", country:"US"}, destination: {id:"30SEG", name:"NGAS SECTOR", address:"101 N. CONTINENTAL BLVD", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "ESS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "5"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: ""}, {qualifier: "CCNM", value: "K02200"}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42675"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "4800030397", orderDate: "42690", orderType: "ZEFO", quantity: 283, sector:"AS - Aerospace Systems", psa:true, incoTerms: "FOB Destination", supplierId: "35970", supplierName: "KOLAKOWSKI LLC", lines: [{lineNumber:"34", itemNumber:"", itemDescription:"GO WEB TOOLS & PROJECTS", fulfillmentMethod:"LO", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"339", executeFrom:"11/01/2016", shipQuantity:"1", locations: {origin: {id:"35970", name:"KOLAKOWSKI LLC", address:"250 N HARBOR DR STE 321", city:"REDONDO BEACH", sau:"CA", postalCode:"90277-2585", country:"US"}, destination: {id:"30SEG", name:"NGAS SECTOR", address:"101 N. CONTINENTAL BLVD", city:"EL SEGUNDO", sau:"CA", postalCode:"90245", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "ESS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "5"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: ""}, {qualifier: "CCNM", value: "K02200"}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42705"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "4800035026", orderDate: "42690", orderType: "ZEFO", quantity: 347, sector:"AS - Aerospace Systems", psa:true, incoTerms: "FOB Destination", supplierId: "51629", supplierName: "MCKOWSKI'S MAINTENANCE SYSTEMS INC", lines: [{lineNumber:"1", itemNumber:"", itemDescription:"LANDSCAPING MAINTENANCE", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"340", executeFrom:"10/26/2015", shipQuantity:"1", locations: {origin: {id:"51629", name:"MCKOWSKI'S MAINTENANCE SYSTEMS INC", address:"12125  PAINE ST", city:"POWAY", sau:"CA", postalCode:"92064-7124", country:"US"}, destination: {id:"30SEG", name:"NGAS SECTOR", address:"16550 W BERNARDO DRIVE", city:"SAN DIEGO", sau:"CA", postalCode:"92127", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "ESS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "5"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N/A"}, {qualifier: "CCNM", value: "L0WK00"}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42333"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "4800035026", orderDate: "42690", orderType: "ZEFO", quantity: 283, sector:"AS - Aerospace Systems", psa:true, incoTerms: "FOB Destination", supplierId: "51629", supplierName: "MCKOWSKI'S MAINTENANCE SYSTEMS INC", lines: [{lineNumber:"3", itemNumber:"", itemDescription:"LANDSCAPE SERVICE", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"341", executeFrom:"10/22/2016", shipQuantity:"1", locations: {origin: {id:"51629", name:"MCKOWSKI'S MAINTENANCE SYSTEMS INC", address:"12125  PAINE ST", city:"POWAY", sau:"CA", postalCode:"92064-7124", country:"US"}, destination: {id:"30SEG", name:"TODD KIRICHKOW", address:"16550 W BERNARDO DRIVE", city:"SAN DIEGO", sau:"CA", postalCode:"92127", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "ESS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "SEG"}, {qualifier: "12", value: "5"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N/A"}, {qualifier: "CCNM", value: "L0WK00"}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42695"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "4800041047", orderDate: "42690", orderType: "ZEFO", quantity: 60, sector:"AS - Aerospace Systems", psa:true, incoTerms: "FOB Destination", supplierId: "34024", supplierName: "TFD GROUP", lines: [{lineNumber:"1", itemNumber:"", itemDescription:"EDCAS - SECTOR LICENSE", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"342", executeFrom:"10/17/2016", shipQuantity:"1", locations: {origin: {id:"34024", name:"TFD GROUP", address:"70 GARDEN CT STE 300", city:"MONTEREY", sau:"CA", postalCode:"93940-5342", country:"US"}, destination: {id:"91ITS", name:"TOM COLLIPI", address:"2000 WEST NASA BLVD", city:"MELBOURNE", sau:"FL", postalCode:"32904", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "ESS"}, {qualifier: "SLI", value: "91"}, {qualifier: "PE", value: "ITS"}, {qualifier: "12", value: "4"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "SEE CHARGE TEXT"}, {qualifier: "CCNM", value: "B49C12"}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42690"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "4800041047", orderDate: "42690", orderType: "ZEFO", quantity: 158, sector:"AS - Aerospace Systems", psa:true, incoTerms: "FOB Destination", supplierId: "34024", supplierName: "TFD GROUP", lines: [{lineNumber:"2", itemNumber:"", itemDescription:"TEMPO - SECTOR LICENSE", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"343", executeFrom:"10/17/2016", shipQuantity:"1", locations: {origin: {id:"34024", name:"TFD GROUP", address:"70 GARDEN CT STE 300", city:"MONTEREY", sau:"CA", postalCode:"93940-5342", country:"US"}, destination: {id:"91ITS", name:"TOM COLLIPI", address:"2000 WEST NASA BLVD", city:"MELBOURNE", sau:"FL", postalCode:"32904", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "ESS"}, {qualifier: "SLI", value: "91"}, {qualifier: "PE", value: "ITS"}, {qualifier: "12", value: "4"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "SEE CHARGE TEXT"}, {qualifier: "CCNM", value: "B49C12"}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42690"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "4800041047", orderDate: "42690", orderType: "ZEFO", quantity: 491, sector:"AS - Aerospace Systems", psa:true, incoTerms: "FOB Destination", supplierId: "34024", supplierName: "TFD GROUP", lines: [{lineNumber:"3", itemNumber:"", itemDescription:"TFD DATABASE - SECTOR L", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"344", executeFrom:"10/17/2016", shipQuantity:"1", locations: {origin: {id:"34024", name:"TFD GROUP", address:"70 GARDEN CT STE 300", city:"MONTEREY", sau:"CA", postalCode:"93940-5342", country:"US"}, destination: {id:"91ITS", name:"TOM COLLIPI", address:"2000 WEST NASA BLVD", city:"MELBOURNE", sau:"FL", postalCode:"32904", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "ESS"}, {qualifier: "SLI", value: "91"}, {qualifier: "PE", value: "ITS"}, {qualifier: "12", value: "4"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "SEE CHARGE TEXT"}, {qualifier: "CCNM", value: "B49C12"}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42690"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "4800041065", orderDate: "42690", orderType: "ZEFO", quantity: 425, sector:"AS - Aerospace Systems", psa:true, incoTerms: "FOB Destination", supplierId: "28573", supplierName: "GARTNER GROUP INC", lines: [{lineNumber:"1", itemNumber:"", itemDescription:"YEAR ONEIT LEADERSHIP TE", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"345", executeFrom:"10/19/2016", shipQuantity:"1", locations: {origin: {id:"28573", name:"GARTNER GROUP INC", address:"3190 FAIRVIEW PARK DR", city:"FALLS CHURCH", sau:"VA", postalCode:"22042-4530", country:"US"}, destination: {id:"91ITS", name:"CHRISTINE RUSSO", address:"2980 FAIRVIEW PARK DRIVE", city:"FALLS CHURCH", sau:"VA", postalCode:"22042", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "ESS"}, {qualifier: "SLI", value: "91"}, {qualifier: "PE", value: "ITS"}, {qualifier: "12", value: "5"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N/A"}, {qualifier: "CCNM", value: "B4G000"}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42692"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "4800041065", orderDate: "42690", orderType: "ZEFO", quantity: 156, sector:"AS - Aerospace Systems", psa:true, incoTerms: "FOB Destination", supplierId: "28573", supplierName: "GARTNER GROUP INC", lines: [{lineNumber:"2", itemNumber:"", itemDescription:"YR1IT LEADERSHIP TEAM-CR", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"346", executeFrom:"10/19/2016", shipQuantity:"1", locations: {origin: {id:"28573", name:"GARTNER GROUP INC", address:"3190 FAIRVIEW PARK DR", city:"FALLS CHURCH", sau:"VA", postalCode:"22042-4530", country:"US"}, destination: {id:"91ITS", name:"CHRISTINE RUSSO", address:"2980 FAIRVIEW PARK DRIVE", city:"FALLS CHURCH", sau:"VA", postalCode:"22042", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "ESS"}, {qualifier: "SLI", value: "91"}, {qualifier: "PE", value: "ITS"}, {qualifier: "12", value: "5"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N/A"}, {qualifier: "CCNM", value: "B4G000"}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42692"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "4800041097", orderDate: "42690", orderType: "ZEFO", quantity: 174, sector:"AS - Aerospace Systems", psa:true, incoTerms: "FOB Destination", supplierId: "46852", supplierName: "CSO INFORMATION TRANSPORT", lines: [{lineNumber:"1", itemNumber:"", itemDescription:"FACILITY MAINTENANCE", fulfillmentMethod:"LO", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"347", executeFrom:"10/18/2016", shipQuantity:"1", locations: {origin: {id:"46852", name:"CSO INFORMATION TRANSPORT", address:"4005 FLINTRIDGE DR", city:"DALLAS", sau:"TX", postalCode:"75244-6617", country:"US"}, destination: {id:"91ITS", name:"MICHAEL CHAPMAN", address:"8710 FREEPORT PARKWAY SUITE 100", city:"IRVING", sau:"TX", postalCode:"75063", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "ESS"}, {qualifier: "SLI", value: "91"}, {qualifier: "PE", value: "ITS"}, {qualifier: "12", value: "5"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N/A"}, {qualifier: "CCNM", value: "B44704"}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42691"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "4800041170", orderDate: "42690", orderType: "ZEFO", quantity: 357, sector:"AS - Aerospace Systems", psa:true, incoTerms: "FOB Destination", supplierId: "48992", supplierName: "BRPH ARCHITECTS ENGINEERS INC", lines: [{lineNumber:"1", itemNumber:"", itemDescription:"MP-251 B221 STORAGE ROOM", fulfillmentMethod:"LO", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"348", executeFrom:"10/19/2016", shipQuantity:"1", locations: {origin: {id:"48992", name:"BRPH ARCHITECTS ENGINEERS INC", address:"5700 N HIGHWAY 1 STE 400", city:"MELBOURNE", sau:"FL", postalCode:"32940-7265", country:"US"}, destination: {id:"30MEL", name:"GLENN WEYANT", address:"2000 W. NASA BLVD.", city:"MELBOURNE", sau:"FL", postalCode:"32904", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "ESS"}, {qualifier: "SLI", value: "30"}, {qualifier: "PE", value: "MEL"}, {qualifier: "12", value: "4"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "SEE CHARGE TEXT"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42692"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "4900004089", orderDate: "42690", orderType: "ZECF", quantity: 71, sector:"MS - Mission Systems", psa:true, incoTerms: "FOB Destination", supplierId: "16541", supplierName: "PDS TECHNICAL INC-GRI", lines: [{lineNumber:"1", itemNumber:"", itemDescription:"CONTRACTOR_ADAM CARTER", fulfillmentMethod:"LO", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"349", executeFrom:"05/19/2016", shipQuantity:"1", locations: {origin: {id:"16541", name:"PDS TECHNICAL INC-GRI", address:"1925 W JOHN CARPENTER FWY STE 550", city:"IRVING", sau:"TX", postalCode:"75063-3216", country:"US"}, destination: {id:"91ITS", name:"FELICIA ROJAS", address:"201 TECHNOLOGY PARK DRIVE", city:"LEBANON", sau:"VA", postalCode:"24266", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "ESS"}, {qualifier: "SLI", value: "91"}, {qualifier: "PE", value: "ITS"}, {qualifier: "12", value: "4"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "SEE CHARGE TEXT"}, {qualifier: "CCNM", value: "XE6H43"}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42539"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "7000296858", orderDate: "42690", orderType: "MPNG", quantity: 268, sector:"MS - Mission Systems", psa:false, incoTerms: "FOB Destination", supplierId: "21714", supplierName: "TYCO ELECTRONICS CORP", lines: [{lineNumber:"45", itemNumber:"R300611-57", itemDescription:"CABLE ASSEMBLY, RF", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"350", executeFrom:"02/23/2017", shipQuantity:"96", locations: {origin: {id:"21714", name:"TYCO ELECTRONICS CORP", address:"2800 FULLING MILL RD", city:"MIDDLETOWN", sau:"PA", postalCode:"17057-3142", country:"US"}, destination: {id:"39RSD", name:"NORTHROP GRUMMAN CORPORATION", address:"9112 SPECTRUM CENTER BLVD, DOCK  7", city:"SAN DIEGO", sau:"CA", postalCode:"92123", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "IS"}, {qualifier: "SLI", value: "397"}, {qualifier: "PE", value: "RSD"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00024-15-C-6327"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "R3"}, {qualifier: "IL", value: "00611-57"}, {qualifier: "RSFD", value: "42819"}, {qualifier: "QTY", value: "96"}]}]},
{orderNumber: "7000296858", orderDate: "42690", orderType: "MPNG", quantity: 247, sector:"MS - Mission Systems", psa:false, incoTerms: "FOB Destination", supplierId: "21714", supplierName: "TYCO ELECTRONICS CORP", lines: [{lineNumber:"45", itemNumber:"R300611-57", itemDescription:"CABLE ASSEMBLY, RF", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"351", executeFrom:"01/26/2017", shipQuantity:"96", locations: {origin: {id:"21714", name:"TYCO ELECTRONICS CORP", address:"2800 FULLING MILL RD", city:"MIDDLETOWN", sau:"PA", postalCode:"17057-3142", country:"US"}, destination: {id:"39RSD", name:"NORTHROP GRUMMAN CORPORATION", address:"9112 SPECTRUM CENTER BLVD, DOCK  7", city:"SAN DIEGO", sau:"CA", postalCode:"92123", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "IS"}, {qualifier: "SLI", value: "397"}, {qualifier: "PE", value: "RSD"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00024-15-C-6327"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "R3"}, {qualifier: "IL", value: "00611-57"}, {qualifier: "RSFD", value: "42791"}, {qualifier: "QTY", value: "96"}]}]},
{orderNumber: "7000296858", orderDate: "42690", orderType: "MPNG", quantity: 474, sector:"MS - Mission Systems", psa:false, incoTerms: "FOB Destination", supplierId: "21714", supplierName: "TYCO ELECTRONICS CORP", lines: [{lineNumber:"45", itemNumber:"R300611-57", itemDescription:"CABLE ASSEMBLY, RF", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"352", executeFrom:"12/23/2016", shipQuantity:"120", locations: {origin: {id:"21714", name:"TYCO ELECTRONICS CORP", address:"2800 FULLING MILL RD", city:"MIDDLETOWN", sau:"PA", postalCode:"17057-3142", country:"US"}, destination: {id:"39RSD", name:"NORTHROP GRUMMAN CORPORATION", address:"9112 SPECTRUM CENTER BLVD, DOCK  7", city:"SAN DIEGO", sau:"CA", postalCode:"92123", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "IS"}, {qualifier: "SLI", value: "397"}, {qualifier: "PE", value: "RSD"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00024-15-C-6327"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "R3"}, {qualifier: "IL", value: "00611-57"}, {qualifier: "RSFD", value: "42757"}, {qualifier: "QTY", value: "120"}]}]},
{orderNumber: "7000296858", orderDate: "42690", orderType: "MPNG", quantity: 320, sector:"MS - Mission Systems", psa:false, incoTerms: "FOB Destination", supplierId: "21714", supplierName: "TYCO ELECTRONICS CORP", lines: [{lineNumber:"45", itemNumber:"R300611-57", itemDescription:"CABLE ASSEMBLY, RF", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"353", executeFrom:"11/26/2016", shipQuantity:"96", locations: {origin: {id:"21714", name:"TYCO ELECTRONICS CORP", address:"2800 FULLING MILL RD", city:"MIDDLETOWN", sau:"PA", postalCode:"17057-3142", country:"US"}, destination: {id:"39RSD", name:"NORTHROP GRUMMAN CORPORATION", address:"9112 SPECTRUM CENTER BLVD, DOCK  7", city:"SAN DIEGO", sau:"CA", postalCode:"92123", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "IS"}, {qualifier: "SLI", value: "397"}, {qualifier: "PE", value: "RSD"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00024-15-C-6327"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "R3"}, {qualifier: "IL", value: "00611-57"}, {qualifier: "RSFD", value: "42730"}, {qualifier: "QTY", value: "96"}]}]},
{orderNumber: "7000296858", orderDate: "42690", orderType: "MPNG", quantity: 396, sector:"MS - Mission Systems", psa:false, incoTerms: "FOB Destination", supplierId: "21714", supplierName: "TYCO ELECTRONICS CORP", lines: [{lineNumber:"45", itemNumber:"R300611-57", itemDescription:"CABLE ASSEMBLY, RF", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"354", executeFrom:"10/23/2016", shipQuantity:"93", locations: {origin: {id:"21714", name:"TYCO ELECTRONICS CORP", address:"2800 FULLING MILL RD", city:"MIDDLETOWN", sau:"PA", postalCode:"17057-3142", country:"US"}, destination: {id:"39RSD", name:"NORTHROP GRUMMAN CORPORATION", address:"9112 SPECTRUM CENTER BLVD, DOCK  7", city:"SAN DIEGO", sau:"CA", postalCode:"92123", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "IS"}, {qualifier: "SLI", value: "397"}, {qualifier: "PE", value: "RSD"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00024-15-C-6327"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "R3"}, {qualifier: "IL", value: "00611-57"}, {qualifier: "RSFD", value: "42696"}, {qualifier: "QTY", value: "93"}]}]},
{orderNumber: "7000296858", orderDate: "42690", orderType: "MPNG", quantity: 290, sector:"MS - Mission Systems", psa:false, incoTerms: "FOB Destination", supplierId: "21714", supplierName: "TYCO ELECTRONICS CORP", lines: [{lineNumber:"45", itemNumber:"R300611-57", itemDescription:"CABLE ASSEMBLY, RF", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"355", executeFrom:"09/23/2016", shipQuantity:"104", locations: {origin: {id:"21714", name:"TYCO ELECTRONICS CORP", address:"2800 FULLING MILL RD", city:"MIDDLETOWN", sau:"PA", postalCode:"17057-3142", country:"US"}, destination: {id:"39RSD", name:"NORTHROP GRUMMAN CORPORATION", address:"9112 SPECTRUM CENTER BLVD, DOCK  7", city:"SAN DIEGO", sau:"CA", postalCode:"92123", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "IS"}, {qualifier: "SLI", value: "397"}, {qualifier: "PE", value: "RSD"}, {qualifier: "12", value: "1"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00024-15-C-6327"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: "R3"}, {qualifier: "IL", value: "00611-57"}, {qualifier: "RSFD", value: "42666"}, {qualifier: "QTY", value: "104"}]}]},
{orderNumber: "7000304810", orderDate: "42690", orderType: "MSRP", quantity: 233, sector:"MS - Mission Systems", psa:false, incoTerms: "FOB Destination", supplierId: "57786", supplierName: "EFW INC", lines: [{lineNumber:"4", itemNumber:"", itemDescription:"C904738-1 UVPA JPT0203", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"TRUE", deliveryScheduleNumber:"356", executeFrom:"11/24/2016", shipQuantity:"", locations: {origin: {id:"57786", name:"EFW INC", address:"4700 MARINE CREEK PKWY", city:"FORT WORTH", sau:"TX", postalCode:"76179-3505", country:"US"}, destination: {id:"39RSD", name:"IS ASP SAN DIEGO", address:"15120 INNOVATION DRIVE", city:"SAN DIEGO", sau:"CA", postalCode:"92128", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "IS"}, {qualifier: "SLI", value: "397"}, {qualifier: "PE", value: "RSD"}, {qualifier: "12", value: "4"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-08-C-0028"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42728"}, {qualifier: "QTY", value: ""}]}]},
{orderNumber: "7000304810", orderDate: "42690", orderType: "MSRP", quantity: 344, sector:"MS - Mission Systems", psa:false, incoTerms: "FOB Destination", supplierId: "57786", supplierName: "EFW INC", lines: [{lineNumber:"16", itemNumber:"", itemDescription:"C904738-4 UVPA JPT0203", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"357", executeFrom:"11/24/2016", shipQuantity:"1", locations: {origin: {id:"57786", name:"EFW INC", address:"4700 MARINE CREEK PKWY", city:"FORT WORTH", sau:"TX", postalCode:"76179-3505", country:"US"}, destination: {id:"39RSD", name:"IS ASP SAN DIEGO", address:"15120 INNOVATION DRIVE", city:"SAN DIEGO", sau:"CA", postalCode:"92128", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "IS"}, {qualifier: "SLI", value: "397"}, {qualifier: "PE", value: "RSD"}, {qualifier: "12", value: "4"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "N00019-08-C-0028"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42728"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "7000309820", orderDate: "42690", orderType: "MPNG", quantity: 465, sector:"MS - Mission Systems", psa:false, incoTerms: "FOB Destination", supplierId: "21819", supplierName: "WORLD WIDE TECHNOLOGY INC", lines: [{lineNumber:"1", itemNumber:"", itemDescription:"E-FAS2552A-001-R6-04", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"358", executeFrom:"11/12/2016", shipQuantity:"1", locations: {origin: {id:"21819", name:"WORLD WIDE TECHNOLOGY INC", address:"60 WELDON PKWY", city:"MARYLAND HEIGHTS", sau:"MO", postalCode:"63043-3202", country:"US"}, destination: {id:"39MSS", name:"NORTHROP GRUMMAN INFORMATION SYSTEMS", address:"9030 JUNCTION DRIVE", city:"ANNAPOLIS JUNCTION", sau:"MD", postalCode:"20701", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "IS"}, {qualifier: "SLI", value: "397"}, {qualifier: "PE", value: "MSS"}, {qualifier: "12", value: "4"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "H98230-13-C-1375"}, {qualifier: "CCNM", value: "PCDMBR"}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42716"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "7000309823", orderDate: "42690", orderType: "MPNG", quantity: 187, sector:"MS - Mission Systems", psa:false, incoTerms: "FOB Destination", supplierId: "35777", supplierName: "AREA 51 ESG INC", lines: [{lineNumber:"1", itemNumber:"", itemDescription:"0120FBC09300437173SVM PT", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"359", executeFrom:"01/02/2017", shipQuantity:"20", locations: {origin: {id:"35777", name:"AREA 51 ESG INC", address:"51 POST", city:"IRVINE", sau:"CA", postalCode:"92618-5216", country:"US"}, destination: {id:"47TFT", name:"MICHAEL WALKER", address:"30899 PARK DRIVE", city:"PRINCESS ANNE", sau:"MD", postalCode:"21853", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "TS"}, {qualifier: "SLI", value: "470"}, {qualifier: "PE", value: "TFT"}, {qualifier: "12", value: "4"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "W9124Q-08-D-0801"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42767"}, {qualifier: "QTY", value: "20"}]}]},
{orderNumber: "7500143097", orderDate: "42690", orderType: "STND", quantity: 17, sector:"TS - Technology Services", quantity: 452, sector:"MS - Mission Systems", psa:true, incoTerms: "FOB Destination", supplierId: "25172", supplierName: "TEKSYSTEMS INC", lines: [{lineNumber:"3", itemNumber:"", itemDescription:"LINE 3: ACO LABOR", fulfillmentMethod:"LO", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"360", executeFrom:"10/15/2016", shipQuantity:"1", locations: {origin: {id:"25172", name:"TEKSYSTEMS INC", address:"7437 RACE RD", city:"HANOVER", sau:"MD", postalCode:"21076-1112", country:"US"}, destination: {id:"39MSS", name:"NGIS SECTOR", address:"2810 LORD BALTIMORE DRIVE, SUITE 100", city:"BALTIMORE", sau:"MD", postalCode:"21244", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "IS"}, {qualifier: "SLI", value: "397"}, {qualifier: "PE", value: "MSS"}, {qualifier: "12", value: "4"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "HHSM-500-2007-00014I"}, {qualifier: "CCNM", value: "PHM213"}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42688"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "7500145999", orderDate: "42690", orderType: "MPFO", quantity: 38, sector:"MS - Mission Systems", psa:false, incoTerms: "FOB Destination", supplierId: "48847", supplierName: "ARROW ELECTRONICS INC", lines: [{lineNumber:"4", itemNumber:"", itemDescription:"ESTIMATED FREIGHT", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"361", executeFrom:"01/29/2017", shipQuantity:"1", locations: {origin: {id:"48847", name:"ARROW ELECTRONICS INC", address:"665 MAESTRO DR STE 100", city:"RENO", sau:"NV", postalCode:"89511-2208", country:"US"}, destination: {id:"47TFT", name:"NORTHROP GRUMMAN", address:"BUILDING  8, SUITE B", city:"TOPEKA", sau:"KS", postalCode:"66619", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "TS"}, {qualifier: "SLI", value: "470"}, {qualifier: "PE", value: "TFT"}, {qualifier: "12", value: "4"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "3CASPT-13-B-0055"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42794"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "7500146234", orderDate: "42690", orderType: "MPFO", quantity: 493, sector:"MS - Mission Systems", psa:false, incoTerms: "FOB Destination", supplierId: "62908", supplierName: "BOYD METALS OF JOPLIN INC", lines: [{lineNumber:"1", itemNumber:"", itemDescription:"6061FB1 2X5 12 , BAR", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"362", executeFrom:"10/31/2016", shipQuantity:"1", locations: {origin: {id:"62908", name:"BOYD METALS OF JOPLIN INC", address:"1027 BYERS AVE", city:"JOPLIN", sau:"MO", postalCode:"64801-4309", country:"US"}, destination: {id:"47TFT", name:"NGTSI SSG", address:"BLDG 8, SUITE B", city:"TOPEKA", sau:"KS", postalCode:"66619", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "TS"}, {qualifier: "SLI", value: "470"}, {qualifier: "PE", value: "TFT"}, {qualifier: "12", value: "4"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "3CASPT-13-B-0055"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42704"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "7500146234", orderDate: "42690", orderType: "MPFO", quantity: 87, sector:"MS - Mission Systems", psa:false, incoTerms: "FOB Destination", supplierId: "62908", supplierName: "BOYD METALS OF JOPLIN INC", lines: [{lineNumber:"2", itemNumber:"", itemDescription:"11 GA STEEL SHEET 60  X", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"363", executeFrom:"10/31/2016", shipQuantity:"50", locations: {origin: {id:"62908", name:"BOYD METALS OF JOPLIN INC", address:"1027 BYERS AVE", city:"JOPLIN", sau:"MO", postalCode:"64801-4309", country:"US"}, destination: {id:"47TFT", name:"NGTSI SSG", address:"BLDG 8, SUITE B", city:"TOPEKA", sau:"KS", postalCode:"66619", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "TS"}, {qualifier: "SLI", value: "470"}, {qualifier: "PE", value: "TFT"}, {qualifier: "12", value: "4"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "3CASPT-13-B-0055"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42704"}, {qualifier: "QTY", value: "50"}]}]},
{orderNumber: "7500146234", orderDate: "42690", orderType: "MPFO", quantity: 12, sector:"MS - Mission Systems", psa:false, incoTerms: "FOB Destination", supplierId: "62908", supplierName: "BOYD METALS OF JOPLIN INC", lines: [{lineNumber:"3", itemNumber:"", itemDescription:"48 X120  16 GA STEEL SHE", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"364", executeFrom:"10/31/2016", shipQuantity:"15", locations: {origin: {id:"62908", name:"BOYD METALS OF JOPLIN INC", address:"1027 BYERS AVE", city:"JOPLIN", sau:"MO", postalCode:"64801-4309", country:"US"}, destination: {id:"47TFT", name:"NGTSI SSG", address:"BLDG 8, SUITE B", city:"TOPEKA", sau:"KS", postalCode:"66619", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "TS"}, {qualifier: "SLI", value: "470"}, {qualifier: "PE", value: "TFT"}, {qualifier: "12", value: "4"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "3CASPT-13-B-0055"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42704"}, {qualifier: "QTY", value: "15"}]}]},
{orderNumber: "7500146234", orderDate: "42690", orderType: "MPFO", quantity: 344, sector:"MS - Mission Systems", psa:false, incoTerms: "FOB Destination", supplierId: "62908", supplierName: "BOYD METALS OF JOPLIN INC", lines: [{lineNumber:"4", itemNumber:"", itemDescription:"FREIGHT", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"365", executeFrom:"10/31/2016", shipQuantity:"1", locations: {origin: {id:"62908", name:"BOYD METALS OF JOPLIN INC", address:"1027 BYERS AVE", city:"JOPLIN", sau:"MO", postalCode:"64801-4309", country:"US"}, destination: {id:"47TFT", name:"NGTSI SSG", address:"BLDG 8, SUITE B", city:"TOPEKA", sau:"KS", postalCode:"66619", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "TS"}, {qualifier: "SLI", value: "470"}, {qualifier: "PE", value: "TFT"}, {qualifier: "12", value: "4"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "3CASPT-13-B-0055"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42704"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "7500146300", orderDate: "42690", orderType: "MPFO", quantity: 405, sector:"MS - Mission Systems", psa:false, incoTerms: "FOB Destination", supplierId: "25770", supplierName: "BEAVER DRILL & TOOL COMPANY", lines: [{lineNumber:"1", itemNumber:"", itemDescription:"42861, PUNCH", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"366", executeFrom:"10/31/2016", shipQuantity:"1", locations: {origin: {id:"25770", name:"BEAVER DRILL & TOOL COMPANY", address:"3995 MISSION RD", city:"KANSAS CITY", sau:"KS", postalCode:"66103-2749", country:"US"}, destination: {id:"47TFT", name:"NGTSI SSG", address:"BLDG 8, SUITE B", city:"TOPEKA", sau:"KS", postalCode:"66619", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "TS"}, {qualifier: "SLI", value: "470"}, {qualifier: "PE", value: "TFT"}, {qualifier: "12", value: "4"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "3CASPT-13-B-0055"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42704"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "7500146300", orderDate: "42690", orderType: "MPFO", quantity: 34, sector:"MS - Mission Systems", psa:false, incoTerms: "FOB Destination", supplierId: "25770", supplierName: "BEAVER DRILL & TOOL COMPANY", lines: [{lineNumber:"2", itemNumber:"", itemDescription:"419106, REAMER", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"367", executeFrom:"10/31/2016", shipQuantity:"2", locations: {origin: {id:"25770", name:"BEAVER DRILL & TOOL COMPANY", address:"3995 MISSION RD", city:"KANSAS CITY", sau:"KS", postalCode:"66103-2749", country:"US"}, destination: {id:"47TFT", name:"NGTSI SSG", address:"BLDG 8, SUITE B", city:"TOPEKA", sau:"KS", postalCode:"66619", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "TS"}, {qualifier: "SLI", value: "470"}, {qualifier: "PE", value: "TFT"}, {qualifier: "12", value: "4"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "3CASPT-13-B-0055"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42704"}, {qualifier: "QTY", value: "2"}]}]},
{orderNumber: "7500146300", orderDate: "42690", orderType: "MPFO", quantity: 364, sector:"MS - Mission Systems", psa:false, incoTerms: "FOB Destination", supplierId: "25770", supplierName: "BEAVER DRILL & TOOL COMPANY", lines: [{lineNumber:"3", itemNumber:"", itemDescription:"420218, REAMER", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"368", executeFrom:"10/31/2016", shipQuantity:"1", locations: {origin: {id:"25770", name:"BEAVER DRILL & TOOL COMPANY", address:"3995 MISSION RD", city:"KANSAS CITY", sau:"KS", postalCode:"66103-2749", country:"US"}, destination: {id:"47TFT", name:"NGTSI SSG", address:"BLDG 8, SUITE B", city:"TOPEKA", sau:"KS", postalCode:"66619", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "TS"}, {qualifier: "SLI", value: "470"}, {qualifier: "PE", value: "TFT"}, {qualifier: "12", value: "4"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "3CASPT-13-B-0055"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42704"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "7500146300", orderDate: "42690", orderType: "MPFO", quantity: 333, sector:"MS - Mission Systems", psa:false, incoTerms: "FOB Destination", supplierId: "25770", supplierName: "BEAVER DRILL & TOOL COMPANY", lines: [{lineNumber:"4", itemNumber:"", itemDescription:"FREIGHT", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"369", executeFrom:"10/31/2016", shipQuantity:"1", locations: {origin: {id:"25770", name:"BEAVER DRILL & TOOL COMPANY", address:"3995 MISSION RD", city:"KANSAS CITY", sau:"KS", postalCode:"66103-2749", country:"US"}, destination: {id:"47TFT", name:"NGTSI SSG", address:"BLDG 8, SUITE B", city:"TOPEKA", sau:"KS", postalCode:"66619", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "TS"}, {qualifier: "SLI", value: "470"}, {qualifier: "PE", value: "TFT"}, {qualifier: "12", value: "4"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "3CASPT-13-B-0055"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42704"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "7500146414", orderDate: "42690", orderType: "MPFO", quantity: 410, sector:"MS - Mission Systems", psa:false, incoTerms: "FOB Destination", supplierId: "56356", supplierName: "AIRGAS MIDSOUTH INC", lines: [{lineNumber:"1", itemNumber:"", itemDescription:"K4533, CLOTH WIPING, KEM", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"370", executeFrom:"10/30/2016", shipQuantity:"50", locations: {origin: {id:"56356", name:"AIRGAS MIDSOUTH INC", address:"31 N PEORIA AVE", city:"TULSA", sau:"OK", postalCode:"74120-1622", country:"US"}, destination: {id:"47TFT", name:"NORTHROP GRUMMAN", address:"BLDG  8, SUITE B", city:"TOPEKA", sau:"KS", postalCode:"66619", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "TS"}, {qualifier: "SLI", value: "470"}, {qualifier: "PE", value: "TFT"}, {qualifier: "12", value: "4"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "3CASPT-13-B-0055"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42703"}, {qualifier: "QTY", value: "50"}]}]},
{orderNumber: "7500146414", orderDate: "42690", orderType: "MPFO", quantity: 260, sector:"MS - Mission Systems", psa:false, incoTerms: "FOB Destination", supplierId: "56356", supplierName: "AIRGAS MIDSOUTH INC", lines: [{lineNumber:"2", itemNumber:"", itemDescription:"ESTIMATED FREIGHT", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"371", executeFrom:"01/29/2017", shipQuantity:"1", locations: {origin: {id:"56356", name:"AIRGAS MIDSOUTH INC", address:"31 N PEORIA AVE", city:"TULSA", sau:"OK", postalCode:"74120-1622", country:"US"}, destination: {id:"47TFT", name:"NORTHROP GRUMMAN", address:"BLDG  8, SUITE B", city:"TOPEKA", sau:"KS", postalCode:"66619", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "TS"}, {qualifier: "SLI", value: "470"}, {qualifier: "PE", value: "TFT"}, {qualifier: "12", value: "4"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "3CASPT-13-B-0055"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42794"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "7500146435", orderDate: "42690", orderType: "MPFO", quantity: 418, sector:"MS - Mission Systems", psa:false, incoTerms: "FOB Destination", supplierId: "061766", supplierName: "CLASSIC AUTOMATION LLC", lines: [{lineNumber:"1", itemNumber:"", itemDescription:"REPAIR 5998-04-000-1020", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"372", executeFrom:"11/30/2016", shipQuantity:"2", locations: {origin: {id:"061766", name:"CLASSIC AUTOMATION LLC", address:"800 SALT RD", city:"WEBSTER", sau:"NY", postalCode:"14580-9666", country:"US"}, destination: {id:"47TFT", name:"NORTHROP GRUMMAN TS", address:"7215 SW TOPEKA BLVD BLDG 8 STE B", city:"TOPEKA", sau:"KS", postalCode:"66619", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "TS"}, {qualifier: "SLI", value: "470"}, {qualifier: "PE", value: "TFT"}, {qualifier: "12", value: "4"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "3CASPT-13-B-0055"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42734"}, {qualifier: "QTY", value: "2"}]}]},
{orderNumber: "7500146435", orderDate: "42690", orderType: "MPFO", quantity: 23, sector:"MS - Mission Systems", psa:false, incoTerms: "FOB Destination", supplierId: "061766", supplierName: "CLASSIC AUTOMATION LLC", lines: [{lineNumber:"2", itemNumber:"", itemDescription:"REPAIR 5963-05-000-1684", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"373", executeFrom:"11/30/2016", shipQuantity:"2", locations: {origin: {id:"061766", name:"CLASSIC AUTOMATION LLC", address:"800 SALT RD", city:"WEBSTER", sau:"NY", postalCode:"14580-9666", country:"US"}, destination: {id:"47TFT", name:"NORTHROP GRUMMAN TS", address:"7215 SW TOPEKA BLVD BLDG 8 STE B", city:"TOPEKA", sau:"KS", postalCode:"66619", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "TS"}, {qualifier: "SLI", value: "470"}, {qualifier: "PE", value: "TFT"}, {qualifier: "12", value: "4"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "3CASPT-13-B-0055"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42734"}, {qualifier: "QTY", value: "2"}]}]},
{orderNumber: "7500146435", orderDate: "42690", orderType: "MPFO", quantity: 167, sector:"MS - Mission Systems", psa:false, incoTerms: "FOB Destination", supplierId: "061766", supplierName: "CLASSIC AUTOMATION LLC", lines: [{lineNumber:"3", itemNumber:"", itemDescription:"REPAIR 6130-04-000-0896", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"374", executeFrom:"11/30/2016", shipQuantity:"3", locations: {origin: {id:"061766", name:"CLASSIC AUTOMATION LLC", address:"800 SALT RD", city:"WEBSTER", sau:"NY", postalCode:"14580-9666", country:"US"}, destination: {id:"47TFT", name:"NORTHROP GRUMMAN TS", address:"7215 SW TOPEKA BLVD BLDG 8 STE B", city:"TOPEKA", sau:"KS", postalCode:"66619", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "TS"}, {qualifier: "SLI", value: "470"}, {qualifier: "PE", value: "TFT"}, {qualifier: "12", value: "4"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "3CASPT-13-B-0055"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42734"}, {qualifier: "QTY", value: "3"}]}]},
{orderNumber: "7500146435", orderDate: "42690", orderType: "MPFO", quantity: 159, sector:"MS - Mission Systems", psa:false, incoTerms: "FOB Destination", supplierId: "061766", supplierName: "CLASSIC AUTOMATION LLC", lines: [{lineNumber:"4", itemNumber:"", itemDescription:"REPAIR 3040-04-000-3860", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"375", executeFrom:"11/30/2016", shipQuantity:"1", locations: {origin: {id:"061766", name:"CLASSIC AUTOMATION LLC", address:"800 SALT RD", city:"WEBSTER", sau:"NY", postalCode:"14580-9666", country:"US"}, destination: {id:"47TFT", name:"NORTHROP GRUMMAN TS", address:"7215 SW TOPEKA BLVD BLDG 8 STE B", city:"TOPEKA", sau:"KS", postalCode:"66619", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "TS"}, {qualifier: "SLI", value: "470"}, {qualifier: "PE", value: "TFT"}, {qualifier: "12", value: "4"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "3CASPT-13-B-0055"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42734"}, {qualifier: "QTY", value: "1"}]}]},
{orderNumber: "7500146435", orderDate: "42690", orderType: "MPFO", quantity: 408, sector:"MS - Mission Systems", psa:false, incoTerms: "FOB Destination", supplierId: "061766", supplierName: "CLASSIC AUTOMATION LLC", lines: [{lineNumber:"5", itemNumber:"", itemDescription:"FREIGHT", fulfillmentMethod:"EA", fulfillmentTolerance:"0", status:"FALSE", deliveryScheduleNumber:"376", executeFrom:"11/30/2016", shipQuantity:"1", locations: {origin: {id:"061766", name:"CLASSIC AUTOMATION LLC", address:"800 SALT RD", city:"WEBSTER", sau:"NY", postalCode:"14580-9666", country:"US"}, destination: {id:"47TFT", name:"NORTHROP GRUMMAN TS", address:"7215 SW TOPEKA BLVD BLDG 8 STE B", city:"TOPEKA", sau:"KS", postalCode:"66619", country:"US"}}, references: [{qualifier: "P4", value: ""}, {qualifier: "87", value: "TS"}, {qualifier: "SLI", value: "470"}, {qualifier: "PE", value: "TFT"}, {qualifier: "12", value: "4"}, {qualifier: "KD", value: "N"}, {qualifier: "PKG", value: "N"}, {qualifier: "KL", value: "D"}, {qualifier: "CT", value: "3CASPT-13-B-0055"}, {qualifier: "CCNM", value: ""}, {qualifier: "72", value: ""}, {qualifier: "IL", value: ""}, {qualifier: "RSFD", value: "42734"}, {qualifier: "QTY", value: "1"}]}]}
]

    
    svc.shipments=[
    {
        clientCode: "MTL",
        client: "Mattel",
        loadNumber: "31125032",
        shipmentNumber: "SH-036304473",
        proNumber: "12251352701",
        bolNumber: "2403404237",
        airWaybillNumber: "",
        trailerNumber: "636421",
        statusCode: 8,
        status: "Tender Accepted",
        isUrgent: true,
        isOnHold: false,
        isHazmat: false,
        carrierScac: "GDSD",
        carrierName: "GDS Express",
        baseMode: "GROUND",
        vesselName: "",
        transportModeCode: "TL",
        transportMode: "Truckload",
        equipmentCode: "53DV",
        equipment: "53FT Dry Van",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "MTL1",
        logisticsGroup: "MTL1",
        transactionSource: "GIS",
        memo: "",
        origin: {
            originId: "P30",
            originName: "San Bernardino Distribution Center",
            originCity: "San Bernardino",
            originSau: "CA",
            originCountry: "USA",
            originPostalCode: "92408"
        },
        destination: {
            destinationId: "S2425090",
            destinationName: "Amazon.com CMH2",
            destinationCity: "Groveport",
            destinationSau: "OH",
            destinationCountry: "USA",
            destinationPostalCode: "43125"
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "GROUND",
                distance: "1255",
                distanceUom: "MILE",
                percentageOfTotal: "100%",
                exceptions: {}
            }
        }
    },
    {
        clientCode: "MTL",
        client: "Mattel",
        loadNumber: "31125032",
        shipmentNumber: "SH-036304479",
        proNumber: "",
        bolNumber: "",
        airWaybillNumber: "654471555",
        trailerNumber: "636422",
        statusCode: 7,
        status: "Tendered",
        isUrgent: false,
        isOnHold: false,
        isHazmat: false,
        carrierScac: "FXFE",
        carrierName: "FedEx Express",
        baseMode: "AIR",
        vesselName: "",
        transportModeCode: "1AR",
        transportMode: "Overnight - Early AM",
        equipmentCode: "",
        equipment: "",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "MTL1",
        logisticsGroup: "MTL1",
        transactionSource: "GIS",
        memo: "",
        origin: {
            originId: "P30",
            originName: "San Bernardino Distribution Center",
            originCity: "San Bernardino",
            originSau: "CA",
            originCountry: "USA",
            originPostalCode: "92408"
        },
        destination: {
            destinationId: "S2425090",
            destinationName: "Amazon.com CMH2",
            destinationCity: "Groveport",
            destinationSau: "OH",
            destinationCountry: "USA",
            destinationPostalCode: "43125"
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "AIR",
                distance: "415",
                distanceUom: "MILE",
                percentageOfTotal: "100%",
                exceptions: {}
            }
        }
    },
    {
        clientCode: "MTL",
        client: "Mattel",
        loadNumber: "31125032",
        shipmentNumber: "SH-036304483",
        proNumber: "12251352702",
        bolNumber: "2403404237",
        airWaybillNumber: "",
        trailerNumber: "636422",
        statusCode: 7,
        status: "Tendered",
        isUrgent: false,
        isOnHold: false,
        isHazmat: false,
        carrierScac: "MAES",
        carrierName: "Maersk",
        baseMode: "OCEAN",
        vesselName: "MAERSK ALABAMA",
        transportModeCode: "IM",
        transportMode: "Intermodal",
        equipmentCode: "",
        equipment: "",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "MTL1",
        logisticsGroup: "MTL1",
        transactionSource: "GIS",
        memo: "",
        origin: {
            originId: "P30",
            originName: "San Bernardino Distribution Center",
            originCity: "San Bernardino",
            originSau: "CA",
            originCountry: "USA",
            originPostalCode: "92408"
        },
        destination: {
            destinationId: "S2425090",
            destinationName: "Amazon.com CMH2",
            destinationCity: "Groveport",
            destinationSau: "OH",
            destinationCountry: "USA",
            destinationPostalCode: "43125"
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "GROUND",
                distance: "133",
                distanceUom: "MILE",
                percentageOfTotal: "20%"
            },
            200: {
                baseMode: "OCEAN",
                distance: "532",
                distanceUom: "MILE",
                percentageOfTotal: "80%",
                exceptions: [
                {
                    exceptionCode: "SD",
                    exception: "Shipment Delay",
                    reasonCode: "22",
                    reason: "Weather Related",
                    exceptionPoint: "35%"
                }
                ]

            }
        }
    },
    {
        clientCode: "COV",
        client: "Covestro",
        loadNumber: "29163218",
        shipmentNumber: "SH-033643151",
        proNumber: "12251352702",
        bolNumber: "2403404237",
        airWaybillNumber: "",
        trailerNumber: "636422",
        statusCode: 7,
        status: "Delivered",
        isUrgent: false,
        isOnHold: false,
        isHazmat: true,
        carrierScac: "UP",
        carrierName: "Union Pacific",
        baseMode: "RAIL",
        vesselName: "",
        transportModeCode: "RAIL",
        transportMode: "Rail",
        equipmentCode: "",
        equipment: "",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "COV1",
        logisticsGroup: "COV2",
        transactionSource: "MANUAL",
        memo: "",
        origin: {
            originId: "OR54",
            originName: "Baytown Rail SP Covestro LLC",
            originCity: "Baytown",
            originSau: "TX",
            originCountry: "USA",
            originPostalCode: "77523",
            contacts: [
            {
                contactName: "Deanna Mclean",
                contactPhone: "+1(281) 383-7695",
                primaryContact: true
            },
            {
                contactName: "John Doe",
                contactPhone: "+1(555) 867-5309",
                primaryContact: false
            }
            ]
        },
        destination: {
            destinationId: "0005288788",
            destinationName: "Plant ORW2 Covestro LLC",
            destinationCity: "CARSON",
            destinationSau: "CA",
            destinationCountry: "USA",
            destinationPostalCode: "90810",
            contacts: [
            {
                contactName: "NA",
                contactPhone: "+1(310) 603-8797",
                primaryContact: true
            }
            ]
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "RAIL",
                distance: "2477",
                distanceUom: "MILE",
                percentageOfTotal: "100%",
                exceptions: {}
            }
        }
    },
    {
        clientCode: "MTL",
        client: "Mattel",
        loadNumber: "31125032",
        shipmentNumber: "SH-036304473",
        proNumber: "12251352701",
        bolNumber: "2403404237",
        airWaybillNumber: "",
        trailerNumber: "636421",
        statusCode: 8,
        status: "Tender Accepted",
        isUrgent: true,
        isOnHold: false,
        isHazmat: false,
        carrierScac: "GDSD",
        carrierName: "GDS Express",
        baseMode: "GROUND",
        vesselName: "",
        transportModeCode: "TL",
        transportMode: "Truckload",
        equipmentCode: "53DV",
        equipment: "53FT Dry Van",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "MTL1",
        logisticsGroup: "MTL1",
        transactionSource: "GIS",
        memo: "",
        origin: {
            originId: "P30",
            originName: "San Bernardino Distribution Center",
            originCity: "San Bernardino",
            originSau: "CA",
            originCountry: "USA",
            originPostalCode: "92408"
        },
        destination: {
            destinationId: "S2425090",
            destinationName: "Amazon.com CMH2",
            destinationCity: "Groveport",
            destinationSau: "OH",
            destinationCountry: "USA",
            destinationPostalCode: "43125"
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "GROUND",
                distance: "1255",
                distanceUom: "MILE",
                percentageOfTotal: "100%",
                exceptions: {}
            }
        }
    },
    {
        clientCode: "MTL",
        client: "Mattel",
        loadNumber: "31125032",
        shipmentNumber: "SH-036304479",
        proNumber: "",
        bolNumber: "",
        airWaybillNumber: "654471555",
        trailerNumber: "636422",
        statusCode: 7,
        status: "Tendered",
        isUrgent: false,
        isOnHold: false,
        isHazmat: false,
        carrierScac: "FXFE",
        carrierName: "FedEx Express",
        baseMode: "AIR",
        vesselName: "",
        transportModeCode: "1AR",
        transportMode: "Overnight - Early AM",
        equipmentCode: "",
        equipment: "",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "MTL1",
        logisticsGroup: "MTL1",
        transactionSource: "GIS",
        memo: "",
        origin: {
            originId: "P30",
            originName: "San Bernardino Distribution Center",
            originCity: "San Bernardino",
            originSau: "CA",
            originCountry: "USA",
            originPostalCode: "92408"
        },
        destination: {
            destinationId: "S2425090",
            destinationName: "Amazon.com CMH2",
            destinationCity: "Groveport",
            destinationSau: "OH",
            destinationCountry: "USA",
            destinationPostalCode: "43125"
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "AIR",
                distance: "415",
                distanceUom: "MILE",
                percentageOfTotal: "100%",
                exceptions: {}
            }
        }
    },
    {
        clientCode: "MTL",
        client: "Mattel",
        loadNumber: "31125032",
        shipmentNumber: "SH-036304483",
        proNumber: "12251352702",
        bolNumber: "2403404237",
        airWaybillNumber: "",
        trailerNumber: "636422",
        statusCode: 7,
        status: "Tendered",
        isUrgent: false,
        isOnHold: false,
        isHazmat: false,
        carrierScac: "MAES",
        carrierName: "Maersk",
        baseMode: "OCEAN",
        vesselName: "MAERSK ALABAMA",
        transportModeCode: "IM",
        transportMode: "Intermodal",
        equipmentCode: "",
        equipment: "",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "MTL1",
        logisticsGroup: "MTL1",
        transactionSource: "GIS",
        memo: "",
        origin: {
            originId: "P30",
            originName: "San Bernardino Distribution Center",
            originCity: "San Bernardino",
            originSau: "CA",
            originCountry: "USA",
            originPostalCode: "92408"
        },
        destination: {
            destinationId: "S2425090",
            destinationName: "Amazon.com CMH2",
            destinationCity: "Groveport",
            destinationSau: "OH",
            destinationCountry: "USA",
            destinationPostalCode: "43125"
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "GROUND",
                distance: "133",
                distanceUom: "MILE",
                percentageOfTotal: "20%"
            },
            200: {
                baseMode: "OCEAN",
                distance: "532",
                distanceUom: "MILE",
                percentageOfTotal: "80%",
                exceptions: [
                {
                    exceptionCode: "SD",
                    exception: "Shipment Delay",
                    reasonCode: "22",
                    reason: "Weather Related",
                    exceptionPoint: "35%"
                }
                ]

            }
        }
    },
    {
        clientCode: "COV",
        client: "Covestro",
        loadNumber: "29163218",
        shipmentNumber: "SH-033643151",
        proNumber: "12251352702",
        bolNumber: "2403404237",
        airWaybillNumber: "",
        trailerNumber: "636422",
        statusCode: 7,
        status: "Delivered",
        isUrgent: false,
        isOnHold: false,
        isHazmat: true,
        carrierScac: "UP",
        carrierName: "Union Pacific",
        baseMode: "RAIL",
        vesselName: "",
        transportModeCode: "RAIL",
        transportMode: "Rail",
        equipmentCode: "",
        equipment: "",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "COV1",
        logisticsGroup: "COV2",
        transactionSource: "MANUAL",
        memo: "",
        origin: {
            originId: "OR54",
            originName: "Baytown Rail SP Covestro LLC",
            originCity: "Baytown",
            originSau: "TX",
            originCountry: "USA",
            originPostalCode: "77523",
            contacts: [
            {
                contactName: "Deanna Mclean",
                contactPhone: "+1(281) 383-7695",
                primaryContact: true
            },
            {
                contactName: "John Doe",
                contactPhone: "+1(555) 867-5309",
                primaryContact: false
            }
            ]
        },
        destination: {
            destinationId: "0005288788",
            destinationName: "Plant ORW2 Covestro LLC",
            destinationCity: "CARSON",
            destinationSau: "CA",
            destinationCountry: "USA",
            destinationPostalCode: "90810",
            contacts: [
            {
                contactName: "NA",
                contactPhone: "+1(310) 603-8797",
                primaryContact: true
            }
            ]
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "RAIL",
                distance: "2477",
                distanceUom: "MILE",
                percentageOfTotal: "100%",
                exceptions: {}
            }
        }
    },
    {
        clientCode: "MTL",
        client: "Mattel",
        loadNumber: "31125032",
        shipmentNumber: "SH-036304473",
        proNumber: "12251352701",
        bolNumber: "2403404237",
        airWaybillNumber: "",
        trailerNumber: "636421",
        statusCode: 8,
        status: "Tender Accepted",
        isUrgent: true,
        isOnHold: false,
        isHazmat: false,
        carrierScac: "GDSD",
        carrierName: "GDS Express",
        baseMode: "GROUND",
        vesselName: "",
        transportModeCode: "TL",
        transportMode: "Truckload",
        equipmentCode: "53DV",
        equipment: "53FT Dry Van",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "MTL1",
        logisticsGroup: "MTL1",
        transactionSource: "GIS",
        memo: "",
        origin: {
            originId: "P30",
            originName: "San Bernardino Distribution Center",
            originCity: "San Bernardino",
            originSau: "CA",
            originCountry: "USA",
            originPostalCode: "92408"
        },
        destination: {
            destinationId: "S2425090",
            destinationName: "Amazon.com CMH2",
            destinationCity: "Groveport",
            destinationSau: "OH",
            destinationCountry: "USA",
            destinationPostalCode: "43125"
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "GROUND",
                distance: "1255",
                distanceUom: "MILE",
                percentageOfTotal: "100%",
                exceptions: {}
            }
        }
    },
    {
        clientCode: "MTL",
        client: "Mattel",
        loadNumber: "31125032",
        shipmentNumber: "SH-036304479",
        proNumber: "",
        bolNumber: "",
        airWaybillNumber: "654471555",
        trailerNumber: "636422",
        statusCode: 7,
        status: "Tendered",
        isUrgent: false,
        isOnHold: false,
        isHazmat: false,
        carrierScac: "FXFE",
        carrierName: "FedEx Express",
        baseMode: "AIR",
        vesselName: "",
        transportModeCode: "1AR",
        transportMode: "Overnight - Early AM",
        equipmentCode: "",
        equipment: "",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "MTL1",
        logisticsGroup: "MTL1",
        transactionSource: "GIS",
        memo: "",
        origin: {
            originId: "P30",
            originName: "San Bernardino Distribution Center",
            originCity: "San Bernardino",
            originSau: "CA",
            originCountry: "USA",
            originPostalCode: "92408"
        },
        destination: {
            destinationId: "S2425090",
            destinationName: "Amazon.com CMH2",
            destinationCity: "Groveport",
            destinationSau: "OH",
            destinationCountry: "USA",
            destinationPostalCode: "43125"
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "AIR",
                distance: "415",
                distanceUom: "MILE",
                percentageOfTotal: "100%",
                exceptions: {}
            }
        }
    },
    {
        clientCode: "MTL",
        client: "Mattel",
        loadNumber: "31125032",
        shipmentNumber: "SH-036304483",
        proNumber: "12251352702",
        bolNumber: "2403404237",
        airWaybillNumber: "",
        trailerNumber: "636422",
        statusCode: 7,
        status: "Tendered",
        isUrgent: false,
        isOnHold: false,
        isHazmat: false,
        carrierScac: "MAES",
        carrierName: "Maersk",
        baseMode: "OCEAN",
        vesselName: "MAERSK ALABAMA",
        transportModeCode: "IM",
        transportMode: "Intermodal",
        equipmentCode: "",
        equipment: "",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "MTL1",
        logisticsGroup: "MTL1",
        transactionSource: "GIS",
        memo: "",
        origin: {
            originId: "P30",
            originName: "San Bernardino Distribution Center",
            originCity: "San Bernardino",
            originSau: "CA",
            originCountry: "USA",
            originPostalCode: "92408"
        },
        destination: {
            destinationId: "S2425090",
            destinationName: "Amazon.com CMH2",
            destinationCity: "Groveport",
            destinationSau: "OH",
            destinationCountry: "USA",
            destinationPostalCode: "43125"
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "GROUND",
                distance: "133",
                distanceUom: "MILE",
                percentageOfTotal: "20%"
            },
            200: {
                baseMode: "OCEAN",
                distance: "532",
                distanceUom: "MILE",
                percentageOfTotal: "80%",
                exceptions: [
                {
                    exceptionCode: "SD",
                    exception: "Shipment Delay",
                    reasonCode: "22",
                    reason: "Weather Related",
                    exceptionPoint: "35%"
                }
                ]

            }
        }
    },
    {
        clientCode: "COV",
        client: "Covestro",
        loadNumber: "29163218",
        shipmentNumber: "SH-033643151",
        proNumber: "12251352702",
        bolNumber: "2403404237",
        airWaybillNumber: "",
        trailerNumber: "636422",
        statusCode: 7,
        status: "Delivered",
        isUrgent: false,
        isOnHold: false,
        isHazmat: true,
        carrierScac: "UP",
        carrierName: "Union Pacific",
        baseMode: "RAIL",
        vesselName: "",
        transportModeCode: "RAIL",
        transportMode: "Rail",
        equipmentCode: "",
        equipment: "",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "COV1",
        logisticsGroup: "COV2",
        transactionSource: "MANUAL",
        memo: "",
        origin: {
            originId: "OR54",
            originName: "Baytown Rail SP Covestro LLC",
            originCity: "Baytown",
            originSau: "TX",
            originCountry: "USA",
            originPostalCode: "77523",
            contacts: [
            {
                contactName: "Deanna Mclean",
                contactPhone: "+1(281) 383-7695",
                primaryContact: true
            },
            {
                contactName: "John Doe",
                contactPhone: "+1(555) 867-5309",
                primaryContact: false
            }
            ]
        },
        destination: {
            destinationId: "0005288788",
            destinationName: "Plant ORW2 Covestro LLC",
            destinationCity: "CARSON",
            destinationSau: "CA",
            destinationCountry: "USA",
            destinationPostalCode: "90810",
            contacts: [
            {
                contactName: "NA",
                contactPhone: "+1(310) 603-8797",
                primaryContact: true
            }
            ]
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "RAIL",
                distance: "2477",
                distanceUom: "MILE",
                percentageOfTotal: "100%",
                exceptions: {}
            }
        }
    },
    {
        clientCode: "MTL",
        client: "Mattel",
        loadNumber: "31125032",
        shipmentNumber: "SH-036304473",
        proNumber: "12251352701",
        bolNumber: "2403404237",
        airWaybillNumber: "",
        trailerNumber: "636421",
        statusCode: 8,
        status: "Tender Accepted",
        isUrgent: true,
        isOnHold: false,
        isHazmat: false,
        carrierScac: "GDSD",
        carrierName: "GDS Express",
        baseMode: "GROUND",
        vesselName: "",
        transportModeCode: "TL",
        transportMode: "Truckload",
        equipmentCode: "53DV",
        equipment: "53FT Dry Van",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "MTL1",
        logisticsGroup: "MTL1",
        transactionSource: "GIS",
        memo: "",
        origin: {
            originId: "P30",
            originName: "San Bernardino Distribution Center",
            originCity: "San Bernardino",
            originSau: "CA",
            originCountry: "USA",
            originPostalCode: "92408"
        },
        destination: {
            destinationId: "S2425090",
            destinationName: "Amazon.com CMH2",
            destinationCity: "Groveport",
            destinationSau: "OH",
            destinationCountry: "USA",
            destinationPostalCode: "43125"
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "GROUND",
                distance: "1255",
                distanceUom: "MILE",
                percentageOfTotal: "100%",
                exceptions: {}
            }
        }
    },
    {
        clientCode: "MTL",
        client: "Mattel",
        loadNumber: "31125032",
        shipmentNumber: "SH-036304479",
        proNumber: "",
        bolNumber: "",
        airWaybillNumber: "654471555",
        trailerNumber: "636422",
        statusCode: 7,
        status: "Tendered",
        isUrgent: false,
        isOnHold: false,
        isHazmat: false,
        carrierScac: "FXFE",
        carrierName: "FedEx Express",
        baseMode: "AIR",
        vesselName: "",
        transportModeCode: "1AR",
        transportMode: "Overnight - Early AM",
        equipmentCode: "",
        equipment: "",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "MTL1",
        logisticsGroup: "MTL1",
        transactionSource: "GIS",
        memo: "",
        origin: {
            originId: "P30",
            originName: "San Bernardino Distribution Center",
            originCity: "San Bernardino",
            originSau: "CA",
            originCountry: "USA",
            originPostalCode: "92408"
        },
        destination: {
            destinationId: "S2425090",
            destinationName: "Amazon.com CMH2",
            destinationCity: "Groveport",
            destinationSau: "OH",
            destinationCountry: "USA",
            destinationPostalCode: "43125"
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "AIR",
                distance: "415",
                distanceUom: "MILE",
                percentageOfTotal: "100%",
                exceptions: {}
            }
        }
    },
    {
        clientCode: "MTL",
        client: "Mattel",
        loadNumber: "31125032",
        shipmentNumber: "SH-036304483",
        proNumber: "12251352702",
        bolNumber: "2403404237",
        airWaybillNumber: "",
        trailerNumber: "636422",
        statusCode: 7,
        status: "Tendered",
        isUrgent: false,
        isOnHold: false,
        isHazmat: false,
        carrierScac: "MAES",
        carrierName: "Maersk",
        baseMode: "OCEAN",
        vesselName: "MAERSK ALABAMA",
        transportModeCode: "IM",
        transportMode: "Intermodal",
        equipmentCode: "",
        equipment: "",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "MTL1",
        logisticsGroup: "MTL1",
        transactionSource: "GIS",
        memo: "",
        origin: {
            originId: "P30",
            originName: "San Bernardino Distribution Center",
            originCity: "San Bernardino",
            originSau: "CA",
            originCountry: "USA",
            originPostalCode: "92408"
        },
        destination: {
            destinationId: "S2425090",
            destinationName: "Amazon.com CMH2",
            destinationCity: "Groveport",
            destinationSau: "OH",
            destinationCountry: "USA",
            destinationPostalCode: "43125"
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "GROUND",
                distance: "133",
                distanceUom: "MILE",
                percentageOfTotal: "20%"
            },
            200: {
                baseMode: "OCEAN",
                distance: "532",
                distanceUom: "MILE",
                percentageOfTotal: "80%",
                exceptions: [
                {
                    exceptionCode: "SD",
                    exception: "Shipment Delay",
                    reasonCode: "22",
                    reason: "Weather Related",
                    exceptionPoint: "35%"
                }
                ]

            }
        }
    },
    {
        clientCode: "COV",
        client: "Covestro",
        loadNumber: "29163218",
        shipmentNumber: "SH-033643151",
        proNumber: "12251352702",
        bolNumber: "2403404237",
        airWaybillNumber: "",
        trailerNumber: "636422",
        statusCode: 7,
        status: "Delivered",
        isUrgent: false,
        isOnHold: false,
        isHazmat: true,
        carrierScac: "UP",
        carrierName: "Union Pacific",
        baseMode: "RAIL",
        vesselName: "",
        transportModeCode: "RAIL",
        transportMode: "Rail",
        equipmentCode: "",
        equipment: "",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "COV1",
        logisticsGroup: "COV2",
        transactionSource: "MANUAL",
        memo: "",
        origin: {
            originId: "OR54",
            originName: "Baytown Rail SP Covestro LLC",
            originCity: "Baytown",
            originSau: "TX",
            originCountry: "USA",
            originPostalCode: "77523",
            contacts: [
            {
                contactName: "Deanna Mclean",
                contactPhone: "+1(281) 383-7695",
                primaryContact: true
            },
            {
                contactName: "John Doe",
                contactPhone: "+1(555) 867-5309",
                primaryContact: false
            }
            ]
        },
        destination: {
            destinationId: "0005288788",
            destinationName: "Plant ORW2 Covestro LLC",
            destinationCity: "CARSON",
            destinationSau: "CA",
            destinationCountry: "USA",
            destinationPostalCode: "90810",
            contacts: [
            {
                contactName: "NA",
                contactPhone: "+1(310) 603-8797",
                primaryContact: true
            }
            ]
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "RAIL",
                distance: "2477",
                distanceUom: "MILE",
                percentageOfTotal: "100%",
                exceptions: {}
            }
        }
    },
    {
        clientCode: "MTL",
        client: "Mattel",
        loadNumber: "31125032",
        shipmentNumber: "SH-036304473",
        proNumber: "12251352701",
        bolNumber: "2403404237",
        airWaybillNumber: "",
        trailerNumber: "636421",
        statusCode: 8,
        status: "Tender Accepted",
        isUrgent: true,
        isOnHold: false,
        isHazmat: false,
        carrierScac: "GDSD",
        carrierName: "GDS Express",
        baseMode: "GROUND",
        vesselName: "",
        transportModeCode: "TL",
        transportMode: "Truckload",
        equipmentCode: "53DV",
        equipment: "53FT Dry Van",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "MTL1",
        logisticsGroup: "MTL1",
        transactionSource: "GIS",
        memo: "",
        origin: {
            originId: "P30",
            originName: "San Bernardino Distribution Center",
            originCity: "San Bernardino",
            originSau: "CA",
            originCountry: "USA",
            originPostalCode: "92408"
        },
        destination: {
            destinationId: "S2425090",
            destinationName: "Amazon.com CMH2",
            destinationCity: "Groveport",
            destinationSau: "OH",
            destinationCountry: "USA",
            destinationPostalCode: "43125"
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "GROUND",
                distance: "1255",
                distanceUom: "MILE",
                percentageOfTotal: "100%",
                exceptions: {}
            }
        }
    },
    {
        clientCode: "MTL",
        client: "Mattel",
        loadNumber: "31125032",
        shipmentNumber: "SH-036304479",
        proNumber: "",
        bolNumber: "",
        airWaybillNumber: "654471555",
        trailerNumber: "636422",
        statusCode: 7,
        status: "Tendered",
        isUrgent: false,
        isOnHold: false,
        isHazmat: false,
        carrierScac: "FXFE",
        carrierName: "FedEx Express",
        baseMode: "AIR",
        vesselName: "",
        transportModeCode: "1AR",
        transportMode: "Overnight - Early AM",
        equipmentCode: "",
        equipment: "",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "MTL1",
        logisticsGroup: "MTL1",
        transactionSource: "GIS",
        memo: "",
        origin: {
            originId: "P30",
            originName: "San Bernardino Distribution Center",
            originCity: "San Bernardino",
            originSau: "CA",
            originCountry: "USA",
            originPostalCode: "92408"
        },
        destination: {
            destinationId: "S2425090",
            destinationName: "Amazon.com CMH2",
            destinationCity: "Groveport",
            destinationSau: "OH",
            destinationCountry: "USA",
            destinationPostalCode: "43125"
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "AIR",
                distance: "415",
                distanceUom: "MILE",
                percentageOfTotal: "100%",
                exceptions: {}
            }
        }
    },
    {
        clientCode: "MTL",
        client: "Mattel",
        loadNumber: "31125032",
        shipmentNumber: "SH-036304483",
        proNumber: "12251352702",
        bolNumber: "2403404237",
        airWaybillNumber: "",
        trailerNumber: "636422",
        statusCode: 7,
        status: "Tendered",
        isUrgent: false,
        isOnHold: false,
        isHazmat: false,
        carrierScac: "MAES",
        carrierName: "Maersk",
        baseMode: "OCEAN",
        vesselName: "MAERSK ALABAMA",
        transportModeCode: "IM",
        transportMode: "Intermodal",
        equipmentCode: "",
        equipment: "",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "MTL1",
        logisticsGroup: "MTL1",
        transactionSource: "GIS",
        memo: "",
        origin: {
            originId: "P30",
            originName: "San Bernardino Distribution Center",
            originCity: "San Bernardino",
            originSau: "CA",
            originCountry: "USA",
            originPostalCode: "92408"
        },
        destination: {
            destinationId: "S2425090",
            destinationName: "Amazon.com CMH2",
            destinationCity: "Groveport",
            destinationSau: "OH",
            destinationCountry: "USA",
            destinationPostalCode: "43125"
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "GROUND",
                distance: "133",
                distanceUom: "MILE",
                percentageOfTotal: "20%"
            },
            200: {
                baseMode: "OCEAN",
                distance: "532",
                distanceUom: "MILE",
                percentageOfTotal: "80%",
                exceptions: [
                {
                    exceptionCode: "SD",
                    exception: "Shipment Delay",
                    reasonCode: "22",
                    reason: "Weather Related",
                    exceptionPoint: "35%"
                }
                ]

            }
        }
    },
    {
        clientCode: "COV",
        client: "Covestro",
        loadNumber: "29163218",
        shipmentNumber: "SH-033643151",
        proNumber: "12251352702",
        bolNumber: "2403404237",
        airWaybillNumber: "",
        trailerNumber: "636422",
        statusCode: 7,
        status: "Delivered",
        isUrgent: false,
        isOnHold: false,
        isHazmat: true,
        carrierScac: "UP",
        carrierName: "Union Pacific",
        baseMode: "RAIL",
        vesselName: "",
        transportModeCode: "RAIL",
        transportMode: "Rail",
        equipmentCode: "",
        equipment: "",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "COV1",
        logisticsGroup: "COV2",
        transactionSource: "MANUAL",
        memo: "",
        origin: {
            originId: "OR54",
            originName: "Baytown Rail SP Covestro LLC",
            originCity: "Baytown",
            originSau: "TX",
            originCountry: "USA",
            originPostalCode: "77523",
            contacts: [
            {
                contactName: "Deanna Mclean",
                contactPhone: "+1(281) 383-7695",
                primaryContact: true
            },
            {
                contactName: "John Doe",
                contactPhone: "+1(555) 867-5309",
                primaryContact: false
            }
            ]
        },
        destination: {
            destinationId: "0005288788",
            destinationName: "Plant ORW2 Covestro LLC",
            destinationCity: "CARSON",
            destinationSau: "CA",
            destinationCountry: "USA",
            destinationPostalCode: "90810",
            contacts: [
            {
                contactName: "NA",
                contactPhone: "+1(310) 603-8797",
                primaryContact: true
            }
            ]
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "RAIL",
                distance: "2477",
                distanceUom: "MILE",
                percentageOfTotal: "100%",
                exceptions: {}
            }
        }
    },
    {
        clientCode: "MTL",
        client: "Mattel",
        loadNumber: "31125032",
        shipmentNumber: "SH-036304473",
        proNumber: "12251352701",
        bolNumber: "2403404237",
        airWaybillNumber: "",
        trailerNumber: "636421",
        statusCode: 8,
        status: "Tender Accepted",
        isUrgent: true,
        isOnHold: false,
        isHazmat: false,
        carrierScac: "GDSD",
        carrierName: "GDS Express",
        baseMode: "GROUND",
        vesselName: "",
        transportModeCode: "TL",
        transportMode: "Truckload",
        equipmentCode: "53DV",
        equipment: "53FT Dry Van",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "MTL1",
        logisticsGroup: "MTL1",
        transactionSource: "GIS",
        memo: "",
        origin: {
            originId: "P30",
            originName: "San Bernardino Distribution Center",
            originCity: "San Bernardino",
            originSau: "CA",
            originCountry: "USA",
            originPostalCode: "92408"
        },
        destination: {
            destinationId: "S2425090",
            destinationName: "Amazon.com CMH2",
            destinationCity: "Groveport",
            destinationSau: "OH",
            destinationCountry: "USA",
            destinationPostalCode: "43125"
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "GROUND",
                distance: "1255",
                distanceUom: "MILE",
                percentageOfTotal: "100%",
                exceptions: {}
            }
        }
    },
    {
        clientCode: "MTL",
        client: "Mattel",
        loadNumber: "31125032",
        shipmentNumber: "SH-036304479",
        proNumber: "",
        bolNumber: "",
        airWaybillNumber: "654471555",
        trailerNumber: "636422",
        statusCode: 7,
        status: "Tendered",
        isUrgent: false,
        isOnHold: false,
        isHazmat: false,
        carrierScac: "FXFE",
        carrierName: "FedEx Express",
        baseMode: "AIR",
        vesselName: "",
        transportModeCode: "1AR",
        transportMode: "Overnight - Early AM",
        equipmentCode: "",
        equipment: "",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "MTL1",
        logisticsGroup: "MTL1",
        transactionSource: "GIS",
        memo: "",
        origin: {
            originId: "P30",
            originName: "San Bernardino Distribution Center",
            originCity: "San Bernardino",
            originSau: "CA",
            originCountry: "USA",
            originPostalCode: "92408"
        },
        destination: {
            destinationId: "S2425090",
            destinationName: "Amazon.com CMH2",
            destinationCity: "Groveport",
            destinationSau: "OH",
            destinationCountry: "USA",
            destinationPostalCode: "43125"
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "AIR",
                distance: "415",
                distanceUom: "MILE",
                percentageOfTotal: "100%",
                exceptions: {}
            }
        }
    },
    {
        clientCode: "MTL",
        client: "Mattel",
        loadNumber: "31125032",
        shipmentNumber: "SH-036304483",
        proNumber: "12251352702",
        bolNumber: "2403404237",
        airWaybillNumber: "",
        trailerNumber: "636422",
        statusCode: 7,
        status: "Tendered",
        isUrgent: false,
        isOnHold: false,
        isHazmat: false,
        carrierScac: "MAES",
        carrierName: "Maersk",
        baseMode: "OCEAN",
        vesselName: "MAERSK ALABAMA",
        transportModeCode: "IM",
        transportMode: "Intermodal",
        equipmentCode: "",
        equipment: "",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "MTL1",
        logisticsGroup: "MTL1",
        transactionSource: "GIS",
        memo: "",
        origin: {
            originId: "P30",
            originName: "San Bernardino Distribution Center",
            originCity: "San Bernardino",
            originSau: "CA",
            originCountry: "USA",
            originPostalCode: "92408"
        },
        destination: {
            destinationId: "S2425090",
            destinationName: "Amazon.com CMH2",
            destinationCity: "Groveport",
            destinationSau: "OH",
            destinationCountry: "USA",
            destinationPostalCode: "43125"
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "GROUND",
                distance: "133",
                distanceUom: "MILE",
                percentageOfTotal: "20%"
            },
            200: {
                baseMode: "OCEAN",
                distance: "532",
                distanceUom: "MILE",
                percentageOfTotal: "80%",
                exceptions: [
                {
                    exceptionCode: "SD",
                    exception: "Shipment Delay",
                    reasonCode: "22",
                    reason: "Weather Related",
                    exceptionPoint: "35%"
                }
                ]

            }
        }
    },
    {
        clientCode: "COV",
        client: "Covestro",
        loadNumber: "29163218",
        shipmentNumber: "SH-033643151",
        proNumber: "12251352702",
        bolNumber: "2403404237",
        airWaybillNumber: "",
        trailerNumber: "636422",
        statusCode: 7,
        status: "Delivered",
        isUrgent: false,
        isOnHold: false,
        isHazmat: true,
        carrierScac: "UP",
        carrierName: "Union Pacific",
        baseMode: "RAIL",
        vesselName: "",
        transportModeCode: "RAIL",
        transportMode: "Rail",
        equipmentCode: "",
        equipment: "",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "COV1",
        logisticsGroup: "COV2",
        transactionSource: "MANUAL",
        memo: "",
        origin: {
            originId: "OR54",
            originName: "Baytown Rail SP Covestro LLC",
            originCity: "Baytown",
            originSau: "TX",
            originCountry: "USA",
            originPostalCode: "77523",
            contacts: [
            {
                contactName: "Deanna Mclean",
                contactPhone: "+1(281) 383-7695",
                primaryContact: true
            },
            {
                contactName: "John Doe",
                contactPhone: "+1(555) 867-5309",
                primaryContact: false
            }
            ]
        },
        destination: {
            destinationId: "0005288788",
            destinationName: "Plant ORW2 Covestro LLC",
            destinationCity: "CARSON",
            destinationSau: "CA",
            destinationCountry: "USA",
            destinationPostalCode: "90810",
            contacts: [
            {
                contactName: "NA",
                contactPhone: "+1(310) 603-8797",
                primaryContact: true
            }
            ]
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "RAIL",
                distance: "2477",
                distanceUom: "MILE",
                percentageOfTotal: "100%",
                exceptions: {}
            }
        }
    },
    {
        clientCode: "MTL",
        client: "Mattel",
        loadNumber: "31125032",
        shipmentNumber: "SH-036304473",
        proNumber: "12251352701",
        bolNumber: "2403404237",
        airWaybillNumber: "",
        trailerNumber: "636421",
        statusCode: 8,
        status: "Tender Accepted",
        isUrgent: true,
        isOnHold: false,
        isHazmat: false,
        carrierScac: "GDSD",
        carrierName: "GDS Express",
        baseMode: "GROUND",
        vesselName: "",
        transportModeCode: "TL",
        transportMode: "Truckload",
        equipmentCode: "53DV",
        equipment: "53FT Dry Van",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "MTL1",
        logisticsGroup: "MTL1",
        transactionSource: "GIS",
        memo: "",
        origin: {
            originId: "P30",
            originName: "San Bernardino Distribution Center",
            originCity: "San Bernardino",
            originSau: "CA",
            originCountry: "USA",
            originPostalCode: "92408"
        },
        destination: {
            destinationId: "S2425090",
            destinationName: "Amazon.com CMH2",
            destinationCity: "Groveport",
            destinationSau: "OH",
            destinationCountry: "USA",
            destinationPostalCode: "43125"
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "GROUND",
                distance: "1255",
                distanceUom: "MILE",
                percentageOfTotal: "100%",
                exceptions: {}
            }
        }
    },
    {
        clientCode: "MTL",
        client: "Mattel",
        loadNumber: "31125032",
        shipmentNumber: "SH-036304479",
        proNumber: "",
        bolNumber: "",
        airWaybillNumber: "654471555",
        trailerNumber: "636422",
        statusCode: 7,
        status: "Tendered",
        isUrgent: false,
        isOnHold: false,
        isHazmat: false,
        carrierScac: "FXFE",
        carrierName: "FedEx Express",
        baseMode: "AIR",
        vesselName: "",
        transportModeCode: "1AR",
        transportMode: "Overnight - Early AM",
        equipmentCode: "",
        equipment: "",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "MTL1",
        logisticsGroup: "MTL1",
        transactionSource: "GIS",
        memo: "",
        origin: {
            originId: "P30",
            originName: "San Bernardino Distribution Center",
            originCity: "San Bernardino",
            originSau: "CA",
            originCountry: "USA",
            originPostalCode: "92408"
        },
        destination: {
            destinationId: "S2425090",
            destinationName: "Amazon.com CMH2",
            destinationCity: "Groveport",
            destinationSau: "OH",
            destinationCountry: "USA",
            destinationPostalCode: "43125"
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "AIR",
                distance: "415",
                distanceUom: "MILE",
                percentageOfTotal: "100%",
                exceptions: {}
            }
        }
    },
    {
        clientCode: "MTL",
        client: "Mattel",
        loadNumber: "31125032",
        shipmentNumber: "SH-036304483",
        proNumber: "12251352702",
        bolNumber: "2403404237",
        airWaybillNumber: "",
        trailerNumber: "636422",
        statusCode: 7,
        status: "Tendered",
        isUrgent: false,
        isOnHold: false,
        isHazmat: false,
        carrierScac: "MAES",
        carrierName: "Maersk",
        baseMode: "OCEAN",
        vesselName: "MAERSK ALABAMA",
        transportModeCode: "IM",
        transportMode: "Intermodal",
        equipmentCode: "",
        equipment: "",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "MTL1",
        logisticsGroup: "MTL1",
        transactionSource: "GIS",
        memo: "",
        origin: {
            originId: "P30",
            originName: "San Bernardino Distribution Center",
            originCity: "San Bernardino",
            originSau: "CA",
            originCountry: "USA",
            originPostalCode: "92408"
        },
        destination: {
            destinationId: "S2425090",
            destinationName: "Amazon.com CMH2",
            destinationCity: "Groveport",
            destinationSau: "OH",
            destinationCountry: "USA",
            destinationPostalCode: "43125"
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "GROUND",
                distance: "133",
                distanceUom: "MILE",
                percentageOfTotal: "20%"
            },
            200: {
                baseMode: "OCEAN",
                distance: "532",
                distanceUom: "MILE",
                percentageOfTotal: "80%",
                exceptions: [
                {
                    exceptionCode: "SD",
                    exception: "Shipment Delay",
                    reasonCode: "22",
                    reason: "Weather Related",
                    exceptionPoint: "35%"
                }
                ]

            }
        }
    },
    {
        clientCode: "COV",
        client: "Covestro",
        loadNumber: "29163218",
        shipmentNumber: "SH-033643151",
        proNumber: "12251352702",
        bolNumber: "2403404237",
        airWaybillNumber: "",
        trailerNumber: "636422",
        statusCode: 7,
        status: "Delivered",
        isUrgent: false,
        isOnHold: false,
        isHazmat: true,
        carrierScac: "UP",
        carrierName: "Union Pacific",
        baseMode: "RAIL",
        vesselName: "",
        transportModeCode: "RAIL",
        transportMode: "Rail",
        equipmentCode: "",
        equipment: "",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "COV1",
        logisticsGroup: "COV2",
        transactionSource: "MANUAL",
        memo: "",
        origin: {
            originId: "OR54",
            originName: "Baytown Rail SP Covestro LLC",
            originCity: "Baytown",
            originSau: "TX",
            originCountry: "USA",
            originPostalCode: "77523",
            contacts: [
            {
                contactName: "Deanna Mclean",
                contactPhone: "+1(281) 383-7695",
                primaryContact: true
            },
            {
                contactName: "John Doe",
                contactPhone: "+1(555) 867-5309",
                primaryContact: false
            }
            ]
        },
        destination: {
            destinationId: "0005288788",
            destinationName: "Plant ORW2 Covestro LLC",
            destinationCity: "CARSON",
            destinationSau: "CA",
            destinationCountry: "USA",
            destinationPostalCode: "90810",
            contacts: [
            {
                contactName: "NA",
                contactPhone: "+1(310) 603-8797",
                primaryContact: true
            }
            ]
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "RAIL",
                distance: "2477",
                distanceUom: "MILE",
                percentageOfTotal: "100%",
                exceptions: {}
            }
        }
    },
    {
        clientCode: "MTL",
        client: "Mattel",
        loadNumber: "31125032",
        shipmentNumber: "SH-036304473",
        proNumber: "12251352701",
        bolNumber: "2403404237",
        airWaybillNumber: "",
        trailerNumber: "636421",
        statusCode: 8,
        status: "Tender Accepted",
        isUrgent: true,
        isOnHold: false,
        isHazmat: false,
        carrierScac: "GDSD",
        carrierName: "GDS Express",
        baseMode: "GROUND",
        vesselName: "",
        transportModeCode: "TL",
        transportMode: "Truckload",
        equipmentCode: "53DV",
        equipment: "53FT Dry Van",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "MTL1",
        logisticsGroup: "MTL1",
        transactionSource: "GIS",
        memo: "",
        origin: {
            originId: "P30",
            originName: "San Bernardino Distribution Center",
            originCity: "San Bernardino",
            originSau: "CA",
            originCountry: "USA",
            originPostalCode: "92408"
        },
        destination: {
            destinationId: "S2425090",
            destinationName: "Amazon.com CMH2",
            destinationCity: "Groveport",
            destinationSau: "OH",
            destinationCountry: "USA",
            destinationPostalCode: "43125"
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "GROUND",
                distance: "1255",
                distanceUom: "MILE",
                percentageOfTotal: "100%",
                exceptions: {}
            }
        }
    },
    {
        clientCode: "MTL",
        client: "Mattel",
        loadNumber: "31125032",
        shipmentNumber: "SH-036304479",
        proNumber: "",
        bolNumber: "",
        airWaybillNumber: "654471555",
        trailerNumber: "636422",
        statusCode: 7,
        status: "Tendered",
        isUrgent: false,
        isOnHold: false,
        isHazmat: false,
        carrierScac: "FXFE",
        carrierName: "FedEx Express",
        baseMode: "AIR",
        vesselName: "",
        transportModeCode: "1AR",
        transportMode: "Overnight - Early AM",
        equipmentCode: "",
        equipment: "",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "MTL1",
        logisticsGroup: "MTL1",
        transactionSource: "GIS",
        memo: "",
        origin: {
            originId: "P30",
            originName: "San Bernardino Distribution Center",
            originCity: "San Bernardino",
            originSau: "CA",
            originCountry: "USA",
            originPostalCode: "92408"
        },
        destination: {
            destinationId: "S2425090",
            destinationName: "Amazon.com CMH2",
            destinationCity: "Groveport",
            destinationSau: "OH",
            destinationCountry: "USA",
            destinationPostalCode: "43125"
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "AIR",
                distance: "415",
                distanceUom: "MILE",
                percentageOfTotal: "100%",
                exceptions: {}
            }
        }
    },
    {
        clientCode: "MTL",
        client: "Mattel",
        loadNumber: "31125032",
        shipmentNumber: "SH-036304483",
        proNumber: "12251352702",
        bolNumber: "2403404237",
        airWaybillNumber: "",
        trailerNumber: "636422",
        statusCode: 7,
        status: "Tendered",
        isUrgent: false,
        isOnHold: false,
        isHazmat: false,
        carrierScac: "MAES",
        carrierName: "Maersk",
        baseMode: "OCEAN",
        vesselName: "MAERSK ALABAMA",
        transportModeCode: "IM",
        transportMode: "Intermodal",
        equipmentCode: "",
        equipment: "",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "MTL1",
        logisticsGroup: "MTL1",
        transactionSource: "GIS",
        memo: "",
        origin: {
            originId: "P30",
            originName: "San Bernardino Distribution Center",
            originCity: "San Bernardino",
            originSau: "CA",
            originCountry: "USA",
            originPostalCode: "92408"
        },
        destination: {
            destinationId: "S2425090",
            destinationName: "Amazon.com CMH2",
            destinationCity: "Groveport",
            destinationSau: "OH",
            destinationCountry: "USA",
            destinationPostalCode: "43125"
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "GROUND",
                distance: "133",
                distanceUom: "MILE",
                percentageOfTotal: "20%"
            },
            200: {
                baseMode: "OCEAN",
                distance: "532",
                distanceUom: "MILE",
                percentageOfTotal: "80%",
                exceptions: [
                {
                    exceptionCode: "SD",
                    exception: "Shipment Delay",
                    reasonCode: "22",
                    reason: "Weather Related",
                    exceptionPoint: "35%"
                }
                ]

            }
        }
    },
    {
        clientCode: "COV",
        client: "Covestro",
        loadNumber: "29163218",
        shipmentNumber: "SH-033643151",
        proNumber: "12251352702",
        bolNumber: "2403404237",
        airWaybillNumber: "",
        trailerNumber: "636422",
        statusCode: 7,
        status: "Delivered",
        isUrgent: false,
        isOnHold: false,
        isHazmat: true,
        carrierScac: "UP",
        carrierName: "Union Pacific",
        baseMode: "RAIL",
        vesselName: "",
        transportModeCode: "RAIL",
        transportMode: "Rail",
        equipmentCode: "",
        equipment: "",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "COV1",
        logisticsGroup: "COV2",
        transactionSource: "MANUAL",
        memo: "",
        origin: {
            originId: "OR54",
            originName: "Baytown Rail SP Covestro LLC",
            originCity: "Baytown",
            originSau: "TX",
            originCountry: "USA",
            originPostalCode: "77523",
            contacts: [
            {
                contactName: "Deanna Mclean",
                contactPhone: "+1(281) 383-7695",
                primaryContact: true
            },
            {
                contactName: "John Doe",
                contactPhone: "+1(555) 867-5309",
                primaryContact: false
            }
            ]
        },
        destination: {
            destinationId: "0005288788",
            destinationName: "Plant ORW2 Covestro LLC",
            destinationCity: "CARSON",
            destinationSau: "CA",
            destinationCountry: "USA",
            destinationPostalCode: "90810",
            contacts: [
            {
                contactName: "NA",
                contactPhone: "+1(310) 603-8797",
                primaryContact: true
            }
            ]
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "RAIL",
                distance: "2477",
                distanceUom: "MILE",
                percentageOfTotal: "100%",
                exceptions: {}
            }
        }
    },
    {
        clientCode: "MTL",
        client: "Mattel",
        loadNumber: "31125032",
        shipmentNumber: "SH-036304473",
        proNumber: "12251352701",
        bolNumber: "2403404237",
        airWaybillNumber: "",
        trailerNumber: "636421",
        statusCode: 8,
        status: "Tender Accepted",
        isUrgent: true,
        isOnHold: false,
        isHazmat: false,
        carrierScac: "GDSD",
        carrierName: "GDS Express",
        baseMode: "GROUND",
        vesselName: "",
        transportModeCode: "TL",
        transportMode: "Truckload",
        equipmentCode: "53DV",
        equipment: "53FT Dry Van",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "MTL1",
        logisticsGroup: "MTL1",
        transactionSource: "GIS",
        memo: "",
        origin: {
            originId: "P30",
            originName: "San Bernardino Distribution Center",
            originCity: "San Bernardino",
            originSau: "CA",
            originCountry: "USA",
            originPostalCode: "92408"
        },
        destination: {
            destinationId: "S2425090",
            destinationName: "Amazon.com CMH2",
            destinationCity: "Groveport",
            destinationSau: "OH",
            destinationCountry: "USA",
            destinationPostalCode: "43125"
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "GROUND",
                distance: "1255",
                distanceUom: "MILE",
                percentageOfTotal: "100%",
                exceptions: {}
            }
        }
    },
    {
        clientCode: "MTL",
        client: "Mattel",
        loadNumber: "31125032",
        shipmentNumber: "SH-036304479",
        proNumber: "",
        bolNumber: "",
        airWaybillNumber: "654471555",
        trailerNumber: "636422",
        statusCode: 7,
        status: "Tendered",
        isUrgent: false,
        isOnHold: false,
        isHazmat: false,
        carrierScac: "FXFE",
        carrierName: "FedEx Express",
        baseMode: "AIR",
        vesselName: "",
        transportModeCode: "1AR",
        transportMode: "Overnight - Early AM",
        equipmentCode: "",
        equipment: "",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "MTL1",
        logisticsGroup: "MTL1",
        transactionSource: "GIS",
        memo: "",
        origin: {
            originId: "P30",
            originName: "San Bernardino Distribution Center",
            originCity: "San Bernardino",
            originSau: "CA",
            originCountry: "USA",
            originPostalCode: "92408"
        },
        destination: {
            destinationId: "S2425090",
            destinationName: "Amazon.com CMH2",
            destinationCity: "Groveport",
            destinationSau: "OH",
            destinationCountry: "USA",
            destinationPostalCode: "43125"
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "AIR",
                distance: "415",
                distanceUom: "MILE",
                percentageOfTotal: "100%",
                exceptions: {}
            }
        }
    },
    {
        clientCode: "MTL",
        client: "Mattel",
        loadNumber: "31125032",
        shipmentNumber: "SH-036304483",
        proNumber: "12251352702",
        bolNumber: "2403404237",
        airWaybillNumber: "",
        trailerNumber: "636422",
        statusCode: 7,
        status: "Tendered",
        isUrgent: false,
        isOnHold: false,
        isHazmat: false,
        carrierScac: "MAES",
        carrierName: "Maersk",
        baseMode: "OCEAN",
        vesselName: "MAERSK ALABAMA",
        transportModeCode: "IM",
        transportMode: "Intermodal",
        equipmentCode: "",
        equipment: "",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "MTL1",
        logisticsGroup: "MTL1",
        transactionSource: "GIS",
        memo: "",
        origin: {
            originId: "P30",
            originName: "San Bernardino Distribution Center",
            originCity: "San Bernardino",
            originSau: "CA",
            originCountry: "USA",
            originPostalCode: "92408"
        },
        destination: {
            destinationId: "S2425090",
            destinationName: "Amazon.com CMH2",
            destinationCity: "Groveport",
            destinationSau: "OH",
            destinationCountry: "USA",
            destinationPostalCode: "43125"
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "GROUND",
                distance: "133",
                distanceUom: "MILE",
                percentageOfTotal: "20%"
            },
            200: {
                baseMode: "OCEAN",
                distance: "532",
                distanceUom: "MILE",
                percentageOfTotal: "80%",
                exceptions: [
                {
                    exceptionCode: "SD",
                    exception: "Shipment Delay",
                    reasonCode: "22",
                    reason: "Weather Related",
                    exceptionPoint: "35%"
                }
                ]

            }
        }
    },
    {
        clientCode: "COV",
        client: "Covestro",
        loadNumber: "29163218",
        shipmentNumber: "SH-033643151",
        proNumber: "12251352702",
        bolNumber: "2403404237",
        airWaybillNumber: "",
        trailerNumber: "636422",
        statusCode: 7,
        status: "Delivered",
        isUrgent: false,
        isOnHold: false,
        isHazmat: true,
        carrierScac: "UP",
        carrierName: "Union Pacific",
        baseMode: "RAIL",
        vesselName: "",
        transportModeCode: "RAIL",
        transportMode: "Rail",
        equipmentCode: "",
        equipment: "",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "COV1",
        logisticsGroup: "COV2",
        transactionSource: "MANUAL",
        memo: "",
        origin: {
            originId: "OR54",
            originName: "Baytown Rail SP Covestro LLC",
            originCity: "Baytown",
            originSau: "TX",
            originCountry: "USA",
            originPostalCode: "77523",
            contacts: [
            {
                contactName: "Deanna Mclean",
                contactPhone: "+1(281) 383-7695",
                primaryContact: true
            },
            {
                contactName: "John Doe",
                contactPhone: "+1(555) 867-5309",
                primaryContact: false
            }
            ]
        },
        destination: {
            destinationId: "0005288788",
            destinationName: "Plant ORW2 Covestro LLC",
            destinationCity: "CARSON",
            destinationSau: "CA",
            destinationCountry: "USA",
            destinationPostalCode: "90810",
            contacts: [
            {
                contactName: "NA",
                contactPhone: "+1(310) 603-8797",
                primaryContact: true
            }
            ]
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "RAIL",
                distance: "2477",
                distanceUom: "MILE",
                percentageOfTotal: "100%",
                exceptions: {}
            }
        }
    },
    {
        clientCode: "MTL",
        client: "Mattel",
        loadNumber: "31125032",
        shipmentNumber: "SH-036304473",
        proNumber: "12251352701",
        bolNumber: "2403404237",
        airWaybillNumber: "",
        trailerNumber: "636421",
        statusCode: 8,
        status: "Tender Accepted",
        isUrgent: true,
        isOnHold: false,
        isHazmat: false,
        carrierScac: "GDSD",
        carrierName: "GDS Express",
        baseMode: "GROUND",
        vesselName: "",
        transportModeCode: "TL",
        transportMode: "Truckload",
        equipmentCode: "53DV",
        equipment: "53FT Dry Van",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "MTL1",
        logisticsGroup: "MTL1",
        transactionSource: "GIS",
        memo: "",
        origin: {
            originId: "P30",
            originName: "San Bernardino Distribution Center",
            originCity: "San Bernardino",
            originSau: "CA",
            originCountry: "USA",
            originPostalCode: "92408"
        },
        destination: {
            destinationId: "S2425090",
            destinationName: "Amazon.com CMH2",
            destinationCity: "Groveport",
            destinationSau: "OH",
            destinationCountry: "USA",
            destinationPostalCode: "43125"
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "GROUND",
                distance: "1255",
                distanceUom: "MILE",
                percentageOfTotal: "100%",
                exceptions: {}
            }
        }
    },
    {
        clientCode: "MTL",
        client: "Mattel",
        loadNumber: "31125032",
        shipmentNumber: "SH-036304479",
        proNumber: "",
        bolNumber: "",
        airWaybillNumber: "654471555",
        trailerNumber: "636422",
        statusCode: 7,
        status: "Tendered",
        isUrgent: false,
        isOnHold: false,
        isHazmat: false,
        carrierScac: "FXFE",
        carrierName: "FedEx Express",
        baseMode: "AIR",
        vesselName: "",
        transportModeCode: "1AR",
        transportMode: "Overnight - Early AM",
        equipmentCode: "",
        equipment: "",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "MTL1",
        logisticsGroup: "MTL1",
        transactionSource: "GIS",
        memo: "",
        origin: {
            originId: "P30",
            originName: "San Bernardino Distribution Center",
            originCity: "San Bernardino",
            originSau: "CA",
            originCountry: "USA",
            originPostalCode: "92408"
        },
        destination: {
            destinationId: "S2425090",
            destinationName: "Amazon.com CMH2",
            destinationCity: "Groveport",
            destinationSau: "OH",
            destinationCountry: "USA",
            destinationPostalCode: "43125"
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "AIR",
                distance: "415",
                distanceUom: "MILE",
                percentageOfTotal: "100%",
                exceptions: {}
            }
        }
    },
    {
        clientCode: "MTL",
        client: "Mattel",
        loadNumber: "31125032",
        shipmentNumber: "SH-036304483",
        proNumber: "12251352702",
        bolNumber: "2403404237",
        airWaybillNumber: "",
        trailerNumber: "636422",
        statusCode: 7,
        status: "Tendered",
        isUrgent: false,
        isOnHold: false,
        isHazmat: false,
        carrierScac: "MAES",
        carrierName: "Maersk",
        baseMode: "OCEAN",
        vesselName: "MAERSK ALABAMA",
        transportModeCode: "IM",
        transportMode: "Intermodal",
        equipmentCode: "",
        equipment: "",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "MTL1",
        logisticsGroup: "MTL1",
        transactionSource: "GIS",
        memo: "",
        origin: {
            originId: "P30",
            originName: "San Bernardino Distribution Center",
            originCity: "San Bernardino",
            originSau: "CA",
            originCountry: "USA",
            originPostalCode: "92408"
        },
        destination: {
            destinationId: "S2425090",
            destinationName: "Amazon.com CMH2",
            destinationCity: "Groveport",
            destinationSau: "OH",
            destinationCountry: "USA",
            destinationPostalCode: "43125"
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "GROUND",
                distance: "133",
                distanceUom: "MILE",
                percentageOfTotal: "20%"
            },
            200: {
                baseMode: "OCEAN",
                distance: "532",
                distanceUom: "MILE",
                percentageOfTotal: "80%",
                exceptions: [
                {
                    exceptionCode: "SD",
                    exception: "Shipment Delay",
                    reasonCode: "22",
                    reason: "Weather Related",
                    exceptionPoint: "35%"
                }
                ]

            }
        }
    },
    {
        clientCode: "COV",
        client: "Covestro",
        loadNumber: "29163218",
        shipmentNumber: "SH-033643151",
        proNumber: "12251352702",
        bolNumber: "2403404237",
        airWaybillNumber: "",
        trailerNumber: "636422",
        statusCode: 7,
        status: "Delivered",
        isUrgent: false,
        isOnHold: false,
        isHazmat: true,
        carrierScac: "UP",
        carrierName: "Union Pacific",
        baseMode: "RAIL",
        vesselName: "",
        transportModeCode: "RAIL",
        transportMode: "Rail",
        equipmentCode: "",
        equipment: "",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "COV1",
        logisticsGroup: "COV2",
        transactionSource: "MANUAL",
        memo: "",
        origin: {
            originId: "OR54",
            originName: "Baytown Rail SP Covestro LLC",
            originCity: "Baytown",
            originSau: "TX",
            originCountry: "USA",
            originPostalCode: "77523",
            contacts: [
            {
                contactName: "Deanna Mclean",
                contactPhone: "+1(281) 383-7695",
                primaryContact: true
            },
            {
                contactName: "John Doe",
                contactPhone: "+1(555) 867-5309",
                primaryContact: false
            }
            ]
        },
        destination: {
            destinationId: "0005288788",
            destinationName: "Plant ORW2 Covestro LLC",
            destinationCity: "CARSON",
            destinationSau: "CA",
            destinationCountry: "USA",
            destinationPostalCode: "90810",
            contacts: [
            {
                contactName: "NA",
                contactPhone: "+1(310) 603-8797",
                primaryContact: true
            }
            ]
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "RAIL",
                distance: "2477",
                distanceUom: "MILE",
                percentageOfTotal: "100%",
                exceptions: {}
            }
        }
    },
    {
        clientCode: "MTL",
        client: "Mattel",
        loadNumber: "31125032",
        shipmentNumber: "SH-036304473",
        proNumber: "12251352701",
        bolNumber: "2403404237",
        airWaybillNumber: "",
        trailerNumber: "636421",
        statusCode: 8,
        status: "Tender Accepted",
        isUrgent: true,
        isOnHold: false,
        isHazmat: false,
        carrierScac: "GDSD",
        carrierName: "GDS Express",
        baseMode: "GROUND",
        vesselName: "",
        transportModeCode: "TL",
        transportMode: "Truckload",
        equipmentCode: "53DV",
        equipment: "53FT Dry Van",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "MTL1",
        logisticsGroup: "MTL1",
        transactionSource: "GIS",
        memo: "",
        origin: {
            originId: "P30",
            originName: "San Bernardino Distribution Center",
            originCity: "San Bernardino",
            originSau: "CA",
            originCountry: "USA",
            originPostalCode: "92408"
        },
        destination: {
            destinationId: "S2425090",
            destinationName: "Amazon.com CMH2",
            destinationCity: "Groveport",
            destinationSau: "OH",
            destinationCountry: "USA",
            destinationPostalCode: "43125"
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "GROUND",
                distance: "1255",
                distanceUom: "MILE",
                percentageOfTotal: "100%",
                exceptions: {}
            }
        }
    },
    {
        clientCode: "MTL",
        client: "Mattel",
        loadNumber: "31125032",
        shipmentNumber: "SH-036304479",
        proNumber: "",
        bolNumber: "",
        airWaybillNumber: "654471555",
        trailerNumber: "636422",
        statusCode: 7,
        status: "Tendered",
        isUrgent: false,
        isOnHold: false,
        isHazmat: false,
        carrierScac: "FXFE",
        carrierName: "FedEx Express",
        baseMode: "AIR",
        vesselName: "",
        transportModeCode: "1AR",
        transportMode: "Overnight - Early AM",
        equipmentCode: "",
        equipment: "",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "MTL1",
        logisticsGroup: "MTL1",
        transactionSource: "GIS",
        memo: "",
        origin: {
            originId: "P30",
            originName: "San Bernardino Distribution Center",
            originCity: "San Bernardino",
            originSau: "CA",
            originCountry: "USA",
            originPostalCode: "92408"
        },
        destination: {
            destinationId: "S2425090",
            destinationName: "Amazon.com CMH2",
            destinationCity: "Groveport",
            destinationSau: "OH",
            destinationCountry: "USA",
            destinationPostalCode: "43125"
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "AIR",
                distance: "415",
                distanceUom: "MILE",
                percentageOfTotal: "100%",
                exceptions: {}
            }
        }
    },
    {
        clientCode: "MTL",
        client: "Mattel",
        loadNumber: "31125032",
        shipmentNumber: "SH-036304483",
        proNumber: "12251352702",
        bolNumber: "2403404237",
        airWaybillNumber: "",
        trailerNumber: "636422",
        statusCode: 7,
        status: "Tendered",
        isUrgent: false,
        isOnHold: false,
        isHazmat: false,
        carrierScac: "MAES",
        carrierName: "Maersk",
        baseMode: "OCEAN",
        vesselName: "MAERSK ALABAMA",
        transportModeCode: "IM",
        transportMode: "Intermodal",
        equipmentCode: "",
        equipment: "",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "MTL1",
        logisticsGroup: "MTL1",
        transactionSource: "GIS",
        memo: "",
        origin: {
            originId: "P30",
            originName: "San Bernardino Distribution Center",
            originCity: "San Bernardino",
            originSau: "CA",
            originCountry: "USA",
            originPostalCode: "92408"
        },
        destination: {
            destinationId: "S2425090",
            destinationName: "Amazon.com CMH2",
            destinationCity: "Groveport",
            destinationSau: "OH",
            destinationCountry: "USA",
            destinationPostalCode: "43125"
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "GROUND",
                distance: "133",
                distanceUom: "MILE",
                percentageOfTotal: "20%"
            },
            200: {
                baseMode: "OCEAN",
                distance: "532",
                distanceUom: "MILE",
                percentageOfTotal: "80%",
                exceptions: [
                {
                    exceptionCode: "SD",
                    exception: "Shipment Delay",
                    reasonCode: "22",
                    reason: "Weather Related",
                    exceptionPoint: "35%"
                }
                ]

            }
        }
    },
    {
        clientCode: "COV",
        client: "Covestro",
        loadNumber: "29163218",
        shipmentNumber: "SH-033643151",
        proNumber: "12251352702",
        bolNumber: "2403404237",
        airWaybillNumber: "",
        trailerNumber: "636422",
        statusCode: 7,
        status: "Delivered",
        isUrgent: false,
        isOnHold: false,
        isHazmat: true,
        carrierScac: "UP",
        carrierName: "Union Pacific",
        baseMode: "RAIL",
        vesselName: "",
        transportModeCode: "RAIL",
        transportMode: "Rail",
        equipmentCode: "",
        equipment: "",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "COV1",
        logisticsGroup: "COV2",
        transactionSource: "MANUAL",
        memo: "",
        origin: {
            originId: "OR54",
            originName: "Baytown Rail SP Covestro LLC",
            originCity: "Baytown",
            originSau: "TX",
            originCountry: "USA",
            originPostalCode: "77523",
            contacts: [
            {
                contactName: "Deanna Mclean",
                contactPhone: "+1(281) 383-7695",
                primaryContact: true
            },
            {
                contactName: "John Doe",
                contactPhone: "+1(555) 867-5309",
                primaryContact: false
            }
            ]
        },
        destination: {
            destinationId: "0005288788",
            destinationName: "Plant ORW2 Covestro LLC",
            destinationCity: "CARSON",
            destinationSau: "CA",
            destinationCountry: "USA",
            destinationPostalCode: "90810",
            contacts: [
            {
                contactName: "NA",
                contactPhone: "+1(310) 603-8797",
                primaryContact: true
            }
            ]
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "RAIL",
                distance: "2477",
                distanceUom: "MILE",
                percentageOfTotal: "100%",
                exceptions: {}
            }
        }
    },
    {
        clientCode: "MTL",
        client: "Mattel",
        loadNumber: "31125032",
        shipmentNumber: "SH-036304473",
        proNumber: "12251352701",
        bolNumber: "2403404237",
        airWaybillNumber: "",
        trailerNumber: "636421",
        statusCode: 8,
        status: "Tender Accepted",
        isUrgent: true,
        isOnHold: false,
        isHazmat: false,
        carrierScac: "GDSD",
        carrierName: "GDS Express",
        baseMode: "GROUND",
        vesselName: "",
        transportModeCode: "TL",
        transportMode: "Truckload",
        equipmentCode: "53DV",
        equipment: "53FT Dry Van",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "MTL1",
        logisticsGroup: "MTL1",
        transactionSource: "GIS",
        memo: "",
        origin: {
            originId: "P30",
            originName: "San Bernardino Distribution Center",
            originCity: "San Bernardino",
            originSau: "CA",
            originCountry: "USA",
            originPostalCode: "92408"
        },
        destination: {
            destinationId: "S2425090",
            destinationName: "Amazon.com CMH2",
            destinationCity: "Groveport",
            destinationSau: "OH",
            destinationCountry: "USA",
            destinationPostalCode: "43125"
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "GROUND",
                distance: "1255",
                distanceUom: "MILE",
                percentageOfTotal: "100%",
                exceptions: {}
            }
        }
    },
    {
        clientCode: "MTL",
        client: "Mattel",
        loadNumber: "31125032",
        shipmentNumber: "SH-036304479",
        proNumber: "",
        bolNumber: "",
        airWaybillNumber: "654471555",
        trailerNumber: "636422",
        statusCode: 7,
        status: "Tendered",
        isUrgent: false,
        isOnHold: false,
        isHazmat: false,
        carrierScac: "FXFE",
        carrierName: "FedEx Express",
        baseMode: "AIR",
        vesselName: "",
        transportModeCode: "1AR",
        transportMode: "Overnight - Early AM",
        equipmentCode: "",
        equipment: "",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "MTL1",
        logisticsGroup: "MTL1",
        transactionSource: "GIS",
        memo: "",
        origin: {
            originId: "P30",
            originName: "San Bernardino Distribution Center",
            originCity: "San Bernardino",
            originSau: "CA",
            originCountry: "USA",
            originPostalCode: "92408"
        },
        destination: {
            destinationId: "S2425090",
            destinationName: "Amazon.com CMH2",
            destinationCity: "Groveport",
            destinationSau: "OH",
            destinationCountry: "USA",
            destinationPostalCode: "43125"
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "AIR",
                distance: "415",
                distanceUom: "MILE",
                percentageOfTotal: "100%",
                exceptions: {}
            }
        }
    },
    {
        clientCode: "MTL",
        client: "Mattel",
        loadNumber: "31125032",
        shipmentNumber: "SH-036304483",
        proNumber: "12251352702",
        bolNumber: "2403404237",
        airWaybillNumber: "",
        trailerNumber: "636422",
        statusCode: 7,
        status: "Tendered",
        isUrgent: false,
        isOnHold: false,
        isHazmat: false,
        carrierScac: "MAES",
        carrierName: "Maersk",
        baseMode: "OCEAN",
        vesselName: "MAERSK ALABAMA",
        transportModeCode: "IM",
        transportMode: "Intermodal",
        equipmentCode: "",
        equipment: "",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "MTL1",
        logisticsGroup: "MTL1",
        transactionSource: "GIS",
        memo: "",
        origin: {
            originId: "P30",
            originName: "San Bernardino Distribution Center",
            originCity: "San Bernardino",
            originSau: "CA",
            originCountry: "USA",
            originPostalCode: "92408"
        },
        destination: {
            destinationId: "S2425090",
            destinationName: "Amazon.com CMH2",
            destinationCity: "Groveport",
            destinationSau: "OH",
            destinationCountry: "USA",
            destinationPostalCode: "43125"
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "GROUND",
                distance: "133",
                distanceUom: "MILE",
                percentageOfTotal: "20%"
            },
            200: {
                baseMode: "OCEAN",
                distance: "532",
                distanceUom: "MILE",
                percentageOfTotal: "80%",
                exceptions: [
                {
                    exceptionCode: "SD",
                    exception: "Shipment Delay",
                    reasonCode: "22",
                    reason: "Weather Related",
                    exceptionPoint: "35%"
                }
                ]

            }
        }
    },
    {
        clientCode: "COV",
        client: "Covestro",
        loadNumber: "29163218",
        shipmentNumber: "SH-033643151",
        proNumber: "12251352702",
        bolNumber: "2403404237",
        airWaybillNumber: "",
        trailerNumber: "636422",
        statusCode: 7,
        status: "Delivered",
        isUrgent: false,
        isOnHold: false,
        isHazmat: true,
        carrierScac: "UP",
        carrierName: "Union Pacific",
        baseMode: "RAIL",
        vesselName: "",
        transportModeCode: "RAIL",
        transportMode: "Rail",
        equipmentCode: "",
        equipment: "",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "COV1",
        logisticsGroup: "COV2",
        transactionSource: "MANUAL",
        memo: "",
        origin: {
            originId: "OR54",
            originName: "Baytown Rail SP Covestro LLC",
            originCity: "Baytown",
            originSau: "TX",
            originCountry: "USA",
            originPostalCode: "77523",
            contacts: [
            {
                contactName: "Deanna Mclean",
                contactPhone: "+1(281) 383-7695",
                primaryContact: true
            },
            {
                contactName: "John Doe",
                contactPhone: "+1(555) 867-5309",
                primaryContact: false
            }
            ]
        },
        destination: {
            destinationId: "0005288788",
            destinationName: "Plant ORW2 Covestro LLC",
            destinationCity: "CARSON",
            destinationSau: "CA",
            destinationCountry: "USA",
            destinationPostalCode: "90810",
            contacts: [
            {
                contactName: "NA",
                contactPhone: "+1(310) 603-8797",
                primaryContact: true
            }
            ]
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "RAIL",
                distance: "2477",
                distanceUom: "MILE",
                percentageOfTotal: "100%",
                exceptions: {}
            }
        }
    },
    {
        clientCode: "MTL",
        client: "Mattel",
        loadNumber: "31125032",
        shipmentNumber: "SH-036304473",
        proNumber: "12251352701",
        bolNumber: "2403404237",
        airWaybillNumber: "",
        trailerNumber: "636421",
        statusCode: 8,
        status: "Tender Accepted",
        isUrgent: true,
        isOnHold: false,
        isHazmat: false,
        carrierScac: "GDSD",
        carrierName: "GDS Express",
        baseMode: "GROUND",
        vesselName: "",
        transportModeCode: "TL",
        transportMode: "Truckload",
        equipmentCode: "53DV",
        equipment: "53FT Dry Van",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "MTL1",
        logisticsGroup: "MTL1",
        transactionSource: "GIS",
        memo: "",
        origin: {
            originId: "P30",
            originName: "San Bernardino Distribution Center",
            originCity: "San Bernardino",
            originSau: "CA",
            originCountry: "USA",
            originPostalCode: "92408"
        },
        destination: {
            destinationId: "S2425090",
            destinationName: "Amazon.com CMH2",
            destinationCity: "Groveport",
            destinationSau: "OH",
            destinationCountry: "USA",
            destinationPostalCode: "43125"
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "GROUND",
                distance: "1255",
                distanceUom: "MILE",
                percentageOfTotal: "100%",
                exceptions: {}
            }
        }
    },
    {
        clientCode: "MTL",
        client: "Mattel",
        loadNumber: "31125032",
        shipmentNumber: "SH-036304479",
        proNumber: "",
        bolNumber: "",
        airWaybillNumber: "654471555",
        trailerNumber: "636422",
        statusCode: 7,
        status: "Tendered",
        isUrgent: false,
        isOnHold: false,
        isHazmat: false,
        carrierScac: "FXFE",
        carrierName: "FedEx Express",
        baseMode: "AIR",
        vesselName: "",
        transportModeCode: "1AR",
        transportMode: "Overnight - Early AM",
        equipmentCode: "",
        equipment: "",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "MTL1",
        logisticsGroup: "MTL1",
        transactionSource: "GIS",
        memo: "",
        origin: {
            originId: "P30",
            originName: "San Bernardino Distribution Center",
            originCity: "San Bernardino",
            originSau: "CA",
            originCountry: "USA",
            originPostalCode: "92408"
        },
        destination: {
            destinationId: "S2425090",
            destinationName: "Amazon.com CMH2",
            destinationCity: "Groveport",
            destinationSau: "OH",
            destinationCountry: "USA",
            destinationPostalCode: "43125"
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "AIR",
                distance: "415",
                distanceUom: "MILE",
                percentageOfTotal: "100%",
                exceptions: {}
            }
        }
    },
    {
        clientCode: "MTL",
        client: "Mattel",
        loadNumber: "31125032",
        shipmentNumber: "SH-036304483",
        proNumber: "12251352702",
        bolNumber: "2403404237",
        airWaybillNumber: "",
        trailerNumber: "636422",
        statusCode: 7,
        status: "Tendered",
        isUrgent: false,
        isOnHold: false,
        isHazmat: false,
        carrierScac: "MAES",
        carrierName: "Maersk",
        baseMode: "OCEAN",
        vesselName: "MAERSK ALABAMA",
        transportModeCode: "IM",
        transportMode: "Intermodal",
        equipmentCode: "",
        equipment: "",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "MTL1",
        logisticsGroup: "MTL1",
        transactionSource: "GIS",
        memo: "",
        origin: {
            originId: "P30",
            originName: "San Bernardino Distribution Center",
            originCity: "San Bernardino",
            originSau: "CA",
            originCountry: "USA",
            originPostalCode: "92408"
        },
        destination: {
            destinationId: "S2425090",
            destinationName: "Amazon.com CMH2",
            destinationCity: "Groveport",
            destinationSau: "OH",
            destinationCountry: "USA",
            destinationPostalCode: "43125"
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "GROUND",
                distance: "133",
                distanceUom: "MILE",
                percentageOfTotal: "20%"
            },
            200: {
                baseMode: "OCEAN",
                distance: "532",
                distanceUom: "MILE",
                percentageOfTotal: "80%",
                exceptions: [
                {
                    exceptionCode: "SD",
                    exception: "Shipment Delay",
                    reasonCode: "22",
                    reason: "Weather Related",
                    exceptionPoint: "35%"
                }
                ]

            }
        }
    },
    {
        clientCode: "COV",
        client: "Covestro",
        loadNumber: "29163218",
        shipmentNumber: "SH-033643151",
        proNumber: "12251352702",
        bolNumber: "2403404237",
        airWaybillNumber: "",
        trailerNumber: "636422",
        statusCode: 7,
        status: "Delivered",
        isUrgent: false,
        isOnHold: false,
        isHazmat: true,
        carrierScac: "UP",
        carrierName: "Union Pacific",
        baseMode: "RAIL",
        vesselName: "",
        transportModeCode: "RAIL",
        transportMode: "Rail",
        equipmentCode: "",
        equipment: "",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "COV1",
        logisticsGroup: "COV2",
        transactionSource: "MANUAL",
        memo: "",
        origin: {
            originId: "OR54",
            originName: "Baytown Rail SP Covestro LLC",
            originCity: "Baytown",
            originSau: "TX",
            originCountry: "USA",
            originPostalCode: "77523",
            contacts: [
            {
                contactName: "Deanna Mclean",
                contactPhone: "+1(281) 383-7695",
                primaryContact: true
            },
            {
                contactName: "John Doe",
                contactPhone: "+1(555) 867-5309",
                primaryContact: false
            }
            ]
        },
        destination: {
            destinationId: "0005288788",
            destinationName: "Plant ORW2 Covestro LLC",
            destinationCity: "CARSON",
            destinationSau: "CA",
            destinationCountry: "USA",
            destinationPostalCode: "90810",
            contacts: [
            {
                contactName: "NA",
                contactPhone: "+1(310) 603-8797",
                primaryContact: true
            }
            ]
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "RAIL",
                distance: "2477",
                distanceUom: "MILE",
                percentageOfTotal: "100%",
                exceptions: {}
            }
        }
    },
    {
        clientCode: "MTL",
        client: "Mattel",
        loadNumber: "31125032",
        shipmentNumber: "SH-036304473",
        proNumber: "12251352701",
        bolNumber: "2403404237",
        airWaybillNumber: "",
        trailerNumber: "636421",
        statusCode: 8,
        status: "Tender Accepted",
        isUrgent: true,
        isOnHold: false,
        isHazmat: false,
        carrierScac: "GDSD",
        carrierName: "GDS Express",
        baseMode: "GROUND",
        vesselName: "",
        transportModeCode: "TL",
        transportMode: "Truckload",
        equipmentCode: "53DV",
        equipment: "53FT Dry Van",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "MTL1",
        logisticsGroup: "MTL1",
        transactionSource: "GIS",
        memo: "",
        origin: {
            originId: "P30",
            originName: "San Bernardino Distribution Center",
            originCity: "San Bernardino",
            originSau: "CA",
            originCountry: "USA",
            originPostalCode: "92408"
        },
        destination: {
            destinationId: "S2425090",
            destinationName: "Amazon.com CMH2",
            destinationCity: "Groveport",
            destinationSau: "OH",
            destinationCountry: "USA",
            destinationPostalCode: "43125"
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "GROUND",
                distance: "1255",
                distanceUom: "MILE",
                percentageOfTotal: "100%",
                exceptions: {}
            }
        }
    },
    {
        clientCode: "MTL",
        client: "Mattel",
        loadNumber: "31125032",
        shipmentNumber: "SH-036304479",
        proNumber: "",
        bolNumber: "",
        airWaybillNumber: "654471555",
        trailerNumber: "636422",
        statusCode: 7,
        status: "Tendered",
        isUrgent: false,
        isOnHold: false,
        isHazmat: false,
        carrierScac: "FXFE",
        carrierName: "FedEx Express",
        baseMode: "AIR",
        vesselName: "",
        transportModeCode: "1AR",
        transportMode: "Overnight - Early AM",
        equipmentCode: "",
        equipment: "",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "MTL1",
        logisticsGroup: "MTL1",
        transactionSource: "GIS",
        memo: "",
        origin: {
            originId: "P30",
            originName: "San Bernardino Distribution Center",
            originCity: "San Bernardino",
            originSau: "CA",
            originCountry: "USA",
            originPostalCode: "92408"
        },
        destination: {
            destinationId: "S2425090",
            destinationName: "Amazon.com CMH2",
            destinationCity: "Groveport",
            destinationSau: "OH",
            destinationCountry: "USA",
            destinationPostalCode: "43125"
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "AIR",
                distance: "415",
                distanceUom: "MILE",
                percentageOfTotal: "100%",
                exceptions: {}
            }
        }
    },
    {
        clientCode: "MTL",
        client: "Mattel",
        loadNumber: "31125032",
        shipmentNumber: "SH-036304483",
        proNumber: "12251352702",
        bolNumber: "2403404237",
        airWaybillNumber: "",
        trailerNumber: "636422",
        statusCode: 7,
        status: "Tendered",
        isUrgent: false,
        isOnHold: false,
        isHazmat: false,
        carrierScac: "MAES",
        carrierName: "Maersk",
        baseMode: "OCEAN",
        vesselName: "MAERSK ALABAMA",
        transportModeCode: "IM",
        transportMode: "Intermodal",
        equipmentCode: "",
        equipment: "",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "MTL1",
        logisticsGroup: "MTL1",
        transactionSource: "GIS",
        memo: "",
        origin: {
            originId: "P30",
            originName: "San Bernardino Distribution Center",
            originCity: "San Bernardino",
            originSau: "CA",
            originCountry: "USA",
            originPostalCode: "92408"
        },
        destination: {
            destinationId: "S2425090",
            destinationName: "Amazon.com CMH2",
            destinationCity: "Groveport",
            destinationSau: "OH",
            destinationCountry: "USA",
            destinationPostalCode: "43125"
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "GROUND",
                distance: "133",
                distanceUom: "MILE",
                percentageOfTotal: "20%"
            },
            200: {
                baseMode: "OCEAN",
                distance: "532",
                distanceUom: "MILE",
                percentageOfTotal: "80%",
                exceptions: [
                {
                    exceptionCode: "SD",
                    exception: "Shipment Delay",
                    reasonCode: "22",
                    reason: "Weather Related",
                    exceptionPoint: "35%"
                }
                ]

            }
        }
    },
    {
        clientCode: "COV",
        client: "Covestro",
        loadNumber: "29163218",
        shipmentNumber: "SH-033643151",
        proNumber: "12251352702",
        bolNumber: "2403404237",
        airWaybillNumber: "",
        trailerNumber: "636422",
        statusCode: 7,
        status: "Delivered",
        isUrgent: false,
        isOnHold: false,
        isHazmat: true,
        carrierScac: "UP",
        carrierName: "Union Pacific",
        baseMode: "RAIL",
        vesselName: "",
        transportModeCode: "RAIL",
        transportMode: "Rail",
        equipmentCode: "",
        equipment: "",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "COV1",
        logisticsGroup: "COV2",
        transactionSource: "MANUAL",
        memo: "",
        origin: {
            originId: "OR54",
            originName: "Baytown Rail SP Covestro LLC",
            originCity: "Baytown",
            originSau: "TX",
            originCountry: "USA",
            originPostalCode: "77523",
            contacts: [
            {
                contactName: "Deanna Mclean",
                contactPhone: "+1(281) 383-7695",
                primaryContact: true
            },
            {
                contactName: "John Doe",
                contactPhone: "+1(555) 867-5309",
                primaryContact: false
            }
            ]
        },
        destination: {
            destinationId: "0005288788",
            destinationName: "Plant ORW2 Covestro LLC",
            destinationCity: "CARSON",
            destinationSau: "CA",
            destinationCountry: "USA",
            destinationPostalCode: "90810",
            contacts: [
            {
                contactName: "NA",
                contactPhone: "+1(310) 603-8797",
                primaryContact: true
            }
            ]
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "RAIL",
                distance: "2477",
                distanceUom: "MILE",
                percentageOfTotal: "100%",
                exceptions: {}
            }
        }
    },
    {
        clientCode: "MTL",
        client: "Mattel",
        loadNumber: "31125032",
        shipmentNumber: "SH-036304473",
        proNumber: "12251352701",
        bolNumber: "2403404237",
        airWaybillNumber: "",
        trailerNumber: "636421",
        statusCode: 8,
        status: "Tender Accepted",
        isUrgent: true,
        isOnHold: false,
        isHazmat: false,
        carrierScac: "GDSD",
        carrierName: "GDS Express",
        baseMode: "GROUND",
        vesselName: "",
        transportModeCode: "TL",
        transportMode: "Truckload",
        equipmentCode: "53DV",
        equipment: "53FT Dry Van",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "MTL1",
        logisticsGroup: "MTL1",
        transactionSource: "GIS",
        memo: "",
        origin: {
            originId: "P30",
            originName: "San Bernardino Distribution Center",
            originCity: "San Bernardino",
            originSau: "CA",
            originCountry: "USA",
            originPostalCode: "92408"
        },
        destination: {
            destinationId: "S2425090",
            destinationName: "Amazon.com CMH2",
            destinationCity: "Groveport",
            destinationSau: "OH",
            destinationCountry: "USA",
            destinationPostalCode: "43125"
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "GROUND",
                distance: "1255",
                distanceUom: "MILE",
                percentageOfTotal: "100%",
                exceptions: {}
            }
        }
    },
    {
        clientCode: "MTL",
        client: "Mattel",
        loadNumber: "31125032",
        shipmentNumber: "SH-036304479",
        proNumber: "",
        bolNumber: "",
        airWaybillNumber: "654471555",
        trailerNumber: "636422",
        statusCode: 7,
        status: "Tendered",
        isUrgent: false,
        isOnHold: false,
        isHazmat: false,
        carrierScac: "FXFE",
        carrierName: "FedEx Express",
        baseMode: "AIR",
        vesselName: "",
        transportModeCode: "1AR",
        transportMode: "Overnight - Early AM",
        equipmentCode: "",
        equipment: "",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "MTL1",
        logisticsGroup: "MTL1",
        transactionSource: "GIS",
        memo: "",
        origin: {
            originId: "P30",
            originName: "San Bernardino Distribution Center",
            originCity: "San Bernardino",
            originSau: "CA",
            originCountry: "USA",
            originPostalCode: "92408"
        },
        destination: {
            destinationId: "S2425090",
            destinationName: "Amazon.com CMH2",
            destinationCity: "Groveport",
            destinationSau: "OH",
            destinationCountry: "USA",
            destinationPostalCode: "43125"
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "AIR",
                distance: "415",
                distanceUom: "MILE",
                percentageOfTotal: "100%",
                exceptions: {}
            }
        }
    },
    {
        clientCode: "MTL",
        client: "Mattel",
        loadNumber: "31125032",
        shipmentNumber: "SH-036304483",
        proNumber: "12251352702",
        bolNumber: "2403404237",
        airWaybillNumber: "",
        trailerNumber: "636422",
        statusCode: 7,
        status: "Tendered",
        isUrgent: false,
        isOnHold: false,
        isHazmat: false,
        carrierScac: "MAES",
        carrierName: "Maersk",
        baseMode: "OCEAN",
        vesselName: "MAERSK ALABAMA",
        transportModeCode: "IM",
        transportMode: "Intermodal",
        equipmentCode: "",
        equipment: "",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "MTL1",
        logisticsGroup: "MTL1",
        transactionSource: "GIS",
        memo: "",
        origin: {
            originId: "P30",
            originName: "San Bernardino Distribution Center",
            originCity: "San Bernardino",
            originSau: "CA",
            originCountry: "USA",
            originPostalCode: "92408"
        },
        destination: {
            destinationId: "S2425090",
            destinationName: "Amazon.com CMH2",
            destinationCity: "Groveport",
            destinationSau: "OH",
            destinationCountry: "USA",
            destinationPostalCode: "43125"
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "GROUND",
                distance: "133",
                distanceUom: "MILE",
                percentageOfTotal: "20%"
            },
            200: {
                baseMode: "OCEAN",
                distance: "532",
                distanceUom: "MILE",
                percentageOfTotal: "80%",
                exceptions: [
                {
                    exceptionCode: "SD",
                    exception: "Shipment Delay",
                    reasonCode: "22",
                    reason: "Weather Related",
                    exceptionPoint: "35%"
                }
                ]

            }
        }
    },
    {
        clientCode: "COV",
        client: "Covestro",
        loadNumber: "29163218",
        shipmentNumber: "SH-033643151",
        proNumber: "12251352702",
        bolNumber: "2403404237",
        airWaybillNumber: "",
        trailerNumber: "636422",
        statusCode: 7,
        status: "Delivered",
        isUrgent: false,
        isOnHold: false,
        isHazmat: true,
        carrierScac: "UP",
        carrierName: "Union Pacific",
        baseMode: "RAIL",
        vesselName: "",
        transportModeCode: "RAIL",
        transportMode: "Rail",
        equipmentCode: "",
        equipment: "",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "COV1",
        logisticsGroup: "COV2",
        transactionSource: "MANUAL",
        memo: "",
        origin: {
            originId: "OR54",
            originName: "Baytown Rail SP Covestro LLC",
            originCity: "Baytown",
            originSau: "TX",
            originCountry: "USA",
            originPostalCode: "77523",
            contacts: [
            {
                contactName: "Deanna Mclean",
                contactPhone: "+1(281) 383-7695",
                primaryContact: true
            },
            {
                contactName: "John Doe",
                contactPhone: "+1(555) 867-5309",
                primaryContact: false
            }
            ]
        },
        destination: {
            destinationId: "0005288788",
            destinationName: "Plant ORW2 Covestro LLC",
            destinationCity: "CARSON",
            destinationSau: "CA",
            destinationCountry: "USA",
            destinationPostalCode: "90810",
            contacts: [
            {
                contactName: "NA",
                contactPhone: "+1(310) 603-8797",
                primaryContact: true
            }
            ]
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "RAIL",
                distance: "2477",
                distanceUom: "MILE",
                percentageOfTotal: "100%",
                exceptions: {}
            }
        }
    },
    {
        clientCode: "MTL",
        client: "Mattel",
        loadNumber: "31125032",
        shipmentNumber: "SH-036304473",
        proNumber: "12251352701",
        bolNumber: "2403404237",
        airWaybillNumber: "",
        trailerNumber: "636421",
        statusCode: 8,
        status: "Tender Accepted",
        isUrgent: true,
        isOnHold: false,
        isHazmat: false,
        carrierScac: "GDSD",
        carrierName: "GDS Express",
        baseMode: "GROUND",
        vesselName: "",
        transportModeCode: "TL",
        transportMode: "Truckload",
        equipmentCode: "53DV",
        equipment: "53FT Dry Van",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "MTL1",
        logisticsGroup: "MTL1",
        transactionSource: "GIS",
        memo: "",
        origin: {
            originId: "P30",
            originName: "San Bernardino Distribution Center",
            originCity: "San Bernardino",
            originSau: "CA",
            originCountry: "USA",
            originPostalCode: "92408"
        },
        destination: {
            destinationId: "S2425090",
            destinationName: "Amazon.com CMH2",
            destinationCity: "Groveport",
            destinationSau: "OH",
            destinationCountry: "USA",
            destinationPostalCode: "43125"
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "GROUND",
                distance: "1255",
                distanceUom: "MILE",
                percentageOfTotal: "100%",
                exceptions: {}
            }
        }
    },
    {
        clientCode: "MTL",
        client: "Mattel",
        loadNumber: "31125032",
        shipmentNumber: "SH-036304479",
        proNumber: "",
        bolNumber: "",
        airWaybillNumber: "654471555",
        trailerNumber: "636422",
        statusCode: 7,
        status: "Tendered",
        isUrgent: false,
        isOnHold: false,
        isHazmat: false,
        carrierScac: "FXFE",
        carrierName: "FedEx Express",
        baseMode: "AIR",
        vesselName: "",
        transportModeCode: "1AR",
        transportMode: "Overnight - Early AM",
        equipmentCode: "",
        equipment: "",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "MTL1",
        logisticsGroup: "MTL1",
        transactionSource: "GIS",
        memo: "",
        origin: {
            originId: "P30",
            originName: "San Bernardino Distribution Center",
            originCity: "San Bernardino",
            originSau: "CA",
            originCountry: "USA",
            originPostalCode: "92408"
        },
        destination: {
            destinationId: "S2425090",
            destinationName: "Amazon.com CMH2",
            destinationCity: "Groveport",
            destinationSau: "OH",
            destinationCountry: "USA",
            destinationPostalCode: "43125"
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "AIR",
                distance: "415",
                distanceUom: "MILE",
                percentageOfTotal: "100%",
                exceptions: {}
            }
        }
    },
    {
        clientCode: "MTL",
        client: "Mattel",
        loadNumber: "31125032",
        shipmentNumber: "SH-036304483",
        proNumber: "12251352702",
        bolNumber: "2403404237",
        airWaybillNumber: "",
        trailerNumber: "636422",
        statusCode: 7,
        status: "Tendered",
        isUrgent: false,
        isOnHold: false,
        isHazmat: false,
        carrierScac: "MAES",
        carrierName: "Maersk",
        baseMode: "OCEAN",
        vesselName: "MAERSK ALABAMA",
        transportModeCode: "IM",
        transportMode: "Intermodal",
        equipmentCode: "",
        equipment: "",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "MTL1",
        logisticsGroup: "MTL1",
        transactionSource: "GIS",
        memo: "",
        origin: {
            originId: "P30",
            originName: "San Bernardino Distribution Center",
            originCity: "San Bernardino",
            originSau: "CA",
            originCountry: "USA",
            originPostalCode: "92408"
        },
        destination: {
            destinationId: "S2425090",
            destinationName: "Amazon.com CMH2",
            destinationCity: "Groveport",
            destinationSau: "OH",
            destinationCountry: "USA",
            destinationPostalCode: "43125"
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "GROUND",
                distance: "133",
                distanceUom: "MILE",
                percentageOfTotal: "20%"
            },
            200: {
                baseMode: "OCEAN",
                distance: "532",
                distanceUom: "MILE",
                percentageOfTotal: "80%",
                exceptions: [
                {
                    exceptionCode: "SD",
                    exception: "Shipment Delay",
                    reasonCode: "22",
                    reason: "Weather Related",
                    exceptionPoint: "35%"
                }
                ]

            }
        }
    },
    {
        clientCode: "COV",
        client: "Covestro",
        loadNumber: "29163218",
        shipmentNumber: "SH-033643151",
        proNumber: "12251352702",
        bolNumber: "2403404237",
        airWaybillNumber: "",
        trailerNumber: "636422",
        statusCode: 7,
        status: "Delivered",
        isUrgent: false,
        isOnHold: false,
        isHazmat: true,
        carrierScac: "UP",
        carrierName: "Union Pacific",
        baseMode: "RAIL",
        vesselName: "",
        transportModeCode: "RAIL",
        transportMode: "Rail",
        equipmentCode: "",
        equipment: "",
        freightTermsCode: "FT_PRE_PAID",
        freightTerms: "Pre Paid",
        division: "COV1",
        logisticsGroup: "COV2",
        transactionSource: "MANUAL",
        memo: "",
        origin: {
            originId: "OR54",
            originName: "Baytown Rail SP Covestro LLC",
            originCity: "Baytown",
            originSau: "TX",
            originCountry: "USA",
            originPostalCode: "77523",
            contacts: [
            {
                contactName: "Deanna Mclean",
                contactPhone: "+1(281) 383-7695",
                primaryContact: true
            },
            {
                contactName: "John Doe",
                contactPhone: "+1(555) 867-5309",
                primaryContact: false
            }
            ]
        },
        destination: {
            destinationId: "0005288788",
            destinationName: "Plant ORW2 Covestro LLC",
            destinationCity: "CARSON",
            destinationSau: "CA",
            destinationCountry: "USA",
            destinationPostalCode: "90810",
            contacts: [
            {
                contactName: "NA",
                contactPhone: "+1(310) 603-8797",
                primaryContact: true
            }
            ]
        },
        dates: {
            shipmentCreated: "03/01/2017 06:04 AM",
            scheduledPickupFrom: "03/01/2017 07:00 AM",
            scheduledPickupTo: "03/03/2017 11:00 AM",
            scheduledDeliverFrom: "03/07/2017 08:00 AM",
            scheduledDeliverTo: "03/07/2017 04:00 PM",
            pickupAppointment: "03/03/2017 08:00 AM",
            deliveryAppointment: "03/07/2017 01:00 PM",
            actualPickupArrival: "",
            actualPickupDeparture: "",
            actualDeliveryArrival: "",
            actualDeliveryDeparture: ""
        },
        measures: {
            nominalHeight: 2.5,
            nominalWidth: 2,
            nominalLength: 3,
            nominalDimensionalUom: "IN",
            nominalVolume: 6.16,
            nominalVolumeUom: "CUFT",
            nominalWeight: 34.6,
            nominalWeightUom: "LB",
            totalPieces: 10,
            totalSkids: 1,
            palletPositions: 1
        },
        references: [
        {
            qualifier: "PO",
            name: "Purchase Order Number",
            value: "1GNNTSQL"
        },
        {
            qualifier: "CCLS",
            name: "Consolidation Class",
            value: "AMAZON"
        },
        {
            qualifier: "CO",
            name: "Corporate ID",
            value: "852737"
        }
        ],
        journey: {
            100: {
                baseMode: "RAIL",
                distance: "2477",
                distanceUom: "MILE",
                percentageOfTotal: "100%",
                exceptions: {}
            }
        }
    },
    ];

}]);