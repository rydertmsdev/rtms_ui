App.service('ItemMasterService',
  /**
   * Create Iteam Master Service
   */
  function ItemMasterService($rootScope, $http, $filter, $q) {

    this.createItem = function (module, data, type) {
      
      var deferred = $q.defer();

      if(type == "update" || type == "updateFromOF"){

        if(type == "updateFromOF")
          var url = "/ryderonline/rtms/service/"+module+"/update?insertIfNotExists=true";

        if(type == "update")
          var url = "/ryderonline/rtms/service/"+module+"/update"

        $http.post(url, data)
        .then( function(response) {
              deferred.resolve(response);
          }, function(errResp) {
               deferred.reject(errResp);
        });
      }
      else{
        $http.post("/ryderonline/rtms/service/"+module+"/create", data)
        .then( function(response) {
              deferred.resolve(response);
          }, function(errResp) {
               deferred.reject(errResp);
        });
      }

      
      return deferred.promise;
    }

    this.deleteItem = function (module, data) {
      
      var deferred = $q.defer();

      $http.get("/ryderonline/rtms/service/"+module+"/delete/"+data.customernum+"/"+data.id)
      .then( function(response) {
            deferred.resolve(response);
        }, function(errResp) {
             deferred.reject(errResp);
      });
      
      return deferred.promise;
    }

        /* Get the search result  */

    this.getResults = function (module, data, pageNum, pageSize) {
      
      var deferred = $q.defer();

      $http.post("/ryderonline/rtms/service/"+module+"/searchPaged", data)
      .then( function(response) {
               deferred.resolve(response);
          }, function(errResp) {
               deferred.reject(errResp);
          });
      return deferred.promise;
    }

});

App.service('itemMasterSvc', [
  '$rootScope',
  '$window', 
  '$q', 
  '$timeout', 
  '$filter', 
  '$state',
  function ($rootScope, $window, $q, $timeout, $filter, $state) {
    var svc=this;

    svc.module = "itemmaster";
    svc.itemUpdateDetails = {};
    svc.contactsIndex = [0];
    svc.searchResults = [];
    svc.index = 1;
    svc.pageTotal = "";
    svc.recordsCount = "";
    svc.pageSize = 50;
    svc.rowsPerbatch = 50;

  return svc;
}]);