<div class="form modal-large" style="display: inline-block;" ng-controller="ordersLocationSearchCtrl">
	<ui-header theme="ui-modal-header">
		<ui-header-body>
			<div class="skew" >
				<span>Edit Location</span>
			</div>
		</ui-header-body>
		<ui-header-controls class="inset">
			<button ui-button ng-click="close();"><i class="fa fa-times"></i></button>
		</ui-header-controls>
	</ui-header>
	<div class="modal-body overflow-y">
		<div class="form">
			<form name="locationDetailsForm">
				<ui-section>
					<div class="cols">
						<ui-field type="text" ng-model="locationDetails.entityID" label="Location ID" ng-required="true" ng-disabled="true"></ui-field>
						<ui-field type="text" ng-model="locationDetails.addressName1" label="Location Name" ng-required="true" ></ui-field>							
						<ui-field type="text" ng-model="locationDetails.address1" label="Address Street 1" ng-required="true" ></ui-field>
						<ui-field type="text" ng-model="locationDetails.address2" label="Address Street 2"  ng-required="false"></ui-field>
					</div>
					<div class="cols">
						<ui-field type="text" ng-model="locationDetails.city" label="City"  ng-required="true"></ui-field>
						<ui-field type="select" options="statesList" prop="name" ng-model="locationDetails.state" label="State/Province"  ng-required="true"></ui-field>
						<ui-field type="select" options="countriesList" prop="name" ng-model="locationDetails.country" label="Country" ng-required="true" ></ui-field>
						<ui-field type="text" ng-model="locationDetails.postalCode" label="Postal Code" ng-required="true" ></ui-field>
					</div>
					<div class="cols">
						<ui-field type="select" options="timezoneList" prop="timezoneDesc" ng-model="locationDetails.timeZone" label="Timezone" ng-required="true" ></ui-field>
						<ui-field type="select" options="useTypes" prop="name" ng-model="locationDetails.type" label="Use Type" ng-required="true" ></ui-field>
						<!-- <ui-field type="select" options="locationProfileOptions" prop="name" ng-model="location.profile" label="Location Profile" ng-required="true" ></ui-field>-->
						<div></div> 
						<div></div> 
					</div>
					<div class="cols" ng-repeat="fourColumns in locationCreateFieldColumns">
						<div  ng-repeat="createField in fourColumns">				
							<ui-field type="text" ng-if="createField.displayType == 'readonly'"  ng-model="locationDetails[createField.fieldName]" label="{{createField.displayName}}" ng-disabled="true"></ui-field>
							<ui-field type="text" ng-if="createField.displayType == 'freeform text'" ng-model="locationDetails[createField.fieldName]" label="{{createField.displayName}}" ng-required="createField.required()" ng-disabled="false"></ui-field>
							<ui-field type="select" ng-if="createField.displayType == 'dropdown'" options="createField.options" prop="name" ng-model="locationDetails[createField.fieldName]" label="{{createField.displayName}}" ng-required="createField.required()" ng-disabled="createField.displayType == 'readonly' ? true : false" ></ui-field>			
							<ui-field type="text" ng-if="createField.displayType == 'textarea' " ng-model="locationDetails[createField.fieldName]" label="{{createField.displayName}}" ng-required="createField.required()" ng-disabled="false"></ui-field>					
						</div>
					</div>
					<!-- <div class="cols" ng-repeat="field in locationCreateFields">
						<ui-field type="text" ng-if="field.displayType == 'textarea' " ng-model="locationDetails[field.fieldName]" label="{{field.displayName}}" ng-required="createField.required()" ng-disabled="false"></ui-field>
					</div> -->
					<ui-header theme="section">
						<ui-header-body>
							<span class="ng-binding">Contacts</span>
							
							<!-- <ui-page-actions>
								<button ui-button class="bg-accent" ng-click="addContact()" >
									<span>Add Another Contact</span>
								</button>
							</ui-page-actions> -->
						</ui-header-body>
						<div ui-info="Add Another Contact" class="" ui-button ng-click="addContact()">
							<i class="fa fa-plus"></i>
							<span>Add Another Contact</span>
						</div>
					</ui-header>
					<div id="locationContacts" ng-repeat="index in contactsIndex">
						<div class="contactHeader">
							<span class="contactLabel">
								Contact {{index + 1}}
							</span>
							<span ng-click="deleteContact(index)" ng-if="index > 0" class="deleteContact">
								<i class="fa fa-times text-red"></i>
								<span>Delete Contact</span>
							</span>
						</div>
						<ui-section>
							<div class="cols" ng-repeat="fourColumns in locationContactFieldColumns">
								<div  ng-repeat="contactField in fourColumns">				
									<ui-field type="text" ng-if="contactField.displayType == 'readonly'"  ng-model="locationContacts[index][contactField.fieldName]" label="{{contactField.displayName}}" ng-disabled="true"></ui-field>
									<ui-field type="text" ng-if="contactField.displayType == 'freeform text'" ng-model="locationContacts[index][contactField.fieldName]" label="{{contactField.displayName}}" ng-required="contactField.required()" ng-disabled="false"></ui-field>
									<ui-field type="select" ng-if="contactField.displayType == 'dropdown'" options="contactField.options" prop="name" ng-model="locationContacts[index][contactField.fieldName]" label="{{contactField.displayName}}" ng-required="contactField.required()" ng-disabled="contactField.displayType == 'readonly' ? true : false" ></ui-field>
									<ui-field type="select" ng-if="contactField.displayType == 'language_dropdown'" options="languageOptions" prop="name" ng-model="locationContacts[index][contactField.fieldName]" label="{{contactField.displayName}}" ng-required="contactField.required()" ng-disabled="contactField.displayType == 'readonly' ? true : false" ></ui-field>	
									<ui-field ng-if="contactField.displayType=='single checkbox'" type="toggle" class="no-label" ng-model="locationContacts[index][contactField.fieldName]" ui-info="{{contactField.hoverText}}" title="{{contactField.displayName}}" ng-required="contactField.required()" ng-disabled="contactField.disabled()"></ui-field>					
								</div>
							</div>
						</ui-section>
					</div>
			
				</ui-section>
				<ui-header theme="ui-modal-footer">
					<ui-header-controls>
						<button ui-button="" ng-click="close()" class="text-accent bg-white">
							<span>Cancel</span>
						</button>
						<button ui-button="" type="submit" ng-click="submitLocation(locationDetailsForm.$valid, 'update')" class="bg-accent text-white">
							<span>Update</span>
						</button>
					</ui-header-controls>	
				</ui-header>	
			</form>	
		<div>
	</div>
</div>