App.controller('locationEditCtrl',['$scope', '$timeout', 'uiModalSvc', 'uiLoaderSvc', 'orderSvc', function($scope, $timeout, uiModalSvc, uiLoaderSvc, orderSvc){	

	$scope.countriesList = orderSvc.countriesList;
	$scope.statesList = orderSvc.statesList;

	$scope.searchResults = [];
	$scope.showNoResultFlag = false;
	
	$scope.addressLine1;
	$scope.addressLine2;
	$scope.contactName;
	$scope.city;
	$scope.state;
	$scope.country;
	$scope.timeZone;	
	$scope.postalCode;
	$scope.contactPhone;

	$scope.close=function(){
		uiModalSvc.hide();
	}
}]);