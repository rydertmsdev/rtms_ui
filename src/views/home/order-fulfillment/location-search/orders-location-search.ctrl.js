App.controller('ordersLocationSearchCtrl',['$scope', '$rootScope', '$compile', '$timeout','uiModalSvc', 'uiLoaderSvc', 'LocationService', 'logoutService', 'codeListService', 'generateSearchQuery', 'orderSvc', 'purchaseOrderSearchResult', 'locationSvc', function($scope,$rootScope,$compile,$timeout,uiModalSvc,uiLoaderSvc,LocationService,logoutService,codeListService,generateSearchQuery,orderSvc, purchaseOrderSearchResult, locationSvc){

$scope.locationDetails = locationSvc.locationUpdateDetails;
$scope.StateListMap = {};
$scope.CountryListMap = {};
$scope.TimezoneListMap = {};
$scope.UseTypeMap = {};
$scope.languageOptions = [];
$scope.SearchResultsFieldList = [];
$scope.codeLists = [];
$scope.statesList = [];
$scope.countriesList = [];
$scope.timezoneList = [];
$scope.locationContactFields = [];
$scope.locationCreateFields = [];
$scope.location = {};
$scope.showAddLocation = false;
$scope.createLocationFieldValues = {};
$scope.dynamicContent = [];
$scope.searchContent = [];
$scope.additionalSearchContent = [];
$scope.locationSvc = locationSvc;
locationSvc.searchResults = [];

if(generateSearchQuery.from_server_copy){
	$scope.dynamicContent = generateSearchQuery.from_server_copy.SearchFieldList;
}

angular.forEach($scope.dynamicContent, function(field, index) {
	if(field.sectionSeq == 0 || field.sectionSeq == null){
		$scope.searchContent.push(field);
	}
	else{
		$scope.additionalSearchContent.push(field);
	}
});

if(codeListService.codeLists){
	$scope.codeLists = codeListService.codeLists;
}

if(codeLists.LanguageList){
	$scope.languageOptions = codeLists.LanguageList;
}

// if(codeLists.StateList){
// 	angular.forEach(codeLists.StateList, function(value, index){
// 		$scope.StateListMap[value.code] = value;
// 	});
// }

// if(codeLists.CountryList){
// 	angular.forEach(codeLists.CountryList, function(value, index){
// 		$scope.CountryListMap[value.code] = value;
// 	});
// }

// if(codeLists.TimezoneList){
// 	angular.forEach(codeLists.TimezoneList, function(value, index){
// 		$scope.TimezoneListMap[value.timezoneCode] = value;
// 	});
// }

$scope.useTypes = [{"name":"Origin", "code" : "19"}, {"name":"Destination", "code" : "20"}];
// if($scope.useTypes){
// 	angular.forEach($scope.useTypes, function(value, index){
// 		$scope.UseTypeMap[value.code] = value;
// 	});
// }

if(codeLists.SearchResultsFieldList){
	$scope.SearchResultsFieldList = codeLists.SearchResultsFieldList;
}

// $scope.locationDetails.state = $scope.StateListMap[$scope.locationDetails.state];
// $scope.locationDetails.country = $scope.CountryListMap[$scope.locationDetails.country];
// $scope.locationDetails.timeZone = $scope.TimezoneListMap[$scope.locationDetails.timeZone];
// $scope.locationDetails.type = $scope.UseTypeMap[$scope.locationDetails.type];

$scope.contactsIndex = locationSvc.contactsIndex;
$scope.locationContacts = locationSvc.locationContacts;

$scope.filters={
	country:{
		value:"US"
	}
}

if($scope.codeLists.StateList){
	$scope.statesList = $scope.codeLists.StateList;
}

if($scope.codeLists.CountryList){
	$scope.countriesList = $scope.codeLists.CountryList;
}

if($scope.codeLists.TimezoneList){
	$scope.timezoneList = $scope.codeLists.TimezoneList;
}

if($scope.codeLists.CreateUpdateFieldList){
	$scope.CreateUpdateFieldList = $scope.codeLists.CreateUpdateFieldList;
}

angular.forEach($scope.CreateUpdateFieldList, function(field, index){
	// if(field.displayType == "dropdown"){
	// 	$scope[field.fieldName] = {};
	// 	angular.forEach(field.options, function(value, index){
	// 		$scope[field.fieldName][value.code] = value;
	// 	});
	// 	if($scope.locationDetails[field.fieldName]){
	// 		$scope.locationDetails[field.fieldName] = $scope[field.fieldName][$scope.locationDetails[field.fieldName]];
	// 	}
	// }
	// else if(field.displayType == "language_dropdown"){
	// 	$scope[field.fieldName] = {};
	// 	angular.forEach($scope.languageOptions, function(value, index){
	// 		$scope[field.fieldName][value.code] = value;
	// 	});
	// 	if($scope.locationDetails[field.fieldName]){
	// 		$scope.locationDetails[field.fieldName] = $scope[field.fieldName][$scope.locationDetails[field.fieldName]];
	// 	}
	// 	if($scope.locationDetails.contactList){
	// 		angular.forEach($scope.locationDetails.contactList, function(contact, index){
	// 			if($scope.locationDetails.contactList[index][field.fieldName]){
	// 				$scope.locationDetails.contactList[index][field.fieldName] = $scope[field.fieldName][$scope.locationDetails.contactList[index][field.fieldName]];
	// 			}
	// 		});
	// 	}
	// }


	if(field.sectionSeq == 1){
		$scope.locationCreateFields.push(field);
	}
	else if(field.sectionSeq == 2){
		$scope.locationContactFields.push(field);
	}
});

function columnize(input, cols, fieldLength) {
	var arr = [];
	for(i = 0; i < input.length; i++) {
	  	
	  	if(input[i].required == "Y" || input[i].required == true){
	  		input[i].required = function(){return true};
	  	}
	  	else if(input[i].required == "N" || input[i].required == false || !(input[i].required)){
	  		input[i].required = function(){return false};
	  	}
	  	
	  	if(input[i].disabled == "Y" || input[i].disabled == true){
	  		input[i].disabled = function(){return true};
	  	}
	  	else if(input[i].disabled == "N" || input[i].disabled == false || !(input[i].disabled)){
	  		if(input[i].displayType == 'readonly' || input[i].displayType == 'hidden') {
	  			input[i].disabled = function(){return true};	
	  		}
	  		else{
	  			input[i].disabled = function(){return false};
	  		}	  		
	  	}

	  	if(input[i].displayType != 'hidden' && input[i].displayType != 'none'){
		    var colIdx = Math.floor(i / fieldLength);
	    	arr[colIdx] = arr[colIdx] || [];
	    	arr[colIdx].push(input[i]);
	    }
	    else if(input[i].displayType == 'hidden' && input[i].fieldType == 'CHRG'){
	    	arr[cols] = arr[cols] || [];
	    	arr[cols].push(input[i]);
	    }
	    else if(input[i].displayType == 'hidden' && input[i].fieldType == 'REF'){
	    	arr[cols+1] = arr[cols+1] || [];
	    	arr[cols+1].push(input[i]);
	    }
	}
	return arr;
}

$scope.locationCreateFieldColumns = columnize($scope.locationCreateFields, Math.ceil($scope.locationCreateFields.length/4), 4);
$scope.locationContactFieldColumns = columnize($scope.locationContactFields, Math.ceil($scope.locationContactFields.length/4), 4);
$scope.SearchResultsFieldListColumns = columnize($scope.SearchResultsFieldList, Math.ceil($scope.SearchResultsFieldList.length/4), 4);

$scope.useTypes = [{"name":"Origin", "code" : "19"}, {"name":"Destination", "code" : "20"}];
$scope.selectedUseType = {"name":"Select", "code" : ""};

$scope.allowance = {"name":"Select", "code" : ""};
$scope.restrictions = {"name":"Select", "code" : ""};
$scope.allowanceOptions = [{"name":"26' Dry Van With Lift Gate", "code" : ""},{"name":"36' Dry Van", "code" : ""}, {"name":"53' Dry Van", "code" : ""}, {"name":"45' Flatbed", "code" : ""}];
$scope.restrictionsOptions = [{"name":"26' Dry Van With Lift Gate", "code" : ""},{"name":"36' Dry Van", "code" : ""}, {"name":"53' Dry Van", "code" : ""}, {"name":"45' Flatbed", "code" : ""}];
$scope.primaryOptions = [{'name': 'Yes', 'code': 'Y'}, {'name': 'No', 'code': 'N'}];
$scope.locationProfileOptions = [{'name': 'Yes', 'code': 'Y'}, {'name': 'No', 'code': 'N'}];

$scope.submitLocation=function(flag, type){
	console.log(flag);
	if(flag){
		angular.forEach($scope.locationContactFields, function(fieldSet, i){
			if(fieldSet.displayType == "dropdown" || fieldSet.displayType == "language_dropdown"){
				angular.forEach($scope.locationContacts, function(contact, index){
					$scope.locationContacts[index][fieldSet.fieldName] = contact[fieldSet.fieldName] ? (contact[fieldSet.fieldName].code || contact[fieldSet.fieldName]) : "";
				});
			}
		});

		if(type=="update"){
			$scope.location = $scope.locationDetails;
			$scope.createLocationFieldValues = $scope.locationDetails;
		}

		var requestObj = {  
		   "entityID":$scope.location.entityID || "", 
		   "addressName1":$scope.location.addressName1 || "",
	      // "companyName": $rootScope.userProfile.data.content.customerNum,
	       "address1":$scope.location.address1 || "",
	       "address2":$scope.location.address2 || "",
	       "city":$scope.location.city || "",
	       "state": $scope.location.state ? ($scope.location.state.code || $scope.location.state) : "",
	       "postalCode":$scope.location.postalCode || "",
	       "country": $scope.location.country ? ($scope.location.country.code || $scope.location.country) : "",
		   "type": $scope.location.type ? ($scope.location.type.code || $scope.location.type) : "",
		   "timeZone": $scope.location.timeZone ? ($scope.location.timeZone.timezoneCode || $scope.location.timeZone) : "",
		   // "locationProfile": $scope.location.profile ? $scope.location.profile.code : "",
		   "contactList": $scope.locationContacts
		};

		angular.forEach($scope.locationCreateFields, function(createField, j){
			var codeArr = createField.code.split(".");

			if(codeArr.length == 1){
				requestObj[createField.fieldName] = $scope.createLocationFieldValues[createField.fieldName];
			}
			else if(codeArr.length == 2){
				requestObj[codeArr[0]][codeArr[1]] = $scope.createLocationFieldValues[createField.fieldName];
			}
		});

		uiLoaderSvc.show();
		LocationService.createLocation("address/v1", requestObj, type).then(function(data){
			uiLoaderSvc.hide();

			var message = "New Location created.";

			if(type=="update"){
				message = "Location Updated";
			}

			uiModalSvc.show('ok-cancel-modal.tpl',{
				title:"Success",
				icon: "fa fa-check",
				text: message,
				callback:function(){
					assignSearchDropdownValues();

					$scope.locationSearch();

				}
			});
			
		}, function(err){
			uiLoaderSvc.hide();
			console.log("Service Error - ", err);

			//User not authenticated (403) redirecting to login page
			if(err.status == 403){
				logoutService.logout();
			}
			else{
				var errMessage = err.data.errorDetails;

				uiModalSvc.show('ok-cancel-modal.tpl',{
					title:"Error",
					icon: "fa fa-exclamation-triangle",
					text: "Something went wrong, please try again.",
					callback:function(){
						assignSearchDropdownValues();
					}
				});
			}
		});
	}
}

function assignSearchDropdownValues(){
	angular.forEach($scope.locationDetails, function(value, key){
		if(angular.isObject(value)){
			if(key == "timeZone"){
				$scope.locationDetails[key] = value.timezoneDesc || "";
			}
			else if(key == "contactList"){
				// do nothing as contacList array is handled below
			}
			else{
				$scope.locationDetails[key] = value.name || "";	
			}
		}
	});

	angular.forEach($scope.locationDetails.contactList, function(contact, index){
		angular.forEach(contact, function(value, key){
			if(angular.isObject(value)){
				$scope.locationDetails.contactList[index][key] = value.name || "";
			}
		});
	});
}

$scope.deleteContact=function(index){
	console.log("Delete Contact Index -- ", index);
	$scope.uiModalSvc.show('ok-cancel-modal.tpl',{
		title:"Delete Location",
		text:"Are you sure you want delete Contact " +(index+1),
		callback:function(){
			angular.forEach($scope.contactsIndex, function(value, key){
				if(value == index){
					$scope.contactsIndex.splice(key, 1);
				}
			});
		}
	});	
}

$scope.addLocation=function(){
	$scope.contactsIndex = [0];
	$scope.locationContacts = [];
	$scope.showAddLocation = true;

}

$scope.locationSearch=function(){
	$scope.showAddLocation = false;
}

$scope.deleteLocation=function(locationId, type, index){
	$scope.uiModalSvc.show('ok-cancel-modal.tpl',{
		title:"Delete Location",
		text:"Are you sure you want deactivate this Location",
		callback:function(){

			var reqObj = {"entityID": locationId, "type": type};
			uiLoaderSvc.show();
			LocationService.deleteLocation("address/v1", reqObj).then(function(data){
				uiLoaderSvc.hide();
				uiModalSvc.show('ok-cancel-modal.tpl',{
					title:"Success",
					icon: "fa fa-check",
					text: "Location successfully deactivated.",
					callback:function(){
						//Do nothing on OK button.
					}
				});
				locationSvc.searchResults.splice(index, 1);
			},function(err){
				uiLoaderSvc.hide();
				var errMessage = err.data.errorDetails;

				uiModalSvc.show('ok-cancel-modal.tpl',{
					title:"Error",
					icon: "fa fa-exclamation-triangle",
					text: errMessage,
					callback:function(){
						//Do nothing on OK button.
					}
				});
			});
		}
	});	
}

$scope.editLocation=function(index){
	locationSvc.locationUpdateDetails = locationSvc.searchResults[index];
	locationSvc.locationContacts = locationSvc.searchResults[index].contactList || [];
	locationSvc.contactsIndex = [0];
	angular.forEach(locationSvc.locationContacts, function(contact, cIndex){
		if(cIndex > 0){
			locationSvc.contactsIndex.push(cIndex);
		}
	});
	
	$scope.uiModalSvc.show('location-edit-modal.tpl',{
	});
}

var index =1;
$scope.addContact=function(){
	$scope.contactsIndex.push(index++);
	//angular.element(document.getElementById('locationContacts')).append($compile('<ui-section><div class="cols"><ui-field type="text" ng-model="firstName[$scope.$index]" label="First Name" ></ui-field><ui-field type="text" ng-model="lastName" label="Last Name" ></ui-field><ui-field type="text" ng-model="email" label="Email" ></ui-field><ui-field type="text" ng-model="language" label="Language" ></ui-field></div><div class="cols"><ui-field type="text" ng-model="phone" label="Phone" ></ui-field><ui-field type="text" ng-model="altPhone" label="Alt Phone" ></ui-field><ui-field type="text" ng-model="fax" label="Fax" ></ui-field><ui-field type="text" ng-model="primary" label="Primary" ></ui-field></div></ui-section>')($scope));
}

$scope.close=function(){
	assignSearchDropdownValues();
	uiModalSvc.hide();
}

$scope.doLocationSearch=function(type){

		uiLoaderSvc.show();

		$scope.searchCriteria = generateSearchQuery.searchOrderQueryResp();
		if(!!$scope.location.locationId){
			$scope.searchCriteria["entityID"] = $scope.location.locationId;
		}
		$scope.searchCriteria.customerNum = $rootScope.userProfile.data.content.customerNum;
		// $scope.searchCriteria.status = "NOT_ALLOCATED";

		$scope.requestObject = {};
		// locationSvc.index = 1;
		if(type == "doublePrevious"){
			var totalNumOfPages = locationSvc.rowsPerbatch/locationSvc.pageSize ;
			$scope.requestObject.pageNum = (Math.floor((locationSvc.index - 1)/totalNumOfPages)*totalNumOfPages)-totalNumOfPages+1;	
		}
		else if(type == "previous"){
			$scope.requestObject.pageNum = locationSvc.index - 1;
		}
		else if(type == "next"){
			$scope.requestObject.pageNum = locationSvc.index + 1;
		}
		else if(type == "doubleNext"){
			var totalNumOfPages = locationSvc.rowsPerbatch/locationSvc.pageSize;
			//$scope.requestObject.pageNum = locationSvc.pageTotal + 1;
			$scope.requestObject.pageNum = (Math.floor((locationSvc.index + (totalNumOfPages - 1))/totalNumOfPages)*totalNumOfPages)+1;	
		}
		else{
			$scope.requestObject.pageNum = 1;	
		}
		// $scope.requestObject.pageNum = locationSvc.index;
		$scope.requestObject.pageSize = locationSvc.pageSize;
		// $scope.requestObject.rowsPerBatch = locationSvc.rowsPerbatch;
		// $scope.requestObject.searchCriteria = $scope.searchCriteria;

		console.log("Request Object  ---   ", JSON.stringify($scope.requestObject));
		var orders = LocationService.getResults("address/v1", $scope.searchCriteria, $scope.requestObject.pageNum, locationSvc.pageSize).then(function(resp){
			uiLoaderSvc.hide();
			//clearing the searchObject, but not needed here as we need to retain previous entered values

			if(resp.data && resp.data.masterDataResponse){
				locationSvc.index = $scope.requestObject.pageNum;
				locationSvc.searchResults = resp.data.masterDataResponse;	
				locationSvc.pageTotal = Math.ceil(resp.data.recordsCount/$scope.requestObject.pageSize);
				locationSvc.recordsCount = resp.data.recordsCount;
			}		
		},
		function(data){
			uiLoaderSvc.hide();
			locationSvc.searchResults = [];
			console.log("Service Error - ", data);
			
			//User not authenticated (403) redirecting to login page
			if(data.status == 403){
				logoutService.logout();
			}
			else{
				uiModalSvc.show('ok-cancel-modal.tpl',{
					title:"Error",
					icon: "fa fa-exclamation-triangle",
					text: "Cannot get any results for your search, Please try again.",
					callback:function(){
						//Do nothing on OK button.
					}
				});
			}
		});
	
		$rootScope.$emit("searchClicked");
	}

$scope.clearFilters=function(){
	$scope.location.locationId = "";
	generateSearchQuery.clearValues();
}

}]);
