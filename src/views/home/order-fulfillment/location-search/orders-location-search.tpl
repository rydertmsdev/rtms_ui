<ui-sidebar-nav style="height: 100%; overflow-y: auto;">
	<nav class="sidebar-nav">
		<ui-sidebar-btn></ui-sidebar-btn>
		<a ng-click="locationSearch()" ui-info="Location Search" ui-sref-active="active">
			<i class="fa fa-search"></i>
		</a>
		<a ui-info="Add Location" ui-sref-active="active" ng-click="addLocation()">
			<i class="fa fa-plus"></i>
		</a>
	</nav>
	<ui-sidebar-nav-content ng-if="!showAddLocation">
		<ui-sidebar>
			<ui-sidebar-panel>
				<div class="content form">
					<ui-header theme="ui-sidebar-header">
						<ui-header-body>
							<div>
								<span>
									Search
								</span>
							</div>
						</ui-header-body>
						<ui-header-controls>
							<div ui-button title="Clear" ng-click="clearFilters()">
								<i class="fa fa-times text-red"></i>
							</div>
							<div ui-button title="Apply" ng-click="doLocationSearch('')">
								<i class="fa fa-check text-green"></i>
							</div>
						</ui-header-controls>
					</ui-header>
					<div>
						<ui-menu-group static>
							<ui-header theme="ui-menu-header">
								<ui-header-body>
									<span>
										Filters
									</span>
								</ui-header-body>
							</ui-header>
							<ui-menu-content ui-max-height>
								<ui-field type="text" ng-model="location.locationId" label="Location ID" placeholder="---" ng-required="false" ng-disabled="false"></ui-field>
								<!-- <ui-field type="text" ng-model="filters.name.value" label="Location Name" placeholder="---" ng-required="false" ng-disabled="false"></ui-field> -->
								<dynamic-template data="searchContent"></dynamic-template>
								<!-- <ui-field type="text" ng-model="filters.name.value" label="Name" placeholder="---" ng-required="false" ng-disabled="false"></ui-field>
								<ui-field type="text" ng-model="filters.address1.value" label="Address" placeholder="---" ng-required="false" ng-disabled="false"></ui-field>
								<ui-field type="text" ng-model="filters.city.value" label="City" placeholder="---" ng-required="false" ng-disabled="false"></ui-field>
								<ui-field type="text" ng-model="filters.state.value" label="State/Province" placeholder="---" ng-required="false" ng-disabled="false"></ui-field>
								<ui-field type="text" ng-model="filters.postalCode.value" label="Postal Code" placeholder="---" ng-required="false" ng-disabled="false"></ui-field>
								<ui-field type="text" ng-model="filters.country.value" label="Country" placeholder="---" ng-required="false" ng-disabled="false"></ui-field>
								<ui-field type="text" ng-model="filters.timeZone.value" label="Time Zone" placeholder="---" ng-required="false" ng-disabled="false"></ui-field> -->
							</ui-menu-content>
						</ui-menu-group>
					</div>
				</div>
			</ui-sidebar-panel>
			<ui-sidebar-content ng-if="!showAddLocation">
				<div class="searchResultsContainer">
				<ui-alert-container></ui-alert-container>
			<!-- 	<ui-filters>
					<ui-filter>
						<div>
							<label>Country</label>
							<span>US</span>
						</div>
						<div ui-button ui-filter-remove-btn>
							<i class="fa fa-times"></i>
						</div>
					</ui-filter>
				</ui-filters> -->
				<div ng-if="locationSvc.searchResults.length == 0">
					<ui-section>
						<ui-list class="static">
							<ui-list-body>
								<li>
									<div class="no-results">
										No results to show.
									</div>
								</li>
							</ui-list-body>
						</ui-list>
					</ui-section>
				</div>
				<div ng-if="locationSvc.searchResults.length > 0">
					<ui-section ng-repeat="result in locationSvc.searchResults" >
						<ui-list class="static">
						<ui-list-body>
						<li>
							<ui-list-content>
								<cell class="text rows">
									<ui-header theme="section">
												<ui-header-body>
													<span>
														{{result.addressName1}}
													</span>
												</ui-header-body>
						<!-- 				<ui-header-controls>
										<div ui-button ng-click="showRuleDetails(rule)" ui-info="Details"><i class="fa fa-info text-accent"></i></div>
									</ui-header-controls> -->
									</ui-header>
								</cell>
							</ui-list-content>
							<ui-list-actions-btn ui-button ui-info="More Actions">
								<i class="ic-dot-3"></i>
							</ui-list-actions-btn>

							<ui-list-actions>
								<div ui-info="Edit" class="bg-eBlue" ui-button ng-click="editLocation($index)">
									<i class="fa fa-edit"></i>
								</div>
								<div ui-info="Delete" class="bg-red" ui-button ng-click="deleteLocation(result.entityID,result.type, $index)" >
									<i class="fa fa-trash"></i>
								</div>
							</ui-list-actions>
						</li>
						<li>
							<ui-list-content>
								<cell class="text rows">						
									<div class="cols" ng-repeat="fourColumns in SearchResultsFieldListColumns">
										<div  ng-repeat="searchField in fourColumns">				
											<ui-field type="text" ng-model="result[searchField.fieldName]" label="{{searchField.displayName}}" ng-required="false" ng-disabled="true"></ui-field>	
										</div>
									</div>
								</cell>
							</ui-list-content>
						</li>
						</ui-list-body>
						</ui-list>
						<ui-header theme="ui-list-item-header">
							<ui-header-body>
							</ui-header-body>
							<ui-header-controls>
								<span class="subtitle"><i class="fa fa-globe text-accent"></i>&nbsp;&nbsp;{{result.timeZone}}</span>
							</ui-header-controls>
						</ui-header>
					</ui-section>
				</div>
				<ui-page-actions-spacer></ui-page-actions-spacer>
				<ui-page-actions-container ng-if="locationSvc.searchResults.length > 0">
					<ui-page-actions>
						<div class="group">

							<div ui-button ui-page-action class="bg-lgray" ui-info="Previous Page" ng-if="locationSvc.index > 1" ng-click="doLocationSearch('previous')">
								<i class="fa fa-chevron-left"></i>
							</div>
							<div ui-button ui-page-action class="bg-lgray" ui-info="Previous Page" ng-if="locationSvc.index == 1" disabled="disabled">
								<i class="fa fa-chevron-left"></i>
							</div>
							<div class="text">
								<span>{{locationSvc.index}} / {{locationSvc.pageTotal}}</span>
							</div>
							<div ui-button ui-page-action class="bg-accent"  ui-info="Next Page" ng-if="(locationSvc.index < locationSvc.pageTotal) || ((locationSvc.index == locationSvc.pageTotal) && locationSvc.recordsCountExceeded)" ng-click="doLocationSearch('next')">
								<i class="fa fa-chevron-right"></i>
							</div>
							<div ui-button ui-page-action class="bg-accent"  ui-info="Next Page" ng-if="(locationSvc.index == locationSvc.pageTotal) && !locationSvc.recordsCountExceeded" disabled="disabled">
								<i class="fa fa-chevron-right"></i>
							</div>
						</div>
					</ui-page-actions>
				</ui-page-actions-container>
				</div>
			</ui-sidebar-content>
			
		</ui-sidebar>
	</ui-sidebar-nav-content>
	<ui-sidebar-nav-content ng-if="showAddLocation" >
		<form name="locationDetailsForm">
			<ui-sidebar-content>
				<ui-section>
					<ui-header theme="section">
						<ui-header-body>
							<span class="ng-binding">Add Location</span>
						</ui-header-body>
					</ui-header>
					<ui-section>
						<div class="cols">
							<ui-field type="text" ng-model="location.entityID" label="Location ID" ng-required="true"></ui-field>
							<ui-field type="text" ng-model="location.addressName1" label="Location Name" ng-required="true" ></ui-field>							
							<ui-field type="text" ng-model="location.address1" label="Address Street 1" ng-required="true" ></ui-field>
							<ui-field type="text" ng-model="location.address2" label="Address Street 2"  ng-required="false"></ui-field>
						</div>
						<div class="cols">
							<ui-field type="text" ng-model="location.city" label="City"  ng-required="true"></ui-field>
							<ui-field type="select" options="statesList" prop="name" ng-model="location.state" label="State/Province"  ng-required="true"></ui-field>
							<ui-field type="select" options="countriesList" prop="name" ng-model="location.country" label="Country" ng-required="true" ></ui-field>
							<ui-field type="text" ng-model="location.postalCode" label="Postal Code" ng-required="true" ></ui-field>
						</div>
						<div class="cols">
							<ui-field type="select" options="timezoneList" prop="timezoneDesc" ng-model="location.timeZone" label="Timezone" ng-required="true" ></ui-field>
							<ui-field type="select" options="useTypes" prop="name" ng-model="location.type" label="Use Type" ng-required="true" ></ui-field>
							<!-- <ui-field type="select" options="locationProfileOptions" prop="name" ng-model="location.profile" label="Location Profile" ng-required="true" ></ui-field>-->
							<div></div> 
							<div></div> 
						</div>
						<div class="cols" ng-repeat="fourColumns in locationCreateFieldColumns">
							<div  ng-repeat="createField in fourColumns">				
								<ui-field type="text" ng-if="createField.displayType == 'readonly'"  ng-model="createLocationFieldValues[createField.fieldName]" label="{{createField.displayName}}" ng-disabled="true"></ui-field>
								<ui-field type="text" ng-if="createField.displayType == 'freeform text'" ng-model="createLocationFieldValues[createField.fieldName]" label="{{createField.displayName}}" ng-required="createField.required()" ng-disabled="false"></ui-field>
								<ui-field type="select" ng-if="createField.displayType == 'dropdown'" options="createField.options" prop="name" ng-model="createLocationFieldValues[createField.fieldName]" label="{{createField.displayName}}" ng-required="createField.required()" ng-disabled="createField.displayType == 'readonly' ? true : false" ></ui-field>	
								<ui-field type="text" ng-if="createField.displayType == 'textarea' " ng-model="createLocationFieldValues[createField.fieldName]" label="{{createField.displayName}}" ng-required="createField.required()" ng-disabled="false"></ui-field>	
							</div>
						</div>
						<!-- <div class="cols" ng-repeat="field in locationCreateFields">
							<ui-field type="text" ng-if="field.displayType == 'textarea' " ng-model="createLocationFieldValues[field.fieldName]" label="{{field.displayName}}" ng-required="createField.required()" ng-disabled="false"></ui-field>
						</div> -->
					</ui-section>
					<ui-header theme="section">
						<ui-header-body>
							<span class="ng-binding">Contacts</span>
							
							<!-- <ui-page-actions>
								<button ui-button class="bg-accent" ng-click="addContact()" >
									<span>Add Another Contact</span>
								</button>
							</ui-page-actions> -->
						</ui-header-body>
						<div ui-info="Add Another Contact" class="" ui-button ng-click="addContact()">
							<i class="fa fa-plus"></i>
							<span>Add Another Contact</span>
						</div>
					</ui-header>
					<div id="locationContacts" ng-repeat="index in contactsIndex">
						<div class="contactHeader">
							<span class="contactLabel">
								Contact {{index + 1}}
							</span>
							<span ng-click="deleteContact(index)" ng-if="index > 0" class="deleteContact">
								<i class="fa fa-times text-red"></i>
								<span>Delete Contact</span>
							</span>
						</div>
						<ui-section>
							<!-- <div class="cols">
								<ui-field type="text" ng-model="contacts[$index].firstName" class="firstName_{{$index}}" label="First Name" ></ui-field>
								<ui-field type="text" ng-model="contacts[$index].lastName" class="lastName_{{$index}}" label="Last Name" ></ui-field>
								<ui-field type="text" ng-model="contacts[$index].email" class="email_{{$index}}" label="Email" ></ui-field>
								<ui-field type="select" options="languageOptions" ng-model="contacts[$index].language" class="language_{{$index}}" label="Language" ></ui-field>
							</div>
							<div class="cols">
								<ui-field type="text" ng-model="contacts[$index].phone" class="phone_{{$index}}" label="Phone" ></ui-field>
								<ui-field type="text" ng-model="contacts[$index].altPhone" class="altPhone_{{$index}}" label="Alt Phone" ></ui-field>
								<ui-field type="text" ng-model="contacts[$index].fax" class="fax_{{$index}}" label="Fax" ></ui-field>
								<ui-field type="select" options="primaryOptions" ng-model="contacts[$index].primary" class="primary_{{$index}}" label="Primary" ></ui-field>
							</div> -->
							<div class="cols" ng-repeat="fourColumns in locationContactFieldColumns">
								<div  ng-repeat="contactField in fourColumns">				
									<ui-field type="text" ng-if="contactField.displayType == 'readonly'"  ng-model="locationContacts[index][contactField.fieldName]" label="{{contactField.displayName}}" ng-disabled="true"></ui-field>
									<ui-field type="text" ng-if="contactField.displayType == 'freeform text'" ng-model="locationContacts[index][contactField.fieldName]" label="{{contactField.displayName}}" ng-required="contactField.required()" ng-disabled="false"></ui-field>
									<ui-field type="select" ng-if="contactField.displayType == 'dropdown'" options="contactField.options" prop="name" ng-model="locationContacts[index][contactField.fieldName]" label="{{contactField.displayName}}" ng-required="contactField.required()" ng-disabled="contactField.displayType == 'readonly' ? true : false" ></ui-field>
									<ui-field type="select" ng-if="contactField.displayType == 'language_dropdown'" options="languageOptions" prop="name" ng-model="locationContacts[index][contactField.fieldName]" label="{{contactField.displayName}}" ng-required="contactField.required()" ng-disabled="contactField.displayType == 'readonly' ? true : false" ></ui-field>	
									<ui-field ng-if="contactField.displayType=='single checkbox'" type="toggle" class="no-label" ng-model="locationContacts[index][contactField.fieldName]" ui-info="{{contactField.hoverText}}" title="{{contactField.displayName}}" ng-required="contactField.required()" ng-disabled="contactField.disabled()"></ui-field>							
								</div>
							</div>
						</ui-section>
					</div>

	<!-- 					<ui-header theme="section">
						<ui-header-body>
							<span class="ng-binding">Allowances</span>
						</ui-header-body> -->
						<!-- <div ui-info="Add Allowance" class="" ui-button ng-click="addAllowance()">
							<i class="fa fa-plus"></i>
							<span>Add Allowance</span>
						</div> -->
	<!-- 					</ui-header>
					<div id="allowanceSection">
						<ui-section>
							<div class="cols">
								<ui-field type="multi" options="allowanceOptions" ng-model="location.allowance" label="Allowances" ></ui-field>
								<div>
								</div>
								<div>
								</div>
							</div>
						</ui-section>
					</div>
					<ui-header theme="section">
						<ui-header-body>
							<span class="ng-binding">Restrictions</span>
						</ui-header-body> -->
						<!-- <div ui-info="Add Restriction" class="" ui-button ng-click="addRestriction()">
							<i class="fa fa-plus"></i>
							<span>Add Restriction</span>
						</div> -->
	<!-- 					</ui-header>
					<div id="restrictionSection">
						<ui-section>
							<div class="cols">
								<ui-field type="multi" options="restrictionsOptions" ng-model="location.restrictions" label="Restrictions" ></ui-field>
								<div>
								</div>
								<div>
								</div>
							</div>
						<ui-section>
					</div> -->
			
			</ui-section>
			<ui-page-actions-spacer class="ng-scope"></ui-page-actions-spacer>
			<ui-page-actions-container>
				<ui-page-actions>
	<!-- 					<div class="group">
						<div ui-button ui-page-action class="text-accent bg-white" ui-info="Cancel" ng-click="cancelLocation()">
							<span>Cancel</span>
						</div>
					</div> -->
					<div class="group">
						<button type="submit" ui-button ui-page-action class="text-accent bg-white" ui-info="Submit Location Details" ng-click="submitLocation(locationDetailsForm.$valid, 'add')">
							<span>Submit</span>
							<i class="fa fa-chevron-right"></i>
						</button>
					</div>
				</ui-page-actions>
			</ui-page-actions-container>
		</ui-sidebar-content>	
		</form>	
	<ui-sidebar-nav-content>
</ui-sidebar-nav>

