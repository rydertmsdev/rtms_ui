App.service('dashboardSvc', ['$rootScope', function ($rootScope) {
	var svc=this;




	var values=[];
	var values1=[];
	for (var i=0;i<30;i++){
		var d = new Date();
		d.setDate(d.getDate() - i);
		var v=Math.floor(Math.random()*100)+50;
		values.push([d,v]);
		var v1=Math.floor(Math.random()*100)+100;
		values1.push([d,v1]);
	}


	svc.dashboard={
		panels:[
		{
			title:"Shipment Activity",
			type:"lineChart",
			data:[
			{
				"key" : "New Shipments" ,
				"values" : values,
				"color":"#7977de",
				"area":true
			},
			{
				"key" : "Deliveries" ,
				"values" : values1,
				"color":"#6fe083",
			}],
			w:3,
			h:1,
			x:0,
			y:0
		},
		{
			title:"Summary",
			type:"tiles",
			size: { x: 2, y: 2 }, position: [0, 2],
			data:[
			{
				y:251,
				title:"Delivered",
				color:"#6fe083",
			},
			{
				y:224,
				title:"Received",
				color:"#33ccff",
			},
			{
				y:549,
				title:"En-Route",
				color:"#ffaa33",
			}
			]
		},
		{
			title:"Picked Up",
			type:"ratio",
			color:"#6fe083",
			width:1,
			height:1,
			data: [
			{
				y: 1351,
				color:"#6fe083"
			},
			{
				y: 1497,
				color:"#e0e0e0",
			},
			],
			dataValue:'1,351',
			dataTotal:'2,848'
		},
		{
			title:"Delivered",
			type:"ratio",
			color:"#33ccff",
			width:1,
			height:1,
			data: [
			{
				y: 542,
				color:"#33ccff"
			},
			{
				y: 349,
				color:"#e0e0e0",
			},
			],
			dataValue:542,
			dataTotal:891
		},
		{
			title:"On-Time",
			type:"ratio",
			color:"#ffaa33",
			width:1,
			height:1,
			data: [
			{
				y: 99,
				color:"#ffaa33"
			},
			{
				y: 1,
				color:"#e0e0e0",
			},
			],
			dataValue:"99%",
				//dataTotal:891
			},
			{
				title:"Urgent",
				type:"ratio",
				color:"#ff6259",
				width:1,
				height:1,
				data: [
				{
					y: 35,
					color:"#ff6259"
				},
				{
					y: 65,
					color:"#e0e0e0",
				},
				],
				dataValue:"35%",
				//dataTotal:891
			},
			]
		}
	}]);






























