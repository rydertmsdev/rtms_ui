<ui-dashboard gridster="uiDashboardSvc.gridConfig">
	<ui-dashboard-panel 
	gridster-item 
	ng-repeat="(pIndex,panel) in dashboard.panels" 
	row="panel.x" 
	col="panel.y" 
	size-x="panel.w" 
	size-y="panel.h">
	<ui-section>
		<ui-header theme="section">
			<ui-header-body>
				<div>
					<span ng-bind-html="panel.title"></span>
				</div>
			</ui-header-body>
			<ui-header-controls>
				<div ui-button ui-header-actions-btn ng-click="editPanel(pIndex)" ui-info="Edit Panel">
					<i class="ic-dot-3"></i>
				</div>
			</ui-header-controls>
		</ui-header>
		<ui-dashboard-panel-content panel="panel"></ui-dashboard-panel-content>
	</ui-section>
</ui-dashboard-panel>
</ui-dashboard>
