<ui-sidebar-nav>
	<nav class="sidebar-nav">
		<a ng-click="addPanel()" ui-info="New Panel">
			<i class="fa fa-plus"></i>
		</a>
	</nav>
	<ui-sidebar-nav-content>
		<ui-view></ui-view>
	</ui-sidebar-nav-content>
</ui-sidebar-nav>

