App.controller('dashboardAddPanelModalCtrl',['$rootScope', '$scope', '$timeout', 'uiModalSvc', 'uiDashboardSvc', function($rootScope, $scope, $timeout, uiModalSvc, uiDashboardSvc){	
	$scope.uiDashboardSvc=uiDashboardSvc;
	
	$scope.tabs=[
	{
		active:!$scope.args.edit,
		name:"Type",
		template:"dashboard-add-panel-type.tpl"
	},
	{
		active:$scope.args.edit,
		name:"Configuration",
		template:"dashboard-add-panel-configure.tpl"
	},
	]

	var values=[];
	var values1=[];
	for (var i=0;i<30;i++){
		var d = new Date();
		d.setDate(d.getDate() - i);
		var v=Math.floor(Math.random()*100)+50;
		values.push([d,v]);
		var v1=Math.floor(Math.random()*100)+100;
		values1.push([d,v1]);
	}

	$scope.newPanelType="";
	$scope.setPanelType=function(type){
		if (type=="lineChart"){
			$scope.panel=$scope.panels[0];
		}
		if (type=="tiles"){
			$scope.panel=$scope.panels[1];
		}
		if (type=="ratio"){
			$scope.panel=$scope.panels[2];
		}
	}

	$scope.panels=[
	{
		title:"Line Chart",
		type:"lineChart",
		data:[
		{
			"key" : "New Shipments" ,
			"values" : values,
			"color":"#7977de",
			"area":true,
			source:"Shipments - New"
		},
		{
			"key" : "Deliveries" ,
			"values" : values1,
			"color":"#6fe083",
			"area":false,
			source:"Shipments - Delivered"
		}],
		w:3,
		h:1,
		x:0,
		y:0
	},
	{
		title:"Tiles",
		type:"tiles",
		size: { x: 2, y: 2 }, position: [0, 2],
		data:[
		{
			y:251,
			title:"Delivered",
			color:"#6fe083",
			source:"Shipments - Delivered"
		},
		{
			y:224,
			title:"Received",
			color:"#33ccff",
			source:"Shipments - Received"
		},
		{
			y:549,
			title:"En-Route",
			color:"#ffaa33",
			source:"Shipments - En-Route"
		}
		]
	},
	{
		title:"Round",
		type:"ratio",
		color:"#6fe083",
		width:1,
		height:1,
		data: [
		{
			y: 1351,
			color:"#6fe083",
			source:"Shipments - Picked Up"
		},
		{
			y: 1497,
			color:"#e0e0e0",
			source:"Shipments - Total"
		},
		],
		dataValue:'1,351',
		dataTotal:'2,848'
	},
	]

	$scope.dataSources=[
	"Shipments - New",
	"Shipments - Delivered",
	"Shipments - Picked Up",
	"Shipments - Received",
	"Shipments - En-Route",
	"Shipments - Total",
	"Northrop Grumman - On-Dock",
	"Northrop Grumman - Delayed",
	"Northrop Grumman - Exception"
	]


	$scope.getTitleProp=function(){
		if ($scope.panel.type=='lineChart') return "key";
		if ($scope.panel.type=='tiles') return "title";
		if ($scope.panel.type=='ratio') return "title";
	}

	$scope.removeSource=function(index){
		$scope.panel.data.splice(index,1);
	}

	$scope.close=function(){
		uiModalSvc.hide();
		$scope.args.callback();
	}

	$scope.$watch('panel',function(newval){
		if (!newval) return;
		$scope.panelPreview=angular.copy(newval);
		if ($scope.panel.type!='ratio'){
			$scope.panelPreview.data&&$scope.panelPreview.data.pop();
		}
		angular.forEach($scope.panelPreview.data,function(val,ind){
			if (!val.values){
				val.values=[];
				if (!val.y) val.y=Math.floor(Math.random()*100)+50;
				if (!val.color) val.color="#999";
				for (var i=0;i<30;i++){
					var d = new Date();
					d.setDate(d.getDate() - i);
					var v=Math.floor(Math.random()*100)+50;
					val.values.push([d,v]);
				}
			}
		})
	},true);

	$scope.equals=angular.equals;

	if ($scope.args.edit){
		$scope.panel=angular.copy($scope.args.panel);
		$timeout(function(){
			$scope.previewPreload=true;
		},1000);
	}

	$scope.deletePanel=function(){
		uiModalSvc.hide();
		$scope.args.callback({delete:true});
	}

	$scope.submit=function(){
		if ($scope.tabs[0].active){
			$scope.setActiveTab(1);
			$timeout(function(){
				$scope.previewPreload=true;
			},1);
		}
		else{
			uiModalSvc.hide();
			$scope.args.callback($scope.panelPreview);
		}
	}

}]);












