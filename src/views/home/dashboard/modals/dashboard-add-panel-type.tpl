<div class="modal-body form overflow-y">
	<ui-section-container>
		<div class="cols">
			<ui-section ng-repeat="panel in panels" ng-class="{'selected':panel.type==$parent.panel.type}">
				<ui-header theme="section">
					<ui-header-body>
						<div>
							<span ng-bind-html="panel.title"></span>
						</div>
					</ui-header-body>
				</ui-header>
			<ui-dashboard-panel-content panel="panel" class="square"></ui-dashboard-panel-content>				
				<ui-section-overlay ng-click="setPanelType(panel.type)"></ui-section-overlay>
			</ui-section>
		</div>
	</ui-section-container>
</div>