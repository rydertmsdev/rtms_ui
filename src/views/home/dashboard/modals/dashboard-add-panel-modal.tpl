<div class="modal-full" ng-controller="dashboardAddPanelModalCtrl">
	<ui-header theme="ui-modal-header">
		<ui-header-body>
			<div>
				<span>
					Add Panel
				</span>
			</div>
		</ui-header-body>
		<ui-header-controls>
			<div ui-button ng-click="close()">
				<i class="fa fa-times"></i>
			</div>
		</ui-header-controls>
	</ui-header>
	<ui-tabs tabs="tabs" class="hidden"></ui-tabs>
	<ui-header theme="ui-modal-footer">
		<ui-header-body ng-if="args.edit">
			<div ui-button ng-click="deletePanel()" class="accent">
				<span>Delete Panel</span>
			</div>
		</ui-header-body>
		<ui-header-controls>
			<div ui-button ng-click="close()" class="bg-gray">
				<span>Cancel</span>
			</div>
			<div ui-button type="submit" ng-click="submit()" class="bg-accent" ng-disabled="!panel.type">
				<span ng-if="tabs[0].active">Next</span>
				<span ng-if="tabs[1].active&&!args.edit">Add</span>
				<span ng-if="args.edit">Save</span>
			</div>
		</ui-header-controls>
	</ui-header>
</div>