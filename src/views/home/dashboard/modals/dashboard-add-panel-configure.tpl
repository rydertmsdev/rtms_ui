<div class="modal-body overflow-y">
	<div class="cols">
		<div class="form rows" style="overflow: hidden;z-index:0">
			<ui-header theme="subsection">
				<ui-header-body>
					<div>
						<span>Settings</span>
					</div>
				</ui-header-body>
			</ui-header>
			<div class="cols">
				<ui-field type="text" label="Title" ng-disabled="false" ng-model="panel.title"></ui-field>	
			</div>
			<ui-header theme="subsection" style="border-bottom: none">
				<ui-header-body>
					<div>
						<span>
							Data
						</span>
					</div>
				</ui-header-body>
			</ui-header>
			<ui-list class="static">			
				<ui-list-header>
					<li ui-width="200px" ng-hide="panel.type=='ratio'">
						<span>Title</span>
					</li>
					<li>
						<span>Source</span>
					</li>

					<li class="text-center" ui-width="54px">Color</li>

					<li class="text-center" ui-width="54px" ng-if="panel.type!='ratio'" >Remove</li>
				</ui-list-header>
				<ui-list-body>
					<li ng-repeat="(iIndex,item) in panel.data | listPlaceholder:panel.type!='ratio'">
						<ui-list-content>
							<cell ui-width="200px"  ng-hide="panel.type=='ratio'">
								<label>Title</label>
								<ui-field type="text" ng-model="item[getTitleProp()]" class="inline"></ui-field>
							</cell>

							<cell>
								<label>Source</label>
								<ui-field type="select" options="dataSources" placeholder="Select Data Source" ng-model="item.source" class="inline"></ui-field>
							</cell>
							<cell ui-width="54px">
								<label>Color</label>
								<div ng-style="{'color':item.color}">
									<button ui-info="Change Color" class="field-color" ui-color ng-model="item.color" ng-disabled="panel.type=='ratio'&&iIndex==panel.data.length-1"></button>
								</div>
							</cell>
						</ui-list-content>
						<div ui-button ng-click="removeSource($index)" ng-disabled="equals(item,{})" ui-width="54px" ng-if="panel.type!='ratio'" ui-info="Remove Source">
							<i class="fa fa-times text-red" style="font-size: 18px"></i>
						</div>
					</li>
				</ui-list-body>
			</ui-list>
		</div>
		<div class="rows text-center">
			<ui-header theme="subsection">
				<ui-header-body>
					<div>
						<span>Preview</span>
					</div>
				</ui-header-body>
			</ui-header>
			<div class="dashboard-panel-preview" ng-if="previewPreload">
				<ui-section>
					<ui-header theme="section">
						<ui-header-body>
							<div>
								<span ng-bind-html="panel.title"></span>
							</div>
						</ui-header-body>
					</ui-header>
					<ui-dashboard-panel-content panel="panelPreview" class="square">
					</ui-dashboard-panel-content>	
					<ui-section-overlay></ui-section-overlay>
				</ui-section>
			</div>
		</div>
	</div>
</div>