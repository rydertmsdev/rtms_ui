App.controller('dashboardCtrl',['$scope', '$state', 'uiModalSvc', 'uiLoaderSvc','uiDashboardSvc', 'dashboardSvc', function($scope, $state, uiModalSvc, uiLoaderSvc, uiDashboardSvc, dashboardSvc){
	$scope.$state=$state;
	$scope.uiDashboardSvc=uiDashboardSvc;
	$scope.uiSidebarSvc.showSidebar=false;
	$scope.addPanel=function(){
		uiModalSvc.show('dashboard-add-panel-modal.tpl',{
			callback:function(newPanel){
				if (!newPanel) return;
				dashboardSvc.dashboard.panels.push(angular.copy(newPanel))
			}
		});
	}
	$scope.editPanel=function(pIndex){
		uiModalSvc.show('dashboard-add-panel-modal.tpl',{
			edit:true,
			panel:$scope.dashboard.panels[pIndex],
			callback:function(newPanel){
				if (!newPanel) return;
				if (newPanel.delete){
					$scope.dashboard.panels.splice(pIndex,1);
				}
				else{
					$scope.dashboard.panels[pIndex]=angular.copy(newPanel);
				}
			}
		});
	}

	uiLoaderSvc.preload(function(){
		$scope.dashboard=dashboardSvc.dashboard;
		$scope.uiDashboardSvc=uiDashboardSvc;
		return true;
	});


}]);

