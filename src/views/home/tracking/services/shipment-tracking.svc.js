App.service('shipmentTrackingSvc', [
  '$rootScope',
  '$window', 
  '$q', 
  '$timeout', 
  '$filter', 
  '$http',
  'uiLoaderSvc', 
  'uiModalSvc', 
  'testData', 
  function ($rootScope, $window, $q, $timeout, $filter, $http, uiLoaderSvc, uiModalSvc, testData) {
    var svc=this;

    svc.searchResults=testData.shipments;

  return svc;
}]);


/*

  {
    clientCode: "MTL",
    client: "Mattel",
    loadNumber: "31125032",
    shipmentNumber: "SH-036304479",
    proNumber: "",
    bolNumber: "",
    airWaybillNumber: "654471555",
    trailerNumber: "636422",
    statusCode: 7,
    status: "Tendered",
    isUrgent: false,
    isOnHold: false,
    isHazmat: false,
    carrierScac: "FXFE",
    carrierName: "FedEx Express",
    baseMode: "AIR",
    vesselName: "",
    transportModeCode: "1AR",
    transportMode: "Overnight - Early AM",
    equipmentCode: "",
    equipment: "",
    freightTermsCode: "FT_PRE_PAID",
    freightTerms: "Pre Paid",
    division: "MTL1",
    logisticsGroup: "MTL1",
    transactionSource: "GIS",
    memo: "",
    origin: {
      originId: "P30",
      originName: "San Bernardino Distribution Center",
      originCity: "San Bernardino",
      originSau: "CA",
      originCountry: "USA",
      originPostalCode: "92408"
    },
    destination: {
      destinationId: "S2425090",
      destinationName: "Amazon.com CMH2",
      destinationCity: "Groveport",
      destinationSau: "OH",
      destinationCountry: "USA",
      destinationPostalCode: "43125"
    },
    dates: {
      shipmentCreated: "03/01/2017 06:04 AM",
      scheduledPickupFrom: "03/01/2017 07:00 AM",
      scheduledPickupTo: "03/03/2017 11:00 AM",
      scheduledDeliverFrom: "03/07/2017 08:00 AM",
      scheduledDeliverTo: "03/07/2017 04:00 PM",
      pickupAppointment: "03/03/2017 08:00 AM",
      deliveryAppointment: "03/07/2017 01:00 PM",
      actualPickupArrival: "",
      actualPickupDeparture: "",
      actualDeliveryArrival: "",
      actualDeliveryDeparture: ""
    },
    measures: {
      nominalHeight: 2.5,
      nominalWidth: 2,
      nominalLength: 3,
      nominalDimensionalUom: "IN",
      nominalVolume: 6.16,
      nominalVolumeUom: "CUFT",
      nominalWeight: 34.6,
      nominalWeightUom: "LB",
      totalPieces: 10,
      totalSkids: 1,
      palletPositions: 1
    },
    references: [
    {
      qualifier: "PO",
      name: "Purchase Order Number",
      value: "1GNNTSQL"
    },
    {
      qualifier: "CCLS",
      name: "Consolidation Class",
      value: "AMAZON"
    },
    {
      qualifier: "CO",
      name: "Corporate ID",
      value: "852737"
    }
    ],
    journey: {
      100: {
        baseMode: "AIR",
        distance: "415",
        distanceUom: "MILE",
        percentageOfTotal: "100%",
        exceptions: {}
      }
    }
  },*/


































