<ui-sidebar-nav>
	<nav class="sidebar-nav">
		<ui-sidebar-btn></ui-sidebar-btn>
		<a ui-sref="home.tracking.search" ui-info="Shipment Search" ui-sref-active="active">
			<i class="fa fa-search"></i>
		</a>
		<a ui-sref="home.tracking.export" ui-info="Export" ui-sref-active="active">
			<i class="fa fa-download"></i>
		</a>
	</nav>
	<ui-sidebar-nav-content>
		<ui-view></ui-view>
	</ui-sidebar-nav-content>
</ui-sidebar-nav>
