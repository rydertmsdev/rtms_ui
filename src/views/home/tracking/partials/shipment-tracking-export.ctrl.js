App.controller('shipmentTrackingExportCtrl',['$scope', '$timeout', 'uiLoaderSvc', '$state', 'shipmentTrackingSvc', function($scope, $timeout, uiLoaderSvc, $state, shipmentTrackingSvc){

	$scope.tableConfig={
		selectable:false,
		rowClick:function(row){
			row.$$selected=!row.$$selected;
		}
	}

	$scope.tableData=[];

	angular.forEach(shipmentTrackingSvc.searchResults,function(shipment,index){
		$scope.tableData.push({
			shipmentNumber:shipment.shipmentNumber,
			loadNumber:shipment.loadNumber,
			origin:shipment.origin.originName,
			destination:shipment.destination.destinationName,
			status:shipment.status,
		})
	})

	$scope.tableColumns=[
	{
		title:"Shipment Number",
		key:"shipmentNumber",
		type:"text",
	},
	{
		title:"Load Number",
		key:"loadNumber",
		type:"text",
	},
	{
		title:"Origin",
		key:"origin",
		type:"text",
	},
	{
		title:"Destination",
		key:"destination",
		type:"text",
	},
	{
		title:"Status",
		key:"status",
		type:"text",
	},
	];
}]);
