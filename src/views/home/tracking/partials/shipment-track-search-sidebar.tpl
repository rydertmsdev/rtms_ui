<div class="form form-dark">
	<div class="rows">
		<div class="section-subheader">
			<div class="icon"><i class="fa fa-map-marker"></i></div>
			<div class="text"><div class="title">Origin</div></div>
		</div>

		<div class="field"><input placeholder=""/><label>Address ID:</label><span></span></div>
		<div class="field"><input placeholder=""/><label>Name</label><span></span></div>
		<div class="field"><input placeholder=""/><label>City</label><span></span></div>
		<div class="field"><input placeholder=""/><label>State/Province</label><span></span></div>
		<div class="field"><input placeholder=""/><label>Country</label><span></span></div>
		<div class="field"><input placeholder=""/><label>Postal Code</label><span></span></div>
		<div class="section-subheader">
			<div class="icon"><i class="fa fa-map-pin"></i></div>
			<div class="text"><div class="title">Destination</div></div>
		</div>		
		<div class="section-subheader">
			<div class="icon"><i class="fa fa-calendar"></i></div>
			<div class="text"><div class="title">Ship Date</div></div>
		</div>		
		<div class="section-subheader">
			<div class="icon"><i class="fa fa-calendar"></i></div>
			<div class="text"><div class="title">Delivery Date</div></div>
		</div>		
		<div class="section-subheader">
			<div class="icon"><i class="fa fa-cube"></i></div>
			<div class="text"><div class="title">Shipment</div></div>
		</div>
		<div class="field"><input placeholder=""/><label>Number</label><span></span></div>
		<div class="field"><input placeholder=""/><label>Status</label><span></span></div>
		<div class="field"><input placeholder=""/><label>Identification Number (UN/NA)</label><span></span></div>
				
		<div class="section-subheader">
			<div class="icon"><i class="fa fa-minus"></i></div>
			<div class="text"><div class="title">Load</div></div>
		</div>		
		<div class="section-subheader">
			<div class="icon"><i class="fa fa-stop-circle"></i></div>
			<div class="text"><div class="title">Stop</div></div>
		</div>	
		<div class="section-subheader">
			<div class="icon"><i class="fa fa-pause-circle"></i></div>
			<div class="text"><div class="title">Legs</div></div>
		</div>
	</div>
</div>
