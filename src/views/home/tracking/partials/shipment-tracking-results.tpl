<ui-sidebar>
	<ui-sidebar-panel>
	<ui-header theme="ui-sidebar-header">
		<ui-header-body>
			<div>
				<span>
					Search
				</span>
			</div>
		</ui-header-body>
		<ui-header-controls>
			<div ui-button title="Clear" ng-click="clearFilters()">
				<i class="fa fa-times text-red"></i>
			</div>
			<div ui-button title="Apply" ng-click="doSearch()">
				<i class="fa fa-check text-green"></i>
			</div>
		</ui-header-controls>
	</ui-header>
	<div class="overflow-y">
		<ui-menu-group>
			<ui-header theme="ui-menu-header" ui-button ui-menu-collapse>
				<ui-header-body>
					<span>
						General
					</span>
				</ui-header-body>
				<ui-header-controls>
					<div ui-menu-collapse-indicator></div>
				</ui-header-controls>
			</ui-header>
			<ui-menu-content ui-max-height>
				<ui-field type="text" ng-model="filters.shipment.shipmentNumber" label="Shipment Number" placeholder="---"></ui-field>
				<ui-field type="text" ng-model="filters.shipment.poNumber" label="Purchase Order Number" placeholder="---"></ui-field>
				<ui-field type="text" ng-model="filters.shipment.salesOrderNumber" label="Sales Order #" placeholder="---"></ui-field>
				<ui-field type="text" ng-model="filters.shipment.customerPoNumber" label="Customer PO #" placeholder="---"></ui-field>
				<ui-field type="text" ng-model="filters.shipment.pickupNumber" label="Pickup #" placeholder="---"></ui-field>
				<ui-field type="text" ng-model="filters.shipment.idNumber" label="Identification Number" placeholder="---"></ui-field>
				<ui-field type="text" ng-model="filters.shipment.trailerNumber" label="Trailer Number" placeholder="---"></ui-field>
				<ui-field type="text" ng-model="filters.shipment.ryderOrderNumber" label="Ryder Order Number" placeholder="---"></ui-field>
				<ui-field type="text" ng-model="filters.shipment.status" label="Status" placeholder="---"></ui-field>
			</ui-menu-content>
		</ui-menu-group>
		<ui-menu-group collapsed>
			<ui-header theme="ui-menu-header" ui-button ui-menu-collapse>
				<ui-header-body>
					<div>
						Origin
					</div>
				</ui-header-body>
				<ui-header-controls>
					<div ui-menu-collapse-indicator></div>
				</ui-header-controls>
			</ui-header>
			<ui-menu-content ui-max-height>
				<ui-field type="text" ng-model="filters.origin.id" label="Origin ID" placeholder="---"></ui-field>
				<ui-field type="text" ng-model="filters.origin.address" label="Origin Address" placeholder="---"></ui-field>
				<ui-field type="text" ng-model="filters.origin.city" label="Origin City" placeholder="---"></ui-field>
				<ui-field type="text" ng-model="filters.origin.sau" label="Origin State/Province" placeholder="---"></ui-field>
				<ui-field type="text" ng-model="filters.origin.postalCode" label="Origin Postal Code" placeholder="---"></ui-field>
				<ui-field type="text" ng-model="filters.origin.country" label="Origin Country" placeholder="---"></ui-field>
			</ui-menu-content>
		</ui-menu-group>
		<ui-menu-group collapsed>
			<ui-header theme="ui-menu-header" ui-button ui-menu-collapse>
				<ui-header-body>
					<div>
						Destination
					</div>
				</ui-header-body>
				<ui-header-controls>
					<div ui-menu-collapse-indicator></div>
				</ui-header-controls>
			</ui-header>
			<ui-menu-content ui-max-height>
				<ui-field type="text" ng-model="filters.destination.id" label="Destination ID" placeholder="---"></ui-field>
				<ui-field type="text" ng-model="filters.destination.address" label="Destination Address" placeholder="---"></ui-field>
				<ui-field type="text" ng-model="filters.destination.city" label="Destination City" placeholder="---"></ui-field>
				<ui-field type="text" ng-model="filters.destination.sau" label="Destination State/Province" placeholder="---"></ui-field>
				<ui-field type="text" ng-model="filters.destination.postalCode" label="Destination Postal Code" placeholder="---"></ui-field>
				<ui-field type="text" ng-model="filters.destination.country" label="Destination Country" placeholder="---"></ui-field>
			</ui-menu-content>
		</ui-menu-group>
		<ui-menu-group collapsed>
			<ui-header theme="ui-menu-header" ui-button ui-menu-collapse>
				<ui-header-body>
					<div>
						Create Date
					</div>
				</ui-header-body>
				<ui-header-controls>
					<div ui-menu-collapse-indicator></div>
				</ui-header-controls>
			</ui-header>
			<ui-menu-content ui-max-height>
				<ui-field type="date" ng-model="filters.shipment.createDateFrom" label="Create Date - From" placeholder="---"></ui-field>
				<ui-field type="date" ng-model="filters.shipment.createDateTo" label="Create Date - To" placeholder="---"></ui-field>
			</ui-menu-content>
		</ui-menu-group>
		<ui-menu-group collapsed>
			<ui-header theme="ui-menu-header" ui-button ui-menu-collapse>
				<ui-header-body>
					<div>
						Ship Date
					</div>
				</ui-header-body>
				<ui-header-controls>
					<div ui-menu-collapse-indicator></div>
				</ui-header-controls>
			</ui-header>
			<ui-menu-content ui-max-height>
				<ui-field type="select" ng-model="filters.shipDate.type" label="Date Type" placeholder="---"></ui-field>
				<ui-field type="date" ng-model="filters.shipDate.from" label="From" placeholder="---"></ui-field>
				<ui-field type="date" ng-model="filters.shipDate.to" label="To" placeholder="---"></ui-field>
			</ui-menu-content>
		</ui-menu-group>
		<ui-menu-group collapsed>
			<ui-header theme="ui-menu-header" ui-button ui-menu-collapse>
				<ui-header-body>
					<div>
						Delivery Date
					</div>
				</ui-header-body>
				<ui-header-controls>
					<div ui-menu-collapse-indicator></div>
				</ui-header-controls>
			</ui-header>
			<ui-menu-content ui-max-height>
				<ui-field type="select" ng-model="filters.deliveryDate.type" label="Date Type" placeholder="---"></ui-field>
				<ui-field type="date" ng-model="filters.deliveryDate.from" label="From" placeholder="---"></ui-field>
				<ui-field type="date" ng-model="filters.deliveryDate.to" label="To" placeholder="---"></ui-field>
			</ui-menu-content>
		</ui-menu-group>
	</div>
	</ui-sidebar-panel>
	<ui-sidebar-content>
		<ui-alert-container></ui-alert-container>
		<ui-filters>
			<ui-filter>
				<div>
					<label>Sector</label>
					<span> MS - Mission Systems</span>
				</div>
				<div ui-button ui-filter-remove-btn>
					<i class="fa fa-times"></i>
				</div>
			</ui-filter>
			<ui-filter>
				<div>
					<label>Destination</label>
					<span>Northrop Grumman</span>
				</div>
				<div ui-button ui-filter-remove-btn>
					<i class="fa fa-times"></i>
				</div>
			</ui-filter>
		</ui-filters>
		<ui-section ng-repeat="(shipmentIndex,shipment) in svc.searchResults" ng-click="expandShipment(shipmentIndex)" class="rows">
			<div class="cols cols-2-3 ui-list-padded">
				<div class="rows">
					<div class="ui-list-header">
						<div class="ui-list-title">
							{{shipment.shipmentNumber}}
							<i class="ic ic-fireball text-orange" ng-if="shipment.isUrgent" ui-info="Expedited"></i>
						</div>
						<div class="ui-list-subtitle">
							<label>{{shipment.status}}</label>
						</div>
					</div>
					<div class="spacer">

					</div>
					<div class="ui-list-detail">
						<label>SECTOR</label>
						<div>MS - Mission Systems</div>
					</div>
					<div class="ui-list-detail">
						<label>Load number</label>
						<div>{{shipment.loadNumber}}</div>
					</div>
					<div class="ui-list-detail">
						<label>Type</label>
						<div>Air</div>
					</div>
				</div>
				<div class="rows">
					<div class="cols">
						<div class="rows">
							<label>Origin</label>
							<span>{{shipment.origin.originName}}</span>
							<span class="subtitle">{{shipment.origin.originCity}}, {{shipment.origin.originSau}} {{shipment.origin.originPostalCode}}</span>
						</div>
						<div class="spacer">

						</div>
						<div class="rows text-right">
							<label>Destination</label>
							<span>{{shipment.destination.destinationName}}</span>
							<span class="subtitle">{{shipment.destination.destinationCity}}, {{shipment.destination.destinationSau}} {{shipment.destination.originPostalCode}}</span>
						</div>
					</div>
					<div>
						<timeline class="horizontal time-scale">
							<timeline-event ng-repeat="event in shipment.events track by $index" ng-style="{'left':event.progress*100+'%'}" ng-class="{'minor':event.minor}">
								<timeline-badge class="{{::event.badgeClass}}" ng-click="showShipmentDetails(shipment,$event,2)" 
								ui-info ui-info-template="event.template" ui-info-data="{'event':event}">
								<i class="glyphicon {{::event.badgeIconClass}}"></i>
							</timeline-badge>
						</timeline-event>
						<timeline-progress ng-style="{'width':shipment.progress*100+'%'}">
						</timeline-progress>
					</timeline>
				</div>
				<div class="cols">
					<div class="rows">
						<label>Picked Up</label>
						<span>{{shipment.dates.shipmentCreated}}</span>
					</div>
					<div class="spacer">

					</div>
					<div class="rows text-right">
						<label>Expected Delivery</label>
						<span>{{shipment.dates.deliveryAppointment}}</span>
					</div>
				</div>
			</div>
		</div>
		<div class="subform" ui-max-height ng-class="{'min':$index!=listDetailIndex}">
			<div class="cols cols-2-3 ui-list-padded">
				<div class="rows">
					<div class="ui-list-detail">
						<label>Width</label>
						<div>4 ft.</div>
					</div>
					<div class="ui-list-detail">
						<label>Height</label>
						<div>2 ft.</div>
					</div>
					<div class="ui-list-detail">
						<label>Length</label>
						<div>6 ft.</div>
					</div>
					<div class="ui-list-detail">
						<label>Weight</label>
						<div>65 lbs.</div>
					</div>
				</div>
				<div class="rows">
					<div class="cols">
						<div class="rows">
							<label>Pick Up Scheduled</label>
							<span>{{shipment.dates.shipmentCreated}}</span>
						</div>
						<div class="spacer">

						</div>
						<div class="rows text-right">
							<label>Delivery Scheduled</label>
							<span>{{shipment.dates.deliveryAppointment}}</span>
						</div>
					</div>
					<div class="spacer">

					</div>
					<div class="cols">
						<div class="rows">
							<label>Pick Up Approved</label>
							<span>{{shipment.dates.shipmentCreated}}</span>
						</div>
						<div class="spacer">

						</div>
						<div class="rows text-right">
							<label>Delivery Approved</label>
							<span>{{shipment.dates.deliveryAppointment}}</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<ui-header theme="ui-list-item-header">
			<ui-header-controls>
				<div ui-button ui-info="Follow Shipment" ng-click="">
					<i class="fa fa-arrow-right"></i>
					<span>Follow</span>
				</div>
				<div ui-button ui-info="Shipment Details" ng-click="showShipmentDetails(shipment,$event)">
					<i class="fa fa-info-circle"></i>
					<span>Details</span>
				</div>
			</ui-header-controls>
		</ui-header>
	</ui-section>
	<ui-page-actions-spacer></ui-page-actions-spacer>
	<ui-page-actions-container>
		<ui-page-actions>
			<div class="group">
				<div ui-button ui-page-action class="bg-lgray" ui-info="Previous Page">
					<i class="fa fa-chevron-left"></i>
				</div>
				<div class="text">
					1 of 2
				</div>
				<div ui-button ui-page-action class="bg-accent"  ui-info="Next Page">
					<i class="fa fa-chevron-right"></i>
				</div>
			</div>
		</ui-page-actions>
	</ui-page-actions-container>
</ui-sidebar-content>
</ui-sidebar>










