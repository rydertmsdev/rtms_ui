<ui-sidebar>
	<ui-sidebar-panel>

		<div class="rows form">
			<ui-menu-content>
				<ui-menu-group>
					<ui-header theme="ui-menu-header" >
						<ui-header-body>
							<span>View By</span>
						</ui-header-body>
					</ui-header>
					<ui-menu-content ui-max-height>
						<ui-menu-item ng-click="openViewBy()" class="active">
							<span>
								Standard View
							</span>
						</ui-menu-item>
						<ui-menu-item ng-click="openViewBy()">
							<span>
								Expanded View
							</span>
						</ui-menu-item>
						<ui-menu-item ng-click="openViewBy()">
							<span>
								Customer View
							</span>
						</ui-menu-item>
					</ui-menu-content>
				</ui-menu-group>
			</ui-menu-content>
		</div>
	</div>
</ui-sidebar-panel>
<ui-sidebar-content>
	<ui-section>
		<div ui-table="tableConfig" ui-table-columns="tableColumns" ui-table-data="tableData" ui-table-actions="tableActions">
		</div>
	</ui-section>
	<ui-page-actions-spacer></ui-page-actions-spacer>
	<ui-page-actions-container>
		<ui-page-actions>
			<div class="group">
				<div ui-button ui-page-action class="bg-eBlue" ui-info="Download">
					<i class="fa fa-download"></i>
				</div>
			</div>
			<div class="group">
				<div ui-button ui-page-action class="bg-accent" ui-info="Print">
					<i class="fa fa-print"></i>
				</div>
			</div>
		</ui-page-actions>
	</ui-page-actions-container>
</ui-sidebar-content>
</ui-sidebar>











