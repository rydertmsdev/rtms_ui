<div class="section-header">
	<div class="heading">
		<div class="button" ng-class="{'active':$parent.showFilters}" ng-click="$parent.showFilters=!$parent.showFilters">
			<i class="fa fa-filter"></i>
			<span>Filters</span>
		</div>
	</div>
	<div class="controls">
		<span class="page-count">50 Results</span>
		<div><i class="fa fa-chevron-left"></i></div>
		<span class="page-index">1 / 2</span>
		<div><i class="fa fa-chevron-right"></i></div>
		<div class="min-max">
			<i class="fa fa-sort-amount-asc"></i>
			<ul>
				<li ng-repeat="s in sortOptions">
					<i class="fa fa-{{$index==0?'long-arrow-down':''}}"></i>
					<span>{{s}}</span>
				</li>
			</ul>
		</div>
	</div>
</div>	
<div class="filter-bar">
	<div ng-repeat="(filterKey,filter) in filters" ng-if="filter.value||filter.values">
		<span class="filter-title">{{filter.name}}</span>
		<span class="filter-value" ng-if="filter.value">{{filter.value.name||filter.value}}</span>
		<span class="filter-value" ng-repeat="v in filter.values" ng-if="filter.values">
			<span>{{v}}</span>
			<i ng-if="$index<filter.values.length-1"></i>
		</span>
		<i class="fa fa-times" ng-click="filter.value='';filter.values=''"></i>
	</div>
</div>
<ul class="list overflow-y">
	<li ng-repeat="result in shipments | limitTo:50 " >
		<div class="form summary" style="width:100%">
			<div class="cols"  style="width:100%">
				<div class="rows"  style="box-shadow: none !important">
					<div class="field">
						<input type="text" ng-model="result.shipmentNumber" placeholder="---" ng-disabled="true">
						<label for="">Shipment Number</label>
						<span></span>
					</div>
					<div class="field">
						<input type="text" ng-model="result.loadNumber" ng-disabled="true">
						<label for="">Load Number</label>
						<span></span>
					</div>
				</div>
				<div class="rows" style="box-shadow: none !important">
					<div class="field">
						<input type="text" ng-model="result.origin.originName" ng-disabled="true">
						<label for="">Origin</label>
						<span></span>
					</div>
					<div class="field">
						<input type="text" ng-value="toDate(result.dates.shipmentCreated) | date:'short'" ng-disabled="true">
						<label for="">Ship Date</label>
						<span></span>
					</div>
				</div>
				<div class="rows w30" style="box-shadow: none !important"> 
					<div class="ship-status">
						<div class="icon icon-big ltgray">
							<i class="fa fa-map-marker"></i>
						</div>
						<div class="line bg-green"></div>
						<div class="icon text-gray" ng-hide="result.Status=='Delivered'">
							<i class="fa fa-{{getTransportIcon(result.transportMode)}}"></i>
						</div>
						<div class="line bg-lgray"></div>
						<div class="icon icon-big ltgray">
							<i class="fa fa-map-pin"></i>
						</div>
					</div>
					<div class="ship-mode">
						<span>{{result.transportMode}}</span>
					</div>
					<div class="ship-badge">
						<span class="bg-green" ng-if="result.statusCode==7">On-time</span>
						<span class="bg-orange" ng-if="result.statusCode==8">Delayed</span>
					</div>
				</div>
				<div class="rows" style="box-shadow: none !important">
					<div class="field">
						<input type="text" ng-model="result.destination.destinationName" ng-disabled="true">
						<label for="">Destination Name</label>
						<span></span>
					</div>
					<div class="field">
						<input type="text" ng-value="toDate(result.dates.deliveryAppointment) | date:'short'" ng-disabled="true">
						<label for="">Delivery Date</label>
						<span></span>
					</div>
				</div>
				<div class="list-actions">
					<div class="list-round-icon" ng-click="$parent.showShipmentDetail(result);">
						<i class="fa fa-info"></i>
					</div>
				</div>
			</div>
		</div>
		<div class="list-right-corner-badge" ng-if="result.isUrgent">
			<div class="badge">
				<i class="ic ic-fireball"></i>
			</div>
		</div>
	</li>
</ul>




