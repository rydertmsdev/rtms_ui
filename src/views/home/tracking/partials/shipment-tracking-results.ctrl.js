App.controller('shipmentTrackingResultsCtrl',['$scope', '$timeout', 'shipmentTrackingSvc', function($scope, $timeout, shipmentTrackingSvc){
	$scope.uiAlertSvc.clear();
	$scope.uiAlertSvc.show({
		icon:"fa fa-info",
		label:"Service Availability",
		text:"Order Fulfillment availability will be limited between 1am and 5am due to scheduled maintenance",
		//static:true
	});


	$scope.uiLoaderSvc.preload(function(){
		return $scope.svc=shipmentTrackingSvc;
	})

	$scope.showShipmentDetails=function(shipment,$event,tab){
		$event&&$event.stopPropagation();
		$scope.uiModalSvc.show('shipment-tracking-details-modal.tpl',{
			shipment:shipment,
			tab:tab
		});
	}

	$scope.expandShipment=function($index){
		$scope.listDetailIndex=($scope.listDetailIndex==$index)?-1:$index;
	}

}]);
