<div class="section-header">
	<div class="heading">
		<div>
			<i class="fa fa-cube"></i>
			<span>Shipment Details: {{$parent.shipmentDetail.shipmentNumber}}</span>
		</div>
	</div>
	<div class="controls">
		<div class="min-max" ng-click="$parent.closeShipmentDetail()"><i class="fa fa-times"></i></div>
	</div>
</div>	
<div class="overflow-y">
	<div class="form summary">
		<div class="rows">
			<div class="cols">
				<div class="field">
					<input disabled type="text" ng-model="$parent.shipmentDetail.shipmentNumber"/>
					<label>Shipment Number</label>
					<span></span>
				</div>
				<div class="field">
					<input disabled type="text" ng-model="$parent.shipmentDetail.loadNumber"/>
					<label>Load Number</label>
					<span></span>
				</div>
				<div class="field">
					<input disabled type="text" ng-model="$parent.shipmentDetail.clientCode"/>
					<label>Client Code</label>
					<span></span>
				</div>
				<div class="field">
					<input disabled type="text" ng-model="$parent.shipmentDetail.client"/>
					<label>Client Name</label>
					<span></span>
				</div>
			</div>
			
			<div class="cols">
				<div class="field">
					<input disabled type="text" ng-model="$parent.shipmentDetail.status"/>
					<label>Status</label>
					<span></span>
				</div>
				<div class="field">
					<input disabled type="text" ng-model="$parent.shipmentDetail.trailerNumber"/>
					<label>Trailer Number</label>
					<span></span>
				</div>
				<div class="field">
					<input disabled type="text" ng-model="$parent.shipmentDetail.transportModeCode"/>
					<label>Transport Mode Code</label>
					<span></span>
				</div>
				<div class="field">
					<input disabled type="text" ng-model="$parent.shipmentDetail.baseMode"/>
					<label>Base Mode</label>
					<span></span>
				</div>
			</div>
			<div class="cols">
				<div class="toggle">
					<input type="checkbox" ng-model="$parent.shipmentDetail.isUrgent" permission="shipment.create.details.urgent"/>
					<label>Urgent</label>
					<span></span>
				</div>
				<div class="field">

				</div>
				<div class="field">

				</div>
				<div class="field">

				</div>
			</div>
			<div class="section-subheader">
				<div class="text">
					<span>Measures</span>
				</div>
			</div>
			<div class="cols">
				<div class="field">
					<input disabled ng-model="$parent.shipmentDetail.measures.nominalHeight"/>
					<label>Nominal Height</label>
					<span></span>
				</div>
				<div class="field">
					<input disabled ng-model="$parent.shipmentDetail.measures.nominalWidth"/>
					<label>Nominal Width</label>
					<span></span>
				</div>
				<div class="field">
					<input disabled ng-model="$parent.shipmentDetail.measures.nominalLength"/>
					<label>Nominal Length</label>
					<span></span>
				</div>
				<div class="field">
					<input disabled ng-model="$parent.shipmentDetail.measures.nominalDimensionalUom"/>
					<label>Nominal Dimensional Uom</label>
					<span></span>
				</div>
			</div>
			<div class="cols">

				<div class="field">
					<input disabled ng-model="$parent.shipmentDetail.measures.nominalVolume"/>
					<label>Nominal Volume</label>
					<span></span>
				</div>
				<div class="field">
					<input disabled ng-model="$parent.shipmentDetail.measures.nominalVolumeUom"/>
					<label>Nominal Volume Uom</label>
					<span></span>
				</div>
				<div class="field">
					<input disabled ng-model="$parent.shipmentDetail.measures.nominalWeight"/>
					<label>Nominal Weight</label>
					<span></span>
				</div>
				<div class="field">
					<input disabled ng-model="$parent.shipmentDetail.measures.nominalWeightUom"/>
					<label>Nominal Weight Uom</label>
					<span></span>
				</div>
			</div>
			<div class="cols">
				<div class="field">
					<input disabled ng-model="$parent.shipmentDetail.measures.totalPieces"/>
					<label>Total Pieces</label>
					<span></span>
				</div>
				<div class="field">
					<input disabled ng-model="$parent.shipmentDetail.measures.totalSkids"/>
					<label>Total Skids</label>
					<span></span>
				</div>
				<div class="field">
					<input disabled ng-model="$parent.shipmentDetail.measures.palletPositions"/>
					<label>Pallet Positions</label>
					<span></span>
				</div>
				<div class="field">

				</div>
			</div>

<!-- 

			
			<div class="cols">
				<div class="field">
					<input disabled type="text" ng-model="$parent.shipmentDetail.x"/>
					<label>x</label>
					<span></span>
				</div>
				<div class="field">
					<input disabled type="text" ng-model="$parent.shipmentDetail.x"/>
					<label>x</label>
					<span></span>
				</div>
				<div class="field">
					<input disabled type="text" ng-model="$parent.shipmentDetail.x"/>
					<label>x</label>
					<span></span>
				</div>
				<div class="field">
					<input disabled type="text" ng-model="$parent.shipmentDetail.x"/>
					<label>x</label>
					<span></span>
				</div>
			</div>

 -->





			<div class="cols">
				<div class="rows">
					<div class="section-subheader">
						<div class="text">
							<span>Origin</span>
						</div>
					</div>
					<div class="cols">
						<div class="field">
							<input disabled type="text" ng-model="$parent.shipmentDetail.origin.originName"/>
							<label>Name</label>
							<span></span>
						</div>
						<div class="field">
							<input disabled type="text" ng-model="$parent.shipmentDetail.origin.originAddress"/>
							<label>Address</label>
							<span></span>
						</div>
					</div>
					<div class="cols">
						<div class="field">
							<input disabled type="text" ng-model="$parent.shipmentDetail.origin.originCity"/>
							<label>City</label>
							<span></span>
						</div>
						<div class="field">
							<input disabled type="text" ng-model="$parent.shipmentDetail.origin.originSau"/>
							<label>State or Province</label>
							<span></span>
						</div>
					</div>
					<div class="cols">
						<div class="field">
							<input disabled type="text" ng-model="$parent.shipmentDetail.origin.originPostalCode"/>
							<label>Postal Code</label>
							<span></span>
						</div>
						<div class="field">
							<input disabled type="text" ng-model="$parent.shipmentDetail.origin.originCountry"/>
							<label>Country</label>
							<span></span>
						</div>
					</div>
				</div>
				<div class="rows">
					<div class="section-subheader">
						<div class="text">
							<span>Destination</span>
						</div>
					</div>
					<div class="cols">
						<div class="field">
							<input disabled type="text" ng-model="$parent.shipmentDetail.destination.destinationName"/>
							<label>Name</label>
							<span></span>
						</div>
						<div class="field">
							<input disabled type="text" ng-model="$parent.shipmentDetail.destination.destinationAddress"/>
							<label>Address</label>
							<span></span>
						</div>
					</div>
					<div class="cols">
						<div class="field">
							<input disabled type="text" ng-model="$parent.shipmentDetail.destination.destinationCity"/>
							<label>City</label>
							<span></span>
						</div>
						<div class="field">
							<input disabled type="text" ng-model="$parent.shipmentDetail.destination.destinationSau"/>
							<label>State or Province</label>
							<span></span>
						</div>
					</div>
					<div class="cols">
						<div class="field">
							<input disabled type="text" ng-model="$parent.shipmentDetail.destination.destinationPostalCode"/>
							<label>Postal Code</label>
							<span></span>
						</div>
						<div class="field">
							<input disabled type="text" ng-model="$parent.shipmentDetail.destination.destinationCountry"/>
							<label>Country</label>
							<span></span>
						</div>
					</div>
				</div>
			</div>
			<div class="section-subheader">
				<div class="text">
					<span>References</span>
				</div>
			</div>
			<div class="rows">
				<div class="field" style="margin-top: 20px;">
					<div class="badges">
						<div class="badge" ng-repeat="ref in $parent.shipmentDetail.references">
							<span>{{ref.qualifier}}</span>
							<i class="inset">
								{{ref.value}}
							</i>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>