App.controller('shipmentTrackingCtrl',['$scope', '$state', 'uiLoaderSvc', 'testData', function($scope, $state, uiLoaderSvc, testData){
	$scope.$state=$state;
	$scope.showFilters=true;

	$scope.shipments=testData.shipments;

	$scope.toDate=function(date){
		return Date.parse(date);
	}
	$scope.doSearch=function(){
		uiLoaderSvc.test().then(function(){
			if (!$scope.pinSidebar){
				$scope.showFilters=false;
			}
		});
		
	}
	$scope.getTransportIcon=function(i){
		if (i=='Truckload') return "truck";
		if (i=='Rail') return "train";
		if (i=='Overnight - Early AM') return "rocket text-purple";
		return "plane";
	}
	$scope.sortOptions=["Shipment Number", "Status", "Type"]

	$scope.showShipmentDetail=function(shipment){
		$scope.shipDetailsVisible=true;
		$scope.shipmentDetail=shipment;
	}
	$scope.closeShipmentDetail=function(){
		$scope.shipDetailsVisible=false;
	}

}]);

