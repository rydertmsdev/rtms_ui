<div class="form rows form-condensed">
	<ui-header theme="subsection">
		<ui-header-body>
			<div>
				<span>{{event.title}}</span>
			</div>
		</ui-header-body>
		<ui-header-controls>
			<span class="subtitle">{{event.timestamp | date:'medium'}}</span>
		</ui-header-controls>
	</ui-header>
	<div class="cols">
		<ui-field type="text" label="Carrier" ng-disabled="true" ng-model="event.carrier"></ui-field>	
		<ui-field type="text" label="Tracking" ng-disabled="true" ng-model="event.tracking"></ui-field>	
	</div>
</div>