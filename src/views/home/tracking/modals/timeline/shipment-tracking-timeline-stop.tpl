<div class="form rows form-condensed">
	<ui-header theme="subsection">
		<ui-header-body>
			<div>
				<span>{{event.title}}</span>
			</div>
		</ui-header-body>
		<ui-header-controls>
			<span class="subtitle">{{event.timestamp | date:'medium'}}</span>
		</ui-header-controls>
	</ui-header>
	<div class="cols">
		<ui-field type="text" label="Name" ng-disabled="true" ng-model="event.name"></ui-field>	
		<ui-field type="text" label="Address" ng-disabled="true" ng-model="event.address"></ui-field>	
	</div>
	<div class="cols">
		<ui-field type="text" label="City" ng-disabled="true" ng-model="event.city"></ui-field>	
		<ui-field type="text" label="State or Province" ng-disabled="true" ng-model="event.sau"></ui-field>	
	</div>
	<div class="cols">
		<ui-field type="text" label="Postal Code" ng-disabled="true" ng-model="event.postalCode"></ui-field>	
		<ui-field type="text" label="Country" ng-disabled="true" ng-model="event.country"></ui-field>	
	</div>	
	<ui-header theme="subsection">
		<ui-header-body>
			<div>
				<span>Dates</span>
			</div>
		</ui-header-body>
	</ui-header>	
	<div class="cols">
		<ui-field type="date" label="Actual" ng-disabled="true" ng-model="event.dateActual"></ui-field>	
		<ui-field type="date" label="Scheduled" ng-disabled="true" ng-model="event.dateScheduled"></ui-field>	
	</div>
</div>