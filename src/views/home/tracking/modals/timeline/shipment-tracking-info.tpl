<div class="form rows form-condensed">
	<ui-header theme="subsection">
		<ui-header-body>
			<div>
				<span>{{event.title}}</span>
			</div>
		</ui-header-body>
		<ui-header-controls>
			<span class="subtitle">{{event.timestamp | date:'medium'}}</span>
		</ui-header-controls>
	</ui-header>
	<div class="cols">
		<ui-field type="text" label="Details" ng-disabled="true" ng-model="event.details"></ui-field>	
	</div>
</div>