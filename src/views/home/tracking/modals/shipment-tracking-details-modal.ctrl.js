App.controller('shipmentTrackingDetailsModalCtrl',['$scope', '$templateCache', '$timeout', 'uiModalSvc', 'orderSvc', function($scope, $templateCache, $timeout, uiModalSvc, orderSvc){
	$scope.close=function(){
		uiModalSvc.hide();
	}

	$scope.references= [
		{
			qualifier: "PO",
			name: "Purchase Order Number",
			value: "1GNNTSQL"
		},
		{
			qualifier: "CCLS",
			name: "Consolidation Class",
			value: "AMAZON"
		},
		{
			qualifier: "CO",
			name: "Corporate ID",
			value: "852737"
		},
		{
			qualifier: "CO",
			name: "Corporate ID",
			value: "852737"
		},
		{
			qualifier: "CO",
			name: "Corporate ID",
			value: "852737"
		},
		{
			qualifier: "CO",
			name: "Corporate ID",
			value: "852737"
		},
		{
			qualifier: "CO",
			name: "Corporate ID",
			value: "852737"
		},
		{
			qualifier: "CO",
			name: "Corporate ID",
			value: "852737"
		}
		];

	$scope.tabs=[
		{
			active:true,
			name:"Details",
			template:"shipment-tracking-modal-details.tpl"
		},
		{
			name:"References",
			template:"shipment-tracking-modal-references.tpl"
		},
		{
			name:"Journey",
			template:"shipment-tracking-modal-journey.tpl"
		},
		{
			name:"Technical",
			template:"shipment-tracking-modal-technical.tpl"
		},
		{
			name:"Items",
			url:"https://st.ryderonline.ryder.com/ryderonline/rydertrac/shipmentlineitems"
		},
	]

	if ($scope.args.tab){
		$scope.tabs.filter(function(tab){
			tab.active=false;
		})
		$scope.tabs[$scope.args.tab].active=true;
	}

	$scope.events = [{
		badgeClass: 'bg-blue',
		badgeIconClass: 'fa fa-map-marker',
		template:'shipment-tracking-timeline-stop.tpl',
		title:"Origin",
		name: "Warehouse",
		address: "29340 W. Warehouse Ct",
		city: "El Segundo",
		sau:"CA",
		postalCode: "90993",
		country: "United States",
		dateActual:Date.now()/1000,
		dateScheduled:Date.now()/1000,
		timestamp:Date.now()
	},{
		badgeClass: 'bg-red',
		badgeIconClass: 'fa fa-exclamation',
		template:'shipment-tracking-info.tpl',
		title: 'Exception',
		details:'Delayed due to inclement weather',
		timestamp:Date.now()
	},{
		badgeClass: '',
		badgeIconClass: 'fa fa-plane',
		template:'shipment-tracking-timeline-transport.tpl',
		title: 'Air Transport',
		carrier:'Ryder',
		tracking:"1Z 999 AA1 01 2345 6784",
		timestamp:Date.now()
	},{
		badgeClass: '',
		badgeIconClass: 'fa fa-truck',
		template:'shipment-tracking-timeline-transport.tpl',
		title: 'Ground Transport',
		carrier:'UPS',
		tracking:"1Z 999 AA1 01 2345 6784",
		timestamp:Date.now()
	},{
		badgeClass: 'bg-orange',
		badgeIconClass: 'fa fa-pause',
		template:'shipment-tracking-timeline-stop.tpl',
		title:"Stop",
		name: "Distribution Center",
		address: "5255 Main St.",
		city: "Dallas",
		sau:"TX",
		postalCode: "75001",
		country: "United States",
		dateActual:Date.now()/1000,
		dateScheduled:Date.now()/1000,
		timestamp:Date.now()
	},{
		badgeClass: 'bg-green',
		badgeIconClass: 'fa fa-map-pin',
		template:'shipment-tracking-timeline-stop.tpl',
		title:"Delivery",
		name: "Client",
		address: "1235 Industrial Blvd.",
		city: "New York",
		sau:"NY",
		postalCode: "10011",
		country: "United States",
		dateActual:Date.now()/1000,
		dateScheduled:Date.now()/1000,
		timestamp:Date.now()
	}];
}]);
