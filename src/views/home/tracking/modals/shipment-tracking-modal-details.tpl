<div class="modal-body overflow-y">
	<div ui-info class="form form-condensed">
		<ui-header theme="subsection">
			<ui-header-body>
				<div>
					<i class="fa fa-truck"></i>
					<span>Tracking Numbers</span>
				</div>
			</ui-header-body>
		</ui-header>
		<div class="cols">
			<ui-field type="text" label="Shipment Number" ng-disabled="true"></ui-field>
			<ui-field type="text" label="Load Number" ng-disabled="true"></ui-field>
			<ui-field></ui-field>
			<ui-field></ui-field>
		</div>
		<div class="cols">
			<div class="rows">
				<ui-header theme="subsection">
					<ui-header-body>
						<div>
							<i class="fa fa-map-marker"></i>
							<span>Origin</span>
						</div>
					</ui-header-body>
				</ui-header>
				<div class="cols">
					<ui-field type="text" label="Name" ng-disabled="true" ng-model="po.lines[0].locations.origin.name"></ui-field>	
					<ui-field type="text" label="Address" ng-disabled="true" ng-model="po.lines[0].locations.origin.address"></ui-field>	
				</div>
				<div class="cols">
					<ui-field type="text" label="City" ng-disabled="true" ng-model="po.lines[0].locations.origin.city"></ui-field>	
					<ui-field type="text" label="State or Province" ng-disabled="true" ng-model="po.lines[0].locations.origin.sau"></ui-field>	
				</div>
				<div class="cols">
					<ui-field type="text" label="Postal Code" ng-disabled="true" ng-model="po.lines[0].locations.origin.postalCode"></ui-field>	
					<ui-field type="text" label="Country" ng-disabled="true" ng-model="po.lines[0].locations.origin.country"></ui-field>	
				</div>
			</div>
			<div class="rows">
				<ui-header theme="subsection">
					<ui-header-body>
						<div>
							<i class="fa fa-map-pin"></i>
							<span>Destination</span>
						</div>
					</ui-header-body>
				</ui-header>
				<div class="cols">
					<ui-field type="text" label="Name" ng-disabled="true" ng-model="po.lines[0].locations.destination.name"></ui-field>	
					<ui-field type="text" label="Address" ng-disabled="true" ng-model="po.lines[0].locations.destination.address"></ui-field>	
				</div>
				<div class="cols">
					<ui-field type="text" label="City" ng-disabled="true" ng-model="po.lines[0].locations.destination.city"></ui-field>	
					<ui-field type="text" label="State or Province" ng-disabled="true" ng-model="po.lines[0].locations.destination.sau"></ui-field>	
				</div>
				<div class="cols">
					<ui-field type="text" label="Postal Code" ng-disabled="true" ng-model="po.lines[0].locations.destination.postalCode"></ui-field>	
					<ui-field type="text" label="Country" ng-disabled="true" ng-model="po.lines[0].locations.destination.country"></ui-field>	
				</div>
			</div>
		</div>
		<div class="cols">
			<div class="rows">
				<ui-header theme="subsection">
					<ui-header-body>
						<div>
							<i class="fa fa-calendar"></i>
							<span>Pick Up Date</span>
						</div>
					</ui-header-body>
				</ui-header>
				<ui-field type="date" label="Actual" ng-disabled="true"></ui-field>	
				<ui-field type="date" label="Approved" ng-disabled="true"></ui-field>	
				<ui-field type="date" label="Scheduled" ng-disabled="true"></ui-field>	
			</div>
			<div class="rows">
				<ui-header theme="subsection">
					<ui-header-body>
						<div>
							<i class="fa fa-calendar"></i>
							<span>Delivery Date</span>
						</div>
					</ui-header-body>
				</ui-header>
				<ui-field type="date" label="Actual" ng-disabled="true"></ui-field>	
				<ui-field type="date" label="Approved" ng-disabled="true"></ui-field>	
				<ui-field type="date" label="Scheduled" ng-disabled="true"></ui-field>	
			</div>
		</div>
		<ui-header theme="subsection">
			<ui-header-body>
				<div>
					<i class="ic-measure"></i>
					<span>Dimensions</span>
				</div>
			</ui-header-body>
		</ui-header>
		<div class="cols">
			<ui-field type="text" label="Width" ng-disabled="true"></ui-field>
			<ui-field type="text" label="Length" ng-disabled="true"></ui-field>
			<ui-field type="text" label="Height" ng-disabled="true"></ui-field>
			<ui-field type="text" label="Weight" ng-disabled="true"></ui-field>
		</div>
	</div>
</div>
