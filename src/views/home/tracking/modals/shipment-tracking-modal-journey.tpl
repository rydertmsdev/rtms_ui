<div class="form form-condensed modal-body cols" style="padding:0;box-shadow:0 5px 5px -5px rgba(0,0,0,0.15) inset;border-top:1px solid rgba(0,0,0,0.15);overflow:hidden;width:100%;height: 100%">
	<div class="journey-map">

	</div>
	<div class="content overflow-y timeline-map-half">
		<timeline class="no-track">
			<timeline-event ng-repeat="event in events" side="right">
				<timeline-badge class="{{event.badgeClass}}">
					<i class="glyphicon {{event.badgeIconClass}}"></i>
				</timeline-badge>
				<timeline-panel>
					<div ng-include="event.template"></div>
				</timeline-panel>
			</timeline-event>
		</timeline>
	</div>
</div>