<div class="form modal-full" ng-controller="shipmentTrackingDetailsModalCtrl">
	<ui-header theme="ui-modal-header">
		<ui-header-body>
			<div>
				<i class="fa fa-crosshairs"></i>
				<span>Shipment X</span>
			</div>
		</ui-header-body>
		<ui-header-controls>
			<div ui-button ng-click="prevShipment();"><i class="fa fa-chevron-left"></i></div>
			<div ui-button ng-click="nextShipment();"><i class="fa fa-chevron-right"></i></div>
		</ui-header-controls>
		<ui-header-controls class="inset">
			<div ui-button ng-click="poDetailsEditable=false;close()"><i class="fa fa-times"></i></div>
		</ui-header-controls>
	</ui-header>
	<ui-tabs tabs="tabs"></ui-tabs>
	<ui-header theme="ui-modal-footer">
		<ui-header-body>
			<div ui-button type="submit" ng-click="" class="">
				<span>Print</span>
			</div>
			<div ui-button type="submit" ng-click="" class="">
				<span>Email</span>
			</div>
		</ui-header-body>
		<ui-header-controls>
			<div ui-button type="submit" ng-click="close()" class="bg-accent">
				<span>Close</span>
			</div>
		</ui-header-controls>
	</ui-header>
</div>