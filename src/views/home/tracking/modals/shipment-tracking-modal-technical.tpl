	<ui-list class="overflow-y">			
		<ui-list-header  style="border-top:none">
			<li>
				<span>Event</span>
				<div ui-list-sort-btn>
					<i class="fa fa-sort"></i>
				</div>
			</li>
			<li ui-width="200px">		
				<span>Transaction Type</span>
				<div ui-list-sort-btn>
					<i class="fa fa-sort"></i>
				</div>
			</li>
			<li ui-width="100px">		
				<span>Event Code</span>
				<div ui-list-sort-btn>
					<i class="fa fa-sort"></i>
				</div>
			</li>
			<li ui-width="100px">		
				<span>Entity ID</span>
				<div ui-list-sort-btn>
					<i class="fa fa-sort"></i>
				</div>
			</li>
			<li ui-width="100px">		
				<span>Event Date</span>
				<div ui-list-sort-btn>
					<i class="fa fa-sort"></i>
				</div>
			</li>
		</ui-list-header>
		<ui-list-body>
			<li ng-repeat="shipment in events" ng-class="{'selected':shipment.$$selected}" ng-click="selectItem(shipment)" style="min-height: 44px">
				<ui-list-content>	
					<div>

					</div>
					<div class="rows" ui-width="200px">

					</div>
					<div class="rows" ui-width="100px">

					</div>
					<div class="rows" ui-width="100px">

					</div>
					<div class="rows" ui-width="100px">

					</div>
				</ui-list-content>
			</li>
		</ui-list-body>
	</ui-list>