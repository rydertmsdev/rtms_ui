<div class="modal-body form">
	<ui-info-container>
		<ui-info ng-repeat="ref in references">
			<div class="form rows">
				<ui-header theme="subsection">
					<ui-header-body>
						<div>
							<span>{{ref.name}}</span>
						</div>
					</ui-header-body>
					<ui-header-controls>
						<span class="subtitle">{{ref.qualifier}}</span>
					</ui-header-controls>
				</ui-header>
				<div class="text">
					{{ref.value}}
				</div>
			</div>
		</ui-info>
	</ui-info-container>
</div>