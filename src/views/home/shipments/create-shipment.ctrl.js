App.controller('createShipmentCtrl',['$scope', '$state', 'createShipmentSvc', function($scope, $state, createShipmentSvc){
	$scope.$state=$state;

	$scope.tabs=[
	{
		name:"Create Shipment",
		iconClass:"fa fa-plus",
		state:"home.create-shipment.default",
		forceState:true,
		uiInfo:"Create Standard Shipment"
	},
	{
		name:"Desktop Shipment",
		iconClass:"fa fa-desktop",
		state:"home.create-shipment.desktop",
		forceState:true,
		uiInfo:"Create Desktop Shipment"
	}
	]

	$scope.createShipmentSvc=createShipmentSvc;

}]);

