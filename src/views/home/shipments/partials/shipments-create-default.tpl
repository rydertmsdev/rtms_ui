	<div class="cols">
		<ui-section>
			<ui-header theme="section">
				<ui-header-body>
					<div>
						Origin
					</div>
				</ui-header-body>
				<ui-header-controls hide-actions>
					<div ui-button ui-header-actions-btn  class="accent">
						<i class="ic-dot-3"></i>
					</div>
					<div ui-button title="Set as my default address" class="text-eBlue" ng-click="homeAddress=!homeAddress">
						<i class="fa fa-check"></i>
						<span>{{origin.setAsDefault?'Unset':'Set'}} as Default</span>
					</div>
					<div ui-button title="Address Book" class="text-orange" ng-click="openAddressLookup();">
						<i class="fa fa-address-book"></i>
						<span>Address Book</span>
					</div>
				</ui-header-controls>
			</ui-header>
			<div class="form address-form" ng-class="{'home-location':homeAddress}">
				<ui-field type="select" placeholder="Address Look up" ng-model="origin.name" prop="name" options="originIds" class="no-label address-lookup" ng-required="true" ng-disabled="false" refresh="getLocations" refresh-args="{'name':'location', 'code':'19'}" on-select="selectedLocation" selected-args="{'name': origin.entityId.name, 'code':'19'}"></ui-field>
				<div class="cols">
					<div class="form-very-condensed">
						<ui-field type="text" ng-value="origin.address + ' '+origin.address2" class="no-label" ng-required="true" ng-disabled="true"></ui-field>
						<ui-field type="text" ng-value="origin.city+', '+origin.sau + ' ' + origin.postalCode" class="no-label" ng-required="true" ng-disabled="true"></ui-field>
						<ui-field type="text" ng-value="origin.country" class="no-label" ng-required="true" ng-disabled="true"></ui-field>
					</div>
				</div>
				<div class="address-form-id" >
					<span ng-if="origin.id">ID: {{origin.id}}</span>
				</div>
			</div>
		</ui-section>

		<ui-section>
			<ui-header theme="section">
				<ui-header-body>
					<div>
						Destination
					</div>
				</ui-header-body>
				<ui-header-controls hide-actions>
					<div ui-button ui-header-actions-btn  class="accent">
						<i class="ic-dot-3"></i>
					</div>
					<div ui-button title="Address Book" class="text-orange" ng-click="openAddressLookup();">
						<i class="fa fa-address-book"></i>
						<span>Address Book</span>
					</div>
				</ui-header-controls>
			</ui-header>
			<div class="form address-form">
				<ui-field type="select" ng-model="destination.name" options="optionsTest" class="no-label address-lookup" ng-required="true" ng-disabled="false" placeholder="Select Destination"></ui-field>
				<div class="cols">
					<div class="form-very-condensed">
						<ui-field type="text" ng-value="destination.address + ' '+destination.address2" class="no-label" ng-required="true" ng-disabled="true"></ui-field>
						<ui-field type="text" ng-value="destination.city+', '+destination.sau + ' ' + destination.postalCode" class="no-label" ng-required="true" ng-disabled="true"></ui-field>
						<ui-field type="text" ng-value="destination.country" class="no-label" ng-required="true" ng-disabled="true"></ui-field>
					</div>
				</div>
				<div class="address-form-id">
					<span ng-if="destination.id">ID: {{destination.id}}</span>
				</div>
			</div>
		</ui-section>
	</div>


	<ui-section>
		<ui-header theme="section">
			<ui-header-body>
				<div>
					<span>
						Ready to Pick Up
					</span>
				</div>
			</ui-header-body>
		</ui-header>
		<div class="form cols">
			<ui-field type="date" ng-model="newShipment.pickUpDate" label="Pick Up Date" ng-required="true" ng-disabled="false"></ui-field>
			<ui-field type="time" ng-model="newShipment.pickUpTime" label="Pick Up Time" ng-disabled="false"></ui-field>
		</div>
	</ui-section>

	<ui-section>
		<ui-header theme="section">
			<ui-header-body>
				<div>
					<span>
						Carrier
					</span>
				</div>
			</ui-header-body>
		</ui-header>
		<div class="form cols">
			<div class="cols">
				<ui-field type="select" options="carriers" ng-model="newShipment.carrier" label="Select Carrier" ng-required="false" ng-disabled="false"></ui-field>
				<ui-field type="text" ng-model="newShipment.otherCarrierName" label="Carrier Name" ng-required="false" ng-disabled="false" ng-if="newShipment.carrier=='Other'"></ui-field>
			</div>
			<ui-field type="text" ng-model="newShipment.carrierAccountNumber" label="Account Number" ng-required="false" ng-disabled="false"></ui-field>
		</div>
	</ui-section>

	<ui-section>
		<ui-header theme="section">
			<ui-header-body>
				<div>
					<span>
						Return
					</span>
				</div>
			</ui-header-body>
		</ui-header>
		<div class="form rows">
				<ui-field type="toggle" ng-model="newShipment.isReturn" label="Return/RMA" title="Generate Return Label" ng-required="false" ng-disabled="false"></ui-field>
				<ui-field ng-show="newShipment.isReturn" type="select" options="returnReasons" ng-model="newShipment.returnReason" label="Return Reason" ng-required="true" ng-disabled="false" placeholder="Select reason for the return"></ui-field>
				<ui-field type="text" ng-model="newShipment.otherCarrierName" label="Enter Return Reason" ng-required="true" ng-disabled="false" ng-if="newShipment.returnReason=='OTHER'"></ui-field>
			</div>
		</div>
	</ui-section>

	<ui-section>
		<ui-header theme="section">
			<ui-header-body>
				<div>
					<span>
						Dimensions
					</span>
				</div>
			</ui-header-body>
		</ui-header>
		<div class="form cols">
			<div class="cols">
				<ui-field type="text" ng-model="newShipment.packageType" label="Package Type" ng-required="false" ng-disabled="false" placeholder=""></ui-field>
			</div>
			<div class="form cols">
				<ui-field type="text" ng-model="newShipment.weight" label="Weight" ng-required="false" ng-disabled="false" placeholder=""></ui-field>
				<ui-field type="select" options="wuoms" ng-model="newShipment.weightUom" label="Weight UOM" ng-required="false" ng-disabled="false"></ui-field>
			</div>
		</div>
		<div class="form cols">
			<div class="cols">
				<ui-field type="text" ng-model="newShipment.length" label="Length" ng-required="false" ng-disabled="false"></ui-field>
				<ui-field type="text" ng-model="newShipment.width" label="Width" ng-required="false" ng-disabled="false"></ui-field>
			</div>
			<div class="cols">
				<ui-field type="text" ng-model="newShipment.height" label="Height" ng-required="false" ng-disabled="false"></ui-field>
				<ui-field type="select" options="uoms" ng-model="newShipment.uom" label="UOM" ng-required="false" ng-disabled="false"></ui-field>
			</div>
		</div>
		<div class="form cols">
			<ui-field type="text" ng-model="newShipment.volume" label="Volume" ng-required="false" ng-disabled="true" placeholder="---"></ui-field>
			<ui-field type="text" ng-model="newShipment.volumeUom" label="Volume UOM" ng-required="false" ng-disabled="true"  placeholder="---"></ui-field>
		</div>
	</ui-section>	

	<ui-section class="media-mobile-hide">
		<ui-header theme="section">
			<ui-header-body>
				<div>
					<span>
						Documents
					</span>
				</div>
			</ui-header-body>
		</ui-header>

		<div class="form cols">
			<ng-dropzone class="dropzone" options="dzOptions" callbacks="dzCallbacks" methods="dzMethods"></ng-dropzone>
		</div>
	</ui-section>
	<ui-page-actions-spacer></ui-page-actions-spacer>
	<ui-page-actions-container>
		<ui-page-actions>
			<div class="group">
				<div ui-button ui-page-action class="text-accent bg-white" ui-info="Submit" ng-click="submitShipment()">
					<span>Submit</span>
					<i class="fa fa-chevron-right"></i>
				</div>
			</div>
		</ui-page-actions>
	</ui-page-actions-container>