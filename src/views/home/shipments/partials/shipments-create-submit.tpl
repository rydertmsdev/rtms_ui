<div class="overflow-y" ui-section-container>
	<ui-section>
		<ui-header theme="section">
			<ui-header-body>
				<div>
					<span>
						Creating Shipment
					</span>
				</div>
			</ui-header-body>
		</ui-header>
		<div class="order-complete">
			<ul>
				<li class="order-complete-section" ng-class="complete">
					<span class="icon">
						<i class="fa fa-check"></i>
					</span>
					<span class="text">Shipment request submitted</span>
				</li>
				<li class="order-complete-section" ng-class="complete">
					<span class="icon">
						<i class="fa fa-info"></i>
					</span>
					<span class="text">Shipment Number - <a href="{{shipmentUrl}}" target="_new" >{{shipmentNumber}}</a></span>
				</li>
				<li class="order-complete-section" ng-class="complete">
					<span class="icon">
						<i class="fa fa-info"></i>
					</span>
					<span class="text">Load Number  - <a href="{{loadUrl}}" target="_new"> {{loadNumber}}</a></span>
				</li>
				<li class="order-complete-section" ng-class="complete">
					<span class="icon">
						<i class="fa fa-info"></i>
					</span>
					<span class="text">Unique ID  - <a href="{{loadUrl}}" target="_new"> {{uniqueId}}</a></span>
				</li>
			</ul>
		</div>
	</ui-section>

	<ui-section  ng-show="documents.length > 0">
		<ui-header theme="section">
			<ui-header-body>
				<div>
					<i class="fa fa-exclamation-circle"></i>
					<span>
						Shipping Documents
					</span>
				</div>
			</ui-header-body>
			<ui-header-controls>
				<button ui-button ng-click="printDiv()">
					<i class="fa fa-print"></i>
					<span>Print</span>
				</button>
			</ui-header-controls>
		</ui-header>
	</ui-section>
</div>