	<div class="cols">
		<ui-section>
			<ui-header theme="section">
				<ui-header-body>
					<div>
						Origin
					</div>
				</ui-header-body>
				<ui-header-controls hide-actions>
					<div ui-button ui-header-actions-btn  class="accent">
						<i class="ic-dot-3"></i>
					</div>
					<div ui-button title="Set as my default address" class="text-eBlue" ng-click="homeAddress=!homeAddress">
						<i class="fa fa-check"></i>
						<span>{{homeAddress?'Unset':'Set'}} as Default</span>
					</div>
					<div ui-button title="Address Book" class="text-orange" ng-click="openAddressLookup();">
						<i class="fa fa-address-book"></i>
						<span>Address Book</span>
					</div>
				</ui-header-controls>
			</ui-header>
			<div class="form address-form" ng-class="{'home-location':homeAddress}">
				<ui-field type="select" ng-model="origin.name" options="optionsTest" class="no-label address-lookup" ng-required="true" ng-disabled="false" placeholder="Select Origin"></ui-field>
				<div class="cols">
					<div class="form-very-condensed">
						<ui-field type="text" ng-value="origin.address + ' '+origin.address2" class="no-label" ng-required="true" ng-disabled="true"></ui-field>
						<ui-field type="text" ng-value="origin.city+', '+origin.sau + ' ' + origin.postalCode" class="no-label" ng-required="true" ng-disabled="true"></ui-field>
						<ui-field type="text" ng-value="origin.country" class="no-label" ng-required="true" ng-disabled="true"></ui-field>
					</div>
				</div>
				<div class="address-form-id" >
					<span ng-if="origin.id">ID: {{origin.id}}</span>
				</div>
			</div>
		</ui-section>

		<ui-section>
			<ui-header theme="section">
				<ui-header-body>
					<div>
						Destination
					</div>
				</ui-header-body>
				<ui-header-controls hide-actions>
					<div ui-button ui-header-actions-btn  class="accent">
						<i class="ic-dot-3"></i>
					</div>
					<div ui-button title="Address Book" class="text-orange" ng-click="openAddressLookup();">
						<i class="fa fa-address-book"></i>
						<span>Address Book</span>
					</div>
				</ui-header-controls>
			</ui-header>
			<div class="form address-form">
				<ui-field type="select" ng-model="destination.name" options="optionsTest" class="no-label address-lookup" ng-required="true" ng-disabled="false" placeholder="Select Destination"></ui-field>
				<div class="cols">
					<div class="form-very-condensed">
						<ui-field type="text" ng-value="destination.address + ' '+destination.address2" class="no-label" ng-required="true" ng-disabled="true"></ui-field>
						<ui-field type="text" ng-value="destination.city+', '+destination.sau + ' ' + destination.postalCode" class="no-label" ng-required="true" ng-disabled="true"></ui-field>
						<ui-field type="text" ng-value="destination.country" class="no-label" ng-required="true" ng-disabled="true"></ui-field>
					</div>
				</div>
				<div class="address-form-id">
					<span ng-if="destination.id">ID: {{destination.id}}</span>
				</div>
			</div>
		</ui-section>
	</div>



	<ui-section>
		<ui-header theme="section">
			<ui-header-body>
				<div>
					Details
				</div>
			</ui-header-body>
		</ui-header>
		<div class="form cols">
			<ui-field type="text" ng-model="newShipment.sector" label="Sector" ng-disabled="false"></ui-field>
			<ui-field type="text" ng-model="newShipment.costCenter" label="Cost Center" ng-disabled="false"></ui-field>
			<ui-field type="text" ng-model="newShipment.internalOrder" label="Internal Order" ng-disabled="false"></ui-field>
			<ui-field type="text" ng-model="newShipment.network" label="Network" ng-disabled="false"></ui-field>
		</div>		
		<div class="form cols">
			<ui-field type="text" ng-model="newShipment.networkActivity" label="Network Activity" ng-disabled="false"></ui-field>
			<ui-field type="text" ng-model="newShipment.gl" label="GL" ng-disabled="false"></ui-field>
			<ui-field type="text" ng-model="newShipment.companyCode" label="Company Code" ng-disabled="false"></ui-field>
			<ui-field type="text" ng-model="newShipment.shippedOnBehalf" label="Shipped on Behalf Of" ng-disabled="false"></ui-field>
		</div>		
		<div class="form cols">
			<ui-field type="text" ng-model="newShipment.shipmentMemo" label="Shipment Memo" ng-required="false" ng-disabled="false"></ui-field>
		</div>
	</ui-section>

	<ui-section>
		<ui-header theme="section">
			<ui-header-body>
				<div>
					Ready to Pick Up
				</div>
			</ui-header-body>
		</ui-header>
		<div class="form cols">
			<ui-field type="date" ng-model="newShipment.readyDate" label="Date" ng-disabled="false"></ui-field>
			<ui-field type="time" ng-model="newShipment.readyTime" label="Time" ng-disabled="false"></ui-field>
		</div>
	</ui-section>

	<ui-section>
		<ui-header theme="section">
			<ui-header-body>
				<div>
					Additional Options
				</div>
			</ui-header-body>
		</ui-header>

		<div class="form cols">
			<ui-field type="toggle" class="no-label" ng-model="newShipment.insideDelivery" title="Inside Delivery" ng-required="false" ng-disabled="false"></ui-field>
			<ui-field type="toggle" class="no-label" ng-model="newShipment.flatbed" title="Flatbed" ng-required="false" ng-disabled="false"></ui-field>
			<ui-field type="toggle" class="no-label" ng-model="newShipment.liftgate" title="Liftgate" ng-required="false" ng-disabled="false"></ui-field>
			<!-- <ui-field type="toggle" class="no-label" ng-model="newShipment.constantSurveillance" title="Constant Surveillance" ng-required="false" ng-disabled="false"></ui-field> -->
			<!-- <ui-field type="toggle" class="no-label" ng-model="newShipment.temperatureControlled" title="Temperature Controlled" ng-required="false" ng-disabled="false"></ui-field> -->
		</div>
	</ui-section>

	<ui-section ng-repeat="handlingUnit in newShipment.handlingUnits" class="fade-in-out">
		<ui-header theme="section">
			<ui-header-body>
				<div ng-bind-html="handlingUnit.name">
				</div>
			</ui-header-body>
			<ui-header-controls hide-actions>
				<div ui-button ui-header-actions-btn  class="accent">
					<i class="ic-dot-3"></i>
				</div>
				<div ui-button ui-info="Delete Handling Unit" class="text-red" ng-click="deleteHandlingUnit($index);">
					<i class="fa fa-times"></i>
					<span>Delete</span>
				</div>
			</ui-header-controls>
		</ui-header>
		<div class="form cols">
			<ui-field type="text" ng-model="handlingUnit.name" label="Handling Unit Name" ng-required="true" ng-disabled="false"></ui-field>
			<ui-field type="select" options="handlingUnitTypes" ng-model="handlingUnit.type" label="Container Type" ng-required="false" ng-disabled="false"></ui-field>
		</div>
		<div class="form cols">
			<ui-field type="text" ng-model="handlingUnit.length" label="Length" ng-required="false" ng-disabled="false"></ui-field>
			<ui-field type="text" ng-model="handlingUnit.width" label="Width" ng-required="false" ng-disabled="false"></ui-field>
		</div>
		<div class="form cols">
			<ui-field type="text" ng-model="handlingUnit.height" label="Height" ng-required="false" ng-disabled="false"></ui-field>
			<ui-field type="select" options="uoms" ng-model="handlingUnit.uom" label="UOM" ng-required="false" ng-disabled="false"></ui-field>
		</div>
		<div class="form cols form-condensed">
			<ui-field type="text" ng-model="handlingUnit.volume" label="Volume" ng-required="false" ng-disabled="true" placeholder="---"></ui-field>
			<ui-field type="text" ng-model="handlingUnit.volumeUom" label="Volume UOM" ng-required="false" ng-disabled="true"  placeholder="---"></ui-field>
		</div>
		<ui-header theme="subsection" style="border-bottom: none">
			<ui-header-body>
				<div>
					<span>
						Items
					</span>
				</div>
			</ui-header-body>
		</ui-header>
		<ui-list class="static">			
			<ui-list-header>
				<li style="max-width: 110px">
					<span>Part Number</span>
				</li>
				<li>
					<span>Part Description</span>
				</li>

				<li class="text-center" ui-width="54px">		
					<span>Width</span>
				</li>
				<li class="text-center" ui-width="54px">		
					<span>Length</span>
				</li>
				<li class="text-center" ui-width="54px">		
					<span>Height</span>
				</li>

				<li class="text-center" ui-width="54px">		
					<span>Dim. UOM</span>
				</li>

				<li class="text-center" ui-width="54px">		
					<span>Weight</span>
				</li>

				<li class="text-center" ui-width="54px">		
					<span>Weight UOM</span>
				</li>

				<li class="text-center" ui-width="54px">		
					<span>Quantity</span>
				</li>

				<li class="text-center" ui-width="54px">Hazmat</li>

				<li class="text-center" ui-width="54px">Remove</li>
			</ui-list-header>
			<ui-list-body>
				<li ng-repeat="(iIndex,item) in handlingUnit.items | listPlaceholder"  class="fade-in-out">
					<ui-list-content>
						<cell class="rows"  style="max-width: 110px">
							<ui-field type="text" ng-model="item.partNumber" class="inline"></ui-field>
						</cell>

						<cell class="rows"  style="">
							<ui-field type="text" ng-model="item.partDescription" class="inline"></ui-field>
						</cell>

						<cell class="rows"  ui-width="54px">
							<ui-field type="text" ng-model="item.width" class="inline"></ui-field>
						</cell>

						<cell class="rows"  ui-width="54px">
							<ui-field type="text" ng-model="item.length" class="inline"></ui-field>
						</cell>

						<cell class="rows"  ui-width="54px">
							<ui-field type="text" ng-model="item.height" class="inline"></ui-field>
						</cell>

						<cell class="rows"  ui-width="54px">
							<ui-field type="select" options="uoms" ng-model="item.dimUom" class="inline"></ui-field>
						</cell>

						<cell class="rows"  ui-width="54px">
							<ui-field type="text" ng-model="item.weight" class="inline"></ui-field>
						</cell>

						<cell class="rows"  ui-width="54px">
							<ui-field type="select" options="wuoms" ng-model="item.weightUom" class="inline"></ui-field>
						</cell>

						<cell class="rows"  ui-width="54px">
							<ui-field type="text" ng-model="item.quantity" class="inline"></ui-field>
						</cell>
						<ui-list-static-actions>
							<cell class="icon">
								<div ui-button ng-click="item.hazmat=!item.hazmat" class="text-lgray">
									<i class="fa fa-exclamation-triangle" ng-class="{'text-orange':item.hazmat}"></i>
								</div>
							</cell>
							<cell class="icon">
								<div ui-button ng-click="removeItem(handlingUnit,$index)" tabindex="-1" ng-disabled="equals(item,{})">
									<i class="fa fa-times text-red"></i>
								</div>
							</cell>
						</ui-list-static-actions>
					</ui-list-content>


				</li>
			</ui-list-body>
		</ui-list>
	</ui-section>

	<ui-page-actions-spacer></ui-page-actions-spacer>
	<ui-page-actions-container>
		<ui-page-actions>
			<div class="group">
				<div ui-button ui-page-action class="bg-red" ui-info="Cancel" ng-click="cancelShipmentProcess();">
					<i class="fa fa-close"></i>
				</div>
			</div>
			<div class="group">
				<div ui-button ui-page-action class="bg-eBlue" ui-info="Add Handling Unit" ng-click="addHandlingUnit()">
					<div>
						<i class="fa fa-cube"></i>
					</div>
				</div>
			</div>
			<div class="group">
				<div ui-button ui-page-action class="bg-blue" ui-info="Save" ng-click="saveShipmentAsDraft();">
					<i class="fa fa-save"></i>
				</div>
			</div>
			<div class="group">
				<div ui-button ui-page-action class="text-accent bg-white" ui-info="Save and Process" ng-click="submitShipment()">
					<span>Save and Process</span>
					<i class="fa fa-chevron-right"></i>
				</div>
			</div>
		</ui-page-actions>
	</ui-page-actions-container>