App.controller('ofStandardSearchResultsCtrl',['$state', '$rootScope', '$scope', '$timeout', 'uiLoaderSvc', 'uiModalSvc', 'orderSvc', 'uniqueFilter', 'generateSearchQuery', 'standardShipmentSearchResult', 'purchaseOrderDetails', 'codeListService', 'getProcessedSearchResults', 'logoutService', 'getRates', 'shipmentSvc', function($state, $rootScope, $scope, $timeout, uiLoaderSvc, uiModalSvc, orderSvc, uniqueFilter, generateSearchQuery, standardShipmentSearchResult, purchaseOrderDetails, codeListService, getProcessedSearchResults, logoutService, getRates, shipmentSvc){
	
	$scope.uiLoaderSvc.preload(function(){
		return $scope.svc=orderSvc;
	});

	$scope.$state=$state;
	$scope.orderSvc=orderSvc;
	$scope.shipmentSvc=shipmentSvc;

	if(codeListService.codeLists && codeListService.codeLists.SearchResultsFieldList){
		$scope.tableHeaders = JSON.parse(JSON.stringify(codeListService.codeLists.SearchResultsFieldList));
	}
	
	$scope.dynamicContent = [];
	$scope.searchContent = [];
	$scope.additionalSearchContent = [];
	
	if(generateSearchQuery.from_server_copy){
		$scope.dynamicContent = generateSearchQuery.from_server_copy.SearchFieldList;
	}

	angular.forEach($scope.dynamicContent, function(field, index) {
		if(field.sectionSeq == 0 || field.sectionSeq == null){
			$scope.searchContent.push(field);
		}
		else{
			$scope.additionalSearchContent.push(field);
		}
	});

	///////////////////////////////////////////////////
	// Sidebar
	//

	$scope.OrderItemReferenceList = [];

	$scope.selectedShipment=function(shipmentObj){
		shipmentSvc.selectedShipment = shipmentObj;
		$state.go('home.shipment.create.standarddetails');
	}

	$scope.doSearch=function(){

		uiLoaderSvc.show();

		$scope.searchCriteria = generateSearchQuery.searchOrderQueryResp();
		$scope.searchCriteria.customerNum = $rootScope.userProfile.data.content.customerNum;
		// $scope.searchCriteria.status = "NOT_ALLOCATED";

		$scope.requestObject = {};
		shipmentSvc.index = 1;
		$scope.requestObject.pageNum = shipmentSvc.index;
		$scope.requestObject.pageSize = shipmentSvc.pageSize;
		$scope.requestObject.rowsPerBatch = shipmentSvc.rowsPerbatch;
		$scope.requestObject.searchCriteria = $scope.searchCriteria;

		var filter = [];
		$scope.requestObject.sortByList = filter;

		// console.log("Request Object  ---   ", JSON.stringify($scope.requestObject));
		var orders = standardShipmentSearchResult.getResults("shipment", $scope.requestObject).then(function(resp){
			uiLoaderSvc.hide();

			if(resp.data && resp.data.content){
				shipmentSvc.shipmentSearchResults = getProcessedSearchResults.processResults(resp.data.content);	
				shipmentSvc.pageTotal = Math.ceil(resp.data.recordsCount/$scope.requestObject.pageSize);
				shipmentSvc.recordsCount = resp.data.recordsCount;
				shipmentSvc.recordsCountExceeded = resp.data.recordsCountExceeded;
			}		
		},
		function(data){
			uiLoaderSvc.hide();
			console.log("Service Error - ", data);
			
			//User not authenticated (403) redirecting to login page
			if(data.status == 403){
				logoutService.logout();
			}
		});

		// shipmentSvc.selectAllFlag = false;	
	
		$rootScope.$emit("searchClicked");
		// $state.reload();
		// orderSvc.search().then(function(){
		// 	uiLoaderSvc.hide();
		// });
	}

	$scope.clearFilters=function(){
		generateSearchQuery.clearValues();

		//removing the sorting icons when cleared
		var removeSortEle = document.querySelectorAll(".fa-sort-desc");
		angular.element(removeSortEle).removeClass("fa-sort-desc").addClass("fa-sort");
		var ascSortEle = document.querySelectorAll(".fa-sort-asc");
		angular.element(ascSortEle).removeClass("fa-sort-asc").addClass("fa-sort");

	}

	$scope.selectClassName=function(arr, val){
		if(arr){
			var tempClass = "";
			if(val == undefined){
				val = "";
			}
			angular.forEach(arr, function (option, index){
				if(option.code == val){
					tempClass = option.cssClasses;
				}
			});
			return tempClass;
		}
		else{
			return "";
		}
	}


	///////////////////////////////////////////////////
	// Main Content
	//
	

	$scope.uiAlertSvc.clear();
	// $scope.uiAlertSvc.show({
	// 	icon:"fa fa-info",
	// 	label:"Service Availability",
	// 	text:"Order Fulfillment availability will be limited between 1am and 5am due to scheduled maintenance",
	// 	//static:true
	// });

	$scope.selectItem=function(item){
		orderSvc.selectItem(item);
	}

	$scope.getRates=function(obj,event){
		event.stopPropagation();

		uiLoaderSvc.show();

		getRates.get("shipments/v1", obj).then(function(resp){
			// console.log(resp);
			uiLoaderSvc.hide();

			uiModalSvc.show('shipment-rate-details.tpl',{
				po:obj,
				type:"update"
			});
		},
		function(data){
			uiLoaderSvc.hide();
			console.log("Service Error - ", data);

			uiModalSvc.show('shipment-rate-details.tpl',{
				po:obj,
				type:"update"
			});

			//User not authenticated (403) redirecting to login page
			if(data.status == 403){
				logoutService.logout();
			}
		});
	}

		$scope.sortedFilter = [];

	$scope.sortResults = function(code, event, type){

		if(shipmentSvc.shipmentSearchResults.length > 0){

			if(!!code){

				var a = angular.element(event.target);

				var currentClass = "";
				
				if(a.find(".fa-sort").length > 0 || a.hasClass("fa-sort")){
					currentClass = "ASC";
					if(a.hasClass("fa-sort")){
						a.removeClass("fa-sort").addClass("fa-sort-asc");
					}
					else{
						a.find(".fa-sort").removeClass("fa-sort").addClass("fa-sort-asc");
					}			
				}
				else if(a.find(".fa-sort-asc").length > 0 || a.hasClass("fa-sort-asc")){
					currentClass = "DESC";
					if(a.hasClass("fa-sort-asc")){
						a.removeClass("fa-sort-asc").addClass("fa-sort-desc");
					}
					else{
						a.find(".fa-sort-asc").removeClass("fa-sort-asc").addClass("fa-sort-desc");
					}			
				}
				else if(a.find(".fa-sort-desc").length > 0 || a.hasClass("fa-sort-desc")){
					currentClass = ""
					if(a.hasClass("fa-sort-desc")){
						a.removeClass("fa-sort-desc").addClass("fa-sort");
					}
					else{
						a.find(".fa-sort-desc").removeClass("fa-sort-desc").addClass("fa-sort");
					}			
				}

				var tempArray = $scope.sortedFilter;

				var pushFlag = false;

				if(tempArray.length > 0){
					angular.forEach(tempArray, function(item, index) {
						if(item.by && item.by == code){
							if(currentClass == ""){
								tempArray.splice(index, 1);
							}
							else{
								item.order = currentClass;
								pushFlag = true;
							}
						}
						// else{
						// 	var tempObj = {};
						// 	tempObj["by"] = code;
						// 	tempObj["order"] = currentClass;
						// 	tempArray.push(tempObj);
						// }
					});
				}
				
				if(tempArray.length == 0 || pushFlag == false){
					if(currentClass != ""){
						var tempObj = {};
						tempObj["by"] = code;
						tempObj["order"] = currentClass;
						tempArray.push(tempObj);	
					}
				}

				// console.log("soted array list -- ", JSON.stringify(tempArray));
			}
			else{
				$scope.sortedFilter = [];
			}

			uiLoaderSvc.show();

			$scope.searchCriteria = generateSearchQuery.searchOrderQueryResp();
			$scope.searchCriteria.customerNum = $rootScope.userProfile.data.content.customerNum;

			$scope.requestObject = {};
			if(type == "doublePrevious"){
				var totalNumOfPages = shipmentSvc.rowsPerbatch/shipmentSvc.pageSize ;
				$scope.requestObject.pageNum = (Math.floor((shipmentSvc.index - 1)/totalNumOfPages)*totalNumOfPages)-totalNumOfPages+1;	
			}
			else if(type == "previous"){
				$scope.requestObject.pageNum = shipmentSvc.index - 1;
			}
			else if(type == "next"){
				$scope.requestObject.pageNum = shipmentSvc.index + 1;
			}
			else if(type == "doubleNext"){
				var totalNumOfPages = shipmentSvc.rowsPerbatch/shipmentSvc.pageSize;
				//$scope.requestObject.pageNum = shipmentSvc.pageTotal + 1;
				$scope.requestObject.pageNum = (Math.floor((shipmentSvc.index + (totalNumOfPages - 1))/totalNumOfPages)*totalNumOfPages)+1;	
			}
			else{
				$scope.requestObject.pageNum = shipmentSvc.index;	
			}


			
			$scope.requestObject.pageSize = shipmentSvc.pageSize;
			$scope.requestObject.rowsPerBatch = shipmentSvc.rowsPerbatch;
			$scope.requestObject.searchCriteria = $scope.searchCriteria;
			$scope.requestObject.sortByList = $scope.sortedFilter;
			// $scope.requestObject.statusList = ["NOT_ALLOCATED"];

			// console.log("Request Object  ---   ", JSON.stringify($scope.requestObject));
			var orders = standardShipmentSearchResult.getResults("shipment", $scope.requestObject).then(function(resp){
				uiLoaderSvc.hide();
				// generateSearchQuery.clearValues();
				// $scope.results=shipmentSvc.orders;	
				// shipmentSvc.shipmentSearchResults = [{"createDt":1487631303000,"createId":"RYDERO","lastUpdateDt":1487631307000,"lastUpdateId":"RYDERO","orderComponentNum":1028878006,"supplyOrder":{"createDt":1487631303000,"createId":"RYDERO","lastUpdateDt":1487631307000,"lastUpdateId":"RYDERO","orderComponentNum":1028878005,"customerNum":"HPNACC","errorMsg":"Not enough inventory is available for itemCode K7X31A8#ABA, warehousecd D775  combination. ","orderDate":1487916000000,"orderNumber":"1003999083pm06","requestedDeliveryDtFrom":1487829723000,"requestedDeliveryDtTo":1488520743000,"requestedShipDtFrom":1487570463000,"requestedShipDtTo":1488520683000,"shippingCondition":"SG","status":"NOT_ALLOCATED","supplyordertypecd":"SALES","urgent":0,"warehouseCd":"D775","type":"SUPPLY_ORDER","entityId":"1028878005"},"parentOrderComponentNum":1028878005,"allocationFlag":"Y","billVolume":null,"billVolumeUom":null,"billWeight":null,"billWeightUom":null,"clientPartNumber":null,"commodityCode":null,"declaredValue":null,"description":"HP ProDisplay P232 Monitor","dueDate":1488002400000,"freightClass":null,"generateAsn":null,"generatedDate":null,"hazmat":null,"height":1.37,"holdCd":null,"itemClass":null,"itemCode":"K7X31A8#ABA","itemEntityCd":null,"itemNumber":"K7X31A8#ABA","labelId":null,"ladenLength":null,"length":2.08,"manufacturerPartNumber":null,"measurementUom":null,"memo":null,"nmfcNumber":null,"nominalVolume":null,"nominalVolumeUom":null,"nominalWeight":null,"nominalWeightUom":null,"orderValue":null,"orderNumber":"1003999083pm06","organizationCd":null,"qtyAllocated":null,"qtyFulfilled":null,"qtyPlanned":null,"qtyShipped":null,"quantity":1900,"quantityUom":"EA","releaseNumber":null,"shipmentNum":null,"stackingFactor":null,"status":"NOT_ALLOCATED","supplierPartNumber":"K7X31A8#ABA","trackingNumber":null,"transmissionDate":null,"unitPrice":null,"upc":null,"vendorCd":"20048690","volume":1.3108,"volumeUom":"CU FT","warehouseCd":"D775","weight":11.95,"weightUom":null,"width":0.46,"customerNum":"HPNACC","lineItemNum":"00001","priority":null,"attributes":null,"references":[{"createDt":1487631303000,"createId":"RYDERO","lastUpdateDt":1487631303000,"lastUpdateId":"RYDERO","lineItemNum":"00001","referenceCode":"CRLN","referenceNum":"000004"},{"createDt":1487631303000,"createId":"RYDERO","lastUpdateDt":1487631303000,"lastUpdateId":"RYDERO","lineItemNum":"00001","referenceCode":"PLIN","referenceNum":"000041"}],"orderItems":null,"securityMappings":null,"type":"ORDER_ITEM","entityId":"1028878006","notAllocated":false,"allocated":true,"id":null},{"createDt":1487631461000,"createId":"RYDERO","lastUpdateDt":1487631462000,"lastUpdateId":"RYDERO","orderComponentNum":1028878008,"supplyOrder":{"createDt":1487631461000,"createId":"RYDERO","lastUpdateDt":1487631462000,"lastUpdateId":"RYDERO","orderComponentNum":1028878007,"customerNum":"HPNACC","errorMsg":"Not enough inventory is available for itemCode K7X31A8#ABA, warehousecd D775  combination. ","orderDate":1487916000000,"orderNumber":"1003999083pm07","requestedDeliveryDtFrom":1487829761000,"requestedDeliveryDtTo":1488520781000,"requestedShipDtFrom":1487570501000,"requestedShipDtTo":1488520721000,"shippingCondition":"SG","status":"NOT_ALLOCATED","supplyordertypecd":"SALES","urgent":0,"warehouseCd":"D775","type":"SUPPLY_ORDER","entityId":"1028878007"},"parentOrderComponentNum":1028878007,"allocationFlag":"Y","billVolume":null,"billVolumeUom":null,"billWeight":null,"billWeightUom":null,"clientPartNumber":null,"commodityCode":null,"declaredValue":null,"description":"HP ProDisplay P232 Monitor","dueDate":1488002400000,"freightClass":null,"generateAsn":null,"generatedDate":null,"hazmat":null,"height":1.37,"holdCd":null,"itemClass":null,"itemCode":"K7X31A8#ABA","itemEntityCd":null,"itemNumber":"K7X31A8#ABA","labelId":null,"ladenLength":null,"length":2.08,"manufacturerPartNumber":null,"measurementUom":null,"memo":null,"nmfcNumber":null,"nominalVolume":null,"nominalVolumeUom":null,"nominalWeight":null,"nominalWeightUom":null,"orderValue":null,"orderNumber":"1003999083pm07","organizationCd":null,"qtyAllocated":null,"qtyFulfilled":null,"qtyPlanned":null,"qtyShipped":null,"quantity":1900,"quantityUom":"EA","releaseNumber":null,"shipmentNum":null,"stackingFactor":null,"status":"NOT_ALLOCATED","supplierPartNumber":"K7X31A8#ABA","trackingNumber":null,"transmissionDate":null,"unitPrice":null,"upc":null,"vendorCd":"20048690","volume":1.3108,"volumeUom":"CU FT","warehouseCd":"D775","weight":11.95,"weightUom":null,"width":0.46,"customerNum":"HPNACC","lineItemNum":"00001","priority":null,"attributes":null,"references":[{"createDt":1487631461000,"createId":"RYDERO","lastUpdateDt":1487631461000,"lastUpdateId":"RYDERO","lineItemNum":"00001","referenceCode":"CRLN","referenceNum":"000004"},{"createDt":1487631461000,"createId":"RYDERO","lastUpdateDt":1487631461000,"lastUpdateId":"RYDERO","lineItemNum":"00001","referenceCode":"PLIN","referenceNum":"000041"}],"orderItems":null,"securityMappings":null,"type":"ORDER_ITEM","entityId":"1028878008","notAllocated":false,"allocated":true,"id":null},{"createDt":1487632141000,"createId":"RYDERO","lastUpdateDt":1487632142000,"lastUpdateId":"RYDERO","orderComponentNum":1028878010,"supplyOrder":{"createDt":1487632141000,"createId":"RYDERO","lastUpdateDt":1487632142000,"lastUpdateId":"RYDERO","orderComponentNum":1028878009,"customerNum":"HPNACC","errorMsg":"Not enough inventory is available for itemCode K7X31A8#ABA, warehousecd D775  combination. ","orderDate":1487916000000,"orderNumber":"4513999083t18","requestedDeliveryDtFrom":1487829720000,"requestedDeliveryDtTo":1488520740000,"requestedShipDtFrom":1487570460000,"requestedShipDtTo":1488520680000,"shippingCondition":"SG","status":"NOT_ALLOCATED","supplyordertypecd":"SALES","urgent":0,"warehouseCd":"D775","type":"SUPPLY_ORDER","entityId":"1028878009"},"parentOrderComponentNum":1028878009,"allocationFlag":"Y","billVolume":null,"billVolumeUom":null,"billWeight":null,"billWeightUom":null,"clientPartNumber":null,"commodityCode":null,"declaredValue":null,"description":"HP ProDisplay P232 Monitor","dueDate":1488002400000,"freightClass":null,"generateAsn":null,"generatedDate":null,"hazmat":null,"height":1.37,"holdCd":null,"itemClass":null,"itemCode":"K7X31A8#ABA","itemEntityCd":null,"itemNumber":"K7X31A8#ABA","labelId":null,"ladenLength":null,"length":2.08,"manufacturerPartNumber":null,"measurementUom":null,"memo":null,"nmfcNumber":null,"nominalVolume":null,"nominalVolumeUom":null,"nominalWeight":null,"nominalWeightUom":null,"orderValue":null,"orderNumber":"4513999083t18","organizationCd":null,"qtyAllocated":null,"qtyFulfilled":null,"qtyPlanned":null,"qtyShipped":null,"quantity":1900,"quantityUom":"EA","releaseNumber":null,"shipmentNum":null,"stackingFactor":null,"status":"NOT_ALLOCATED","supplierPartNumber":"K7X31A8#ABA","trackingNumber":null,"transmissionDate":null,"unitPrice":null,"upc":null,"vendorCd":"20048690","volume":1.3108,"volumeUom":"CU FT","warehouseCd":"D775","weight":11.95,"weightUom":null,"width":0.46,"customerNum":"HPNACC","lineItemNum":"00001","priority":null,"attributes":null,"references":[{"createDt":1487632141000,"createId":"RYDERO","lastUpdateDt":1487632141000,"lastUpdateId":"RYDERO","lineItemNum":"00001","referenceCode":"PLIN","referenceNum":"000041"},{"createDt":1487632141000,"createId":"RYDERO","lastUpdateDt":1487632141000,"lastUpdateId":"RYDERO","lineItemNum":"00001","referenceCode":"CRLN","referenceNum":"000004"}],"orderItems":null,"securityMappings":null,"type":"ORDER_ITEM","entityId":"1028878010","notAllocated":false,"allocated":true,"id":null},{"createDt":1487626414000,"createId":"RYDERO","lastUpdateDt":1487626517000,"lastUpdateId":"RYDERO","orderComponentNum":1028878018,"supplyOrder":{"createDt":1487626414000,"createId":"RYDERO","lastUpdateDt":1487626517000,"lastUpdateId":"RYDERO","orderComponentNum":1028878017,"customerNum":"HPNACC","errorMsg":"Not enough inventory is available for itemCode K7X31A8#ABA, warehousecd D775  combination. ","orderDate":1487916000000,"orderNumber":"1003999083pm03","requestedDeliveryDtFrom":1487829742000,"requestedDeliveryDtTo":1488520762000,"requestedShipDtFrom":1487570482000,"requestedShipDtTo":1488520702000,"shippingCondition":"SG","status":"NOT_ALLOCATED","supplyordertypecd":"SALES","urgent":0,"warehouseCd":"D775","type":"SUPPLY_ORDER","entityId":"1028878017"},"parentOrderComponentNum":1028878017,"allocationFlag":"Y","billVolume":null,"billVolumeUom":null,"billWeight":null,"billWeightUom":null,"clientPartNumber":null,"commodityCode":null,"declaredValue":null,"description":"HP ProDisplay P232 Monitor","dueDate":1488002400000,"freightClass":null,"generateAsn":null,"generatedDate":null,"hazmat":null,"height":1.37,"holdCd":null,"itemClass":null,"itemCode":"K7X31A8#ABA","itemEntityCd":null,"itemNumber":"K7X31A8#ABA","labelId":null,"ladenLength":null,"length":2.08,"manufacturerPartNumber":null,"measurementUom":null,"memo":null,"nmfcNumber":null,"nominalVolume":null,"nominalVolumeUom":null,"nominalWeight":null,"nominalWeightUom":null,"orderValue":null,"orderNumber":"1003999083pm03","organizationCd":null,"qtyAllocated":null,"qtyFulfilled":null,"qtyPlanned":null,"qtyShipped":null,"quantity":1900,"quantityUom":"EA","releaseNumber":null,"shipmentNum":null,"stackingFactor":null,"status":"NOT_ALLOCATED","supplierPartNumber":"K7X31A8#ABA","trackingNumber":null,"transmissionDate":null,"unitPrice":null,"upc":null,"vendorCd":"20048690","volume":1.3108,"volumeUom":"CU FT","warehouseCd":"D775","weight":11.95,"weightUom":null,"width":0.46,"customerNum":"HPNACC","lineItemNum":"00001","priority":null,"attributes":null,"references":[{"createDt":1487626414000,"createId":"RYDERO","lastUpdateDt":1487626414000,"lastUpdateId":"RYDERO","lineItemNum":"00001","referenceCode":"PLIN","referenceNum":"000041"},{"createDt":1487626414000,"createId":"RYDERO","lastUpdateDt":1487626414000,"lastUpdateId":"RYDERO","lineItemNum":"00001","referenceCode":"CRLN","referenceNum":"000004"}],"orderItems":null,"securityMappings":null,"type":"ORDER_ITEM","entityId":"1028878018","notAllocated":false,"allocated":true,"id":null},{"createDt":1487881442000,"createId":"RYDERO","lastUpdateDt":1487881453000,"lastUpdateId":"RYDERO","orderComponentNum":1028878168,"supplyOrder":{"createDt":1487881442000,"createId":"RYDERO","lastUpdateDt":1487881453000,"lastUpdateId":"RYDERO","orderComponentNum":1028878167,"customerNum":"HPNACC","errorMsg":"Not enough inventory is available for itemCode K7X31A8#ABA, warehousecd D775  combination. ","orderDate":1487570400000,"orderNumber":"9513999083t4","requestedDeliveryDtFrom":1488088921000,"requestedDeliveryDtTo":1488779941000,"requestedShipDtFrom":1487829661000,"requestedShipDtTo":1488779881000,"status":"NOT_ALLOCATED","supplyordertypecd":"SALES","urgent":0,"warehouseCd":"D775","type":"SUPPLY_ORDER","entityId":"1028878167"},"parentOrderComponentNum":1028878167,"allocationFlag":"Y","billVolume":null,"billVolumeUom":null,"billWeight":null,"billWeightUom":null,"clientPartNumber":null,"commodityCode":null,"declaredValue":null,"description":"HP ProDisplay P232 Monitor","dueDate":1488002400000,"freightClass":null,"generateAsn":null,"generatedDate":null,"hazmat":null,"height":1.37,"holdCd":null,"itemClass":null,"itemCode":"K7X31A8#ABA","itemEntityCd":null,"itemNumber":"K7X31A8#ABA","labelId":null,"ladenLength":null,"length":2.08,"manufacturerPartNumber":null,"measurementUom":null,"memo":null,"nmfcNumber":null,"nominalVolume":null,"nominalVolumeUom":null,"nominalWeight":null,"nominalWeightUom":null,"orderValue":null,"orderNumber":"9513999083t4","organizationCd":null,"qtyAllocated":null,"qtyFulfilled":null,"qtyPlanned":null,"qtyShipped":null,"quantity":20,"quantityUom":"EA","releaseNumber":null,"shipmentNum":null,"stackingFactor":null,"status":"NOT_ALLOCATED","supplierPartNumber":"K7X31A8#ABA","trackingNumber":null,"transmissionDate":null,"unitPrice":null,"upc":null,"vendorCd":"20048690","volume":1.3108,"volumeUom":"CU FT","warehouseCd":"D775","weight":11.95,"weightUom":null,"width":0.46,"customerNum":"HPNACC","lineItemNum":"00001","priority":null,"attributes":null,"references":[{"createDt":1487881442000,"createId":"RYDERO","lastUpdateDt":1487881442000,"lastUpdateId":"RYDERO","lineItemNum":"00001","referenceCode":"PLIN","referenceNum":"000041"},{"createDt":1487881442000,"createId":"RYDERO","lastUpdateDt":1487881442000,"lastUpdateId":"RYDERO","lineItemNum":"00001","referenceCode":"CRLN","referenceNum":"000004"}],"orderItems":null,"securityMappings":null,"type":"ORDER_ITEM","entityId":"1028878168","notAllocated":false,"allocated":true,"id":null},{"createDt":1487881442000,"createId":"RYDERO","lastUpdateDt":1487881453000,"lastUpdateId":"RYDERO","orderComponentNum":1028878170,"supplyOrder":{"createDt":1487881442000,"createId":"RYDERO","lastUpdateDt":1487881453000,"lastUpdateId":"RYDERO","orderComponentNum":1028878169,"customerNum":"HPNACC","errorMsg":"Not enough inventory is available for itemCode K7X31A8#ABA, warehousecd D775  combination. ","orderDate":1487570400000,"orderNumber":"9513999083t3","requestedDeliveryDtFrom":1488088921000,"requestedDeliveryDtTo":1488779941000,"requestedShipDtFrom":1487829661000,"requestedShipDtTo":1488779881000,"status":"NOT_ALLOCATED","supplyordertypecd":"SALES","urgent":0,"warehouseCd":"D775","type":"SUPPLY_ORDER","entityId":"1028878169"},"parentOrderComponentNum":1028878169,"allocationFlag":"Y","billVolume":null,"billVolumeUom":null,"billWeight":null,"billWeightUom":null,"clientPartNumber":null,"commodityCode":null,"declaredValue":null,"description":"HP ProDisplay P232 Monitor","dueDate":1488002400000,"freightClass":null,"generateAsn":null,"generatedDate":null,"hazmat":null,"height":1.37,"holdCd":null,"itemClass":null,"itemCode":"K7X31A8#ABA","itemEntityCd":null,"itemNumber":"K7X31A8#ABA","labelId":null,"ladenLength":null,"length":2.08,"manufacturerPartNumber":null,"measurementUom":null,"memo":null,"nmfcNumber":null,"nominalVolume":null,"nominalVolumeUom":null,"nominalWeight":null,"nominalWeightUom":null,"orderValue":null,"orderNumber":"9513999083t3","organizationCd":null,"qtyAllocated":null,"qtyFulfilled":null,"qtyPlanned":null,"qtyShipped":null,"quantity":20,"quantityUom":"EA","releaseNumber":null,"shipmentNum":null,"stackingFactor":null,"status":"NOT_ALLOCATED","supplierPartNumber":"K7X31A8#ABA","trackingNumber":null,"transmissionDate":null,"unitPrice":null,"upc":null,"vendorCd":"20048690","volume":1.3108,"volumeUom":"CU FT","warehouseCd":"D775","weight":11.95,"weightUom":null,"width":0.46,"customerNum":"HPNACC","lineItemNum":"00001","priority":null,"attributes":null,"references":[{"createDt":1487881442000,"createId":"RYDERO","lastUpdateDt":1487881442000,"lastUpdateId":"RYDERO","lineItemNum":"00001","referenceCode":"PLIN","referenceNum":"000041"},{"createDt":1487881442000,"createId":"RYDERO","lastUpdateDt":1487881442000,"lastUpdateId":"RYDERO","lineItemNum":"00001","referenceCode":"CRLN","referenceNum":"000004"}],"orderItems":null,"securityMappings":null,"type":"ORDER_ITEM","entityId":"1028878170","notAllocated":false,"allocated":true,"id":null},{"createDt":1487879643000,"createId":"RYDERO","lastUpdateDt":1487879656000,"lastUpdateId":"RYDERO","orderComponentNum":1028878181,"supplyOrder":{"createDt":1487879643000,"createId":"RYDERO","lastUpdateDt":1487879656000,"lastUpdateId":"RYDERO","orderComponentNum":1028878180,"customerNum":"HPNACC","errorMsg":"Not enough inventory is available for itemCode K7X31A8#ABA, warehousecd D775  combination. ","orderDate":1487570400000,"orderNumber":"9513999083t2","requestedDeliveryDtFrom":1488088922000,"requestedDeliveryDtTo":1488779942000,"requestedShipDtFrom":1487829662000,"requestedShipDtTo":1488779882000,"status":"NOT_ALLOCATED","supplyordertypecd":"SALES","urgent":0,"warehouseCd":"D775","type":"SUPPLY_ORDER","entityId":"1028878180"},"parentOrderComponentNum":1028878180,"allocationFlag":"Y","billVolume":null,"billVolumeUom":null,"billWeight":null,"billWeightUom":null,"clientPartNumber":null,"commodityCode":null,"declaredValue":null,"description":"HP ProDisplay P232 Monitor","dueDate":1488002400000,"freightClass":null,"generateAsn":null,"generatedDate":null,"hazmat":null,"height":1.37,"holdCd":null,"itemClass":null,"itemCode":"K7X31A8#ABA","itemEntityCd":null,"itemNumber":"K7X31A8#ABA","labelId":null,"ladenLength":null,"length":2.08,"manufacturerPartNumber":null,"measurementUom":null,"memo":null,"nmfcNumber":null,"nominalVolume":null,"nominalVolumeUom":null,"nominalWeight":null,"nominalWeightUom":null,"orderValue":null,"orderNumber":"9513999083t2","organizationCd":null,"qtyAllocated":null,"qtyFulfilled":null,"qtyPlanned":null,"qtyShipped":null,"quantity":20,"quantityUom":"EA","releaseNumber":null,"shipmentNum":null,"stackingFactor":null,"status":"NOT_ALLOCATED","supplierPartNumber":"K7X31A8#ABA","trackingNumber":null,"transmissionDate":null,"unitPrice":null,"upc":null,"vendorCd":"20048690","volume":1.3108,"volumeUom":"CU FT","warehouseCd":"D775","weight":11.95,"weightUom":null,"width":0.46,"customerNum":"HPNACC","lineItemNum":"00001","priority":null,"attributes":null,"references":[{"createDt":1487879643000,"createId":"RYDERO","lastUpdateDt":1487879643000,"lastUpdateId":"RYDERO","lineItemNum":"00001","referenceCode":"CRLN","referenceNum":"000004"},{"createDt":1487879643000,"createId":"RYDERO","lastUpdateDt":1487879643000,"lastUpdateId":"RYDERO","lineItemNum":"00001","referenceCode":"PLIN","referenceNum":"000041"}],"orderItems":null,"securityMappings":null,"type":"ORDER_ITEM","entityId":"1028878181","notAllocated":false,"allocated":true,"id":null},{"createDt":1487879653000,"createId":"RYDERO","lastUpdateDt":1487879656000,"lastUpdateId":"RYDERO","orderComponentNum":1028878183,"supplyOrder":{"createDt":1487879653000,"createId":"RYDERO","lastUpdateDt":1487879656000,"lastUpdateId":"RYDERO","orderComponentNum":1028878182,"customerNum":"HPNACC","errorMsg":"Not enough inventory is available for itemCode K7X31A8#ABA, warehousecd D775  combination. ","orderDate":1487570400000,"orderNumber":"9513999083t1","requestedDeliveryDtFrom":1488088922000,"requestedDeliveryDtTo":1488779942000,"requestedShipDtFrom":1487829662000,"requestedShipDtTo":1488779882000,"status":"NOT_ALLOCATED","supplyordertypecd":"SALES","urgent":0,"warehouseCd":"D775","type":"SUPPLY_ORDER","entityId":"1028878182"},"parentOrderComponentNum":1028878182,"allocationFlag":"Y","billVolume":null,"billVolumeUom":null,"billWeight":null,"billWeightUom":null,"clientPartNumber":null,"commodityCode":null,"declaredValue":null,"description":"HP ProDisplay P232 Monitor","dueDate":1488002400000,"freightClass":null,"generateAsn":null,"generatedDate":null,"hazmat":null,"height":1.37,"holdCd":null,"itemClass":null,"itemCode":"K7X31A8#ABA","itemEntityCd":null,"itemNumber":"K7X31A8#ABA","labelId":null,"ladenLength":null,"length":2.08,"manufacturerPartNumber":null,"measurementUom":null,"memo":null,"nmfcNumber":null,"nominalVolume":null,"nominalVolumeUom":null,"nominalWeight":null,"nominalWeightUom":null,"orderValue":null,"orderNumber":"9513999083t1","organizationCd":null,"qtyAllocated":null,"qtyFulfilled":null,"qtyPlanned":null,"qtyShipped":null,"quantity":20,"quantityUom":"EA","releaseNumber":null,"shipmentNum":null,"stackingFactor":null,"status":"NOT_ALLOCATED","supplierPartNumber":"K7X31A8#ABA","trackingNumber":null,"transmissionDate":null,"unitPrice":null,"upc":null,"vendorCd":"20048690","volume":1.3108,"volumeUom":"CU FT","warehouseCd":"D775","weight":11.95,"weightUom":null,"width":0.46,"customerNum":"HPNACC","lineItemNum":"00001","priority":null,"attributes":null,"references":[{"createDt":1487879653000,"createId":"RYDERO","lastUpdateDt":1487879653000,"lastUpdateId":"RYDERO","lineItemNum":"00001","referenceCode":"CRLN","referenceNum":"000004"},{"createDt":1487879653000,"createId":"RYDERO","lastUpdateDt":1487879653000,"lastUpdateId":"RYDERO","lineItemNum":"00001","referenceCode":"PLIN","referenceNum":"000041"}],"orderItems":null,"securityMappings":null,"type":"ORDER_ITEM","entityId":"1028878183","notAllocated":false,"allocated":true,"id":null},{"createDt":1489616585000,"createId":"RYDERO","lastUpdateDt":1489622461000,"lastUpdateId":"RYDERO","orderComponentNum":1028955637,"supplyOrder":{"createDt":1489616585000,"createId":"RYDERO","lastUpdateDt":1489622461000,"lastUpdateId":"RYDERO","orderComponentNum":1028955635,"customerNum":"HPNACC","errorMsg":"Not enough inventory is available for itemCode C9F26A8#ABA, warehousecd D775  combination. ","orderDate":1490504400000,"orderNumber":"4513999083T42","requestedDeliveryDtFrom":1489554120000,"requestedDeliveryDtTo":1490849940000,"requestedShipDtFrom":1489125660000,"requestedShipDtTo":1490677080000,"shippingCondition":"SG","status":"NOT_ALLOCATED","supplyordertypecd":"SALES","urgent":0,"warehouseCd":"D775","type":"SUPPLY_ORDER","entityId":"1028955635"},"parentOrderComponentNum":1028955635,"allocationFlag":"Y","billVolume":null,"billVolumeUom":null,"billWeight":null,"billWeightUom":null,"clientPartNumber":"HPI-C9F26A8","commodityCode":null,"declaredValue":null,"description":"Desc for C9F26A8#ABA","dueDate":1488002400000,"freightClass":null,"generateAsn":null,"generatedDate":null,"hazmat":null,"height":0.17,"holdCd":null,"itemClass":null,"itemCode":"C9F26A8#ABA","itemEntityCd":null,"itemNumber":"C9F26A8#ABA","labelId":null,"ladenLength":null,"length":0.17,"manufacturerPartNumber":null,"measurementUom":null,"memo":null,"nmfcNumber":null,"nominalVolume":null,"nominalVolumeUom":null,"nominalWeight":null,"nominalWeightUom":null,"orderValue":null,"orderNumber":"4513999083T42","organizationCd":null,"qtyAllocated":null,"qtyFulfilled":null,"qtyPlanned":null,"qtyShipped":null,"quantity":100,"quantityUom":"EA","releaseNumber":null,"shipmentNum":null,"stackingFactor":null,"status":"NOT_ALLOCATED","supplierPartNumber":"C9F26A8","trackingNumber":null,"transmissionDate":null,"unitPrice":"1.0","upc":null,"vendorCd":"20048690","volume":0.0072,"volumeUom":"CU FT","warehouseCd":"D775","weight":10,"weightUom":"LB","width":0.25,"customerNum":"HPNACC","lineItemNum":"00001","priority":null,"attributes":null,"references":[{"createDt":1489616585000,"createId":"RYDERO","lastUpdateDt":1489616585000,"lastUpdateId":"RYDERO","lineItemNum":"00001","referenceCode":"CPN","referenceNum":"HPI-C9F26A8"},{"createDt":1489616585000,"createId":"RYDERO","lastUpdateDt":1489616585000,"lastUpdateId":"RYDERO","lineItemNum":"00001","referenceCode":"CRLN","referenceNum":"000004"},{"createDt":1489616585000,"createId":"RYDERO","lastUpdateDt":1489616585000,"lastUpdateId":"RYDERO","lineItemNum":"00001","referenceCode":"PLIN","referenceNum":"000041"}],"orderItems":null,"securityMappings":null,"type":"ORDER_ITEM","entityId":"1028955637","notAllocated":false,"allocated":true,"id":null},{"createDt":1490740621000,"createId":"RYDERO","lastUpdateDt":1490741940000,"lastUpdateId":"RYDERO","orderComponentNum":1028995745,"supplyOrder":{"createDt":1490740621000,"createId":"RYDERO","lastUpdateDt":1490741940000,"lastUpdateId":"RYDERO","orderComponentNum":1028995744,"customerNum":"HPNACC","errorMsg":"Not enough inventory is available for itemCode C9F26A8#ABA, warehousecd D775  combination. ","orderDate":1490590800000,"orderNumber":"4513999083T53","requestedDeliveryDtFrom":1490936520000,"requestedDeliveryDtTo":1491627540000,"requestedShipDtFrom":1490677260000,"requestedShipDtTo":1491627480000,"shippingCondition":"SG","status":"NOT_ALLOCATED","supplyordertypecd":"SALES","urgent":0,"warehouseCd":"D775","type":"SUPPLY_ORDER","entityId":"1028995744"},"parentOrderComponentNum":1028995744,"allocationFlag":"Y","billVolume":null,"billVolumeUom":null,"billWeight":null,"billWeightUom":null,"clientPartNumber":"HPI-C9F26A8","commodityCode":null,"declaredValue":null,"description":"Desc for C9F26A8#ABA","dueDate":1488002400000,"freightClass":null,"generateAsn":null,"generatedDate":null,"hazmat":null,"height":0.17,"holdCd":null,"itemClass":null,"itemCode":"C9F26A8#ABA","itemEntityCd":null,"itemNumber":"C9F26A8#ABA","labelId":null,"ladenLength":null,"length":0.17,"manufacturerPartNumber":null,"measurementUom":null,"memo":null,"nmfcNumber":null,"nominalVolume":null,"nominalVolumeUom":null,"nominalWeight":null,"nominalWeightUom":null,"orderValue":null,"orderNumber":"4513999083T53","organizationCd":null,"qtyAllocated":null,"qtyFulfilled":null,"qtyPlanned":null,"qtyShipped":null,"quantity":100,"quantityUom":"EA","releaseNumber":null,"shipmentNum":null,"stackingFactor":null,"status":"NOT_ALLOCATED","supplierPartNumber":"C9F26A8","trackingNumber":null,"transmissionDate":null,"unitPrice":"1.0","upc":null,"vendorCd":"20048690","volume":0.0072,"volumeUom":"CU FT","warehouseCd":"D775","weight":10,"weightUom":"LB","width":0.25,"customerNum":"HPNACC","lineItemNum":"00001","priority":null,"attributes":null,"references":[{"createDt":1490740621000,"createId":"RYDERO","lastUpdateDt":1490740621000,"lastUpdateId":"RYDERO","lineItemNum":"00001","referenceCode":"PRLN","referenceNum":"0001234"},{"createDt":1490740621000,"createId":"RYDERO","lastUpdateDt":1490740621000,"lastUpdateId":"RYDERO","lineItemNum":"00001","referenceCode":"PLIN","referenceNum":"000041"},{"createDt":1490740621000,"createId":"RYDERO","lastUpdateDt":1490740621000,"lastUpdateId":"RYDERO","lineItemNum":"00001","referenceCode":"CRLN","referenceNum":"000004"},{"createDt":1490740621000,"createId":"RYDERO","lastUpdateDt":1490740621000,"lastUpdateId":"RYDERO","lineItemNum":"00001","referenceCode":"CPN","referenceNum":"HPI-C9F26A8"}],"orderItems":null,"securityMappings":null,"type":"ORDER_ITEM","entityId":"1028995745","notAllocated":false,"allocated":true,"id":null}];
				// shipmentSvc.searchResults = resp.data.content;
				if(resp.data && resp.data.content){
					shipmentSvc.index = $scope.requestObject.pageNum;
					shipmentSvc.shipmentSearchResults = getProcessedSearchResults.processResults(resp.data.content);
					shipmentSvc.pageTotal = Math.ceil(resp.data.recordsCount/$scope.requestObject.pageSize);
					shipmentSvc.recordsCount = resp.data.recordsCount;
					//shipmentSvc.recordsCountExceeded = resp.data.recordsCountExceeded;
					var totalNumOfPages = shipmentSvc.rowsPerbatch/shipmentSvc.pageSize;
					if(shipmentSvc.index > (shipmentSvc.pageTotal - totalNumOfPages) && shipmentSvc.index <= shipmentSvc.pageTotal){
						shipmentSvc.recordsCountExceeded = false;
					}
					else{
						shipmentSvc.recordsCountExceeded = true;	
					}
				}
				//To clear filter UI icons, reloading state 
				// if(type != 'filtering'){
				// 	$state.reload();
				// }
			},
			function(data){
				uiLoaderSvc.hide();
				console.log("Service Error - ", data);

				//User not authenticated (403) redirecting to login page
				if(data.status == 403){
					logoutService.logout();
				}
			});
		}
	}

	if(shipmentSvc.shipmentSearchResults.length > 0){
		//$scope.sortResults(null, null, null);
		shipmentSvc.shipmentSearchResults = getProcessedSearchResults.processResults(shipmentSvc.shipmentSearchResults);
	}
}]);
