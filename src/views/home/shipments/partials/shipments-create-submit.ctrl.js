App.controller('shipmentsCreateSubmit',['$scope', '$state', '$timeout', 'uiLoaderSvc', 'uiModalSvc', 'shipmentSvc', function($scope, $state, $timeout, uiLoaderSvc, uiModalSvc, shipmentSvc){
	$scope.shipmentSvc=shipmentSvc;

	$scope.shipmentNumber = "";
	$scope.shipmentUrl = "";
	$scope.loadNumber = "";
	$scope.loadUrl = "";
	$scope.documents = [];
	$scope.uniqueId = "";


	if(shipmentSvc.shipmentCreateObj && Object.keys(shipmentSvc.shipmentCreateObj).length != 0){
		$scope.shipmentNumber = shipmentSvc.shipmentCreateObj.shipmentNum;
		$scope.shipmentUrl = shipmentSvc.shipmentCreateObj.shipmentUrl;
		$scope.loadNumber = shipmentSvc.shipmentCreateObj.loadNum;
		$scope.loadUrl = shipmentSvc.shipmentCreateObj.loadUrl;
		$scope.documents = shipmentSvc.shipmentCreateObj.docs;
		$scope.uniqueId = shipmentSvc.shipmentCreateObj.uniqueId;
	}
	else{
		console.log("Shipment create object is empty");
	}

	$scope.printDiv = function() {
		uiModalSvc.show('print-modal.tpl',{
			callback:function(){
			}
		});	
	} 
	$scope.addTracking=function(){
		uiModalSvc.show('of-complete-tracking-device-modal.tpl',{});
	}

}]);
