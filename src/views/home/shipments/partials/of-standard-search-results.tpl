<ui-sidebar>
	<ui-sidebar-panel>
		<div class="content form">
			<ui-header theme="ui-sidebar-header">
				<ui-header-body>
					<div>
						<span>
							Search
						</span>
					</div>
				</ui-header-body>
				<ui-header-controls>
					<div ui-button title="Clear" ng-click="clearFilters()">
						<i class="fa fa-times text-red"></i>
					</div>
					<div ui-button title="Apply" ng-click="doSearch()">
						<i class="fa fa-check text-green"></i>
					</div>
				</ui-header-controls>
			</ui-header>
			<div>
				<ui-menu-group static>
					<ui-header theme="ui-menu-header" ui-button ui-menu-collapse>
						<ui-header-body>
							<span>
								General
							</span>
						</ui-header-body>
						<ui-header-controls>
							<div ui-menu-collapse-indicator></div>
						</ui-header-controls>
					</ui-header>
					<ui-menu-content ui-max-height>
						<dynamic-template data="searchContent"></dynamic-template>
					</ui-menu-content>
				</ui-menu-group>
				<ui-menu-group collapsed ng-if="additionalSearchContent.length > 0">
					<ui-header theme="ui-menu-header" ui-button ui-menu-collapse>
						<ui-header-body>
							<span>
								Additional Filters
							</span>
						</ui-header-body>
						<ui-header-controls>
							<div ui-menu-collapse-indicator></div>
						</ui-header-controls>
					</ui-header>
					<ui-menu-content ui-max-height>
						<dynamic-template data="additionalSearchContent"></dynamic-template>
					</ui-menu-content>
				</ui-menu-group>
			</div>
		</div>
	</ui-sidebar-panel>
	<ui-sidebar-content>
		<ui-alert-container></ui-alert-container>
		<ui-section>
			<ui-list>	
				<ui-list-header>
					<!-- <li class="icon text-center">
						<span>Select</span>
					</li> -->
					<li ng-repeat="heading in tableHeaders" code="{{heading.code}}" ng-class="{true:'width250', false:''}[heading.code == 'description']" ng-if="heading.displayType != 'image' && !heading.options">
						<span>{{heading.name}}</span>
						<div ui-list-sort-btn ng-click="sortResults(heading.code, $event, 'filtering')">
							<i class="fa fa-sort"></i>
						</div>
					</li>
					<li ng-repeat="heading in tableHeaders" code="{{heading.code}}" ui-width="50px" ng-if="heading.displayType != 'image' && heading.options">
						<i ng-if="heading.options" class="{{heading.layoutProperties.cssClasses}}"></i>
						<div ui-list-sort-btn ng-click="sortResults(heading.code, $event, 'filtering')">
							<i class="fa fa-sort"></i>
						</div>
					</li>
					<li ng-repeat="heading in tableHeaders" code="{{heading.code}}" ui-width="50px" ng-class="ui-list-flags-header" ng-if="heading.displayType == 'image'">
						<i class="{{heading.layoutProperties.cssClasses}}"></i>
						<div ui-list-sort-btn ng-click="sortResults(heading.code, $event, 'filtering')">
							<i class="fa fa-sort"></i>
						</div>
					</li>
					<li class="icon">Process</li>
				</ui-list-header>		
				<ui-list-body>
					<li ng-if="shipmentSvc.shipmentSearchResults.length == 0">
						<div class="no-results">
							No results to show.
						</div>
					</li>
					<li ng-repeat="po in shipmentSvc.shipmentSearchResults" style="min-height: 40px" ng-class="{'selected':po.$$selected}" >
						<!-- <cell class="icon">
							<div class="checkbox" ng-class="{'checked':po.$$selected}"></div>
						</cell> -->
						<ui-list-content>
							<cell ng-repeat="heading in tableHeaders" code="{{heading.code}}" ng-class="{true:'rows width250', false:'rows text'}[heading.code == 'description']" ng-click="selectedShipment(po)">
								<div ng-if="heading.displayType != 'date' && heading.displayType != 'datetime'">
									<span class="title" ui-info="{{po.resultObject[heading.code]}}">{{po.resultObject[heading.code]}}</span>			
									<span  ng-if="heading.code == 'orderNumber'" class="subtitle">Line {{po.lineItemNum}}</span>
									<span  ng-if="heading.code == 'description'" class="subtitle">Item Code: {{po.itemCode}}</span>
								</div>
								<div ng-if="heading.displayType == 'date'">
									<span class="title">{{po.resultObject[heading.code] | parseDates: heading.displayType}}</span>
								</div>		
							</cell>
							<ui-list-flags ng-repeat="heading in tableHeaders" code="{{heading.code}}" ui-width="50px" ng-class="" ng-if="heading.displayType != 'image' && heading.options">
								<div class="title" ng-class="selectClassName(heading.options, po.resultObject[heading.code])" ui-info="{{heading.name}}">{{heading.name}}</div>
							</ui-list-flags>
							<ui-list-flags ng-repeat="heading in tableHeaders" code="{{heading.code}}" ui-width="50px" ng-class="" ng-if="heading.displayType == 'image'">
								<div ng-class="selectClassName(heading.options, po.resultObject[heading.code])" >
									<!-- <i class="{{heading.layoutProperties.cssClasses}}"></i> -->
								</div>
							</ui-list-flags>
						</ui-list-content>
						<cell class="icon">
							<div ui-button ng-click="getRates(po,$event)" ui-info="Process">
								<i class="fa accent fa-arrow-circle-o-right" ></i>
							</div>
						</cell>
					</li>
				</ui-list-body>
			</ui-list>
		</ui-section>
		<ui-page-actions-spacer ng-if="shipmentSvc.shipmentSearchResults.length > 0"></ui-page-actions-spacer>
		<ui-page-actions-container ng-if="shipmentSvc.shipmentSearchResults.length > 0">
			<ui-page-actions>
				<div class="group">

					<div ui-button ui-page-action class="bg-lgray" ui-info="Previous Page" ng-if="shipmentSvc.index > 1" ng-click="sortResults(null, $event, 'previous')">
						<i class="fa fa-chevron-left"></i>
					</div>
					<div ui-button ui-page-action class="bg-lgray" ui-info="Previous Page" ng-if="shipmentSvc.index == 1" disabled="disabled">
						<i class="fa fa-chevron-left"></i>
					</div>
					<div class="text">
						<span>{{shipmentSvc.index}} / {{shipmentSvc.pageTotal}}</span>
					</div>
					<div ui-button ui-page-action class="bg-accent"  ui-info="Next Page" ng-if="(shipmentSvc.index < shipmentSvc.pageTotal) || ((shipmentSvc.index == shipmentSvc.pageTotal) && shipmentSvc.recordsCountExceeded)" ng-click="sortResults(null, $event, 'next')">
						<i class="fa fa-chevron-right"></i>
					</div>
					<div ui-button ui-page-action class="bg-accent"  ui-info="Next Page" ng-if="(shipmentSvc.index == shipmentSvc.pageTotal) && !shipmentSvc.recordsCountExceeded" disabled="disabled">
						<i class="fa fa-chevron-right"></i>
					</div>
				</div>
			</ui-page-actions>
		</ui-page-actions-container>
	</ui-sidebar-content>
</ui-sidebar>







