App.controller('shipmentsCreateDefaultCtrl',['$scope', '$state', 'addressLookupService', 'uiLoaderSvc', 'uiModalSvc', 'orderSvc', 'shipmentSvc', 'getROLView', function($scope, $state, addressLookupService, uiLoaderSvc, uiModalSvc, orderSvc, shipmentSvc, getROLView){
		
		$scope.createShipmentFields = [];
		$scope.originIds = [{'name':'', 'code': ''}];
		$scope.getLocations=function(text, args){

		// console.log(angular.element(event.target));

		// $scope.$apply();

		if(text.length > 2){

			var requestObj ={};

			if(args.name == 'location'){
				requestObj["code"] = text+"%";
			}

			if(args.name == 'name'){
				requestObj["name"] = "%"+text+"%";
			}

			requestObj["type"] = args.code;

			uiLoaderSvc.show();
			addressLookupService.getResults(orderSvc.module, requestObj).then(function(resp){
				uiLoaderSvc.hide();

				if(resp.data && resp.data.content){
					if(args.name == 'location' && args.code == "19"){
						if(resp.data.content.length == 0){
							$scope.originIds = [{'code' : ''}];
						}
						else{
							$scope.originIds = resp.data.content;
						}
					}
					if(args.name == 'location' && args.code == "20"){
						if(resp.data.content.length == 0){
							$scope.destinationIds = [{'code' : ''}];
						}
						else{
							$scope.destinationIds = resp.data.content;
						}
					}
					if(args.name == 'name' && args.code == "19"){
						if(resp.data.content.length == 0){
							$scope.originNames = [{'name' : ''}];
						}
						else{
							$scope.originNames = resp.data.content;
						}
					}
					if(args.name == 'name' && args.code == "20"){
						if(resp.data.content.length == 0){
							$scope.destinationNames = [{'name' : ''}];
						}
						else{
							$scope.destinationNames = resp.data.content;
						}
					}
				}			
			},
			function(data){
				uiLoaderSvc.hide();
				console.log("Service Error - ", data);

				//User not authenticated (403) redirecting to login page
				if(data.status == 403){
					logoutService.logout();
				}
			});
			// console.log(orderSvc.selectedDelivery().origin.id);
		}
	}

		$scope.selectedLocation=function(item, args){

		if(args.code == "19"){
			origin.id = item.type;
	        origin.name = {'name' : item.name};
	        origin.address = item.address1;
	        origin.address2 = item.address2;
	        origin.city = item.city;
	        origin.sau =  item.state;
	        origin.postalCode = item.postalCode;
	        origin.country = item.country;
	        origin.entityId =  item.code;
		}

		if(args.code == "20"){
			destination.id = item.type;
	        destination.name = {'name' : item.name};
	        destination.address = item.address1;
	        destination.address2 = item.address2;
	        destination.city = item.city;
	        destination.sau = item.state;
	        destination.postalCode = item.postalCode;
	        destination.country = item.country;
	        destination.entityId = item.code;
		}
	}

	$scope.origin={};	
	$scope.destination={};

	var reqObj = {};
	reqObj["codeLists"] = [];

	if($state.current.param == "desktop"){
		reqObj.codeLists.push("CreateDesktopShipmentFieldList");
	}
	else if($state.current.param == "manual"){
		reqObj.codeLists.push("CreateManualShipmentFieldList");
	}
	else if($state.current.param == "misc"){
		reqObj.codeLists.push("CreateMiscShipmentFieldList");
	}

	// reqObj["codeLists"] = ["CreateDesktopShipmentFieldList"];

	uiLoaderSvc.show();
	getROLView.get(shipmentSvc.module, reqObj).then(function(resp){
		uiLoaderSvc.hide();
		if(resp.data.content){
			$scope.createShipmentFields = resp.data.content;
		}

	}, function(err){
		uiLoaderSvc.hide();
		//User not authenticated (403) redirecting to login page
		if(err.err.status == 403){
			logoutService.logout();
		}
		else{
			uiModalSvc.show('ok-cancel-modal.tpl',{
				title:"Error",
				icon: "fa fa-exclamation-triangle",
				text: "There was a system error, please check back again.",
				callback:function(){
				}
			});
			$state.go("home.shipment.create.default");
		}

	});
		
	$scope.carriers=[
	"Fedex",
	"UPS",
	"On-TRAC",
	"DHL",
	"Other"
	]

	$scope.returnReasons=[
	"CONTRACTUAL REQUIREMENTS",
	"CRITICAL FOR PRODUCTION",
	"CUSTOMER REQUESTED STANDARD OVERNIGHT",
	"DESIRED BY RECEIVER AND PREPAID ADD",
	"EXPORT SHIPMENT",
	"SHIPPER AD-HOC DECISION",
	"LEGAL PAPERWORK – MUST BE OVERNIGHT",
	"PROPOSAL REQUIREMENTS",
	"REQUESTED 2ND DAY BY ORIGINATOR",
	"REQUIRED BY ORIGINATOR",
	"SCHEDULED MEETING",
	"SIGNATURE REQUIRED",
	"TIME SENSITIVE MATERIALS",
	"URGENT REPLACEMENT OF DEFECTIVE PART",
	"URGENT – AIRCRAFT ON GROUND",
	"OTHER"]

	$scope.handlingUnitTypes=["Box","Pallet","Container","Other"];
	$scope.uoms=["Ft.","In.","M.","Cm."];
	$scope.wuoms=["Lbs.","Kg."];
	$scope.vUoms={
		"Feet":"Cubic Feet",
		"Inches":"Cubic Inches",
		"Meters":"Cubic Meters",
		"Centimeters":"Cubic Centimeters",
	}

	$scope.dzOptions = {
		url : '/alt_upload_url',
		paramName : 'photo',
		maxFilesize : '10',
		acceptedFiles : 'image/jpeg, images/jpg, image/png',
		addRemoveLinks : true,
	};
	
	
	//Handle events for dropzone
	//Visit http://www.dropzonejs.com/#events for more events
	$scope.dzCallbacks = {
		'addedfile' : function(file){
			console.log(file);
			$scope.newFile = file;
		},
		'success' : function(file, xhr){
			console.log(file, xhr);
		},
	};
	
	//Apply methods for dropzone
	//Visit http://www.dropzonejs.com/#dropzone-methods for more methods
	$scope.dzMethods = {};
	$scope.removeNewFile = function(){
		$scope.dzMethods.removeFile($scope.newFile);
	}


	$scope.submitShipment=function(){
		$state.go('home.shipment.create-submit');
	}

}]);