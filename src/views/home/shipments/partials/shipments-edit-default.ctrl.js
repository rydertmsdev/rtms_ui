App.controller('shipmentsEditDefaultCtrl',['$scope', '$state', 'shipmentSvc', 'submitShipment', 'uiLoaderSvc', 'uiModalSvc', 'getRates', function($scope, $state, shipmentSvc, submitShipment, uiLoaderSvc, uiModalSvc, getRates){

	$scope.equals=angular.equals;

	$scope.shipmentSvc = shipmentSvc;

	$scope.cancelShipmentProcess=function(){
		$state.go('home.shipment.create.default');
	}

	$scope.saveShipmentAsDraft=function(){
		uiLoaderSvc.show();
        submitShipment.submit("shipment/v1", shipmentSvc.selectedShipment).then(function(resp){
            uiLoaderSvc.hide();
            // Shipment submitted
        }, function(err){
            uiLoaderSvc.hide();
            //User not authenticated (403) redirecting to login page
            if(err.status == 403){
                logoutService.logout();
            }

            uiModalSvc.show('ok-cancel-modal.tpl',{
                title:"Error",
                icon: "fa fa-exclamation-triangle",
                text:"Cannot process shipment at this time, please retry again.",
                callback:function(){
                    // nothing to do on OK button
                }
            }); 
        });
	}

	$scope.origin={
		address:"Address",
		city:"City",
		sau:"State or Province,",
		postalCode:"Postal Code",
		country:"Country"
	}
	
	$scope.destination={
		address:"Address",
		city:"City",
		sau:"State or Province,",
		postalCode:"Postal Code",
		country:"Country"
	}
	
	$scope.newShipment={
		id:$uuid(),
		handlingUnits:[],
	}

	$scope.deleteHandlingUnit=function(index){
		$scope.newShipment.handlingUnits.splice(index,1);
	}

	$scope.handlingUnitTypes=["Box","Pallet","Container","Other"];
	$scope.uoms=["Ft.","In.","M.","Cm."];
	$scope.wuoms=["Lbs.","Kg."];
	$scope.vUoms={
		"Feet":"Cubic Feet",
		"Inches":"Cubic Inches",
		"Meters":"Cubic Meters",
		"Centimeters":"Cubic Centimeters",
	}

	$scope.addHandlingUnit=function(){
		$scope.newShipment.handlingUnits.push({
			id:$uuid(),
			name:"Handling Unit "+ ($scope.newShipment.handlingUnits.length+1),
			items:[{}]
		})
	}

	$scope.removeItem=function(handlingUnit,index){
		handlingUnit.items.splice(index,1);
	}

	$scope.submitShipment=function(){

	$scope.tempResult = {
		"idempotencyKey": null,
		"tradingPartner": null,
		"externalTimeStamp": null,
		"customerNumber": "WWG",
		"shipmentNumber": "SH-020736655",
		"actionCode": null,
		"externalShipmentNumber": "1029316682",
		"logisticsGroupCode": "WWG3",
		"divisionCode": "WWG2",
		"status": "",
		"commodityCode": "GMISC",
		"totalAmount": "",
		"discountAmount": null,
		"chargedAmount": null,
		"fuelAmount": "",
		"mergeInTransitConsolidationCode": null,
		"mergeInTransitConsolidationNum": null,
		"memo": "PLEASE PRINT CUSTOMER PURCHASE ORDER ON PACKING LIST All PO's require a valid 5 Digit Numeric or 5 digit numeric preceeded by the letters",
		"dates": {
			"requestShipFrom": "2017-07-04T00:01:00",
			"requestShipTo": "2017-07-14T23:58:00",
			"requestDeliveryFrom": "2017-07-05T00:02:00",
			"requestDeliveryTo": "2017-07-15T23:59:00",
			"scheduledShipFrom": "2017-07-04T00:01:00",
			"scheduledShipTo": "2017-07-04T23:58:00",
			"scheduledDeliveryFrom": "2017-07-04T00:02:00",
			"scheduledDeliveryTo": "2017-07-19T23:59:00",
			"appointmentPickUp": "",
			"appointmentDelivery": "",
			"actualPickup": "",
			"actualDelivery": ""
		},
		"options": {
			"isShipDirect": null,
			"isUrgent": false,
			"isHold": false,
			"carrierScac": null,
			"serviceCode": "",
			"freightTerms": "CC",
			"incoTerms": "",
			"consolidationClass": "",
			"isHazmat": false
		},
		"measures": {
			"length": null,
			"width": null,
			"height": null,
			"dimensionUOM": null,
			"weight": "100",
			"weightUOM": "LB",
			"volume": "200",
			"volumeUOM": "FT",
			"nominalWeight": "100",
			"tareWeight": null,
			"totalSkids": "1",
			"totalHandlingUnits": "1",
			"orderValue": "",
			"declaredValue": "",
			"currency": null,
			"actualWeight": "",
			"actualVolume": "",
			"actualPieces": "",
			"actualHandlingUnits": null
		},
		"equipment": {
			"equipmentInitial": null,
			"equipmentNumber": null,
			"equipmentName": null,
			"country": null,
			"routeNumber": null,
			"equipmentDescriptionCode": null,
			"equipmentCode": null,
			"weightUOM": null,
			"weight": null,
			"volumeUOM": null,
			"volume": null,
			"temperatureUOM": null,
			"minimumTemperature": null,
			"maximumTemperature": null,
			"dimUOM": null,
			"length": null,
			"width": null,
			"height": null
		},
		"locations": {
			"location": [{
				"useCode": "19",
				"locationCode": "200011042",
				"locationName": "DRACO MECHANICAL SUPPLY-11042",
				"locationNameAddtl": "",
				"isPickupAppointmentRequired": false,
				"isDeliveryAppointmentRequired": false,
				"isResidential": false,
				"externalLocationType": null,
				"memo": null,
				"businessHours": null,
				"address": {
					"address": "52722 STATE STREET",
					"address2": null,
					"city": "RIVERDALE",
					"sau": "IA",
					"country": "USA",
					"postalCode": "52722",
					"latitude": "0",
					"longitude": "0",
					"timezone": null
				},
				"contacts": {
					"contact": [{
						"contactType": null,
						"primaryLanguage": null,
						"firstName": "N/A",
						"lastName": null,
						"phone": "N/A",
						"alternatePhone": null,
						"faxNumber": null,
						"email": null,
						"isPrimaryContact": true
					}],
					
				},
				"chargeOverrides": null
			},
			{
				"useCode": "20",
				"locationCode": "005",
				"locationName": "Grainger DC 005",
				"locationNameAddtl": "",
				"isPickupAppointmentRequired": false,
				"isDeliveryAppointmentRequired": false,
				"isResidential": false,
				"externalLocationType": null,
				"memo": null,
				"businessHours": null,
				"address": {
					"address": "701 GRAINGER WAY",
					"address2": null,
					"city": "MINOOKA",
					"sau": "IL",
					"country": "USA",
					"postalCode": "60447",
					"latitude": "0",
					"longitude": "0",
					"timezone": null
				},
				"contacts": {
					"contact": [{
						"contactType": null,
						"primaryLanguage": null,
						"firstName": null,
						"lastName": null,
						"phone": "815 290-7600",
						"alternatePhone": null,
						"faxNumber": null,
						"email": null,
						"isPrimaryContact": true
					}],
					
				},
				"chargeOverrides": null
			}],
			
		},
		"referenceNumbers": {
			"reference": [{
				"qualifier": "SID",
				"value": "RYDERO"
			},
			{
				"qualifier": "HAZM",
				"value": "N"
			},
			{
				"qualifier": "CIO",
				"value": "ORIGINADDRESSID"
			},
			{
				"qualifier": "CID",
				"value": "DESTADDRESSID"
			},
			{
				"qualifier": "FT",
				"value": "FT_COLLECT"
			},
			{
				"qualifier": "ERC",
				"value": "YES"
			},
			{
				"qualifier": "PO",
				"value": "JP20206581"
			},
			{
				"qualifier": "PP",
				"value": "2"
			},
			{
				"qualifier": "SORD",
				"value": "1294686465"
			},
			{
				"qualifier": "SNM",
				"value": "James Peek"
			},
			{
				"qualifier": "RONM",
				"value": "1580"
			},
			{
				"qualifier": "MTYP",
				"value": "SALE"
			},
			{
				"qualifier": "SALG",
				"value": "PSAL"
			},
			{
				"qualifier": "OTYP",
				"value": "SALE"
			},
			{
				"qualifier": "CUSP",
				"value": "60011-W052"
			},
			{
				"qualifier": "REGN",
				"value": "USA"
			},
			{
				"qualifier": "UI",
				"value": "1029316682"
			},
			{
				"qualifier": "SEM",
				"value": "James_Peek@ryder.com"
			},
			{
				"qualifier": "FT",
				"value": "COLLECT"
			},
			{
				"qualifier": "CMDY",
				"value": "MISC"
			},
			{
				"qualifier": "SHDT",
				"value": "2017-06-29T12:46:53"
			},
			{
				"qualifier": "SFPD",
				"value": "2017-07-04T00:01:00"
			},
			{
				"qualifier": "DIV",
				"value": "WWG2"
			},
			{
				"qualifier": "LGRP",
				"value": "WWG3"
			}],
			
		},
		"handlingUnits": {
			"handlingUnit": [{
				"handlingUnitId": null,
				"handlingUnitTypeNumber": "PLT",
				"isPalletOrSkid": null,
				"description": "Pallet",
				"externalHandlingUnitNumber": null,
				"quantity": null,
				"stackCode": null,
				"stackFactor": null,
				"stackGroup": null,
				"isHazmat": false,
				"measures": {
					"length": "0",
					"width": "0",
					"height": "0",
					"dimensionUOM": null,
					"weight": "100",
					"weightUOM": "LB",
					"volume": "200",
					"volumeUOM": "FT",
					"nominalWeight": "",
					"tareWeight": null,
					"totalSkids": null,
					"totalHandlingUnits": null,
					"orderValue": "",
					"declaredValue": "0",
					"currency": null,
					"actualWeight": null,
					"actualVolume": null,
					"actualPieces": null,
					"actualHandlingUnits": null
				},
				"hazmatDetails": [],
				"freightClasses": null,
				"items": null,
				"referenceNumbers": null
			}],
			
		},
		"throughPoints": null,
		"chargeOverrides": null,
		"legs": null,
		"events": null,
		"rates": {
			"rate": [{
				"quoteId": "1067",
				"carrierNum": "UPGF",
				"rank": 5,
				"rateSource": "TM",
				"totalCharge": "130.4500",
				"currencyCd": "USD",
				"equipment": null,
				"serviceCode": "LTL",
				"scheduledPickupFromDate": "2017-07-04T16:30:00",
				"scheduledPickupToDate": "2017-07-04T16:30:00",
				"scheduledDeliveryFromDate": "2017-07-05T07:00:00",
				"scheduledDeliveryToDate": "2017-07-05T07:00:00"
			},
			{
				"quoteId": "1061",
				"carrierNum": "DHRN",
				"rank": 1,
				"rateSource": "TM",
				"totalCharge": "74.5500",
				"currencyCd": "USD",
				"equipment": null,
				"serviceCode": "LTL",
				"scheduledPickupFromDate": "2017-07-04T16:30:00",
				"scheduledPickupToDate": "2017-07-04T16:30:00",
				"scheduledDeliveryFromDate": "2017-07-05T08:00:00",
				"scheduledDeliveryToDate": "2017-07-05T08:00:00"
			},
			{
				"quoteId": "1062",
				"carrierNum": "FXFE",
				"rank": 2,
				"rateSource": "TM",
				"totalCharge": "89.0800",
				"currencyCd": "USD",
				"equipment": null,
				"serviceCode": "LTL",
				"scheduledPickupFromDate": "2017-07-04T16:30:00",
				"scheduledPickupToDate": "2017-07-04T16:30:00",
				"scheduledDeliveryFromDate": "2017-07-06T07:00:00",
				"scheduledDeliveryToDate": "2017-07-06T07:00:00"
			},
			{
				"quoteId": "1063",
				"carrierNum": "LMEL",
				"rank": 3,
				"rateSource": "TM",
				"totalCharge": "103.3100",
				"currencyCd": "USD",
				"equipment": null,
				"serviceCode": "LTL",
				"scheduledPickupFromDate": "2017-07-04T16:30:00",
				"scheduledPickupToDate": "2017-07-04T16:30:00",
				"scheduledDeliveryFromDate": "2017-07-06T08:00:00",
				"scheduledDeliveryToDate": "2017-07-06T08:00:00"
			},
			{
				"quoteId": "1065",
				"carrierNum": "ABFS",
				"rank": 4,
				"rateSource": "TM",
				"totalCharge": "112.7700",
				"currencyCd": "USD",
				"equipment": null,
				"serviceCode": "LTL",
				"scheduledPickupFromDate": "2017-07-04T16:30:00",
				"scheduledPickupToDate": "2017-07-04T16:30:00",
				"scheduledDeliveryFromDate": "2017-07-05T08:00:00",
				"scheduledDeliveryToDate": "2017-07-05T08:00:00"
			}],
			
		}
	};

		getRates.get("shipments/v1", shipmentSvc.selectedShipment).then(function(resp){
			// console.log(resp);
			uiLoaderSvc.hide();

			uiModalSvc.show('shipment-rate-details.tpl',{
				po:$scope.tempResult,
				type:"update"
			});
		},
		function(data){
			uiLoaderSvc.hide();
			console.log("Service Error - ", data);

			uiModalSvc.show('shipment-rate-details.tpl',{
				po:$scope.tempResult,
				type:"update"
			});

			//User not authenticated (403) redirecting to login page
			if(data.status == 403){
				logoutService.logout();
			}
			else{
				uiModalSvc.show('ok-cancel-modal.tpl',{
	                title:"Error",
	                icon: "fa fa-exclamation-triangle",
	                text:"Cannot get rates for this shipment at this time, please try again.",
	                callback:function(){
	                    // nothing to do on OK button
	                }
	            }); 
			}
		});
	}

}]);