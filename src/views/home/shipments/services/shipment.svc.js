/* getRates */
App.service('getRates', function getRates($rootScope, $http, $filter, $q) {

    /* Get the search result  */

    this.get = function (module, obj) {
      
      var deferred = $q.defer();

      $http.post("http://localhost:8080/shipments/v1/rateShipment", obj)
      .then( function(response) {
               deferred.resolve(response);
          }, function(errResp) {
               deferred.reject({ message: "Really bad", err : errResp });
          });
      return deferred.promise;
    }
});

/* get shipment rol view */
App.service('getROLView', function getROLView($rootScope, $http, $filter, $q) {

    /* Get the search result  */

    this.get = function (module, obj) {
      
      var deferred = $q.defer();

      $http.post("/ryderonline/rtms/service/"+module+"/scs/rolViewList", obj)
      .then( function(response) {
               deferred.resolve(response);
          }, function(errResp) {
               deferred.reject({ message: "Really bad", err : errResp });
          });
      return deferred.promise;
    }
});

/* submit shipment */
App.service('submitShipment', function submitShipment($rootScope, $http, $filter, $q) {

    /* Get the search result  */

    this.submit = function (module, obj, type) {
      
      var deferred = $q.defer();

      if(type == "update"){

        $http.patch("http://localhost:8080/shipments/v1/update", obj)
        .then( function(response) {
                 deferred.resolve(response);
            }, function(errResp) {
                 deferred.reject({ message: "Really bad", err : errResp });
            });
      }
      else{
        $http.post("http://localhost:8080/shipments/v1/create", obj)
        .then( function(response) {
                 deferred.resolve(response);
            }, function(errResp) {
                 deferred.reject({ message: "Really bad", err : errResp });
            });
      }
      return deferred.promise;
    }
});

/* Fetches the standard shipment search result */
App.service('standardShipmentSearchResult', function standardShipmentSearchResult($rootScope, $http, $filter, $q) {

    /* Get the search result  */

    this.getResults = function (module, data) {
      
      var deferred = $q.defer();

      $http.post("/ryderonline/rtms/service/"+module+"/scs/shipmentsList", data)
      .then( function(response) {
               deferred.resolve(response);
          }, function(errResp) {
               deferred.reject(errResp);
          });
      return deferred.promise;
    }
});

App.service('shipmentSvc', [
  '$rootScope',
  '$window', 
  '$q', 
  '$timeout', 
  '$filter', 
  'uiLoaderSvc', 
  'uiModalSvc', 
  'testData', 
  'containsFilter', 
  'filterByFilter', 
  'codeListService',
  '$state',
  function ($rootScope, $window, $q, $timeout, $filter, uiLoaderSvc, uiModalSvc, testData, containsFilter, filterByFilter, codeListService, $state) {
    var svc=this;
    svc.module = "shipment"
    svc.selectedShipment;
    svc.selectedRate;
    svc.shipmentSearchResults = [];
    svc.index = 1;
    svc.pageTotal = "";
    svc.recordsCount = "";
    svc.pageSize = 50;
    svc.rowsPerbatch = 50;
    svc.shipmentCreateObj = {};

}]);