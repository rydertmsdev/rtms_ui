<ui-sidebar-nav>
	<nav class="sidebar-nav">
		<ui-sidebar-btn></ui-sidebar-btn>
		<!-- <a ui-sref="home.shipment.create" ui-info="Create Shipment" ui-sref-active="active">
			<i class="fa fa-plus"></i>
		</a> -->
		<a ui-sref="home.shipment.create.default" ui-info="Create Standard Shipment" ui-sref-active="active">
			<i class="fa fa-cubes"></i>
			<span >Standard</span>
		</a>
		<a ui-sref="home.shipment.create.desktop" ui-info="Create Desktop Shipment" ui-sref-active="active">
			<i class="fa fa-envelope-open-o"></i>
			<span>Desktop</span>
		</a>
		<a ui-sref="home.shipment.create.manual" ui-info="Create Manual Shipment" ui-sref-active="active">
			<i class="fa fa-clipboard "></i>
			<span>Manual</span>
		</a>
		<a ui-sref="home.shipment.create.misc" ui-info="Create Misc Shipment" ui-sref-active="active">
			<i class="fa fa-truck"></i>
			<span>Misc</span>
		</a>
	</nav>
	<ui-sidebar-nav-content>
		<ui-view></ui-view>
	</ui-sidebar-nav-content>
</ui-sidebar-nav>
