App.controller("shipmentRatesCtrl", ["$state", "$scope", "$timeout", "uiLoaderSvc" ,"uiModalSvc", "shipmentSvc", '$window', 'submitShipment', function($state, $scope, $timeout, uiLoaderSvc, uiModalSvc, shipmentSvc, $window, submitShipment) {
    $scope.close = function() {
        uiModalSvc.hide();
    }

    $scope.shipmentObj = $scope.args.po || {}

    $scope.rates = [];

    if($scope.shipmentObj && $scope.shipmentObj.rates && $scope.shipmentObj.rates.rate){
        $scope.rates = $scope.shipmentObj.rates.rate;
    }

    $scope.confirmRate=function(){
        angular.forEach($scope.rates, function(rate, rIndex){
            if(rate.$$selected){
                shipmentSvc.selectedRate = rate;
            }
        });

        if(shipmentSvc.selectedRate){
            $scope.shipmentObj["totalAmount"] = shipmentSvc.selectedRate.totalCharge;
            $scope.shipmentObj.options["carrierScac"] = shipmentSvc.selectedRate.carrierNum;
            $scope.shipmentObj.options["serviceCode"] = shipmentSvc.selectedRate.serviceCode;
        }

        //TODO: insert user selected rates in to shipment create obj and send
        uiLoaderSvc.show();
        submitShipment.submit("shipment/v1", $scope.shipmentObj, $scope.args.type).then(function(resp){
            uiLoaderSvc.hide();
            // Shipment submitted
            // Save Shipment number in shipmentSvc service
            $state.go('home.shipment.create-submit');
        }, function(err){
            uiLoaderSvc.hide();
            //User not authenticated (403) redirecting to login page
            if(err.status == 403){
                logoutService.logout();
            }

            err = {"content":{"customerNum":"WWG","uniqueId":"R733006024","shipmentNum":"733006024","shipmentUrl":"/ryderonline/rydertrac/shipmentdetail?S=733006024","loadNum":null,"loadUrl":null,"docs":[{docId: 16226418,docDesc: 'BOL',docUrl: '/ryderonline/rtms/service/load/loads/WWG/16226418/bol',contentName: 'Load_16226418_BOL.pdf',contentType: null,contents: null}]},"messages":[{"code":"OK","message":"Order Detail fulfillment successfully processed.","level":"INFO"}]}

            shipmentSvc.shipmentCreateObj = err.content;


            $state.go('home.shipment.create-submit');

            uiModalSvc.show('ok-cancel-modal.tpl',{
                title:"Error",
                icon: "fa fa-exclamation-triangle",
                text:"Cannot process shipment at this time, please retry again.",
                callback:function(){
                    // nothing to do on OK button
                }
            }); 
        });

        $scope.close();
    }

    $scope.rateSelected=function(rate){
        angular.forEach($scope.rates, function(rate, rIndex){
            rate.$$selected = false;
        });
        rate.$$selected=!rate.$$selected;
    }

}]);