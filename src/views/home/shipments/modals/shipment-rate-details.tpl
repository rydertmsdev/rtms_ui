<div class="form modal-small ng-scope" style="display: inline-block;" ng-controller="shipmentRatesCtrl">
    <ui-header theme="ui-modal-header">
        <ui-header-body>
            <div><i class="fa fa-money"></i> <span>Rates</span></div>
        </ui-header-body>
        <ui-header-controls>
            <div ui-button="" title="Clear" ng-click="close()"><i class="fa fa-times"></i></div>
        </ui-header-controls>
    </ui-header>
    <div class="modal-body overflow-y">
        <ui-list class="form">
            <ui-list-header>
                <li class="icon text-center"><span>Select</span></li>
                <li class="">Carrier Code</li>
                <li class="">Rate Source</li>
                 <li class="">Cost Sequence</li>
                <li class="">Service Code</li>
                <li class="">Rate</li>
            </ui-list-header>
            <ui-list-body>
                <li ng-if="rates.length == 0">
                    <div class="no-results">
                        No results to show.
                    </div>
                </li>
                <!-- ngRepeat: rate in rates array -->
                <li ng-repeat="rate in rates" ng-if="rates.length > 0" style="min-height: 40px" class="ng-scope" ng-click="rateSelected(rate)">
                    <cell class="icon" ng-button="" >
                        <div class="checkbox" ng-class="{'checked':rate.$$selected}"></div>
                    </cell>                    
                    <cell class="">
                        <span ng-bind="::rate.carrierNum" class="ng-binding"></span>
                    </cell>  
                    <cell class="">
                        <span ng-bind="::rate.rateSource" class="ng-binding"></span>
                    </cell>  
                    <cell class="">
                        <span ng-bind="::rate.rank" class="ng-binding"></span>
                    </cell>  
                    <cell class="">
                        <span ng-bind="::rate.serviceCode" class="ng-binding"></span>
                    </cell>                   
                    <cell class="">
                        <span class="ng-binding">{{::rate.totalCharge}}  {{::rate.currencyCd}}</span>
                    </cell>
                </li>
                <!-- end ngRepeat: rate in rates -->
            </ui-list-body>
        </ui-list>
    </div>
    <ui-header theme="ui-modal-footer">
        <ui-header-controls>
            <div ui-button="" ng-click="close()" class="bg-gray"><span>Cancel</span></div>
            <div ui-button="" class="bg-accent" ng-click="confirmRate()">Save and Process</div>
        </ui-header-controls>
    </ui-header>
</div>
